<?php

/**
 * uniAdmin indító fájl
 */
require('config.php');

require('uniAdmin.php');

require 'vendor/autoload.php';

UniAdmin::app()->start($config);
