(function() {
    
    var	h = 300,
    	w = 550,
    	left = (screen.width/2) - (w/2),
    	top = (screen.height/2) - (h/2);

	// $('body').kycoPreload({
	// 	showInContainer: true,
	// 	useOpacity: true,
	// 	progressiveReveal: true,
	// 	forceSequentialLoad: true,
	// });

	//social icons
	$('.fa-share-square-o').on('click', function(e) {
		e.preventDefault();
		var parentTag = $(this).closest('ul'),
			nextTag = $(parentTag[0]).next('ul');
		$(parentTag[0]).hide();
		$(nextTag[0]).show();
	});

	//social-2 window close
	$('.close-2').on('click', function(e) {
		e.preventDefault();
		var parentTag = $(this).closest('ul'),
			nextTag = $(parentTag[0]).prev('ul');
		$(parentTag[0]).hide();
		$(nextTag[0]).show();
	});

	$(".share-facebook").on('click', function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    window.open(href, "facebook", "height=" + h + ",width=" + w + ",left=" + left + ",top=" + top + ",resizable=1") 
	});

	$(".share-twitter").click(function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    window.open(href, "tweet", "height=" + h + ",width=" + w + ",left=" + left + ",top=" + top + ",resizable=1") 
	});

	$(".share-google-plus").click(function(e) {
	    e.preventDefault();
	    var href = $(this).attr('href');
	    window.open(href, "tweet", "height=" + h + ",width=" + w + ",left=" + left + ",top=" + top + ",resizable=1") 
	});

	//add to favorites
	$('.pin').on('click', function(e) {
		e.preventDefault();
		var iTag = $(this).closest('a').find('i'),
			url = $(this).attr('href'),
			remove = $(this).hasClass('remove'),
			q = remove ? url.replace('/pin/', '/pindelete/') : url;

		$.post(q, function(result) {
			if (result.success) {
				if(remove) {
					$(iTag)
						.removeClass('text-warning fa-star')
						.addClass('text-muted fa-star-o')
						.closest('a')
						.attr('href', url)
						.removeClass('remove');
				} else {
					$(iTag)
						.removeClass('text-muted fa-star-o')
						.addClass('text-warning fa-star')
						.closest('a').attr('href', url)
						.addClass('remove');
				}
			} else {
				$(document).find('.error-login').show().delay(3000).fadeOut(800);
			}
		});
	});

	//search-form sidebar
	$('.search-sidebar').on('change', function() {
		$.post('/puppy/list/', function(result) {

				$('#refine_result').submit();

		    	$('.btn-loading')
		    		.text('Searching...')
		    		.addClass('disabled')
		    		.append('<i class="fa fa-fw fa-spinner fa-spin"></i>');
		})
	});

	$('h5 i').on('click', function() {
			$(this).parents().next('div.bg-white').toggle();
	});

	$('.popover-help').popover();


	//helcenter plus, minus icon valtas
	$('.helpcenter').on('click', function() {
		var iTag = $(this).closest('div').next();
	});

	// loading images
	$('.container .thumbnail img').loadNicely();
	$('.tip').tooltip();

	// copy my embed code
	$('.code').on('click', function(){
		$(this).focus();
		$(this).select();
	});

	// fadout
	$(document).find('.fadeout').delay(2500).fadeOut(400);

	// remove-item
	$('.remove-item').on('click', function() {
		if (!confirm('Are you sure you want to do this?')) {
			return false;
		}
	});

	// pay-promote
	$('.pay-promote').on('click', function() {
		if (!confirm('Are you sure you want to pay $5 promote? ')) {
			return false;
		}
	});

	//count words
    $('.countit').keyup(function () {
        var maxCount = $(this).attr('maxlength');
        if ($(this).val().length >= maxCount) {
        $(this).val($(this).val().substr(0, maxCount));
        }
        $('#remain').text(maxCount - $(this).val().length);
    });

    //message
    $('#read-message-button').on('click', function() {
    	$('#read-message').toggle(
    		function() {
    			$('#read-message-button').text(function(i, text) {
    				return text == 'Read Message' ? 'Hide Message' : 'Read Message';
    			});
    		}
    	);
    });

    //upload submit
    $('#upload-submit').on('click', function() {
    	$(this)
    		.attr('value', 'Uploading. Please wait...')
    		.removeClass()
    		.addClass('btn btn-danger mt25 disabled');
    });

    //sending msg
    $('#send-submit').on('click', function() {
    	$(this)
    		.text('Sending. Please wait...')
    		.removeClass()
    		.addClass('btn btn-success btn-lg disabled')
    		.append('<i class="fa fa-fw fa-spinner fa-spin"></i>');
    });

    //button loading
    $('.btn-loading').on('click', function() {

	    $('.loader').show();
		$(window).load(function() {
			$(".loader").fadeOut("slow");
		});

    	// var data = $(this).data();
    	// if (typeof(data.msg) == 'undefined') { data.msg = 'Loading...'; }
    	// $(this)
    	// 	.text(data.msg)
    	// 	.addClass('disabled')
    	// 	.append('<i class="fa fa-fw fa-spinner fa-spin"></i>');
    });

	// breed select
	$('a[href="#breeds"]').on('click', function(e){
		e.preventDefault();
		$('#breeds').modal('show');
	});

	$('#breed-select-submit').on('click', function(e){
		// var breeds = [];
		// $('#breeds input[type="checkbox"]:checked').each(function(index, item){
		// 	breeds.push($.trim($(item).parent().text()));
		// });
		// $('.selected-breeds').html('Selected: ' + breeds.join(', '));
		$.post('/puppy/list', function() {
			$('#refine_result').submit();
		});
	});

	// state select
	$('a[href="#states"]').on('click', function(e){
		e.preventDefault();
		$('#states').modal('show');
	});

	$('#state-select-submit').on('click', function(e){
		// var states = [];
		// $('#states input[type="checkbox"]:checked').each(function(index, item){
		// 	states.push($.trim($(item).parent().text()));
		// });
		// $('.selected-states').html('Selected: ' + states.join(', '));
		$.post('/breeder', function() {
			$('#refine_result').submit();
		});
	});

	$('.share-email-button').on('click', function() {
		var 
			data      = $(this).data(),
			puppyName = data['name'],
			puppyId   = data['id'];

		$('.email').modal();
		$('#puppyName').text(puppyName);
		$('.puppy-id').val(puppyId);
	});

	// main page, search typeahead
	// $.post('/search/breedlist', function(data){
	//     $("#search").typeahead({ 
	//     	source: data,
	// 	    updater : function(item) {
	// 	        this.$element[0].value = item;
	// 	        this.$element[0].form.submit();
	// 	        return item;
	// 	    },
	//     });
	// });

	//Slider

})();
