
    var selectedHandler = function () {
        $('#search').submit();
    };

    $('.typeahead').on('typeahead:selected', selectedHandler);

    var breeds = new Bloodhound({
        datumTokenizer: function(data) {
            return Bloodhound.tokenizers.whitespace(data.name);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: {
            url: '/search/breedlist'
        },
        limit: 7
    });

    var puppies = new Bloodhound({
        datumTokenizer: function(data) {
            return Bloodhound.tokenizers.whitespace(data.puppyName);
        },
        queryTokenizer: Bloodhound.tokenizers.whitespace,
        prefetch: '/search/puppylist',
        limit: 7,
    });

    breeds.initialize();
    puppies.initialize();
    //Torlom a cahebol, kesobb jo lehet de most nagyon idegesitett
    breeds.clearPrefetchCache();
    puppies.clearPrefetchCache();


    $('#multiple-datasets .typeahead').typeahead({
        highlight: true,
    },
    {
        name: 'breed',
        displayKey: 'name',
        source: breeds.ttAdapter(),
        templates: {
            header: '<h3 class="title-name text-primary">Breeds</h3>',
        }
    },
    {
        name: 'puppies',
        displayKey: 'puppyName',
        source: puppies.ttAdapter(),
        templates: {
            header: '<h3 class="title-name text-primary">Puppies</h3>',
            suggestion: Handlebars.compile('<p>{{puppyName}} – ({{breedName}})</p>')
        }
    });

