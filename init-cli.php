<?php

defined('IS_CLI') or define('IS_CLI', PHP_SAPI == 'cli');

if (!IS_CLI) {
	die('not in CLI mode');
}

$_SERVER['HTTP_HOST'] = 'www.';
$_SERVER['HTTP_USER_AGENT'] = '';
$_SERVER['REQUEST_URI'] = '/';
$_SERVER['REMOTE_ADDR'] = '';

defined('DEBUG_MODE') or define('DEBUG_MODE', true);
defined('LANG') or define('LANG', 'hu');

require('config.php');
require('uniAdmin.php');

$config['cache']['class'] = 'classes.cache.FileCache';
$config['cache']['debug'] = false;
$config['db']['debug'] = false;

UniAdmin::app()->startCli($config);

UniAdmin::import(
	'modules.Admin.AdminConfig',
	'modules.Admin.classes.AdminInstaller',
	'modules.Admin.classes.BaseAdminModule',
	'modules.Admin.classes.ContextMenu',
	'modules.Admin.classes.DataDisplay',
	'modules.Admin.classes.ModuleAccessLevel',
	'modules.Admin.classes.ModuleMenu',
	'modules.Admin.classes.ModulePermission',
	'modules.Admin.classes.UserPreferences',
	'modules.Admin.classes.Tabs'
);
