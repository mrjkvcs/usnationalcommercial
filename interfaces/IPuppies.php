<?php

interface IPuppies {
	public function actionAdd();
	public function actionEdit($id);
	public function actionDelete($id);
	public function actionUpload($adId = null);
}