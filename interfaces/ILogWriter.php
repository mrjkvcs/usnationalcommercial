<?php

interface ILogWriter {
	public function write($message);
}
