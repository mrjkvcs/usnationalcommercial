<?php

interface IEmailQueue {
	public function add(Email $data);
	public function revokeById($id);
	public function revokeByEmail($id);
	public function processEmails();
}
