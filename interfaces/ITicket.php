<?php

interface ITicket {
	public function setTicketId($id);
	public function setUserId($id);
	public function setEmail($email);
	public function setMessage($message);
	public function getTicketId();
	public function getId();
	public function getUserId();
	public function getMessage();
	public function getEmail();
	public function getType();
	public function load($id);
	public function save();
	public function insert();
	public function update($id);
}