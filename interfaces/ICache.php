<?php

interface ICache {
	public function isEnabled();
	public function exists($key);
	public function get($key);
	public function set($key, $value, $ttl);
	public function delete($key);
}
