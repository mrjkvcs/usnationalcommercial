<?php

interface IRouteHandler {
	public function getRoute();
	public function getOriginalRoute();
	public function getLanguage();
	public function getCurrentModule($useAliases = false);
	public function getCurrentUri();
	public function getFullRoute($useAliases = false);
	public function getQueryString($asArray = false, $arrayKey = null);
	public function getParameter($id, $offset = 2);
	public function getParameters($offset = 2);
	public function setRoute($route);
	public function setBaseUrl($url);
	public function createUrl($module, $parameters = array(), $language = null, $queryString = array(), $hash = null);
}
