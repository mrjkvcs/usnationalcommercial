<?php

$dictionary['button-submit'] = 'Submit';
$dictionary['button-reset'] = 'Reset form';
$dictionary['button-back'] = 'Go back';
$dictionary['validation-identifier'] = 'Only alphabetic characters, numbers, - and _ signs are allowed.';
$dictionary['already-exists-in-the-database'] = 'Already exists in the database';
$dictionary['field-contents-are-not-the-same'] = 'The Fields don\'t match.';
$dictionary['required-to-fill'] = 'This field is required';
$dictionary['required-to-check'] = 'This field must be checked';
$dictionary['required-to-select'] = 'An option must be selected';
$dictionary['date-format-error'] = 'This date is not valid';
$dictionary['date-is-out-of-range'] = 'This date is out of the allowed range';
$dictionary['required-to-upload'] = 'A file must be uploaded';
$dictionary['format-not-allowed'] = 'This format is not allowed';
$dictionary['image-format-error'] = 'Not allowed image format';
$dictionary['length-is-too-short'] = 'This value is too short';
$dictionary['length-is-too-long'] = 'This value is too long';
$dictionary['value-does-not-contain-numbers'] = 'The given value does not contain numbers';
$dictionary['value-does-not-contain-uppercase-letters'] = 'The given value does not contain uppercase letters';
