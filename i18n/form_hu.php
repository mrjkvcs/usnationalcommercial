<?php

$dictionary['button-submit'] = 'Adatok rögzítése';
$dictionary['button-reset'] = 'Űrlap kiürítése';
$dictionary['button-back'] = 'Visszalépés';
$dictionary['validation-identifier'] = 'Csak az angol ábécé betűi, számok, illetve - és _ jelek megengedettek!';
$dictionary['already-exists-in-the-database'] = 'Már létezik ilyen az adatbázisban';
$dictionary['field-contents-are-not-the-same'] = 'Field contents are not the same';
$dictionary['required-to-fill'] = 'Kötelező kitölteni';
$dictionary['required-to-check'] = 'Kötelező megjelölni';
$dictionary['required-to-select'] = 'Kötelező választani';
$dictionary['date-format-error'] = 'Nem érvényes dátum';
$dictionary['date-is-out-of-range'] = 'A dátum az engedélyezett tartományon kívül esik';
$dictionary['required-to-upload'] = 'Kötelező feltölteni';
$dictionary['format-not-allowed'] = 'Nem engedélyezett formátum';
$dictionary['image-format-error'] = 'Nem megfelelő képformátum';
$dictionary['length-is-too-short'] = 'A megadott érték túl rövid';
$dictionary['length-is-too-long'] = 'A megadott érték túl hosszú';
$dictionary['value-does-not-contain-numbers'] = 'A megadott érték nem tartalmaz számokat';
$dictionary['value-does-not-contain-uppercase-letters'] = 'A megadott érték nem tartalmaz nagybetűket';
