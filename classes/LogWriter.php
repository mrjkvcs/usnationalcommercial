<?php

class LogWriter implements ILogWriter {
	private $_path;
	private $_separator = ' ';
	private $_type;

	public function __construct() {
		$this->_path = UniAdmin::app()->config['log']['fileName'];
		if (isset(UniAdmin::app()->config['log']['separator'])) {
			$this->_separator = UniAdmin::app()->config['log']['separator'];
		}
	}

	public function checkPath($path) {
		if (!file_exists($path)) {
			@mkdir($path, 0777, true);
			@chmod($path, 0777);
		}
		return file_exists($path) && is_dir($path) && is_writable($path);
	}

	public function setType($type) {
		$this->_type = $type;
		return $this;
	}

	public function setPath($path) {
		$this->_path = $path;
		return $this;
	}

	public function setSeparator($separator) {
		$this->_separator = $separator;
		return $this;
	}

	public function getType($type) {
		return $this->_type;
	}

	public function getPath() {
		return $this->_path;
	}

	public function getSeparator() {
		return $this->_separator;
	}

	public function write($message) {
		$logPath = strftime($this->_path);
		$filename = basename($logPath);
		$path = substr($logPath, 0, (-1 * strlen($filename)));
		if ($this->_type) {
			$path .= $this->_type . '/';
		}
		if ($this->checkPath($path)) {
			if (defined('IS_BACKEND')) {
				if (isset(UniAdmin::app()->user)) {
					$message = 'administrator ' . UniAdmin::app()->user->getUsername() . ' ' . $message;
				}
			} else {
				if (isset(UniAdmin::app()->config['authentication']) && UniAdmin::app()->config['authentication'] && isset(UniAdmin::app()->user) && is_object(UniAdmin::app()->user)) {
					if (UniAdmin::app()->config['authentication']['class']) {
						$message = 'user #' . UniAdmin::app()->user->getId() . ' (' . UniAdmin::app()->user->email . ') ' . $message;
					}
				}
			}
			@$handle = fopen($path . $filename, 'a');
			$content = date('Y-m-d H:i:s') . $this->_separator . $_SERVER["REMOTE_ADDR"] . $this->_separator . $message . "\r\n";
			@$ret = fwrite($handle, $content);
			@fclose($handle);
			return (bool) $ret;
		} else {
			return false;
		}
	}
}
