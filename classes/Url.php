<?php

/**
 * URL-ek kezelésére, létrehozására szolgáló osztály
 */
class Url {
	public static function link($module = '', $parameters = array(), $language = '', $queryString = array(), $hash = null, $goFrontend = false) {
		return UniAdmin::app()->route->createUrl($module, $parameters, $language, $queryString, $hash, $goFrontend);
	}

	public static function encoded($module, $parameters = array(), $language = '', $queryString = array(), $hash = null, $goFrontend = false) {
		return htmlspecialchars(self::link($module, $parameters, $language, $queryString, $hash, $goFrontend));
	}

	public static function isActive($link, $checkQueryString = false, $fullMatchOnly = false) {
		if ($fullMatchOnly || $checkQueryString) {
			$currentUri = $checkQueryString ? UniAdmin::app()->route->getCurrentUri() : UniAdmin::app()->route->getFullRoute(true);
			if (substr($link, -1) == '/') {
				$link = substr($link, 0, -1);
			}
			if (substr($currentUri, -1) == '/') {
				$currentUri = substr($currentUri, 0, -1);
			}
			return $link == $currentUri;
		} else {
			$module = strtolower(UniAdmin::app()->route->getCurrentModule(true));
			$testUri = strtolower(UniAdmin::app()->route->createUrl($module));
			if (substr($link, -1) == '/') {
				$link = substr($link, 0, -1);
			}
			if (substr($testUri, -1) == '/') {
				$testUri = substr($testUri, 0, -1);
			}
			if ($link == $testUri) {
				return true;
			}

			$route = UniAdmin::app()->route->getRoute();
			for ($i = 1; $i < count($route); $i++) {
				$testUri = UniAdmin::app()->route->createUrl($module, array_slice($route, 1, $i));
				if (substr($link, -1) == '/') {
					$link = substr($link, 0, -1);
				}
				if (substr($testUri, -1) == '/') {
					$testUri = substr($testUri, 0, -1);
				}
				if ($link == $testUri) {
					return true;
				}
			}

			return false;
		}
	}
}
