<?php

class AjaxResponse {
	public function __construct($parameters) {
		@ob_end_clean();
		ob_start('ob_gzhandler');

		header('Content-Type: application/json; charset=utf-8');

		echo json_encode($parameters);

		ob_end_flush();

		exit;
	}
}
