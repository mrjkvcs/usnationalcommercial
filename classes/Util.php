<?php

class Util {

    public static function somaE()
    {
        return 'hello';
    }

    public static function saveImage($image, $path, $maxWidth, $maxHeight)
    {
        $dimensions = $image->getSize();
        if ($dimensions[0] > $dimensions[1])
        { // fekvő
            $width = ceil(($maxHeight / $dimensions[1]) * $dimensions[0]);
            if ($width < $maxWidth)
            { // korrekció, ha így túl keskeny lett a kép
                $height = ceil(($maxWidth / $dimensions[0]) * $dimensions[1]);
                $width = $maxWidth;
            } else
            {
                $height = $maxHeight;
            }
            $image->resample($width, $height)->crop($maxWidth, $maxHeight);
        } else
        { // álló
            $height = ceil(($maxWidth / $dimensions[0]) * $dimensions[1]);
            if ($height < $maxHeight)
            { // korrekció, ha így túl alacsony lett a kép
                $width = ceil(($maxHeight / $dimensions[1]) * $dimensions[0]);
                $height = $maxHeight;
            } else
            {
                $width = $maxWidth;
            }
            $image->resample($width, $height)->crop($maxWidth, $maxHeight, 'center', 'top');
        }
        $image->save($path);
    }

    /**
     * @param $phone
     *
     * @return mixed
     */
    public static function formatPhone($phone)
    {
        $phone = preg_replace("/[^0-9]/", "", $phone);
        if (strlen($phone) == 7)
            return preg_replace("/([0-9]{3})([0-9]{4})/", "$1-$2", $phone);
        elseif (strlen($phone) == 10)
            return preg_replace("/([0-9]{3})([0-9]{3})([0-9]{4})/", "($1) $2-$3", $phone);
        else
            return $phone;
    }

    /**
     * @param $str
     *
     * @return mixed
     */
    public static function clearText($str)
    {
        $str = trim(strip_tags($str));

        return preg_replace("/[^0-9]/", "", $str);
    }

    public static function setSession($name)
    {
        if (isset($_SESSION[$name]))
        {
            unset($_SESSION[$name]);

            return 1;
        }
    }

    public static function setBackgroundClass($module)
    {
        $class = 'fixed-header';
        $title = '';
        switch ($module)
        {
            case 'Golf' :
                $class .= '-golf';
                break;
            case 'Contact' :
                $class .= '-contact';
                break;
            default:
                $class;
                $title = 'Brokerage, Property Management, Consulting <br/> & Investments';
        }

        return ['class' => $class, 'title' => $title];
    }

    public static function isImage($id, $modul = 'contact')
    {
        if (! file_exists('images/' . $modul . '/' . $id . '.jpg'))
        {
            return false;
        }

        return true;
    }
}
