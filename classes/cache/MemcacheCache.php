<?php

class MemcacheCache implements ICache {
	protected $handler;
	protected $connectionError = false;
	protected $hash = '';
	protected $isEnabled;

	public function __construct($hash = null) {
		$this->hash = is_null($hash) ? md5($_SERVER['HTTP_HOST']) : $hash;
		if (extension_loaded('Memcache')) {
			$this->handler = new Memcache();
			@$ret = $this->handler->connect('localhost', 11211);
			if ($ret === false) {
				$this->connectionError = true;
			}
		}
	}

	public function isEnabled() {
		if (is_null($this->isEnabled)) {
			$this->isEnabled = extension_loaded('Memcache') && !$this->connectionError;
		}
		return $this->isEnabled;
	}

	protected function checkEnabled() {
		if (!$this->isEnabled()) {
			UniAdmin::addDebugInfo('cannot load Memcache Cache', 'cache', 3);
			UniAdmin::flushDebugInfos();
		}
		return true;
	}

	public function exists($key) {
		$this->checkEnabled();
		// mivel nincs külön exists() metódus a lekérdezéshez, hozzáfűzünk egy nullt, ami
		// hibát dob, ha nincs ilyen kulcs az adatbázisban
		$ret = $this->handler->append($this->hash . $key, null);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('checking ' . $key . ' in Memcache cache: ' . ($ret ? 'exists' : 'does not exists'), 'cache');
		}
		return $ret;
	}

	public function get($key) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading ' . $key . ' from Memcache cache', 'cache');
		}
		return $this->handler->get($this->hash . $key);
	}

	public function set($key, $value, $ttl) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('storing ' . $key . ' to Memcache cache', 'cache');
		}
		return $this->handler->set($this->hash . $key, $value, false, $ttl);
	}

	public function delete($key) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('deleting ' . $key . ' from Memcache cache', 'cache');
		}
		return $this->handler->delete($this->hash . $key);
	}
}
