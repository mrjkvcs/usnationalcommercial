<?php

class FileCache implements ICache {
	protected $hash = '';
	protected $baseDir = 'temp';

	public function __construct($hash = null) {
		$this->hash = is_null($hash) ? md5($_SERVER['HTTP_HOST']) : $hash;
	}

	public function setBaseDir($directory) {
		$this->baseDir = rtrim($directory, '/');
		return $this;
	}

	public function getBaseDir() {
		return $this->baseDir;
	}

	public function isEnabled() {
		if (!file_exists($this->baseDir)) {
			@mkdir($this->baseDir, 0777, true);
		}
		return file_exists($this->baseDir) 
			&& is_dir($this->baseDir) 
			&& is_writable($this->baseDir);
	}

	protected function checkEnabled() {
		if (!$this->isEnabled()) {
			UniAdmin::addDebugInfo('cannot load File cache, directory is not writable', 'cache', 3);
			UniAdmin::flushDebugInfos();
		}
		return true;
	}

	protected function createCacheFilename($key) {
		return $this->baseDir . DIRECTORY_SEPARATOR . $this->hash . '-' . $key;
	}

	public function exists($key) {
		$this->checkEnabled();
		$cacheFile = $this->createCacheFilename($key);
		$ret = false;
		if (file_exists($cacheFile)) {
			$cacheExpire = filemtime($cacheFile);
			if ($cacheExpire > time()) {
				$ret = true;
			}
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('checking ' . $key . ' in File cache: ' . ($ret ? 'exists' : 'does not exists'), 'cache');
		}
		return $ret;
	}

	public function get($key, $returnFileHandler = false, $decode = true) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading ' . $key . ' from File cache', 'cache');
		}
		$cacheFile = $this->createCacheFilename($key);
		if (!file_exists($cacheFile)) {
			return false;
		}
		$cacheExpire = filemtime($cacheFile);
		if ($cacheExpire < time()) {
			return false;
		}
		if ($returnFileHandler == true) {
			return fopen($cacheFile, 'r');
		} else {
			return $decode ? json_decode(file_get_contents($cacheFile), true) : file_get_contents($cacheFile);
		}
		return false;
	}

	public function set($key, $value, $ttl, $encode = true) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('storing ' . $key . ' to File cache', 'cache');
		}
		$cacheFile = $this->createCacheFilename($key);
		$cacheExpire = time() + $ttl;
		if (file_put_contents($cacheFile, $encode ? json_encode($value) : $value)) {
			touch($cacheFile, $cacheExpire);
			return true;
		}
		return false;
	}

	public function delete($key) {
		$this->checkEnabled();
		$cacheFile = $this->createCacheFilename($key);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('deleting ' . $key . ' from File cache', 'cache');
		}
		if (file_exists($cacheFile) && is_writable($cacheFile)) {
			unlink($cacheFile);
			return true;
		}
		return false;
	}
}
