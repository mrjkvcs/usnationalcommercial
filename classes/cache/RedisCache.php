<?php

class RedisCache implements ICache {
	protected $hash = '';
	protected $client;

	public function __construct($hash = null) {
		$this->hash = is_null($hash) ? md5($_SERVER['HTTP_HOST']) : $hash;
		UniAdmin::import('extensions.predis.Autoloader', true);
		$this->client = new Predis\Client();
	}

	public function isEnabled() {
		return extension_loaded('redis');
	}

	protected function checkEnabled() {
		if (!$this->isEnabled()) {
			UniAdmin::addDebugInfo('cannot load Redis Cache, missing redis extension', 'cache', 3);
			UniAdmin::flushDebugInfos();
		}
		return true;
	}

	public function exists($key) {
		$this->checkEnabled();
		$ret = $this->client->exists($this->hash . $key);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('checking ' . $key . ' in Redis cache: ' . ($ret ? 'exists' : 'does not exists'), 'cache');
		}
		return $ret;
	}

	public function get($key) {
		$this->checkEnabled();
		$value = $this->client->get($this->hash . $key);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading ' . $key . ' from Redis cache', 'cache');
		}
		if (!$value) {
			return false;
		}
		return unserialize($value);
	}

	public function set($key, $value, $ttl) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('storing ' . $key . ' to Redis cache, ttl ' . $ttl . ' sec', 'cache');
		}
		return $this->client->setEx($this->hash . $key, $ttl, serialize($value));
	}

	public function delete($key) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('deleting ' . $key . ' from Redis cache', 'cache');
		}
		return $this->client->del($this->hash . $key);
	}
}
