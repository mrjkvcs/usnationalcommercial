<?php

class ApcCache implements ICache {
	protected $hash = '';
	protected $isEnabled;

	public function __construct($hash = null) {
		$this->hash = is_null($hash) ? md5($_SERVER['HTTP_HOST']) : $hash;
	}

	public function isEnabled() {
		if (is_null($this->isEnabled)) {
			$this->isEnabled = extension_loaded('apc') && ini_get('apc.enabled');
		}
		return $this->isEnabled;
	}

	protected function checkEnabled() {
		if (!$this->isEnabled()) {
			UniAdmin::addDebugInfo('cannot load APC Cache, missing apc extension', 'cache', 3);
			UniAdmin::flushDebugInfos();
		}
		return true;
	}

	public function exists($key) {
		$this->checkEnabled();
		$ret = apc_exists($this->hash . $key);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('checking ' . $key . ' in APC cache: ' . ($ret ? 'exists' : 'does not exists'), 'cache');
		}
		return $ret;
	}

	public function get($key) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading ' . $key . ' from APC cache', 'cache');
		}
		return apc_fetch($this->hash . $key);
	}

	public function set($key, $value, $ttl) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('storing ' . $key . ' to APC cache', 'cache');
		}
		return apc_store($this->hash . $key, $value, $ttl);
	}

	public function delete($key) {
		$this->checkEnabled();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('deleting ' . $key . ' from APC cache', 'cache');
		}
		return apc_delete($this->hash . $key);
	}
}
