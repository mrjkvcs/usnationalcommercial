<?php

class NoCache implements ICache {
	public function __construct($hash = null) { }

	public function isEnabled() {
		return false;
	}

	public function exists($key) {
		return false;
	}

	public function get($key) {
		return null;
	}

	public function set($key, $value, $ttl) {
		return false;
	}

	public function delete($key) {
		return false;
	}
}
