<?php

class SysMessage
{
    public static function displayError($message, $autoHide = false)
    {
        echo '<div class="alert alert-danger alert-dismissable' . ($autoHide ? ' fadeout' : '') . '"><i class="fa fa-fw fa-times"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $message . '</div>';
    }

    public static function displayNotice($message, $autoHide = false)
    {
        echo '<div class="alert alert-warning alert-dismissable' . ($autoHide ? ' fadeout' : '') . '"><i class="fa fa-fw fa-exclamation-triangle"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $message . '</div>';
    }

    public static function displayInfo($message, $autoHide = false)
    {
        echo '<div class="alert alert-info alert-dismissable' . ($autoHide ? ' fadeout' : '') . '"><i class="fa fa-fw fa-info"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $message . '</div>';
    }

    public static function displaySuccess($message, $autoHide = false)
    {
        echo '<div class="alert alert-success alert-dismissable' . ($autoHide ? ' fadeout' : '') . '"><i class="fa fa-fw fa-check"></i><button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>' . $message . '</div>';
    }
}