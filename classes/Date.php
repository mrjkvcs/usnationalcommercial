<?php

/**
 * Dátum kezelő függvények
 */
class Date {
	/**
	 * Megformázza az időbélyeg alapján a kapott dátumot
	 * @param int|string $timestamp
	 * @param string $format
	 * @return string
	 */
	public static function format($timestamp = null, $format = 'full') {
		if (is_null($timestamp)) {
			$timestamp = time();
		} else if (!is_numeric($timestamp)) {
			$timestamp = strtotime($timestamp);
		}
		switch ($format) {
			case 'none':
				return '';
			case 'full':
			case 'short':
					return strftime(Setting::getDateFormat(true) . ($format == 'full' ? ' %H:%M' : ''), $timestamp);
				break;
			case 'fullnumeric':
			case 'shortnumeric':
					return strftime(Setting::getDateFormat() . ($format == 'fullnumeric' ? ' %H:%M' : ''), $timestamp);
				break;
			case 'dynamic':
					$today = time();
					$reldays = floor(($timestamp - $today) / 86400);
					if ($reldays == 0) {
						return t('day') . strftime(' %H:%M', $timestamp);
					} else if ($reldays == 1) {
						return t('tomorrow') . strftime(' %H:%M', $timestamp);
					} else if ($reldays == -1) {
						return t('yesterday') . strftime(' %H:%M', $timestamp);
					} else if ($reldays < -1 && $reldays >= -7) {
						return strftime('%A %H:%M', $timestamp);
					} else if (date('Y') == date('Y', $timestamp)) {
						return strftime('%B %e. %H:%M', $timestamp);
					} else {
						return strftime(Setting::getDateFormat(true) . ' %H:%M', $timestamp);
					}
				break;
			default:
					return strftime($format, $timestamp);
				break;
		}
	}

	/**
	 * Visszaadja az életkor egy születési dátumból
	 * @param string|int $birthdate a születési dátum Y-m-d vagy időbélyeg formában
	 * @return int|bool
	 */
	public static function getAge($birthdate) {
		if (is_numeric($birthdate)) $birthdate = date('Y-m-d',$birthdate);
		if (!empty($birthdate) && substr($birthdate, 0, 4) != '0000') {
			list($year,$month,$day) = explode("-",$birthdate);
			$year_diff = date("Y") - $year;
			$month_diff = date("m") - $month;
			$day_diff = date("d") - $day;
			if ($month_diff < 0) $year_diff--;
			elseif (($month_diff==0) && ($day_diff < 0)) $year_diff--;
			return $year_diff;
		} else return false;
	}

	/**
	 * Ellenőrzi, hogy egy dátum helyes-e
	 * @param string
	 */
	public static function check($date) {
		$parts = explode('-' , $date);
		if (isset($parts[0]) && isset($parts[1]) && isset($parts[2])) {
			return checkdate($parts[1], $parts[2], $parts[0]);
		} else {
			return false;
		}
	}
}
