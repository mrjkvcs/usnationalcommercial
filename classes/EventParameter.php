<?php

class EventParameter {
	protected $_moduleId;
	protected $_event;
	protected $_history = array();

	public function __construct() {}

	public function startEvent($moduleId, $event) {
		$this->_moduleId = $moduleId;
		$this->_event = $event;
		return $this;
	}

	public function addToHistory($fullPath, $className, $methodName) {
		$this->_history[] = array(
			'path' => $fullPath,
			'class' => $className,
			'method' => $methodName,
		);
		return $this;
	}

	public function getModule() {
		return $this->_moduleId;
	}

	public function getEvent() {
		return $this->_event;
	}

	public function getHistory() {
		return $this->_history;
	}
}
