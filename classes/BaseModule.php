<?php

/**
 * Modulok létrehozása és kezelése
 * @abstract
 */
abstract class BaseModule {
	public $action;
	protected $validators = array();

	public function __construct() { }

	/**
	 * Lekéri egy adott eseményhez tartozó eseménykezelőket, majd meghívja azokat
	 */
	public function dispatchEvent($event, $parameter = null) {
		UniAdmin::app()->dispatchEvent(get_class($this), $event, $parameter);
		// @todo ide rekurzív hívás szükséges
		if (get_parent_class($this) && get_parent_class($this) != 'BaseModule' && get_parent_class($this) != 'BaseAdminModule') {
			UniAdmin::app()->dispatchEvent(get_parent_class($this), $event, $parameter);
		}
	}

	/**
	 * Létrehozza az osztály settings objektumát az aktuális beállításokkal
	 * Személyes beállítások esetén (csak a back-end oldalon) a bejelentkezett adminisztrátor személyes beállításait adja vissza
	 * @param string a modul neve. Ha nincs megadva, az aktuális modult veszi alapul. Több paraméter esetén mindegyik megadott modul beállításait betölti (a későbbiek felülírják a korábban megadottakat!)
	 */
	public function loadSettings() {
		$params = func_get_args();
		if (!count($params)) $params = array(get_class($this));
		foreach ($params as $module) {
			$result = Db::sql("SELECT `id`,`value` FROM `UniadminSettings` WHERE `moduleId` = '$module' ORDER BY `position`");
			if ($result) {
				while ($rs=Db::loop($result,'object')) {
					$this->settings->{$rs->id} = Setting::get($rs->id,$module);
				}
			}
		}
	}

	/**
	 * Visszaadja a modul beállításait JSON formátumban. Paraméterezése megegyezik a loadSettings metóduséval
	 * @return string
	 */
	public function loadSettingsJSON() {
		$params = func_get_args();
		if (!count($params)) $params = array(get_class($this));
		$ret = '{ settings: { ';
		$i=0;
		foreach ($params as $module) {
			$result = Db::sql("SELECT `id`,`value` FROM `UniadminSettings` WHERE `moduleId` = '$module' ORDER BY `position`");
			if ($result) {
				while ($rs=Db::loop($result)) {
					if ($i++) $ret .= ', ';
					$ret .= "'".$rs["id"]."': '".Setting::get($rs["id"],$module)."'";
				}
			}
		}
		$ret .= ' } }';
		return $ret;
	}

	/**
	 * betölti a megadott szótár fájlt
	 * @param string $fileName, formátuma basic, webshop.basic, modules.webshop.i18n.basic lehet
	 * @param string $lang a szótárfájl nyelve, ha nincs megadva az aktuális nyelvet veszi alapul
	 */
	public function loadDictionary($fileName, $lang = null) {
		global $dictionary;

		if (!isset($dictionary)) {
			$dictionary = array();
		}

		if (!$lang) {
			$lang = LANG;
			if (!$lang) return false;
		}

		$parts = preg_split('/\./', $fileName, -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) == 1) {
			$parts[1] = $parts[0];
			$parts[0] = get_class($this);
			if (substr($parts[0], -10) == 'Controller') {
				$parts[0] = substr($parts[0], 0, -10);
			}
		}
		if (count($parts) == 2) {
			if (strtolower($parts[0]) == 'i18n') {
				$viewFile = 'i18n.' . $parts[1];
			} else {
				$parts[0] = ucfirst(strtolower($parts[0]));
				if (!defined('IS_BACKEND') || $parts[0] == 'Admin') {
					$fileName = 'modules.' . $parts[0] . '.i18n.' . $parts[1];
				} else {
					$fileName = 'modules.Admin.modules.' . $parts[0] . '.i18n.' . $parts[1];
				}
			}
		}

		$path = preg_replace('/\./', '/', $fileName);

		if (file_exists($path . '_' . $lang . '.php')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading dictionary ' . $fileName . '_' . $lang, 'dictionary');
			}
			include_once($path . '_' . $lang . '.php');
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot load dictionary ' . $fileName . '_' . $lang, 'dictionary', 2);
			}
		}
	}

	public function redirect($module = '', $action = array(), $language = null, $parameters = array(), $hash = null) {
		if (UniAdmin::app()->isAjax()) {
			if (isset($_REQUEST['return']) && $_REQUEST['return'] == 1) {
				header('Content-Type: application/json');
				echo json_encode(array(
					'module' => $module,
					'action' => $action,
					'language' => $language,
					'parameters' => $parameters,
				));
				exit;
			} else {
				$ch = curl_init();
				curl_setopt($ch, CURLOPT_URL, $_SERVER['HTTP_HOST'] . Url::link($module, $action, $language, $parameters, $hash));
				curl_setopt($ch, CURLOPT_HEADER, false);
				curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
				curl_setopt($ch, CURLOPT_HTTPHEADER, array('X-requested-with: XMLHttpRequest'));
				$ret = curl_exec($ch);
				curl_close($ch);
				echo $ret;
				exit;
			}
		}

		header('Location: ' . Url::link($module, $action, $language, $parameters, $hash));
		exit;
	}

	/**
	 * Függvény, amelyet a kiválasztott Controller bármilyen action-jének meghívása előtt hív meg a keretrendszer
	 */
	public function beforeAction() {}

	/**
	 * Függvény, amelyet a kiválasztott Controller bármilyen action-jének meghívása után hív meg a keretrendszer
	 */
	public function afterAction() {}

	public function actionDefault() {}
	
	/**
	 * Hívási paraméterek automatikus validálása. A $validators tömb action->csupa kisbetűs action neveket kell, hogy tartalmazzon
	 */
	public function callValidator() {
		$parameters = func_get_args();
		if (is_array($this->validators) && !empty($parameters)) {
			$action = strtolower($parameters[0]);
			$validators = array_change_key_case($this->validators, CASE_LOWER);
			if (array_key_exists($action, $validators)) {
				if (is_callable($validators[$action])) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('calling action parameter validator for ' . get_class($this) . '->' . $action, 'framework');
					}
					return call_user_func_array($validators[$action], array_slice($parameters, 1));
				} else {
					throw new Error('Cannot call validator for method ' . $this->action);
				}
			}
		}
	}

	/**
	 * Betölti és visszaadja a megadott view fájl tartalmát
	 * @param string $viewFile, formátuma article, news.article, modules.news.views.article lehet, illetve a speciális views.form formátum is engedélyezett
	 * @param array $parameters az átadandó paraméterek asszociatív tömbként, amelyeket változóként el lehet érni a view-ben
	 */
	public function loadView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		if (is_null($viewFile)) {
			if (isset($this->viewFile)) {
				$viewFile = $this->viewFile;
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('no view file set', 'views', 3);
				}
				return false;
			}
		}

		$parts = preg_split('/\./', $viewFile, -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) == 1) {
			$parts[1] = $parts[0];
			$parts[0] = get_class($this);
			if (substr($parts[0], -10) == 'Controller') {
				$parts[0] = substr($parts[0], 0, -10);
			}
		}
		if (count($parts) == 2) {
			if (strtolower($parts[0]) == 'views') {
				$viewFile = 'views.' . $parts[1];
			} else {
				$parts[0] = ucfirst(strtolower($parts[0]));
				if (!defined('IS_BACKEND') || $parts[0] == 'Admin') {
					$viewFile = 'modules.' . $parts[0] . '.views.' . $parts[1];
				} else {
					$viewFile = 'modules.Admin.modules.' . $parts[0] . '.views.' . $parts[1];
				}
			}
		}

		if (!is_null($cacheTtl) && UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get('view-' . $viewFile);
			if ($cachedVersion) {
				return $cachedVersion;
			}
		}

		$path = preg_replace('/\./', '/', $viewFile);

		if (file_exists($path . '.php')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading view file ' . $viewFile, 'views');
			}
			if (!is_array($parameters)) {
				$parameters = array($parameters);
			}
			if (!empty($parameters)) {
				foreach ($parameters as $key => $value) {
					$$key = $value;
				}
			}
			ob_start();
			require($path . '.php');
			$viewContents = ob_get_clean();

			if (isset($cachedVersion)) {	// ha a cache engedélyezve van
				UniAdmin::app()->cache->set('view-' . $viewFile, $viewContents, $cacheTtl);
			}
			return $viewContents;
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot find view file ' . $viewFile, 'views', 2, __FILE__, __LINE__);
			}
		}
	}

	/**
	 * megjeleníti a megadott view fájlt
	 * @param string $viewFile, formátuma article, news.article, modules.news.views.article lehet, illetve a speciális views.form formátum is engedélyezett. Elhagyása esetén az objektum $viewFile változóját próbálja használni.
	 * @param array $parameters az átadandó paraméterek asszociatív tömbként, amelyeket változóként el lehet érni a view-ben
	 */
	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		echo $this->loadView($viewFile, $parameters, $cacheTtl);
	}

    /**
     * Menu link active helper
     * @param string $model
     * @param $menu
     * @return string
     */
    public function isMenuActive($model = '', $menu) {
        $menu = strtoupper($menu);
        $link = Url::link($model);
        $active = Url::isActive($link, true, false) ? ' class="active"' : '';
        return '<li' . $active . '><a href="' . $link . '">' . $menu . '</a> </li>';
    }
}
