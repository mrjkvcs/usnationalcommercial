<?php

/**
 * Az alkalmazás központi vezérlőit tartalmazó osztály
 */
class AdminApplication {
	public $config = array();
	public $page;
	public $route;
	public $language;
	public $module;
	protected $variables = array();
	protected $eventhandlers = array();
	protected $isAjax = false;
	protected $isMobileDevice;

	/**
	 * Beállít egy globális esemény-figyelőt egy modul megadott eseményéhez
	 * @param string $module a modul azonosítója
	 * @param string $event az esemény neve
	 * @param callback $callback az eseményhez rendelt függvény, ami meghívásra kerül
	 * @param int $priority az eseménykezelő prioritása, 1 = magas, 2 = normál, 3 = alacsony, alapértelmezetten 2
	 */
	public function addEventObserver($module, $event, $callback, $priority = 2) {
		$found = false;
		foreach ($this->eventhandlers as $item) {
			if ($item['module'] == $module && $item['event'] == strtolower($event) && $item['callback'] == $callback) {
				$found = true;
				break;
			}
		}
		if (!$found) {
			$this->eventhandlers[] = array('module' => $module, 'event' => strtolower($event), 'callback' => $callback, 'priority' => $priority);
		}
		return $this;
	}

	/**
	 * Betölti az adott modulhoz tartozó eseménykezelőket
	 * Ez a metódus automatikusan meghívásra kerül a BaseModule osztály példányosításakor
	 */
	public function loadEventObservers($reload = false) {
		if (!is_array($this->eventhandlers) || $reload) {
			$this->eventhandlers = array();
		}
		if (UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get('uniAdminEventHandlers');
			if ($cachedVersion && is_array($cachedVersion)) {
				$this->eventhandlers = $cachedVersion;
				return $this;
			}
		}
		if (isset(UniAdmin::app()->config['events'])) {
			if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('loading all event handlers (' . count(UniAdmin::app()->config['events']) . ' config based event handlers registered)', 'events');
				}
			foreach (UniAdmin::app()->config['events'] as $e) {
				$this->addEventObserver(
					isset($e['moduleId']) ? $e['moduleId'] : $e[0],
					isset($e['event']) ? $e['event'] : $e[1],
					array(
						isset($e['path']) ? $e['path'] : $e[2],
						isset($e['method']) ? $e['method'] : $e[3]
					),
					isset($e['priority']) ? $e['priority'] : (isset($e[4]) ? $e[4] : 2)
				);
			}
		}
		$result = Db::sql("
			SELECT `moduleId`, `event`, `path`, `method`, `priority`
			FROM `UniadminModuleAddons`
		");
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading all event handlers (' . Db::numrows($result) . ' database event handlers registered)', 'events');
		}
		while ($rs = Db::loop($result)) {
			$this->addEventObserver($rs['moduleId'], $rs['event'], array($rs['path'], $rs['method']), intval($rs['priority']));
		}
		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->set('uniAdminEventHandlers', $this->eventhandlers, 3600);
		}
		return $this;
	}

	public function initModels() {
//		UniAdmin::import('extensions.activerecord.ActiveRecord', true);
        UniAdmin::import('vendor.php-activerecord.php-activerecord.ActiveRecord', true);
		ActiveRecord\Config::initialize(function($cfg) {
			$dbConfig = UniAdmin::app()->config['db'];
			$cfg->set_model_directory('models');
			$cfg->set_connections(array(
				'development' => 'mysql://' . urlencode($dbConfig['username']) . ':' . urlencode($dbConfig['password']) . '@' . $dbConfig['host'] . '/' . $dbConfig['database'] . '?decode=true&charset=' . $dbConfig['charset']
			));
		});
	}

	/**
	 * Meghívja az aktuális modul megadott eseményéhez rendelt eseménykezelőket
	 * (prioritás alapján, illetve ezen belül a regisztrálás sorrendjében)
	 * @param string $module a modul azonosítója
	 * @param string $event az esemény neve
	 */
	public function dispatchEvent($module, $event, $parameter = null) {
		if (!is_object($parameter) || !is_a($parameter, 'AdminApplication')) {
			if (!is_object($parameter) || !is_a($parameter, 'EventParameter')) {
				$parameter = new EventParameter();
			}
			$parameter->startEvent($module, strtolower($event));
		}
		$callback = $priority = array();

		foreach ($this->eventhandlers as $key => $data) {
			if (strtolower($data['module']) == strtolower($module) && strtolower($data['event']) == strtolower($event)) {
				$callback[$key] = $data['callback'];
				$priority[$key] = $data['priority'];
			}
		}

		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('firing ' . $event . ' event in ' . $module . ' module, ' . count($callback) . ' event handlers found', 'events');
		}

		if (empty($callback)) {
			return;
		}

		if (is_array($priority)) {
			array_multisort($priority, SORT_ASC, $callback, SORT_ASC);
		}

		if (is_array($callback)) {
			foreach ($callback as $item) {
				if (is_array($item)) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('found event observer ' . $item[0] . '->' . $item[1], 'events');
					}

					$parts = explode('.', $item[0]);
					$className = end($parts);

					// Visszatettem, mert különben az admin menü nem működött rendesen, mindent betett gondolkodás nélkül
					if (!defined('IS_BACKEND')) {
						// csak adminban kell a jogosultságot ellenőrizni
						$accessGranted = true;
					} else if (UniAdmin::app()->user->isRoot()) {
						// root usernek alapból igen
						$accessGranted = true;
					} else if (substr($item[0], 0, 22) != 'modules.Admin.modules.') {
						// ha nem admin modul, akkor is engedélyezzük a hozzáférést
						$accessGranted = true;
					} else if (substr($item[0], -10) != 'Controller') {
						// ha nem Controller, akkor is hozzáférhet
						$accessGranted = true;
					} else {
						// admin modul
						if (UniAdmin::app()->user->getInheritance()) {
							$userGroup = UniAdmin::app()->user->getGroupId();
						}
						$query = sprintf("
							SELECT COUNT(*)
							FROM UniadminModuleAccess
							WHERE type = '%s' AND moduleId = '%s' AND userId = '%s'",
							isset($userGroup) ? 'g' : 'u',
							$className,
							isset($userGroup) ? $userGroup : UniAdmin::app()->user->getId());
						$accessGranted = Db::sqlBool($query);	// csoport vagy felhasználói szintű ellenőrzés
					}

					if (!$accessGranted) continue;

					UniAdmin::import($item[0]);
					if (class_exists($className)) {
						$_module = new $className;
						if (method_exists($_module, $item[1])) {
							if (!is_a($parameter, 'AdminApplication')) {
								// eseményhívás naplózása a paraméter objektumba
								$parameter->addToHistory($item[0], $className, $item[1]);
							}

							$ret = call_user_func(array($_module, $item[1]), $parameter);

							if ($ret === false) {
								if (DEBUG_MODE) {
									UniAdmin::addDebugInfo('false returned, stopping further callbacks', 'events');
								}
								return;
							}
						} else {
							if (DEBUG_MODE) {
								UniAdmin::addDebugInfo('cannot find method ' . $className . '->' . $item[1], 'events', 2, __FILE__, __LINE__);
							}
						}
					} else {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('error loading module ' . $className, 'events', 2, __FILE__, __LINE__);
						}
					}
				} else if (is_callable($item)) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('calling anonymous event handler function', 'events', 1, __FILE__, __LINE__);
					}
					$ret = call_user_func($item, $parameter);
					if ($ret === false) {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('false returned, stopping further callbacks', 'events', 1, __FILE__, __LINE__);
						}
						return;
					}
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('cannot find method ' . $className . '->' . $item, 'events', 2, __FILE__, __LINE__);
					}
				}
			}
		}
	}

	/**
	 * Beállít egy globálisan elérhető változót
	 * @param string $id a változó azonosítója
	 * @param mixed $value a változó értéke
	 */
	public function setVariable($id, $value) {
		$this->variables[$id] = $value;
	}

	/**
	 * Ha nem sikerülne az AJAX mód automatikus felismerése, manuálisan is állítható
	 */
	public function setAjaxMode($state) {
		$this->isAjax = (bool) $state;
		return $this;
	}

	/**
	 * Megadja, hogy egy adott globális változó létezik-e?
	 * @param string $id a változó azonosítója
	 * @return bool
	 */
	public function hasVariable($id) {
		return isset($this->variables[$id]);
	}

	/**
	 * Visszaadja egy globális változó értékét
	 * @param string $id a változó azonosítója
	 * @return mixed
	 */
	public function getVariable($id) {
		return isset($this->variables[$id]) ? $this->variables[$id] : null;
	}

	/**
	 * Visszaadja az összes globális változót asszociatív tömbként
	 * @return array
	 */
	public function getVariables() {
		return $this->variables;
	}

	public function start(&$config) {
		$this->config = $config;

		if (DEBUG_MODE) {
			$_applicationStart = microtime(true);
			UniAdmin::addDebugInfo('starting session', 'framework');
		}
		@session_start();

		if (is_array($config['db'])) {
			if (isset($config['db']['connection'])) {
				Db::addConnection($config['db']['connection']);
				Db::setCurrentConnection($config['db']['connection']);
			}
			Db::connect($config['db']['database'], $config['db']['username'], $config['db']['password'],
				$config['db']['host'], $config['db']['charset']);
			if (isset($config['db']['debug']) && $config['db']['debug']) {
				Db::setDebugMode($config['db']['debug']);
			}
			if (isset($config['db']['models']) && $config['db']['models']) {
				$this->initModels();
			}
		}

		if (isset($config['mongodb']) && is_array($config['mongodb'])) {
			\Mongo\Db::connect($config['mongodb']['server'], $config['mongodb']['options']);
		}

		if (isset($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') {
			$this->isAjax = true;
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('ajax mode on', 'framework');
			}
		}

		if (isset($config['import']) && count($config['import'])) {
			foreach ($config['import'] as $module) {
				UniAdmin::import($module);
			}
		}

		if (isset($config['forcedImport']) && count($config['forcedImport'])) {
			foreach ($config['forcedImport'] as $module) {
				UniAdmin::import($module, true);
			}
		}

		$this->loadEventObservers();
		$this->dispatchEvent('application', 'start', $this);

		if ($this->config['routehandler']['class']) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('starting route handler', 'framework');
			}
			UniAdmin::import($this->config['routehandler']['class']);
			$parts = explode('.', $this->config['routehandler']['class']);
			$className = end($parts);
			$this->route = new $className;

			$moduleToLoad = ucfirst(strtolower($this->route->getCurrentModule()));
			if ($moduleToLoad != 'Admin') {
				$this->language = $this->route->getLanguage();
				if (!$this->language) {
					if (UniAdmin::app()->config['language'] == 'detect') {
						// átirányít a megfelelő nyelvre, ha nem a böngésző nyelve nem az alapértelmezett nyelv
						Setting::detectLanguage();
					}
					$this->language = Setting::getDefaultLanguage();
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('setting language ' . $this->language, 'framework');
					}
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('setting language ' . $this->language . ', extracted from route', 'framework');
					}
				}
				if (!defined('LANG')) {
					define('LANG', $this->language);
				}
				$this->variables['lang'] = $this->language;

				setlocale(LC_ALL, Setting::getLocale());

				global $dictionary;
				if (!isset($dictionary)) {
					$dictionary = array();
				}
				if (file_exists('i18n/base_' . LANG . '.php')) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('loading basic dictionary i18n.base_' . LANG, 'framework');
					}
					include('i18n/base_' . LANG . '.php');
				} else {
					$defaultLang = Setting::getDefaultLanguage();
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('loading basic dictionary i18n.base_' . LANG, 'framework');
					}
					include('i18n/base_' . $defaultLang . '.php');
				}
			}

			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('starting page handler', 'framework');
			}

			UniAdmin::import('modules.Pagesetup.Pagesetup');
			$this->page = new Pagesetup();

			if (defined('MAINTENANCE_MODE') && MAINTENANCE_MODE) {
				header('HTTP/1.1 503 Service Temporarily Unavailable');
				$this->config['page'] = 'maintenance';
				UniAdmin::addDebugInfo('maintenance mode is on', 'framework');
				$this->page->display();
				return;
			}

			if ($moduleToLoad) {
				try {
					$this->dispatchEvent('application', 'beforeModule', $this);
					$moduleController = $moduleToLoad . 'Controller';
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('starting module ' . $moduleController, 'framework');
					}
					UniAdmin::import('modules.' . $moduleToLoad . '.' . $moduleController);
					if (class_exists($moduleController)) {
						$action = $this->route->getCurrentAction();
						$actionFiltered = strtolower(substr($action, 6));

						$this->module = new $moduleController($actionFiltered);

						// újra elkérjük az actiont, hátha a modul megváltoztatta
						$action = $this->route->getCurrentAction();
						$actionFiltered = strtolower(substr($action, 6));

						if ($moduleController != 'AdminController') {
							if ($action && method_exists($this->module, $action)) {
								$this->module->action = $actionFiltered;
								if (DEBUG_MODE) {
									UniAdmin::addDebugInfo('loading action ' . get_class($this->module) . '->' . $action, 'framework');
								}
								$route = array_slice($this->route->getRoute(), 1);

								// paraméter validálás
								$ret = call_user_func_array(array($this->module, 'callValidator'), $route);
								if (empty($ret) || $ret === true) {
									// beforeAction metódus hívása
									call_user_func_array(array($this->module, 'beforeAction'), $route);

									// kiválasztott action hívása
									call_user_func_array(array($this->module, $action), array_slice($route, 1));

									// afterAction metódus hívása
									call_user_func_array(array($this->module, 'afterAction'), $route);
								} else {
									$ret = str_replace(array('[module]', '[action]'), array(strtolower($moduleToLoad), isset($route[0]) ? $route[0] : null), (string) $ret);
									header('Location: ' . $ret);
									exit;
								}
							} else {
								if (DEBUG_MODE) {
									UniAdmin::addDebugInfo('error loading action ' . get_class($this->module) . '->' . $action, 'framework', 2);
								}
							}
						}
					} else {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('error loading module ' . $moduleController, 'framework', 3, __FILE__, __LINE__);
							UniAdmin::addDebugInfo(UniAdmin::formatBacktrace(true));
						}
					}
				} catch (HttpException $e) {	// http hibakódok
					$errorCode = substr(get_class($e), 4, 3);

					$this->module = $e;

					switch ($errorCode) {
						case '403':		header('HTTP/1.1 403 Forbidden');				break;
						case '500':		header('HTTP/1.1 500 Internal Server Error');	break;
						default:		header('HTTP/1.1 404 Not Found');				break;
					}
				} catch (Error $e) {	// minden egyéb kivétel
					$this->module = $e;
				}

				$this->dispatchEvent('application', 'beforeRender');

				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('starting page display', 'framework');
				}
				try {
					$this->page->display();
				} catch (Error $e) {
					ob_end_clean();
					$e->renderView();
				}

				$this->dispatchEvent('application', 'end');
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('no module to load' . $moduleController, 'framework', 3, __FILE__, __LINE__);
					UniAdmin::addDebugInfo(UniAdmin::formatBacktrace(true));
				}
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('no route handler defined', 'framework', 3, __FILE__, __LINE__);
				UniAdmin::addDebugInfo(UniAdmin::formatBacktrace(true));
			}
		}

		if (DEBUG_MODE && !$this->isAjax) {
			$_applicationEnd = microtime(true);
			$_memoryUsage = memory_get_peak_usage();
			UniAdmin::addDebugInfo('finishing application ' . date('Y-m-d H:i:s') . ', running time ' . (round($_applicationEnd - $_applicationStart, 3) * 1000) . 'ms, peak memory usage ' . Text::formatBytes($_memoryUsage, 2), 'framework');
			UniAdmin::flushDebugInfos();
		}
	}

	public function startCli(&$config) {
		$this->config = $config;

		@session_start();

		if (is_array($config['db'])) {
			if (isset($config['db']['connection'])) {
				Db::addConnection($config['db']['connection']);
				Db::setCurrentConnection($config['db']['connection']);
			}
			Db::connect($config['db']['database'], $config['db']['username'], $config['db']['password'],
				$config['db']['host'], $config['db']['charset']);
			if (isset($config['db']['debug']) && $config['db']['debug']) {
				Db::setDebugMode($config['db']['debug']);
			}
			if (isset($config['db']['models']) && $config['db']['models']) {
				$this->initModels();
			}
		}
		
		if (isset($config['mongodb']) && is_array($config['mongodb'])) {
			\Mongo\Db::connect($config['mongodb']['server'], $config['mongodb']['options']);
		}

		if (isset($config['import']) && count($config['import'])) {
			foreach ($config['import'] as $module) {
				UniAdmin::import($module);
			}
		}

		if (isset($config['forcedImport']) && count($config['forcedImport'])) {
			foreach ($config['forcedImport'] as $module) {
				UniAdmin::import($module, true);
			}
		}

		$this->loadEventObservers();
		$this->dispatchEvent('application', 'start', $this);

	}

	/**
	 * Alkalmazás megjelenítése
	 */
	public function render() {
		if (method_exists($this->module, 'renderView')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('rendering module ' . get_class($this->module), 'framework');
			}
			UniAdmin::app()->module->renderView();
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('error calling render function', 'framework', 3, __FILE__, __LINE__);
				UniAdmin::addDebugInfo(UniAdmin::formatBacktrace(true));
			}
		}
	}

	public function isAjax() {
		return $this->isAjax;
	}

	public function isMobileDevice() {
		if (is_null($this->isMobileDevice)) {
			if (isset($_SESSION['_IS_MOBILE'])) {
				$this->isMobileDevice = (bool) $_SESSION['_IS_MOBILE'];
			} else {
				UniAdmin::import('extensions.Mobile_Detect.Mobile_Detect');
				$mobile = new Mobile_Detect();
				$this->isMobileDevice = $mobile->isMobile();
				$_SESSION['_IS_MOBILE'] = $this->isMobileDevice;
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('detecting mobile device: ' . ($this->isMobileDevice ? 'true' : 'false'), 'framework');
				}
			}
		}
		return $this->isMobileDevice;
	}

	public function __get($key) {
		switch ($key) {	// extra komponensek betöltése első használatkor, amennyiben szükséges és lehetséges
			case 'user':
				if ($this->config['authentication']['class']) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('starting user authentication', 'framework');
					}
					UniAdmin::import($this->config['authentication']['class']);
					$parts = explode('.', $this->config['authentication']['class']);
					$className = end($parts);
					$this->user = new $className;
					return $this->user;
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('user authentication not enabled', 'framework', 3);
					}
					return false;
				}
			break;
			case 'currentUser':
				if ($this->user->checkLogin()) {
					$this->initModels();
					$this->currentUser = User::find($this->user->getId());
					return $this->currentUser;
				} else {
					// itt egy üres user modelt kellene visszaadni
					return false;
				}
			break;
			case 'log':
				if ($this->config['log']['class']) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('starting log writer', 'framework');
					}
					UniAdmin::import($this->config['log']['class']);
					$parts = explode('.', $this->config['log']['class']);
					$className = end($parts);
					$this->log = new $className;
					return $this->log;
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('no log writer set', 'framework', 3);
					}
					return false;
				}
			break;
			case 'cache':
				if (!$this->config['cache']['class']) {
					$this->config['cache']['class'] = 'classes.cache.NoCache';
				}
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('starting cache', 'framework');
				}
				UniAdmin::import($this->config['cache']['class']);
				$parts = explode('.', $this->config['cache']['class']);
				$className = end($parts);
				$this->cache = new $className(isset($this->config['cache']['hash'])?$this->config['cache']['hash']:null);
				return $this->cache;
			break;
			case 'path':
				if ($this->config['path']['class']) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('starting breadcrumb', 'framework');
					}
					UniAdmin::import($this->config['path']['class']);
					$parts = explode('.', $this->config['path']['class']);
					$className = end($parts);
					$this->path = new $className;
					return $this->path;
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('no breadcrumb controller set', 'framework', 3);
					}
					return false;
				}
			break;
			case 'currencyFormat':
				if (self::getVariable('currency')) {
					$currencyId = self::getVariable('currency');
				} else {
					if (!defined('IS_BACKEND') && UniAdmin::app()->user->checkLogin()) {	// felhasználó országa szerinti pénznem
						$currencyId = Db::sqlField(sprintf("
							SELECT currencyId
							FROM Country, User
							WHERE User.countryId = Country.id
							AND User.id = '%d'",
							UniAdmin::app()->user->getId()
						));
					} else {	// nyelv szerinti pénznem
						$currencyId = Currency::getCurrencyByLanguage();
					}
					if (!$currencyId) {
						$currencyId = Currency::getDefaultCurrencyId();
					}
				}
				// @todo itt is a Currency osztály metódusait kellene használni
				$this->currencyFormat = Db::sqlrow(sprintf("
						SELECT id AS currencyId, currency, decimals, decimalPoint, thousandsSeparator, rate 
						FROM UniadminCurrency 
						WHERE id = '%s'", $currencyId
					), 'object'
				);
				return $this->currencyFormat;
			break;
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('no such application property: ' . $key, 'framework', 3);
		}
	}
}
