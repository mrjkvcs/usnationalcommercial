<?php
class Menu {

	static function getMenuBottom() {
	    $menus = array(
	    	// 'FIND A PUPPY' => array(
	    	// 	array('title' => 'Dogs For Sale', 'url' => Url::link('puppy', 'list')),
	    	// 	// array('title' => 'Dog Breed Information', 'url' => Url::link('breed')),
	    	// 	// array('title' => 'Dog Breeders', 'url' => Url::link('breed'))
	    	// ),
	    	// 'BREEDERS & SELLERS' => array(
	    	// 	array('title' => 'I have no idea what', 'url' => Url::link('')),
	    	// 	// array('title' => 'Dog Names', 'url' => Url::link('breed'))
	    	// ),
	    	'BREEDER' => array(
	    		array('title' => 'Breeders Directory', 'url' => Url::link('breeder'))
	    	),
	    	'COMPANY INFO' => array(
	    		array('title' => 'About Us', 'url' => Url::link('about-us')),
	    		array('title' => 'Privacy Policy', 'url' => Url::link('document', 'privacy')),
	    		array('title' => 'Terms of Use', 'url' => Url::link('document', 'terms'))
	    	),
	    	'CONTACT & SUPPORT' => array(
	    		array('title' => 'Advertising Plans', 'url' => Url::link('plans'), 'icon' => 'fa fa-fw fa-money'),
	    		// array('title' => 'How It Works', 'url' => Url::link('document', 'how-it-works'), 'icon' => 'fa fa-fw fa-question-circle'),
	    		array('title' => 'Support', 'url' => Url::link('support'), 'icon' => 'fa fa-fw fa-question-circle'),
	    		array('title' => 'Help Center', 'url' => Url::link('helpcenter'), 'icon' => 'fa fa-fw fa-question-circle'),
	    	),
	    	'STAY CONNECTED' => array(
	    		array('title' => 'Email', 'url' => Url::link('support'), 'icon'=> 'fa fa-fw fa-envelope'),
	    		array('title' => 'Facebook', 'url' => 'https://www.facebook.com/ChoosePuppy', 'icon' => 'fa fa-fw fa-facebook', 'target' => true),
	    		array('title' => 'Twitter', 'url' => 'https://twitter.com/choosepuppy', 'icon' => 'fa fa-fw fa-twitter', 'target' => true)
	    	),
	    );
		return $menus;
	}

	static function getMenu() {
		$menus = array(
			array(
				'title' => 'Home',
				'url' => Url::link(),
			),
			array(
				'title' => 'Listings',
				'url' => Url::link('puppy', 'list'),
			),
			array(
				'title' => 'Dog Breeds',
				'url' => Url::link('breed'),
			),
			array(
				'title' => 'Place a New Listing',
				'url' => Url::link('puppy', 'add'),
				'class' => 'btn btn-success navbar-signup',
				'login' => true
			),
			array(
				'title' => 'Breed Notification !!!',
				'url' => Url::link('notify'),
				'class' => '',
				'login' => true,
				'notify' => 'notifySum'
			),
		);
		return $menus;
	}
}