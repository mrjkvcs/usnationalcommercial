<?php

/**
 * Szöveg kezelő függvények
 */
class Text {
	/**
	 * Leellenőrzi, hogy egy szövegre illeszthető-e egy előre definiált, vagy egy felhasználói reguláris kifejezés
	 * @param string $text a szöveg
	 * @param string $type az előre definiált típusok vagy egyéni reguláris kifejezés
	 * @return bool illeszkedés esetén igaz, különben hamis
	 */
	public static function check($text, $type) {
		switch ($type) {
			case 'email':
				return preg_match('/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i', $text);
			case 'phone':
				return preg_match('/^[0-9[:space:]\+\/\(\)-]+$/i', $text);
			case 'domain':
				return preg_match('/^((http|https|ftp|ftps):\/\/){0,1}[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,4})$/i', $text);
			case 'numbers':
				return preg_match('/^(\-){0,1}[0-9]+$/i', $text);
			case 'text':
				return preg_match('/^[a-z]+$/i', $text);
			case 'alphanumeric':
				return preg_match('/^[a-z0-9]+$/i', $text);
			case 'username':
				return preg_match('/^[a-z0-9\.\_\-]+$/i', $text);
			case 'date':
				return Date::check($text);
			default:
				if (mb_substr($type, 0, 1) != mb_substr($type, -1)) {
					$type = '/' . $type . '/u';
				}
				return preg_match($type, $text);
		}
	}

	/**
	 * Átkonvertálja a szövegben megtalálható URL-eket és e-mail címeket kattintható hivatkozásokká.
	 * @param string $text a szöveg
	 * @return string
	 */
	public static function convertLinks($text) {
		$search = array("/([\w\.\/\&\=\?\-]+)@([\w\.\/\&\=\?\-\+]+)/", "/((ftp(7?):\/\/)|(ftp\.))([\w\.\/\&\=\?\-\~\(\)\+]+)/", "/((http(s?):\/\/)|(www\.))([\w\.\/\&\=\?\-\~\(\)\+\%+#]+)/");
		$replace = array('<a href="mailto:$1@$2">$1@$2</a>', '<a href="ftp$3://$4$5" target="_blank">$4$5</a>', '<a href="http$3://$4$5" target="_blank">$4$5</a>');
		return preg_replace($search, $replace, $text);
	}

	/*
	 * Webbiztos formára (szóközök, ékezetek és speciális karakterek nélküli alakra) hozza a megadott szöveget, majd visszaadja azt.
	 * @param string $text az szöveg
	 * @param string $separator az eltávolítandó karakterek helyére kerülő szöveg
	 * @return string
	 * @since 4.3.0
	 */
	public static function plain($text, $separator = '-') {
		$text = iconv('UTF-8', 'ASCII//TRANSLIT', mb_strtolower(trim($text)));
		$text = preg_replace('/[^a-z0-9 \-\_]/', '', $text);
		$text = preg_replace('/[\ \_]+/', $separator, trim($text));
		return trim($text, '-');
	}

	/**
	 * Elvágja a megadott szöveget egy maximális hossznál
	 * @param string $text a levágandó szöveg
	 * @param int $length a szöveg maximális lehetséges hossza (a lezáró karakterekkel együtt!)
	 * @param bool $wholewordonly megadja, hogy a levágást az utolsó egész szó végén tegye meg, amelyik még a megadott hosszon belül van
	 * @param string $truncation a levágott szöveg végére helyezendő karakterek (ha a hossza nagyobb volt, mint a kívánt hossz)
	 * @return string
	 * @since 4.2.0
	 */
	public static function truncate($text, $length = 30, $wholewordonly = false, $truncation = '...') {
		if (mb_strlen($text) <= $length) return $text;
		if ($wholewordonly) {
			$parts = preg_split('/(\s|\t|:punct:)/', trim($text), -1, PREG_SPLIT_DELIM_CAPTURE);
			$new = '';
			$i = 0;
			while (1) {
				if (isset($parts[$i])) {
					if ($i) {
						if (!isset($parts[$i+1]) || mb_strlen($new . $parts[$i] . $parts[$i + 1]) > $length - mb_strlen($truncation)) {
							break;
						}
						$new .= $parts[$i];
						$i++;
					}
					if (mb_strlen($new.$parts[$i]) > $length - mb_strlen($truncation)) {
						break;
					}
					$new .= $parts[$i];
					$i++;
				} else {
					break;
				}
			}
			return trim($new) . $truncation;
		} else {
			return trim(mb_substr($text, 0, $length - mb_strlen($truncation))) . $truncation;
		}
	}

	/**
	 * Kijelöli a megadott szövegben a kifejezés(eke)t
	 * @param string $text
	 * @param string $keywords
	 * @return string
	 * @since 4.4.8
	 */
	public static function highlight($text, $keywords) {
		if ($keywords != '') {
			$patterns = array();
			$replaces = array();
			$words = explode(' ', $keywords);

			foreach($words as $word) {
				$patterns[] = '/'.$word.'/i';
				$replaces[] = '<span class="highlighted">$0</span>';
			}

			return preg_replace($patterns, $replaces, $text);
		} else {
			return $text;
		}
	}

	public static function formatCurrency($price, $includeCurrency = false, $currency = null) {
		return Currency::format($price, $includeCurrency, $currency);
	}

	public static function formatBytes($bytes, $precision = 0) {
		$units = array('B', 'KiB', 'MiB', 'GiB', 'TiB', 'PiB');
		$power = ($bytes > 0) ? floor(log($bytes, 1024)) : 0;
		return sprintf('%01.' . $precision . 'f %s', $bytes / pow(1024, $power), $units[$power]);
	}

	public static function getCountryName($countryId, $languageId = null) {
		if (is_null($languageId)) {
			$languageId = LANG;
		}
		return Db::sqlField(sprintf("
			SELECT name
			FROM CountryText
			WHERE countryId = '%s'
			AND languageId = '%s'",
			$countryId,
			$languageId
		));
	}
}
