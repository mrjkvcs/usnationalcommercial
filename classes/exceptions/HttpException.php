<?php

/**
 * HTTP hibakódok
 */
class HttpException extends Error {
	public function renderView() {
		$errorCode = substr(get_class($this), 4, 3);
		echo BaseModule::loadView('views.http.' . $errorCode, array(
			'message' => $this->getMessage(),
		));
	}
}
