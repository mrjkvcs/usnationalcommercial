<?php

/**
 * Alap szintű kivételkezelés
 */
class Error extends Exception {
	/**
	 * A hiba megjelenítéséhez szükséges
	 */
	public function renderView() {
		echo BaseModule::loadView('views.error', array(
			'message' => $this->getMessage(),
		));
	}
}
