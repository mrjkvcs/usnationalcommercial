<?php

class Currency {

	/**
	 * Elérhető pénznemek azonosító->pénznem tömbje
	 */
	protected static $_currencies;

	/**
	 * Visszaadja egy tömbben az elérhető pénznemeket, elsőként az alapértelmezettet
	 * A tömb indexe a pénznem karaketeres azonosítója, az értéke a pénznem jele
	 * @return array
	 */
	public static function getCurrencies() {
		if (is_null(self::$_currencies)) {
			if (UniAdmin::app()->cache->isEnabled()) {
				$cachedVersion = UniAdmin::app()->cache->get('UniAdminCurrencies');
				if ($cachedVersion) {
					self::$_currencies = $cachedVersion;
					return $cachedVersion;
				}
			}
			self::$_currencies =  Db::toArray("
				SELECT id, currency
				FROM UniadminCurrency
				ORDER BY isDefault DESC",
				true
			);
			if (UniAdmin::app()->cache->isEnabled()) {
				UniAdmin::app()->cache->set('UniAdminCurrencies', self::$_currencies, 86400);
			}
		}
		return self::$_currencies;
	}

	/**
	 * Visszadja a $currencyId azonosítójú pénznemhez a pénznemet
	 * pl.: eur -> €, huf -> Ft
	 * @param string $currencyId
	 * @return string|bool
	 */
	public static function getCurrency($currencyId) {
		if (is_null(self::$_currencies)) {
			self::getCurrencies();
		}
		return isset(self::$_currencies[$currencyId]) ? self::$_currencies[$currencyId] : false;
	}

	public static function getDefaultCurrencyId() {
		return array_shift(array_keys(self::getCurrencies()));
	}

	public static function getDefaultCurrency() {
		return self::getCurrency(self::getDefaultCurrencyId());
	}

	public static function getCurrencyByLanguage($languageId = null) {
		if (is_null($languageId)) {
			$languageId = defined('IS_BACKEND') ? CONTENT_LANG : LANG;
		}
		return Db::sqlField(sprintf("
			SELECT currencyId
			FROM UniadminLanguages
			WHERE id = '%s'",
			$languageId
		));
	}

	public static function getExchangeRate($currencyId) {
		return Db::sqlField(sprintf("
			SELECT rate
			FROM UniadminCurrency
			WHERE id = '%s'",
			$currencyId
		));
	}

	public static function exchange($value, $from = null, $to = null) {
		$currencyFormat = UniAdmin::app()->currencyFormat;
		if (is_null($to)) {
			$to = $currencyFormat->currencyId;
		}
		if (is_null($from)) {
			$from = Db::sqlField(sprintf("SELECT id FROM UniadminCurrency WHERE isDefault = 1"));
		}
		if ($from == $to) {	// nem kell váltani
			return $value;
		}
		$rateTo = $to == $currencyFormat->currencyId ? $currencyFormat->rate : Db::sqlField(sprintf("SELECT rate FROM UniadminCurrency WHERE id = '%s'", $to));
		$rateFrom = $from == $currencyFormat->currencyId ? $currencyFormat->rate : Db::sqlField(sprintf("SELECT rate FROM UniadminCurrency WHERE id = '%s'", $from));
		return floatval(($value * $rateFrom) / $rateTo);
	}

	public static function format($price, $includeCurrency = false, $currency = null, $decimals = null) {
		$currencyFormat = UniAdmin::app()->currencyFormat;
		if (is_null($currency) || $currency == $currencyFormat->currencyId) {
			$ret = number_format(
				$price,
				is_null($decimals) ? intval($currencyFormat->decimals) : $decimals,
				$currencyFormat->decimalPoint,
				$currencyFormat->thousandsSeparator
			);
			if ($includeCurrency) {
				$ret .= ' ' . $currencyFormat->currency;
			}
		} else {
			$rs = Db::sqlrow(sprintf("
				SELECT currency, decimals, decimalPoint, thousandsSeparator
				FROM UniadminCurrency
				WHERE id = '%s'",
				$currency
			));
			if ($rs) {
				if ($price == round($price)) {	// ha nem szükséges, nem írunk ki tizedesjegyeket
					$decimals = 0;
				}
				$ret = number_format(
					$price,
					is_null($decimals) ? intval($rs['decimals']) : $decimals,
					$rs['decimalPoint'],
					$rs['thousandsSeparator']
				);
				if ($includeCurrency) {
					$ret .= ' ' . $rs['currency'];
				}
			} else {
				return false;
			}
		}
		return $ret;
	}

	/**
	 * A feltehetőleg a nyelv szerint formázott $price pénzértéket floattá alakítja
	 * @param string $price
	 * @param string $currencyId
	 * @return float
	 */
	public static function reverseFormat($price, $currencyId = null) {
		$currencyFormat = UniAdmin::app()->currencyFormat;
		if ($currencyId != $currencyFormat->currencyId) {
			$currencyFormat = Db::sqlrow(sprintf("
				SELECT currency, decimals, decimalPoint, thousandsSeparator
				FROM UniadminCurrency
				WHERE id = '%s'",
				$currencyId
			),'object');
		}
		if (!$currencyFormat) {
			return floatval($price);
		}
		$ret = str_replace(array($currencyFormat->thousandsSeparator,' '),'', $price);
		if ($currencyFormat->decimalPoint != '.') {
			$ret = str_replace($currencyFormat->decimalPoint, '.', $ret);
		}
		return floatval($ret);
	}
}
