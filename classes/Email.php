<?php

/**
 * E-mail küldő osztály
 */
class Email extends BaseModule {
	public $type = '';
	public $to = array();
	public $from = '';
	public $replyTo = '';
	public $cc = array();
	public $bcc = array();
	public $priority = 3;
	public $confirm = false;
	public $attachments = array();
	public $subject = '';
	public $message = '';
	public $disableTemplate = false;
	public $sender;
	public $smtp;

	/**
	 * Konstruktor, e-mail üzenet létrehozása
	 * @param string $type az üzenet típusa html vagy plain
	 */
	public function  __construct($type = 'html') {
		$this->type = $type;
	}

	/**
	 * Címzett hozzáadása
	 * @param string|array $address a címzett e-mail címe, vagy az azokat tartalmazó tömb
	 * @param bool $clear meghatározza, hogy cím hozzáadás előtt törölje-e a korábban megadott címeket
	 * @return Email
	 */
	public function to($address, $clear = false) {
		if (is_array($address)) {
			if (!$clear) {
				$this->to = array_merge($this->to, $address);
			} else {
				$this->to = $address;
			}
		} else {
			if (!$clear) {
				$this->to[] = $address;
			} else {
				$this->to = array($address);
			}
		}
		return $this;
	}

	/**
	 * Másolatot kapó címzettek beállítása
	 * @param string|array $address
	 * @return Email
	 */
	public function cc($address) {
		if (is_array($address)) $this->cc = array_merge($this->cc, $address);
		else $this->cc[] = $address;
		return $this;
	}

	/**
	 * Titkos másolatot kapó címzettek beállítása
	 * @param string|array $address
	 * @return Email
	 */
	public function bcc($address) {
		if (is_array($address)) $this->bcc = array_merge($this->bcc, $address);
		else $this->bcc[] = $address;
		return $this;
	}

	/**
	 * Feladó meghatározása
	 * Amennyiben ez a metódus nem kerül meghívásra, az adminisztrációs rendszer alapértelmezett neve és e-mail címe kerül beállításra küldéskor
	 * @param string $address a feladó email címe, vagy neve és e-mail címe
	 * @return Email
	 */
	public function from($address, $name = '') {
		$this->from = array($address, $name);
		return $this;
	}

	/**
	 * Válaszcím megadása
	 * @param string $address
	 * @return Email
	 */
	public function replyTo($address) {
		$this->replyTo = $address;
		return $this;
	}

	/**
	 * Üzenet tárgyának megadása
	 * @param string $subject
	 * @return Email
	 */
	public function setSubject($subject) {
		$this->subject = $subject;
		return $this;
	}

	/**
	 * Üzenet tartalmának megadása
	 * Amennyiben engedélyezett, az 'outgoing_emails' dokumentumsablonba tölti bele a tartalmat
	 * @param string $message
	 * @return Email
	 */
	public function setMessage($message) {
		$this->message = $message;
		return $this;
	}

	/**
	 * Üzenet prioritásának beállítása
	 * 1 = Magas, 2 = Normál, 3 = Alacsony
	 * @param int $priority
	 * @return Email
	 */
	public function setPriority($priority) {
		$this->priority = $priority;
		return $this;
	}

	/**
	 * Küldő
	 * @param string $sender
	 * @return Email
	 */
	public function setSender($sender) {
		$this->sender = $sender;
		return $this;
	}

	public function setSmtp($host, $username, $password, $port = 25) {
		$this->smtp = array(
			'host' => $host,
			'username' => $username,
			'password' => $password,
			'port' => $port,
		);
		return $this;
	}

	/**
	 * Megerősítés kérése
	 * @param bool $value
	 * @return Email
	 */
	public function confirmRead($value = true) {
		$this->confirm = $value;
		return $this;
	}

	/**
	 * Letiltja, hogy az üzenet tartalmát a kimenő üzenet sablonba tölse be, így csak maga a szöveg fog az üzenetbe kerülni
	 * @param bool $state
	 * @return Email
	 */
	public function disableTemplate($state = true) {
		$this->disableTemplate = $state;
		return $this;
	}

	/**
	 * Fájl csatolása az üzenethez
	 * @param string $file a csatolandó fájl útvonala
	 * @param string $filename a fájl neve. Ha nincs megadva, az előző paraméterben megadott fájl nevét veszi alapul
	 * @param string $filetype a fájl MIME típusa. Ha nincs megadva, megkísérli meghatározni azt beépített metódusok segítségével
	 * @return Email
	 */
	public function attachFile($file, $filename = null, $filetype = null) {
		if (file_exists($file)) {
			if (empty($filename)) $filename = basename($file);
			if (empty($filetype)) $filetype = File::getMimeType($file);
			$this->attachments[] = array(
				'path' => $file,
				'name' => $filename,
				'type' => $filetype);
		}
		return $this;
	}

	/**
	 * Betölt az e-mail szövegébe egy korábban létrehozott sablont
	 * @param string $id a sablon azonosítója
	 * @param array $variables a sablonban lecserélendő változókat tartalmazó asszociatív tömb
	 * @param string $lang a sablon nyelve
	 * @return Email
	 */
	public function fromTemplate($id, $variables, $lang = null) {
		$template = new Template($id, $lang);
		$template->addParameter($variables);
		$this->setMessage($template->evaluate());
		return $this;
	}

	/*
	 * Visszaadja a címzetteket (csak a TO mezőt)
	 */
	public function getTo() {
		return $this->to;
	}

	/**
	 * Üzenet küldése
	 * @return bool
	 */
	public function send() {
		if (count($this->to) && ($this->message || count($this->attachments))) {
			if (isset(UniAdmin::app()->config['emailQueue'])
				&& is_array(UniAdmin::app()->config['emailQueue'])
				&& isset(UniAdmin::app()->config['emailQueue']['class'])
			) {
				UniAdmin::import(UniAdmin::app()->config['emailQueue']['class']);
				$parts = explode('.', UniAdmin::app()->config['emailQueue']['class']);
				$className = end($parts);
				if (class_exists($className)) {
					
					$emailQueue = new $className();
					$emailQueue->add($this);
					return true;
				}
				return false;
			}

			UniAdmin::import('extensions.phpmailer.PHPMailer');

			$email = new PHPMailer();
			$email->Encoding = 'base64';	// random spaces fix

			if (!is_null($this->smtp)) {
				$email->IsSMTP();
				$email->SMTPDebug = DEBUG_MODE ? 1 : 0;
				$email->SMTPAuth = true;
				$email->Host = $this->smtp['host'];
				$email->Port = $this->smtp['port'];
				$email->Username = $this->smtp['username'];
				$email->Password = $this->smtp['password'];
			}

			if ($this->type == 'html') {
				$email->isHTML();
			}

			foreach ($this->to as $to) {
				$email->AddAddress($to);
			}

			foreach ($this->cc as $cc) {
				$email->AddCC($cc);
			}

			foreach ($this->bcc as $bcc) {
				$email->AddBCC($bcc);
			}

			if ($this->replyTo) {
				$email->AddReplyTo($this->replyTo);
			}

			if (!$this->from) {
				$this->from = array(WebsiteInfo::get('emailSender'), WebsiteInfo::get('title'));
			}
			if (is_array($this->from) && count($this->from) == 2) {
				$email->SetFrom($this->from[0], $this->from[1]);
			} else {
				$email->SetFrom($this->from);
			}

			if ($this->priority != 3) {
				$email->Priority = $this->priority;
			}

			if ($this->subject) {
				$email->Subject = $this->subject;
			}

			if ($this->confirm) {
				if (is_array($this->from) && count($this->from) == 2) {
					$email->ConfirmReadingTo = $this->from[0];
				} else {
					$email->ConfirmReadingTo = $this->from;
				}
			}

			$email->AltBody = html_entity_decode(strip_tags($this->message), ENT_COMPAT, 'utf-8');
			if (!$this->disableTemplate) {
				$replaced = $this->loadView('views.email', array('content' => $this->message));
				if ($replaced) $email->MsgHTML($replaced);
				else $email->MsgHTML($this->message);
			} else {
				$email->MsgHTML($this->message);
			}

			foreach ($this->attachments as $attachment) {
				$email->AddAttachment($attachment['path'], $attachment['name'], 'base64', $attachment['type']);
			}

			if ($this->sender) $email->Sender = $this->sender;

			return $email->Send();
		}
	}
}
