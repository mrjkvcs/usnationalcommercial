<?php

/**
 * Fájl műveletek
 * @since 4.3.0
 */
class File {
	/**
	 * Eltávolítja a fájl elérési útvonalából a betörésgyanús elemeket
	 * @param string $name a fájl neve, elérési útvonallal
	 * @return string a megtisztított útvonal
	 */
	public static function sanitizeName($name) {
        //a /-es hívás ne false-t adjon vissza
        if ('/' === $name) {
            return '.';
        }
		$name = str_replace('../', '', trim($name));
		while (substr($name, 0, 1) == '/') {
            $name = substr($name, 1);
		}
		return $name;
	}

	/**
	 * Megpróbálja megállapítani a fájl MIME típusát, és visszadja azt
	 * @param string $file
	 * @return string
	 */
	public static function getMimeType($file) {
		if (function_exists('finfo_open')) {
			$finfo = finfo_open(FILEINFO_MIME_TYPE);
			$filetype = finfo_file($finfo, $file);
		} else if (function_exists('mime_content_type')) {
			$filetype = mime_content_type($file);
		} else {
			$ext = end(explode('.',$file));
			switch ($ext) {
				case "jpg":
				case "jpeg":
				case "jpe": 
						$filetype = "image/jpg";
					break;
				case "png":
				case "gif":
						$filetype = "image/".strtolower($ext);
					break;
				case "pdf":
						$filetype = "application/pdf";
					break;
				case "doc":
						$filetype = "application/msword";
					break;
				case "xls":
						$filetype = "application/vnd.ms-excel";
					break;
				case "css":
						$filetype = "text/css";
					break;
				case "js":
						$filetype = "text/javascript";
					break;
				case "htm":
				case "html":
						$filetype = "text/html";
					break;
				case "csv":
				case "txt":
				default:
						$filetype = "text/plain";
					break;
			}
		}
		return $filetype;
	}

	public static function getIcon($extension) {
		switch ($extension) {
			case 'doc':
			case 'docx':	$ret = 'doc';
				break;
			case 'xls':
			case 'xlsx':	$ret = 'xls';
				break;
			case 'ppt':
			case 'pptx':	$ret = 'ppt';
				break;
			case 'css':
			case 'cue':
			case 'gz':
			case 'js':
			case 'mp3':
			case 'pdf':
			case 'php':
			case 'ps':
			case 'rar':
			case 'rtf':
			case 'ttf':
			case 'txt':
			case 'xml':
			case 'zip':		$ret = $extension;
				break;
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
			case 'tif':
			case 'tiff':	$ret = 'image';
				break;
			case 'htm':
			case 'html':	$ret = 'html';
				break;
			default:		$ret = 'default';
				break;
		}
		return $ret;
	}

	public static function getDescription($extension) {
		switch ($extension) {
			case 'cue':		return t('CUE Sheet');
				break;
			case 'doc':		return t('Word dokumentum');
				break;
			case 'jpg':
			case 'jpeg':
			case 'gif':
			case 'png':
			case 'tif':
			case 'tiff':	return t('Képfájl');
				break;
			case 'mp3':		return t('MP3 audió');
				break;
			case 'pdf':		return t('PDF dokumentum');
				break;
			case 'php':		return t('PHP program');
				break;
			case 'ppt':		return t('Powerpoint bemutató');
				break;
			case 'swf':		return t('Flash animáció');
				break;
			case 'sql':		return t('SQL adatbázis másolat');
				break;
			case 'txt':		return t('Egyszerű szövegfájl');
				break;
			case 'ttf':		return t('TrueType betűtípus');
				break;
			case 'zip':		return t('Tömörített állomány');
				break;
			case 'xls':		return t('Excel táblázat');
				break;
			default:		return $extension . ' ' . t('fájl');
		}
	}

	public static function resizeImage($filename, $maxWidth = null, $maxHeight = null) {
		if (is_null($maxWidth)) {
			$maxWidth = Setting::get('autoResizeImagesWidth', 'Filemanager');
		}
		if (is_null($maxHeight)) {
			$maxHeight = Setting::get('autoResizeImagesHeight', 'Filemanager');
		}
		if (!$maxWidth) {
			$maxWidth = 800;
		}
		if (!$maxHeight) {
			$maxHeight = 600;
		}

		$image = new Image($filename);
		if (!$image->getImage()) {
			return false;
		}

		return $image
			->resample($maxWidth, $maxHeight)
			->save($filename);
	}
}
