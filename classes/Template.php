<?php

/**
 * Sablonok kezelése
 * A rendszer #{azonosito} formában tárolja a sablonok változóit
 */
class Template {
	private $_templateText;
	private $_title;
	private $_parameters = array();

	/**
	 * Sablon példányosítása
	 * @param string $id a sablon azonosítója
	 * @param string $lang a sablon nyelve
	 */
	public function __construct($id = null, $language = null) {
		if (!is_null($id)) {
			$this->_templateText = $this->load($id, $language);
		}
	}

	/**
	 * Létező sablon betöltése
	 * @param string $id
	 * @param string $lang
	 * @return string|bool
	 */
	public function load($id, $language = null) {
		if (is_null($language)) {
			$language = LANG;
		}
		$rs = Db::sqlrow(sprintf("
			SELECT `content`, `title`
			FROM `Document`
			WHERE `id` = '%s'
			AND `languageId` = '%s'",
			$id,
			$language
		));
		if ($rs) {
			$this->_title = $rs['title'];
			return $rs['content'];
		}
		return false;
	}

	public function setText($text) {
		$this->_templateText = $text;
		return $this;
	}

	public function getText() {
		return $this->_templateText;
	}

	/**
	 * Visszaadja a betöltött sablon címét, amennyiben az az adatbázisból lett betöltve
	 */
	public function getTitle() {
		return $this->_title;
	}

	public function getParameters() {
		return $this->_parameters;
	}

	/**
	 * Felvesz egy vagy több paramétert a paraméterlistába
	 * @param string|array $name
	 * @param string|array $value
	 */
	public function addParameter($name, $value = null) {
		if (is_array($name) && is_array($value)) {
			for ($i = 0; $i< count($name); $i++) {
				$this->_parameters[$name[$i]] = $value[$i];
			}
		} else if (is_array($name) && !$value) {
			$this->_parameters = array_merge($this->_parameters, $name);
		} else if (is_array($name) && $value) {
			for ($i = 0; $i < count($name); $i++) {
				$this->_parameters[$name[$i]] = $value;
			}
		} else {
			$this->_parameters[$name] = $value;
		}
		return $this;
	}

	/**
	 * Lecseréli egy sablonban a megadott változókat az aktuális értékekre
	 * @param string $text a sablon tartalma
	 * @param array $variables a sablonban lecserélendő változókat tartalmazó asszociatív tömb
	 * @return string
	 */
	public function replace($text, $variables) {
		$search = array();
		$replace = array();

		// globális változók behelyettesítése
		foreach (UniAdmin::app()->getVariables() as $key => $value) {
			$variables[$key] = (string) $value;
		}

		// csere
		foreach ($variables as $id => $value)
		{
			$search[] = '/#{'.$id.'}/';
			$replace[] = $value;
		}
		return preg_replace($search, $replace, $text);
	}

	/**
	 * Visszaadja az előzetesen betöltött és paraméterezett sablont
	 */
	public function evaluate() {
		return $this->replace($this->_templateText, $this->_parameters);
	}
}
