<?php

class FRadio extends Field {
	protected $_sqlsource;
	protected $_options;
	protected $_separator = '<br />';

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'OPTIONS':	$value = html_entity_decode($value, ENT_COMPAT, 'utf-8');
								if (strpos($value, ';') !== false) {
									
									$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
									$ret = array();
									foreach ($items as $item) {
										$parts = explode('=', $item);
										$ret[$parts[0]] = isset($parts[1]) ? implode('=', array_slice($parts,1)) : $parts[0];
									}
									$this->setOptions($ret);
								} else {
									$this->setOptions($value);
								}
					break;
				case 'SQLSOURCE':	$this->setSQLSource($value);
					break;
				case 'SEPARATOR':	$this->setSeparator($value);
					break;
			}
		}

		return $this;
	}

	public function setSqlSource($sql) {
		$this->_sqlsource = $sql;
		return $this;
	}

	public function setOptions($options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		$this->_options = $options;
		return $this;
	}

	public function setUserValue($userValue) {
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				if ($value == $userValue) {
					$this->_value = $key;
					break;
				}
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] == $userValue) {
						$this->_value = $key;
						break;
					}
				}
			}
		}
		return $this;
	}

	public function setSeparator($value) {
		$this->_separator = $value;
		return $this;
	}

	public function addOptions($options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		foreach ($options as $key => $value) {
			$this->_options[$key] = $value;
		}
		return $this;
	}

	public function getSqlSource() {
		return $this->_sqlsource;
	}

	public function getOptions() {
		return $this->_options;
	}

	public function getSeparator() {
		return $this->_separator;
	}

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}
		if (!$this->_required || $this->getValue()) return true;
		else {
			$this->_error = t('required-to-fill');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, required field not filled', 'form', 2);
			}
			return false;
		}
	}

	public function renderContent() {
		$ret = '';
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$this->_className .= ' radio-' . $this->_id;
		$count = 0;
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				if ($count++) {
					$ret .= $this->_separator;
				}
				$ret .= '<label class="radio-inline" for="' . $this->_id . '_' . $key . '" ><input type="radio" name="' . $this->_id . '" id="' . $this->_id . '_' . $key . '"'
					. ' value="' . $key . '"'
					. ($this->_disabled ? ' disabled="disabled"' : '')
					. ($this->_readonly ? ' readonly="readonly"' : '')
					. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
					. ($key == $this->getValue() || ($key == 'NULL' && is_null($this->getValue())) ? ' checked="checked"' : '') . '>' . t($value) . '</label>';
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] && $rs[1]) {
						if ($count++) {
							$ret .= $this->_separator;
						}
						$ret .= '<input type="radio" name="' . $this->_id . '" id="' . $this->_id . '_' . $rs[0] . '"'
							. ' value="' . $rs[0] . '"'
							. ($this->_disabled ? ' disabled="disabled"' : '')
							. ($this->_readonly ? ' readonly="readonly"' : '')
							. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
							. ($rs[0] == $this->getValue() || ($rs[0] == 'NULL' && is_null($this->getValue())) ? ' checked="checked"' : '') . '> <label for="' . $this->_id . '_' . $rs[0] . '" class="label-' . $rs[0] . '">' . t($rs[1]) . '</label>';
					}
				}
			}
		}
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return '<div>' . $ret . '</div>';
	}
}
