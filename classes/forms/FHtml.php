<?php

class FHtml extends Field {
	protected $_text;
	protected $_sqlsource;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'TEXT':	$this->setText($value);
					break;
				case 'SQLSOURCE':	$this->setSQLSource($value);
					break;
			}
		}

		return $this;
	}

	public function queryValue() {
		return false;
	}

	/**
	 * Beállítja az értéket: ezt hívja a Form::loadValues, továbbítjuk az értéket a setText felé
	 */
	public function setValue($text) {
		$this->setText($text);
		return $this;
	}

	public function setText($text) {
		$this->_text = $text;
		return $this;
	}

	public function setSqlSource($sql) {
		$this->_sqlsource = $sql;
		return $this;
	}

	public function getValue() {
		return $this->getText();
	}

	public function getText() {
		return $this->evaluateFilters($this->_text);
	}

	public function getSqlSource() {
		return $this->_sqlsource;
	}

	public function validate() {
		return true;
	}

	public function renderContent() {
		$ret = '<div id="' . $this->_id . '" class="html-field">';
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		if (!is_null($this->_sqlsource)) {
			$value = Db::sqlField($this->_sqlsource);
			if ($value) {
				$ret .= $this->evaluateFilters($value);
			}
		}
		$ret .= $this->getValue();
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		$ret .= '</div>';
		return $ret;
	}
}
