<?php

/**
 * Űrlapkezelő
 */
class Form extends Widget {
	protected $_id;
	protected $_classNames = array();
	protected $_action;
	protected $_method = 'post';
	protected $_callback = 'back';
	protected $_afterSaveCallback;
	protected $_backAction = array();
	protected $_title;
	protected $_table;
	protected $_submitValue;
	protected $_elements = array();
	protected $_groups = array();
	protected $_extraButtons = array();
	protected $_isMultipart = false;
	protected $_hasSubmitButton = true;
	protected $_hasBackButton = true;
	protected $_hasResetButton = false;
	protected $_hasAutoFocus = true;
	protected $_saveMethod = 'insert';
	protected $_tinyMceExtraPlugins = array();
	protected $_tinyMceRemoveHost = true;
	protected $_hasCompose = false;
	protected $_valuesQueried = false;

	public function __construct($parameters = null, $autoRender = false) {
		UniAdmin::import('classes.forms.*');
		UniAdmin::app()->page->addCss('style.form');
		$this->loadDictionary('i18n.form');

		$this->_action = $_SERVER['REQUEST_URI'];
		$this->_submitValue = t('button-submit');

		if (is_array($parameters)) {
			if (isset($parameters['load'])) {
				$this->loadFromFile($parameters['load']);
			}
			if (isset($parameters['loadValues'])) {
				$this->_saveMethod = 'update';
			}
			if (isset($parameters['className'])) {
				$this->_classNames += is_array($parameters['className']) ? $parameters['className'] : array($parameters['className']);
			}
			if (isset($parameters['backAction'])) {
				$this->setBackAction($parameters['backAction'][0], isset($parameters['backAction'][1]) ? $parameters['backAction'][1] : array());
			}
			if (isset($parameters['callback'])) {
				$this->setCallback($parameters['callback']);
			}
		}

		$eventParameter = new EventParameter();
		$eventParameter->form = $this;
		$this->dispatchEvent('afterInit' . (defined('IS_BACKEND') ? 'Admin' : ''), $eventParameter);

		if ($autoRender) {
			if ($this->isSent()) {
				$this->queryValues()->save();
			} else if ($this->_table && isset($parameters['loadValues'])) {
				$this->loadValues($parameters['loadValues']);
			}
			$this->renderView();
		}
	}

	public function setId($id) {
		$this->_id = $id;
		return $this;
	}

	public function setClassName($classNames) {
		$this->_classNames = array();
		$this->addClassName($classNames);
		return $this;
	}

	public function setAction($action) {
		$this->_action = $action;
		return $this;
	}

	public function setMethod($method) {
		$this->_method = $method;
		return $this;
	}

	public function setBackAction($method, $action = array()) {
		$this->_backAction = array($method, $action);
		return $this;
	}

	public function setTitle($title) {
		$this->_title = $title;
		return $this;
	}

	public function setTable($table) {
		$this->_table = $table;
		return $this;
	}

	public function setSubmitValue($value) {
		$this->_submitValue = $value;
		return $this;
	}

	public function setCallback($callback = null) {
		$this->_callback = $callback;
		return $this;
	}

	public function setAfterSaveCallback($callback = null) {
		$this->_afterSaveCallback = $callback;
		return $this;
	}

	public function setValue($fieldId, $value) {
		$this->getField($fieldId)->setValue($value);
		return $this;
	}

	public function setValues($values) {
		if (is_array($values)) {
			foreach ($values as $fieldId => $value) {
				$field = $this->getField($fieldId);
				if ($field) {
					$field->setValue($value);
				}
			}
		}
		return $this;
	}

	public function setError($fieldId, $error) {
		$this->getField($fieldId)->setError($error);
		return $this;
	}

	public function setGroups($groupList) {
		$this->_groups = $groupList;
		return $this;
	}

	public function setExtraButtons($buttonList) {
		if (is_array($buttonList)) {
			$this->_extraButtons = $buttonList;
			return $this;
		} else {
			return false;
		}
	}

	public function setTinyMceExtraPlugins($pluginlist) {
		$this->_tinyMceExtraPlugins = $pluginlist;
		return $this;
	}

	public function setTinyMceRemoveHost($state) {
		$this->_tinyMceRemoveHost = (bool) $state;
		return $this;
	}

	public function setMultipart($state = true) {
		$this->_isMultipart = (bool) $state;
		return $this;
	}

	public function addClassName($classNames) {
		if (!is_array($classNames)) {
			$classNames = array($classNames);
		}
		$this->_classNames += $classNames;
		return $this;
	}

	public function addField(Field $field) {
		if (!$field->getId()) {
			if (DEBUG_MODE) {
				throw new Error('Form error: adding field without ID, parameters: ' . print_r($field));
			}
		}
		if ($field->getType() == 'File' || is_subclass_of(get_class($field), 'FFile')) {
			$this->_isMultipart = true;
		} else if ($field->getType() == 'Compose') {
			$this->_hasCompose = true;
		}
		$this->_elements[$field->getId()] = $field;
		return $this;
	}

	function addFieldBefore($selectedFieldId, $newField) {
		if ($newField->getType() == 'File' || is_subclass_of(get_class($newField), 'FFile')) {
			$this->_isMultipart = true;
		} else if ($newField->getType() == 'Compose') {
			$this->_hasCompose = true;
		}

		$item = false;
		$num = 0;
		$Field = null;
		foreach ($this->_elements as $fieldId => $field) {
			if ($fieldId == $selectedFieldId) {
				$item = $num;
				$Field = $field;
				break;
			}
			$num++;
		}

		if ($item === false) {
			$item = count($this->_elements);
		} else {
			$newField->setGroup($Field->getGroup());
		}

		$newFieldArr = array($newField->getId() => $newField);

		$this->_elements = array_merge(
			array_slice($this->_elements, 0, $item),
			$newFieldArr,
			array_slice($this->_elements, $item)
		);

		return $this;
	}

	function addFieldAfter($selectedFieldId, $newField) {
		if ($newField->getType() == 'File' || is_subclass_of(get_class($newField), 'FFile')) {
			$this->_isMultipart = true;
		} else if ($newField->getType() == 'Compose') {
			$this->_hasCompose = true;
		}

		$item = false;
		$num = 0;
        $Field = null;
		foreach ($this->_elements as $fieldId => $field) {
			if ($fieldId == $selectedFieldId) {
				$item = $num;
                $Field = $field;
				break;
			}
			$num++;
		}

		if ($item === false) {
			$item = count($this->_elements);
		} else {
			$item++;
			$newField->setGroup($Field->getGroup());
		}

		$newFieldArr = array($newField->getId() => $newField);

		$this->_elements = array_merge(
			array_slice($this->_elements, 0, $item),
			$newFieldArr,
			array_slice($this->_elements, $item)
		);

		return $this;
	}

	public function addFields() {
		$a = func_get_args();
		foreach ($a as $v) {
			$this->addField($v);
		}
		return $this;
    }

	public function addDbField($dbField, $value = null, $isKey = false) {
		$field = new FHidden($dbField);
		$field->setField($dbField)
			->setValue($value)
			->setKey($isKey)
			->required(false);
		$this->addField($field);
		return $this;
	}

	public function addExtraButton($value) {
		$this->_extraButtons[] = $value;
		return $this;
	}

	public function addGroup($groupId, $groupTitle) {
		if (!isset($this->_groups[$groupId])) {
			$this->_groups[$groupId] = $groupTitle;
		}
		return $this;
	}

	public function addTinyMceExtraPlugin($pluginId) {
		$this->_tinyMceExtraPlugins[] = $pluginId;
		return $this;
	}

	public function removeClassName($classNames = null) {
		if (empty($classNames)) {
			$classNames = array();
			return $this;
		}
		if (!is_array($classNames)) {
			$classNames = array($classNames);
		}
		$this->_classNames = array_diff($this->_classNames, $classNames);
		return $this;
	}

	public function removeField() {
		$fields = func_get_args();
		foreach ($fields as $fieldId) {
			if (isset($this->_elements[$fieldId])) {
				unset($this->_elements[$fieldId]);
			}
		}

		return $this;
	}

	public function removeGroup($groupId, $removeFields = false) {
		if (isset($this->_groups[$groupId])) {
			if ($removeFields) {
				foreach ($this->_elements as $fieldId => $field) {
					if ($field->getGroup() == $groupId) {
						unset($this->_elements[$fieldId]);
					}
				}
			}
			unset($this->_groups[$groupId]);
		}
		return $this;
	}

	public function removeExtraButton($buttonText) {
		$key = array_search($buttonText, $this->_extraButtons);
		if ($key !== false) {
			unset($this->_extraButtons[$key]);
		}
		return $this;
	}

	public function disableSubmit($state = true) {
		$this->_hasSubmitButton = !$state;
		return $this;
	}

	public function disableBack($state = true) {
		$this->_hasBackButton = !$state;
		return $this;
	}

	public function disableReset($state = true) {
		$this->_hasResetButton = !$state;
		return $this;
	}

	public function disableAutoFocus($state = true) {
		$this->_hasAutoFocus = !$state;
		return $this;
	}

	public function loadFromFile($fileName) {
		$parts = preg_split('/\./', $fileName, -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) == 2) {
			$parts[0] = ucfirst(strtolower($parts[0]));
			if (!defined('IS_BACKEND') || $parts[0] == 'Admin') {
				$fileName = 'modules.' . $parts[0] . '.forms.' . $parts[1];
			} else {
				$fileName = 'modules.Admin.modules.' . $parts[0] . '.forms.' . $parts[1];
			}
		}

		$path = preg_replace('/\./', '/', $fileName);

		if (file_exists($path . '.xml')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading form ' . $fileName, 'form');
			}
			$content = file_get_contents($path . '.xml');
			if ($content) {
				$xmlparser = xml_parser_create('utf-8');
				xml_set_object($xmlparser, $this);
				xml_set_element_handler($xmlparser, 'startElement', '');
				xml_parse($xmlparser, $content);

				return $this;
			}
		} else {
			throw new Error('Form error: cannot load form ' . $fileName);
		}
	}

	function startElement($parser, $name, $attrs) {
		if ($name == 'FIELD' && isset($attrs['TYPE'])) {
			$fieldType = 'F' . ucfirst($attrs['TYPE']);
			if (strpos($fieldType, '[') !== false) {	// méret is meg van adva
				$pos = strpos($fieldType, '[');
				$size = substr($fieldType, $pos + 1, -1);
				if (!isset($attrs['SIZE'])) {
					$attrs['SIZE'] = $size;
				}
				$fieldType = substr($fieldType, 0, $pos);
			}
			if (class_exists($fieldType)) {
				$field = new $fieldType($attrs);
			} else {
				throw new Error('Form error: cannot load field type ' . $fieldType);
			}
		}
		foreach ($attrs as $id => $value) {
			switch ($id) {
				case 'ID':
					if ($name == 'FORM') {
						$this->setId($value);
					}
				break;
				case 'CLASSNAME':
					$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
					foreach ($items as $item) {
						$this->addClassName($item);
					}
				break;
				case 'METHOD':
					$this->setMethod($value);
				break;
				case 'ACTION':
					$this->setAction($value);
				break;
				case 'TABLE':
					$this->setTable($value);
				break;
				case 'DISABLESUBMIT':
					$this->disableSubmit();
				break;
				case 'DISABLERESET':
					$this->disableReset();
				break;
				case 'DISABLEBACK':
					$this->disableBack();
				break;
				case 'DISABLEAUTOFOCUS':
					$this->disableAutoFocus();
				break;
				case 'SUBMITBUTTON':
					$this->setSubmitValue($value);
				break;
				case 'MULTIPART':
					$this->setMultipart(strtolower($value) == 'true' || strtolower($value) == 'multipart');
				break;
				case 'EXTRABUTTON':
					$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
					foreach ($items as $item) {
						$this->addExtraButton($item);
					}
				break;
				case 'TITLE':
					$this->setTitle($value);
				break;
				case 'GROUPS':
					$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
					$groupList = array();
					foreach ($items as $item) {
						list ($i, $v) = explode('=', $item);
						if (!$v) {
							$groupList[] = $i;
						} else {
							$groupList[$i] = $v;
						}
					}
					$this->setGroups($groupList);
				break;
				case 'FIXEDSIZE':
					preg_match('/^([0-9]+),([0-9]+)$/', $value, $items);
					if (is_array($items) && count($items) == 3) {
						$this->setFixedComposeSize(true, $items[1], $items[2]);
					} else {
						$this->setFixedComposeSize(false);
					}
				break;
			}
		}
		if (isset($field)) {
			$this->addField($field);
			unset($field);
		}
	}

	public function isSent() {
		if ($this->_method == 'post') return isset($_POST['sent']);
		else return isset($_GET['sent']);
	}

	public function getId() {
		return $this->_id;
	}

	public function getClassName() {
		return array_unique($this->_classNames);
	}

	public function getMethod() {
		return $this->_method;
	}

	public function getBackAction() {
		return $this->_backAction;
	}

	public function getAction() {
		return $this->_action;
	}

	public function getTitle() {
		return t($this->_title);
	}

	public function getTable() {
		return $this->_table;
	}

	public function hasField($id) {
		return isset($this->_elements[$id]);
	}
	
	/**
	 * @param string $id
	 * @return Field
	 * @throws Error 
	 */
	public function getField($id) {
		if (isset($this->_elements[$id])) {
			return $this->_elements[$id];
		}
		throw new Error('Form error: field ' . $id . ' not found');
	}

	public function getSubmitValue() {
		return $this->_submitValue;
	}

	public function getCallback() {
		return $this->_callback;
	}

	public function getElements() {
		return $this->_elements;
	}

	public function getValue($fieldId) {
		return $this->getField($fieldId)->getValue();
	}

	public function getValues() {
		return $this->invoke('getValue');
	}

	public function getError($fieldId) {
		return $this->getField($fieldId)->getError();
	}

	public function getErrors() {
		$errors = array();
		foreach ($this->_elements as $fieldId => $field) {
			$error = $field->getError();
			if ($error) {
				$errors[$fieldId] = $error;
			}
		}
		return $errors;
	}

	public function getGroups() {
		return $this->_groups;
	}

	public function getExtraButtons() {
		return $this->_extraButtons;
	}

	public function getTinyMceExtraPlugins() {
		return $this->_tinyMceExtraPlugins;
	}

	public function getTinyMceRemoveHost() {
		return $this->_tinyMceRemoveHost;
	}

	public function isMultipart() {
		return $this->_isMultipart;
	}

	public function hasSubmitButton() {
		return $this->_hasSubmitButton;
	}

	public function hasBackButton() {
		return $this->_hasBackButton;
	}

	public function hasResetButton() {
		return $this->_hasResetButton;
	}

	public function hasAutoFocus() {
		return $this->_hasAutoFocus;
	}

	public function hasErrors() {
		foreach ($this->getElements() as $field) {
			if ($field->getError()) {
				return true;
			}
		}
		return false;
	}

	public function queryValues($forceQuery = false) {
		// csak egyszer töltjük be az értékeket, ha kifejezetten nem kérjük az újratöltést
		if ($this->_valuesQueried && !$forceQuery) {
			return $this;
		}
		foreach ($this->_elements as $fieldId => $field) {
			if (method_exists($field, 'queryValue')) {	// queryValue függvény hívása, amennyiben ilyen van definiálva
				$value = $field->queryValue($this->_method);
				if ($value !== false) {
					$field->setValue($value);
				}
			} else {
				if (strpos($fieldId, '[') !== false) {
					$names = preg_split('/(\[|\])/', $fieldId, -1, PREG_SPLIT_NO_EMPTY);
					if ($this->_method == 'post') {
						$varName = '$_POST[\'' . $names[0] . '\']';
					} else {
						$varName = '$_GET[\'' . $names[0] . '\']';
					}
					for ($i = 1; $i < count($names); $i++) {
						$varName .= "['" . $names[$i] . "']";
					}
					$value = @eval('return ' . $varName . ';');
				} else {
					if ($this->_method == 'post') {
						$value = isset($_POST[$fieldId]) ? $_POST[$fieldId] : null;
					} else {
						$value = isset($_GET[$fieldId]) ? $_GET[$fieldId] : null;
					}
				}
				if (get_magic_quotes_gpc() && is_string($value)) {
					$value = stripslashes($value);
				}
				$field->setValue($value);
			}
		}
		$this->_valuesQueried = true;
		return $this;
	}

	public function validateFields() {
		$isValid = true;
		foreach ($this->_elements as $fieldId => $field) {
			if (!$field->validate()) {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('field ' . $fieldId . ' is not valid', 'form', 2);
				}
				$isValid = false;
			}
			if ($field->getMatchTo() && $this->getField($field->getMatchTo()) && $field->getValue() != $this->getField($field->getMatchTo())->getValue()) {
				$field->setError(t('field-contents-are-not-the-same'));
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('field ' . $field->getId() . ' does not match ' . $this->getField($field->getMatchTo())->getId(), 'form', 2);
				}
				$isValid = false;
			}
		}

		return $isValid;
	}

	public function loadValues($condition) {
		if (!$this->_table || !$condition || !count($this->_elements)) {
			throw new Error('Form error: cannot load form values, missing required conditions');
		}

		$fields = array();
		foreach ($this->_elements as $field) {
			if ($field->getField()) {
				$fields[$field->getField()] = $field;
			}
		}

		if (!count($fields)) {
			throw new Error('Form error: cannot load form values, missing database fields');
		}

		$query = sprintf("SELECT `%s` FROM `%s` WHERE %s LIMIT 1",
			implode('`, `', array_keys($fields)), $this->_table, $condition);
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('loading form values: ' . $query, 'form');
		}
		$rs = Db::sqlrow($query);

		if (!$rs) {
			throw new Error('Form error: cannot find record to load data from');
		}

		foreach ($fields as $dbField => $field) {
			$field->setValue($rs[$dbField]);
		}

		return $this;
	}

	public function save($type = null) {
		if (is_null($type)) {
			$type = $this->_saveMethod;
		}
		$type = strtolower($type);
		if (!$this->validateFields()) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot save form, fields do not validate', 'form', 2);
			}
			return false;
		}
		if ($type != 'insert' && $type != 'update') {
			throw new Error('Form error: cannot save form, no valid save type set');
		}

		if ($this->_table) {
			$values = array();
			$keys = array();
			foreach ($this->_elements as $field) {
				if ($field->getField()
					&& $field->getType() != 'Html'
					&& !is_subclass_of(get_class($field), 'FHtml')
					// && $field->getValue() !== null
				) {
					if (is_null($field->getValue()) || strtolower($field->getValue()) == 'null') {	// NULL mentése az adatbázisba
						$values[$field->getField()] = 'NULL';
					} else if (strtolower($field->getValue() == "'null'")) {	// null string mentése
						$values[$field->getField()] = $field->getValue();
					} else {	// minden egyéb esetben escapelés
						$values[$field->getField()] = "'" . Db::escapeString($field->getValue()) . "'";
					}
					if ($type == 'update' && $field->isKey()) {
						$keys[$field->getField()] = Db::escapeString($field->getValue());
					}
				}
			}

			if (!count($values)) {
				throw new Error('From error: cannot save form, no fields to save');
			}

			if ($type == 'update' && !count($keys)) {
				throw new Error('Form error: cannot save form, keys not present');
			}

			$keySql = '';
			foreach ($keys as $key => $value) {
				if ($keySql) {
					$keySql .= ' AND ';
				}
				$keySql .= "`" . $key . "` = '" . $value . "'";
			}

			foreach ($this->_elements as $field) {
				if ($field->isUnique() && $field->getField()) {	// unique mezők ellenőrzése
					$num = Db::sqlField(sprintf("
						SELECT COUNT(*)
						FROM `%s`
						WHERE `%s` = '%s'%s",
						$this->_table,
						$field->getField(),
						$field->getValue(),
						($type == 'insert' ? '' : ' AND ' . str_replace(' = ', ' <> ', $keySql))));
					if ($num) {
						$field->setError(t('already-exists-in-the-database'));
					}
				}
			}

			if (!$this->validateFields()) {	// újabb, végső ellenőrzés
				return false;
			}

			if ($type == 'insert') {
				$sql = "INSERT INTO `" . $this->_table . "` (`" . implode('`, `', array_keys($values))
					. "`) VALUES (" . implode(', ', $values) . ")";
			} else {
				$sql = '';
				foreach ($values as $key => $value) {
					if ($sql) {
						$sql .= ', ';
					}
					$sql .= "`" . $key . "` = " . $value;
				}
				$sql = "UPDATE `" . $this->_table . "` SET " . $sql . " WHERE " . $keySql;

			}

			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('saving form data ' . $sql, 'form');
			}
			Db::sql($sql);

			$insertId = ($type == 'insert' ? Db::insertid() : implode(';', $keys));

		} else {
			$insertId = '';
		}

		foreach ($this->getElements() as $field) {
			if (method_exists($field, 'evaluate')) {	// evaluate függvény hívása, amennyiben ilyen van definiálva
				$field->evaluate($insertId, $type);
			}
		}

		if (is_callable($this->_afterSaveCallback)) {
			call_user_func($this->_afterSaveCallback, $insertId, $this, $type);
		}

		if ($this->_callback) {
			if (defined('IS_BACKEND') && $this->_callback == 'back') {
				if (!count($this->_backAction)) {
					$route = UniAdmin::app()->route->getRoute();
					$module = $route[0];
					if (isset($route[1])) {
						$action = $route[1];
					}
				} else {
					$module = $this->_backAction[0];
					if (isset($this->_backAction[1])) {
						$action = $this->_backAction[1];
					}
				}

				header('Location: ' . Url::link($module, isset($action) ? $action : array(), LANG, array('msg' => ($type == 'insert' ? 'ok' : 'saved'))));
				exit;
			} else {
				if (is_callable($this->_callback)) {
					call_user_func($this->_callback, $insertId, $this);
				}
			}
		}

		return $insertId;
	}

	/*
	 * Meghív egy megadott függvényt az átadott mezőlistára, vagy az űrlap összes mezőjére
	 * @param callback $callback a meghívandó függvény
	 * @param array $fieldIdList a mezők azonosítói
	 * @return array A függvények visszatérési értékeit adja vissza egy fieldId-vel indexelt tömbben
	 */
    public function map($callback, array $fieldIdList = array()) {
        $values = array();
		if (is_callable($callback)) {
			foreach ($this->_elements as $fieldId => $field) {
				if (empty($fieldIdList) || in_array($fieldId, $fieldIdList)) {
					$values[$fieldId] = call_user_func($callback, $field);
				}
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('error calling callback for Form::map', 'form', 2);
			}
		}
		return $values;
    }

	/*
	 * Egy mező metódust hív meg az átadott mezőlistára vagy az űrlap összes mezőjére
	 * @param string $methodName a Field metódus neve
	 * @param mixed $args a lehetséges paraméterek
	 * @param array $fieldIdList a mezők azonosítói
	 * @return array A metódushívások visszatérési értékeit adja vissza egy fieldId-vel indexelt tömbben
	 */
    public function invoke($methodName, $args = array(), array $fieldIdList = array()) {
        if (!is_array($args)) {
    	    $args = (array) $args;
	}
        $values = array();
		foreach ($this->_elements as $fieldId => $field) {
            if (is_callable(array($field, $methodName))) {
				if (empty($fieldIdList) || in_array($fieldId,$fieldIdList)) {
					$values[$fieldId] = call_user_func_array(array($field, $methodName), $args);
				}
            } else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('error calling method ' . $methodName . ' on field ' . get_class($field), 'form', 2);
				}
            }
		}
		return $values;
    }

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		if ($this->_hasCompose) {
			UniAdmin::app()->page->addScript('extensions.tinymce.jquery-tinymce');
		}
		parent::renderView('views.form');
	}
}
