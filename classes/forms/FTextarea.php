<?php

class FTextarea extends Field {
	protected $_cols = 50;
	protected $_rows = 7;
    protected $_maxlength;
	protected $_placeholderText;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'SIZE':	list ($cols, $rows) = explode(',', $value);
								$this->setSize($cols, $rows);
					break;
				case 'PLACEHOLDER':	$this->setPlaceholderText($value);
					break;
                case 'MAXLENGTH' : $this->setMaxLength($value);
                    break;
			}
		}

		return $this;
	}

	public function setSize($cols, $rows) {
		if ($cols) $this->_cols = intval($cols);
		if ($rows) $this->_rows = intval($rows);
		return $this;
	}

	public function setPlaceholderText($value) {
		$this->_placeholderText = $value;
		return $this;
	}

    public function setMaxLength($value) {
        $this->_maxlength = $value;
        return $this;
    }

	public function getSize() {
		return array($this->_rows, $this->_cols);
	}

	public function getPlaceholderText() {
		return $this->_placeholderText;
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<textarea name="' . $this->_id . '" id="' . $this->_id . '"'
			. ' rows="' . $this->_rows . '" cols="' . $this->_cols . '" maxlength="'. $this->_maxlength .'"'
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_placeholderText ? ' placeholder="' . t($this->_placeholderText) . '"' : '')
			. '>' . $this->getValue() . '</textarea>'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
