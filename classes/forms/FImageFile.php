<?php

class FImageFile extends FFile {
	private $_maxWidth;
	private $_maxHeight;
	private $_image;

	public function __construct($parameters = array()) {
		$this->_maxWidth = Setting::get('autoResizeImagesWidth', 'Filemanager');
		$this->_maxHeight = Setting::get('autoResizeImagesHeight', 'Filemanager');
		if (!$this->_maxWidth) {
			$this->_maxWidth = 800;
		}
		if (!$this->_maxHeight) {
			$this->_maxHeight = 600;
		}
		parent::__construct($parameters);
	}

	public function validate() {
		if (parent::validate() && (
			!$this->_required || 
			in_array($this->getMimeType(), array(
				'image/gif', 
				'image/jpeg', 
				'image/pjpeg', 
				'image/png')))) 
		{
			return true;
		} else {
			if (!$this->_error) {
				$this->_error = t('image-format-error') . ': ' . $this->getMimeType();
			}
			return false;
		}
	}

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'MAXWIDTH':		$this->setMaxWidth(intval($value));
					break;
				case 'MAXHEIGHT':		$this->setMaxHeight(intval($value));
					break;
			}
		}

		return $this;
	}

	public function setMaxWidth($maxWidth) {
		$this->_maxWidth = $maxWidth;
		return $this;
	}

	public function setMaxHeight($maxHeight) {
		$this->_maxHeight = $maxHeight;
		return $this;
	}

	public function getMaxWidth() {
		return $this->_maxWidth;
	}

	public function getMaxHeigth() {
		return $this->_maxHeight;
	}

	public function getImage() {
		if (is_null($this->_image)) {
			$this->_image = new Image($this->getTempName());
		}
		return $this->_image->getSize() ? $this->_image : false;
	}

	public function evaluate($value = null) {
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('opening uploaded image ' . $this->getTempName(), 'form');
		}
		$image = $this->getImage();
		if ($image) {
			$image->resample($this->getMaxWidth(), $this->getMaxHeigth());
			$savePath = $this->transformDestinationPath($value);
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('saving uploaded image ' . $this->getTempName() . ' to ' . $savePath, 'form');
			}
			$ret = $image->save($savePath);
			return $ret ? $savePath : false;
		} else {
			return parent::evaluate($value);
		}
	}
}
