<?php

class FFileBrowser extends FInputText {
	private $_baseDir;
	private $_currentDir;
	private $_extensions = '*';

	public function __construct($parameters = array()) {
		UniAdmin::app()->page->addScript('modules.Admin.modules.Filebrowser.scripts.filebrowser');
		parent::__construct($parameters);
	}

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'BASEDIR':			$this->setBaseDir($value);
					break;
				case 'CURRENTDIR':		$this->setCurrentDir($value);
					break;
				case 'EXTENSIONS':		$this->setExtensions($value);
					break;
			}
		}

		return $this;
	}

	public function setBaseDir($dir) {
		$this->_baseDir = $dir;
		return $this;
	}

	public function setCurrentDir($dir) {
		$this->_currentDir = $dir;
		return $this;
	}

	public function setExtensions($extensions) {
		$this->_extensions = $extensions;
		return $this;
	}

	public function getBaseDir() {
		return $this->_baseDir;
	}

	public function getCurrentDir() {
		return $this->_currentDir;
	}

	public function getExtensions() {
		return $this->_extensions;
	}

	public function renderContent() {
		$ret = '';
		$this->_className .= ' filebrowser';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<input type="text" name="' . $this->_id . '" id="' . $this->_id . '"'
			. ' size="' . $this->_size . '"'
			. ' value="' . $this->getValue() . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ' data-basedir="' . $this->_baseDir . '" data-extensions="' . $this->_extensions . '" />'
			. '&nbsp;<input type="button" value="' . t('Tallóz') . '" class="filebrowser-button" />'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
