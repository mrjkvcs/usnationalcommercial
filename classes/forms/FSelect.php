<?php

class FSelect extends Field {
	protected $_size = 1;
	protected $_sqlsource;
	protected $_options;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'OPTIONS':	if (strpos($value, ';') !== false) {
									$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
									$ret = array();
									foreach ($items as $item) {
										if (strpos($item, '=') !== false) {
											list ($i, $v) = explode('=', $item);
											$ret[$i] = $v ? $v : $i;
										} else {
											$ret[$item] = $item;
										}
									}
									$this->setOptions($ret);
								} else {
									$this->setOptions(html_entity_decode($value));
								}
					break;
				case 'SQLSOURCE':	$this->setSQLSource($value);
					break;
				case 'SIZE':	$this->setSize($value);
					break;
			}
		}

		return $this;
	}

	public function setSize($size) {
		$this->_size = $size;
		return $this;
	}

	public function setSqlSource($sql) {
		$this->_sqlsource = $sql;
		return $this;
	}

	public function setOptions($options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		$this->_options = $options;
		return $this;
	}

	public function setUserValue($userValue) {
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				if ($value == $userValue) {
					$this->_value = $key;
					break;
				}
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] == $userValue) {
						$this->_value = $key;
						break;
					}
				}
			}
		}
		return $this;
	}

	public function addOptions($options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		foreach ($options as $key => $value) {
			$this->_options[$key] = $value;
		}
		return $this;
	}

	public function getSize() {
		return $this->_size;
	}

	public function getSqlSource() {
		return $this->_sqlsource;
	}

	public function getOptions() {
		return $this->_options;
	}

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}
		if (!$this->_required || $this->getValue()) {
			return true;
		} else {
			$this->_error = t('required-to-select');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, required field not filled', 'form', 2);
			}
			return false;
		}
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<select name="' . $this->_id . '" id="' . $this->_id . '" size="' . $this->_size . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ($this->_tabindex ? ' tabindex="' . $this->_tabindex . '"' : '')
			. '>';
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				$ret .= '<option value="' . $key . '"'.($key == $this->getValue() || ($key == 'NULL' && is_null($this->getValue())) ? ' selected="selected"' : '').'>' . t($value) . '</option>';
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] && $rs[1]) {
						$ret .= '<option value="' . $rs[0] . '"'.($rs[0] == $this->getValue() || ($rs[0] == 'NULL' && is_null($this->getValue())) ? ' selected="selected"' : '').'>' . t($rs[1]) . '</option>';
					}
				}
			}
		}
		$ret .= '</select>'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
