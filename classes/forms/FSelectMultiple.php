<?php

class FSelectMultiple extends FSelect {
	protected $_value = array();
	protected $_size = 3;

	public function renderContent() {
		if (!is_array($this->_value)) {
			$this->_value = array($this->_value);
		}
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		$ret = '';
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<select name="' . $this->_id . '[]" id="' . $this->_id . '" multiple="multiple"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ' size="' . $this->_size . '">';
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				$ret .= '<option value="' . $key . '"' 
					. (in_array($key, $this->_value) || ($key == 'NULL' && in_array(null, $this->_value)) ? ' selected="selected"' : '').'>' 
					. t($value) . '</option>';
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] && $rs[1]) {
						$ret .= '<option value="' . $rs[0] . '"' 
							. (in_array($rs[0], $this->_value) || ($rs[0] == 'NULL' && in_array(null, $this->_value)) ? ' selected="selected"' : '') . '>' 
							. t($rs[1]) . '</option>';
					}
				}
			}
		}
		$ret .= '</select>'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . $this->_tooltip . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
