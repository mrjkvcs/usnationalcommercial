<?php

class FCheckboxList extends Field {
	protected $_value = array();
	protected $_sqlsource;
	protected $_options;
	protected $_uncheckedValue = 0;
	protected $_checkedValue = 1;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'OPTIONS':	if (strpos($value, ';') !== false) {
									$items = preg_split('/\;/', $value, -1, PREG_SPLIT_NO_EMPTY);
									$ret = array();
									foreach ($items as $item) {
										if (strpos($item, '=') !== false) {
											list ($i, $v) = explode('=', $item);
											$ret[$i] = $v ? $v : $i;
										} else {
											$ret[$item] = $item;
										}
									}
									$this->setOptions($ret);
								} else {
									$this->setOptions(html_entity_decode($value));
								}
					break;
				case 'SQLSOURCE':	$this->setSQLSource($value);
					break;
			}
		}

		return $this;
	}

	public function setValue($values) {
		if (!is_array($values)) {
			$values = array($values);
		}
		foreach ($values as $key => $value) {
			if ($value == $this->_checkedValue) {
				$this->_value[] = $key;
			}
		}
		return $this;
	}

	public function setSqlSource($sql) {
		$this->_sqlsource = $sql;
		return $this;
	}

	public function setOptions($options) {
		if (!is_array($options)) {
			$options = array($options);
		}
		$this->_options = $options;
		return $this;
	}

	public function getSqlSource() {
		return $this->_sqlsource;
	}

	public function getOptions() {
		return $this->_options;
	}

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}
		if (!$this->_required || count($this->getValue())) {
			return true;
		} else {
			$this->_error = t('required-to-select');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, required field not filled', 'form', 2);
			}
			return false;
		}
	}

	public function renderContent() {
		if (!is_array($this->_value)) {
			$this->_value = array($this->_value);
		}
		$ret = '';
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		if (isset($this->_options) && is_array($this->_options)) {
			foreach ($this->_options as $key => $value) {
				$ret .= '<p><input type="checkbox" name="' . $this->_id . '[' . $key . ']" id="' . $this->_id . '-' . $key . '" value="' . $this->_checkedValue . '"'
					. ($this->_disabled ? ' disabled="disabled"' : '')
					. ($this->_readonly ? ' readonly="readonly"' : '')
					. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
					. (in_array($key, $this->_value) ? ' checked="checked"' : '') . ' />'
					. ' <label for="' . $this->_id . '-' . $key . '">' . t($value) . '</label>'
					. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '')
					. '</p>';
			}
		}
		if (!is_null($this->_sqlsource)) {
			$result = Db::sql($this->_sqlsource);
			if (Db::numrows($result)) {
				while ($rs = Db::loop($result, 'array')) {
					if ($rs[0] && $rs[1]) {
						$ret .= '<p><input type="checkbox" name="' . $this->_id . '[' . $rs[0] . ']" id="' . $this->_id . '-' . $rs[0] . '" value="' . $this->_checkedValue . '"'
							. ($this->_disabled ? ' disabled="disabled"' : '')
							. ($this->_readonly ? ' readonly="readonly"' : '')
							. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
							. (in_array($rs[0], $this->_value) ? ' checked="checked"' : '') . ' />'
							. ' <label for="' . $this->_id . '-' . $rs[0] . '">' . t($rs[1]) . '</label>'
							. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '')
							. '</p>';
					}
				}
			}
		}
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
