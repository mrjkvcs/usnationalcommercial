<?php

class FHidden extends Field {
	public function renderContent() {
		return '<input type="hidden" name="' . $this->_id . '" id="' . $this->_id . '" value="' . $this->getValue() . '" />';
	}
}
