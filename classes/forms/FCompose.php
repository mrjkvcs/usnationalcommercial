<?php

class FCompose extends FTextarea {
	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<textarea name="' . $this->_id . '" id="' . $this->_id . '" rows="' . $this->_rows . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ' cols="' . $this->_cols . '" class="compose' . ($this->_className ? ' ' . trim($this->_className) : '') . '">'
			. $this->getValue() . '</textarea>';
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
