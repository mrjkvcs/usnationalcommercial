<?php

class FFile extends FInputText {
	private $_destination;
	private $_extensions = '*';

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}

		if ($this->_required && !is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			$this->_error = t('required-to-upload');
			return false;
		}

		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			$extension = strtolower(end(explode('.', $this->getOriginalName())));
			if ($this->_extensions != '*' && !in_array($extension, explode(',', $this->_extensions))) {
				$this->_error = t('format-not-allowed');
				return false;
			}
		}
		return true;
	}

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'DESTINATION':		$this->setDestination($value);
					break;
				case 'EXTENSIONS':		$this->setExtensions($value);
					break;
			}
		}

		return $this;
	}

	public function queryValue() {
		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			return 1;
		}
	}

	public function setDestination($targetPath) {
		$this->_destination = $targetPath;
		return $this;
	}

	public function setExtensions($extensions) {
		$this->_extensions = $extensions;
		return $this;
	}

	public function getDestination() {
		return $this->_destination;
	}

	public function getExtensions() {
		return $this->_extensions;
	}

	public function getOriginalName() {
		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			return $_FILES[$this->_id]['name'];
		} else {
			return false;
		}
	}

	public function getTempName() {
		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			return $_FILES[$this->_id]['tmp_name'];
		} else {
			return false;
		}
	}

	public function getFileSize() {
		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			return $_FILES[$this->_id]['size'];
		} else {
			return false;
		}
	}

	public function getMimeType() {
		if (is_uploaded_file($_FILES[$this->_id]['tmp_name'])) {
			return $_FILES[$this->_id]['type'];
		} else {
			return false;
		}
	}

	public function isUploaded() {
		return is_uploaded_file($_FILES[$this->_id]['tmp_name']);
	}

	public function transformDestinationPath($value) {
		$extension = end(explode('.', $this->getOriginalName()));
		return str_replace(array('[id]', '[extension]'), array($value, $extension), $this->getDestination());
	}


	public function evaluate($value) {
		if ($this->getDestination()) {
			$path = $this->transformDestinationPath($value);
			$ret = move_uploaded_file($this->getTempName(), $path);
			return $ret ? $path : false;
		}
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<input type="file" name="' . $this->_id . '" id="' . $this->_id . '"'
			. ' size="' . $this->_size . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '') . ' />'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
