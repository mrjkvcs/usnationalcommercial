<?php

class FCheckbox extends Field {
	protected $_value;
	protected $_text;
	protected $_uncheckedValue = 0;
	protected $_checkedValue = 1;

	public function __construct($parameters = array()) {
		$this->_value = $this->_uncheckedValue;
		parent::__construct($parameters);
	}

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'TEXT':			$this->setText($value);
					break;
				case 'CHECKEDVALUE':	$this->setCheckedValue($value);
					break;
				case 'UNCHECKEDVALUE':	$this->setUncheckedValue($value);
					break;
			}
		}

		return $this;
	}

	public function queryValue($method) {
		if ($method == 'post') {
			$value = isset($_POST[$this->_id]) && $_POST[$this->_id] == $this->_checkedValue ? $this->_checkedValue : $this->_uncheckedValue;
		} else {
			$value = isset($_GET[$this->_id]) && $_GET[$this->_id] == $this->_checkedValue ? $this->_checkedValue : $this->_uncheckedValue;
		}
		return $value;
	}

	public function setCheckedValue($value) {
		$this->_checkedValue = $value;
		return $this;
	}

	public function setUncheckedValue($value) {
		$this->_uncheckedValue = $value;
		return $this;
	}

	public function setText($text) {
		$this->_text = $text;
		return $this;
	}

	public function setValue($value) {
		if ($value == $this->_checkedValue || $value == $this->_uncheckedValue) {
			$this->_value = $value;
		} else {
			$this->_value = null;
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' value not in allowed set, filling null', 'form', 2);
			}
		}
		return $this;
	}

	public function setUserValue($userValue, array $aliases = array()) {
		if (empty($aliases)) {
			$aliases = array($this->_text);
		}
		$this->_value = in_array($userValue, $aliases) ? $this->_checkedValue : $this->_uncheckedValue;
	}

	public function getCheckedValue() {
		return $this->_checkedValue;
	}

	public function getUncheckedValue() {
		return $this->_uncheckedValue;
	}

	public function getText() {
		return $this->_text;
	}

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}
		if (!$this->_required
			|| $this->_value == $this->_checkedValue
		) {
			return true;
		} else {
			$this->_error = t('required-to-check');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, required field not filled', 'form', 2);
			}
			return false;
		}
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<input type="checkbox" name="' . $this->_id . '" id="' . $this->_id . '" value="' . $this->_checkedValue . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ($this->_value ? ' checked="checked"' : '') . ' />'
			. ($this->_text ? ' <label for="' . $this->_id . '">' . t($this->_text) . '</label>' : '')
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
