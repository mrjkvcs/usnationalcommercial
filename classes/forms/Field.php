<?php

abstract class Field {
	protected $_id;
	protected $_label;
	protected $_key = false;
	protected $_unique = false;
	protected $_prefix;
	protected $_suffix;
	protected $_value;
	protected $_dbField;
	protected $_required = true;
	protected $_disabled = false;
	protected $_readonly = false;
	protected $_renderable = true;
	protected $_validation;
	protected $_matchTo;
	protected $_hidden = false;
	protected $_hint;
	protected $_tooltip;
	protected $_className;
	protected $_filters = array();
	protected $_error;
	protected $_groupId;
	protected $_tabindex;

	public function __construct($parameters = array()) {
		if (!is_array($parameters) && !empty($parameters)){
			$this->setId($parameters);
		} else {
			$this->buildFromParameters($parameters);
		}
	}

	public function buildFromParameters($parameters) {
		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'ID':			$this->setId($value);
					break;
				case 'FIELD':		$this->setField($value);
					break;
				case 'KEY':			$this->setKey(strtoupper($value) == 'KEY' || strtoupper($value) == 'TRUE');
					break;
				case 'UNIQUE':		$this->setUnique(strtoupper($value) == 'UNIQUE' || strtoupper($value) == 'TRUE');
					break;
				case 'NAME':
				case 'LABEL':		$this->setLabel($value);
					break;
				case 'PREFIX':		$this->setPrefix($value);
					break;
				case 'SUFFIX':		$this->setSuffix($value);
					break;
				case 'TOOLTIP':		$this->setTooltip($value);
					break;
				case 'HINT':		$this->setHint($value);
					break;
				case 'VALUE':		$this->setValue($value);
					break;
				case 'VALIDATEAS':	$this->setValidation($value);
					break;
				case 'MATCH':		$this->setMatchTo($value);
					break;
				case 'FILTER':		$this->addFilter($value);
					break;
				case 'REQUIRED':	$this->required($value == 'required' || $value == 'true');
					break;
				case 'HIDDEN':		$this->hide();
					break;
				case 'DISABLED':	$this->disable($value == 'disabled' || $value == 'true');
					break;
				case 'READONLY':	$this->setReadonly($value == 'readonly' || $value == 'true');
					break;
				case 'CLASS':		$this->setClassName($value);
					break;
				case 'GROUP':		$this->setGroup($value);
					break;
				case 'RENDERABLE':	$this->setRenderable($value == 'false' ? false : true);
					break;
				case 'TABINDEX' : 	$this->setTabIndex($value);
					break;
			}
		}
	}

	public function setId($id) {
		$this->_id = $id;
		return $this;
	}

	public function setLabel($label) {
		$this->_label = $label;
		return $this;
	}

	public function setValue($value) {
		$this->_value = $value;
		return $this;
	}

	public function setUserValue($value) {
		return $this->setValue($value);
	}

	public function setKey($key = true) {
		$this->_key = $key;
		return $this;
	}

	public function setUnique($unique = true) {
		$this->_unique = $unique;
		return $this;
	}

	public function setPrefix($html) {
		$this->_prefix = $html;
		return $this;
	}

	public function setSuffix($html) {
		$this->_suffix = $html;
		return $this;
	}

	public function setTooltip($text) {
		$this->_tooltip = $text;
		return $this;
	}

	public function setHint($text) {
		$this->_hint = $text;
		return $this;
	}

	public function setField($field) {
		$this->_dbField = $field;
		if (is_null($this->_id)) {
			$this->_id = $field;
		}
		return $this;
	}

	public function setValidation($validationRule) {
		$this->_validation = $validationRule;
		return $this;
	}

	public function setMatchTo($fieldId) {
		$this->_matchTo = $fieldId;
		return $this;
	}

	public function setClassName($className) {
		$this->_className = $className;
		return $this;
	}

	public function setError($message) {
		$this->_error = $message;
		return $this;
	}

	public function setGroup($groupId) {
		$this->_groupId = $groupId;
		return $this;
	}

	public function setRenderable($state = true) {
		$this->_renderable = $state;
		return $this;
	}

	public function setReadonly($state = true) {
		$this->_readonly = $state;
		return $this;
	}

	public function setRequired($state = true) {
		$this->_required = $state;
		return $this;
	}

	public function setTabIndex($value) {
		$this->_tabindex = $value;
		return $this;
	}

	public function addFilter($filter) {
		if (is_array($filter)) {	// tömb van megadva - osztály (objektum) és metódus
			$this->_filters[] = $filter;
		} else if (is_string($filter)) {	// szöveg
			$filters = preg_split('/\s/', $filter, -1, PREG_SPLIT_NO_EMPTY);
			foreach ($filters as $filterFunction) {
				$this->_filters[] = trim($filterFunction);
			}
		} else if (is_callable($filter)) {
			$this->_filters[] = $filter;
		}
		return $this;
	}

	public function appendTo(Form $form) {
		$form->addField($this);
		return $this;
	}

	public function appendBefore($beforeField, Form $form) {
		$form->addFieldBefore($beforeField, $this);
		return $this;
	}

	public function appendAfter($afterField, Form $form) {
		$form->addFieldAfter($afterField, $this);
		return $this;
	}

	public function required($state = true) {
		return $this->setRequired($state);
	}

	public function optional($state = true) {
		return $this->setRequired(!$state);
	}

	public function hide() {
		$this->_hidden = true;
		return $this;
	}

	public function show() {
		$this->_hidden = false;
		return $this;
	}

	public function disable() {
		$this->_disabled = true;
		return $this;
	}

	public function enable() {
		$this->_disabled = false;
		return $this;
	}

	public function getType() {
		return substr(get_class($this), 1);
	}

	public function getId() {
		return $this->_id;
	}

	public function getLabel() {
		return $this->_label;
	}

	public function getValue() {
		return $this->evaluateFilters($this->_value);
	}

	public function evaluateFilters($value) {
		foreach ($this->_filters as $filter) {
			if (is_callable($filter)) {
				$value = call_user_func($filter, $value);
			}
		}
		return $value;
	}

	public function getPrefix() {
		return $this->_prefix;
	}

	public function getSuffix() {
		return $this->_suffix;
	}

	public function getTooltip() {
		return $this->_tooltip;
	}

	public function getHint() {
		return $this->_hint;
	}

	public function getField() {
		return $this->_dbField;
	}

	public function getValidation() {
		return $this->_validation;
	}

	public function getMatchTo() {
		return $this->_matchTo;
	}

	public function getClassName() {
		return $this->_className;
	}

	public function getError() {
		return $this->_error;
	}

	public function getGroup() {
		return $this->_groupId;
	}

	public function getFilters() {
		return $this->_filters;
	}

	public function getTabIndex() {
		return $this->_tabindex;
	}

	public function isRequired() {
		return $this->_required;
	}

	public function isReadonly() {
		return $this->_readonly;
	}

	public function isRenderable() {
		return $this->_renderable;
	}

	public function isKey() {
		return $this->_key;
	}

	public function isUnique() {
		return $this->_unique;
	}

	public function isHidden() {
		return $this->_hidden;
	}

	public function validate() {
		if ($this->_error) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, error message set', 'form', 2);
			}
			return false;
		}
		if ($this->_required && !$this->_value) {
			$this->_error = t('required-to-fill');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, required field not filled', 'form', 2);
			}
			return false;
		}
		if ($this->_value && $this->_validation && !Text::check($this->_value, $this->_validation)) {
			$this->_error = t('format-not-allowed');
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo($this->getId() . ' failed validation, format error', 'form', 2);
			}
			return false;
		}
		return true;
	}

	public function renderLabel() {
		if (!$this->_label) {
			return '&nbsp;';
		}
		$ret = '<label' . ($this->_required ? ' class="required"' : '') . ' for="' . $this->_id. '">' . t($this->_label) . '</label>';
		if ($this->_hint) {
			$ret .= '<span class="field-hint">' . t($this->_hint) . '</span>';
		}
		return $ret;
	}

	public function renderContent() {
		$ret = '';
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= $this->getValue();
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}

	public function renderError() {
		if ($this->_error) {
			return ' <p class="text-danger text-danger-form">*' . t($this->_error) . '</p>';
		}
	}

	public function __toString() {
		return $this->renderContent();
	}
}
