<?php

class FInputText extends Field {
	protected $_size = 20;
	protected $_minLength;
	protected $_maxLength;
	protected $_placeholderText;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'SIZE':		$this->setSize($value);
					break;
				case 'MINLENGTH':	$this->setMinLength($value);
					break;
				case 'MAXLENGTH':	$this->setMaxLength($value);
					break;
				case 'PLACEHOLDER':	$this->setPlaceholderText($value);
					break;
			}
		}

		return $this;
	}

	public function setSize($size) {
		$this->_size = $size;
		return $this;
	}

	public function setMinLength($value) {
		$this->_minLength = $value;
		return $this;
	}

	public function setMaxLength($value) {
		$this->_maxLength = $value;
		return $this;
	}

	public function setPlaceholderText($value) {
		$this->_placeholderText = $value;
		return $this;
	}

	public function getSize() {
		return $this->_size;
	}

	public function getMinLength() {
		return $this->_minLength;
	}

	public function getMaxLength() {
		return $this->_maxLength;
	}

	public function getPlaceholderText() {
		return $this->_placeholderText;
	}

	public function validate() {
		$isValid = true;
		if ($this->_maxLength && mb_strlen($this->getValue()) > $this->_maxLength) {
			$this->_error = t('length-is-too-long');
			$isValid = false;
		}
		if ($this->_required && $this->_minLength && mb_strlen($this->getValue()) < $this->_minLength) {
			$this->_error = t('length-is-too-short');
			$isValid = false;
		}
		return parent::validate() && $isValid;
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<input type="text" name="' . $this->_id . '" id="' . $this->_id  . '"'
			. ' size="' . $this->_size . '"'
			. ($this->_maxLength ? ' maxlength="' . $this->_maxLength . '"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_placeholderText ? ' placeholder="' . t($this->_placeholderText) . '"' : '')
			. ($this->_tabindex ? ' tabindex="' . t($this->_tabindex) . '"' : '')
			. ' value="' . htmlspecialchars($this->getValue()) . '" />'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
