<?php

class FDate extends Field {
	private $_firstYear = '-100';
	private $_lastYear = '-18';
	private $_year;
	private $_month;
	private $_day;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'FIRSTYEAR':		$this->setFirstYear($value);
					break;
				case 'LASTYEAR':	$this->setLastYear($value);
					break;
			}
		}

		return $this;
	}

	public function setFirstYear($year) {
		$this->_firstYear = $year;
		return $this;
	}

	public function setLastYear($year) {
		$this->_lastYear = $year;
		return $this;
	}

	public function getFirstYear() {
		return $this->_firstYear;
	}

	public function getLastYear() {
		return $this->_lastYear;
	}

	public function setValue($value) {
		if (!preg_match('/^[0-9]{4}\-[0-9]{1,2}\-[0-9]{1,2}$/', $value)) {
			return false;
		}
		list ($this->_year, $this->_month, $this->_day) = explode('-', $value);
		return $this;
	}

	public function queryValue($method) {
		$value = '';
		if ($method == 'post') {
			if (isset($_POST[$this->_id . '-year']) && isset($_POST[$this->_id . '-month']) && isset($_POST[$this->_id . '-day'])) {
				$value = $_POST[$this->_id . '-year'] . '-' . $_POST[$this->_id . '-month'] . '-' . $_POST[$this->_id . '-day'];
			}
		} else {
			if (isset($_GET[$this->_id . '-year']) && isset($_GET[$this->_id . '-month']) && isset($_GET[$this->_id . '-day'])) {
				$value = $_GET[$this->_id . '-year'] . '-' . $_GET[$this->_id . '-month'] . '-' . $_GET[$this->_id . '-day'];
			}
		}
		return $value;
	}

	public function getValue() {
		if ($this->_year && $this->_month && $this->_day) {
			$this->_value = $this->_year . '-' . str_pad($this->_month, 2, '0', STR_PAD_LEFT) . '-' . str_pad($this->_day, 2, '0', STR_PAD_LEFT);
		}
		return parent::getValue();
	}

	public function validate() {
		if (!$this->_error && (!$this->_required || Date::check($this->getValue()))) {
			$year = date('Y', strtotime($this->getValue()));
			if ($year < $this->_calculateFirst() || $year > $this->_calculateLast()) {
				$this->_error = t('date-is-out-of-range');
				return false;
			} else {
				return true;
			}
		} else {
			if (!$this->_error) $this->_error = t('date-format-error');
			return false;
		}
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' form-control';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$dateFormat = Setting::getDateFormat();
		$dateFormat = str_replace(
			array('.', '-', '%Y', '%y', '%b', '%B', '%m', '%d', '%e'),
			array('', '', $this->_showYear(), $this->_showYear(), $this->_showMonth(), $this->_showMonth(),
				$this->_showMonth(), $this->_showDay(), $this->_showDay()
			),
			$dateFormat
		);
		$ret .= $dateFormat;
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}

	protected function _showYear() {
		$ret = '<select name="' . $this->_id . '-year" id="' . $this->_id . '-year"' . ($this->_disabled ? ' disabled="disabled"' : '') . ($this->_readonly ? ' readonly="readonly"' : '') . '>';
		for ($i = $this->_calculateLast(); $i >= $this->_calculateFirst(); $i--) {
			$ret .= '<option value="' . $i . '"' . ($this->_year == $i ? ' selected="selected"' : '') . '>' . $i . '</option>';
		}
		$ret .= '</select>';
		return $ret;
	}

	protected function _showMonth() {
		$ret = '<select name="' . $this->_id . '-month" id="' . $this->_id . '-month"' . ($this->_disabled ? ' disabled="disabled"' : '') . ($this->_readonly ? ' readonly="readonly"' : '') . '>';
		for ($i = 1; $i <= 12; $i++) {
			$time = mktime(0, 0, 0, $i, 1, 2000);
			$ret .= '<option value="' . $i . '"' . ($this->_month == $i ? ' selected="selected"' : '') . '>' . strftime('%b', $time) . '</option>';
		}
		$ret .= '</select>';
		return $ret;
	}

	protected function _showDay() {
		$ret = '<select name="' . $this->_id . '-day" id="' . $this->_id . '-day"' . ($this->_disabled ? ' disabled="disabled"' : '') . ($this->_readonly ? ' readonly="readonly"' : '') . '>';
		for ($i = 1; $i <= 31; $i++) {
			$ret .= '<option value="' . $i . '"' . ($this->_day == $i ? ' selected="selected"' : '') . '>' . $i . '</option>';
		}
		$ret .= '</select>';
		return $ret;
	}

	protected function _calculateFirst() {
		switch (substr($this->_firstYear, 0, 1)) {
			case '+':		return intval(date('Y') + substr($this->_firstYear, 1));	break;
			case '-':		return intval(date('Y') - substr($this->_firstYear, 1));	break;
			case '':		return date('Y');											break;
			default:		return intval($this->_firstYear);							break;
		}
	}

	protected function _calculateLast() {
		switch (substr($this->_lastYear, 0, 1)) {
			case '+':		return intval(date('Y') + substr($this->_lastYear, 1));		break;
			case '-':		return intval(date('Y') - substr($this->_lastYear, 1));		break;
			case '':		return date('Y');											break;
			default:		return intval($this->_lastYear);							break;
		}
	}
}
