<?php

class FReCaptcha extends Field {
	protected $isValid;

	public function __construct($parameters = array()) {
		UniAdmin::import('extensions.recaptcha.RecaptchaConfig');
		parent::__construct($parameters);
	}

	public function validate() {
		if (!is_null($this->isValid)) {
			return $this->isValid;
		}
		UniAdmin::import('extensions.recaptcha.recaptchalib', true);

		$response = recaptcha_check_answer(
			RecaptchaConfig::$privateKey,
			$_SERVER['REMOTE_ADDR'],
			$_POST['recaptcha_challenge_field'],
			$_POST['recaptcha_response_field']
		);
		$this->isValid = $response->is_valid;
		if (!$this->isValid) {
			$this->_error = 'captcha-failed';
			return false;
		} else {
			return true;
		}
	}

	public function renderContent() {
		?>
		<script type="text/javascript" src="http://www.google.com/recaptcha/api/challenge?k=<?php echo RecaptchaConfig::$publicKey; ?>">
		</script>
		<noscript>
			<iframe src="http://www.google.com/recaptcha/api/noscript?k=<?php echo RecaptchaConfig::$publicKey; ?>" height="300" width="500" frameborder="0"></iframe><br>
			<textarea name="recaptcha_challenge_field" rows="3" cols="40"></textarea>
			<input type="hidden" name="recaptcha_response_field" value="manual_challenge">
		</noscript>
		<?php
	}
}
