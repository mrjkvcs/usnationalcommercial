<?php

class FPassword extends FInputText {
	protected $_mustHaveNumbers = false;
	protected $_mustHaveUppercase = false;

	public function buildFromParameters($parameters) {
		parent::buildFromParameters($parameters);

		foreach ($parameters as $key => $value) {
			switch (strtoupper($key)) {
				case 'REQUIRENUMBERS':		$this->requireNumbers(strtoupper($value) == 'REQUIRENUMBERS' || strtoupper($value) == 'TRUE');
					break;
				case 'REQUIREUPPERCASE':	$this->requireUppercase(strtoupper($value) == 'REQUIREUPPERCASE' || strtoupper($value) == 'TRUE');
					break;
			}
		}

		return $this;
	}

	public function isEmpty() {
		return $this->getValue() ? false : true;
	}

	public function requireNumbers($value) {
		$this->_mustHaveNumbers = (bool) $value;
		return $this;
	}

	public function requireUppercase($value) {
		$this->_mustHaveUppercase = (bool) $value;
		return $this;
	}

	public function validate() {
		$isValid = true;
		if ($this->_mustHaveNumbers && !preg_match('/[0-9]+/', $this->_value)) {
			$this->_error = t('value-does-not-contain-numbers');
			$isValid = false;
		}
		if ($this->_mustHaveUppercase && strtolower($this->_value) == $this->_value) {
			$this->_error = t('value-does-not-contain-uppercase-letters');
			$isValid = false;
		}
		return parent::validate() && $isValid;
	}

	public function renderContent() {
		$ret = '';
		if ($this->_tooltip) {
			$this->_className .= ' tooltip';
		}
		if (isset($this->_prefix)) {
			$ret .= t($this->_prefix) . ' ';
		}
		$ret .= '<input type="password" name="' . $this->_id . '" id="' . $this->_id . '"'
			. ($this->_disabled ? ' disabled="disabled"' : '')
			. ($this->_readonly ? ' readonly="readonly"' : '')
			. ($this->_className ? ' class="' . trim($this->_className) . '"' : '')
			. ($this->_placeholderText ? ' placeholder="' . t($this->_placeholderText) . '"' : '')
			. ($this->_tabindex ? ' tabindex="' . $this->_tabindex . '"' : '')
			. ' size="' . $this->_size . '" value="" />'
			. ($this->_tooltip ? ' <span id="tt_' . $this->_id . '" class="tooltip" style="display:none;">' . t($this->_tooltip) . '</span>' : '');
		if (isset($this->_suffix)) {
			$ret .= ' ' . t($this->_suffix);
		}
		return $ret;
	}
}
