<?php

class Setting {
	protected static $defaultLanguage;
	protected static $locale = array();
	protected static $dateFormat = array(
		'short' => array(),
		'long' => array(),
	);
	protected static $languages = array();
	protected static $settings = array();
	protected static $currentCountry;

	protected static function fetchLanguage($id = null) {
		if (!is_null($id) && UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get('uniAdminLanguageInfo-' . $id);
			if ($cachedVersion) {
				$language = $cachedVersion;
			}
		}
		if (!isset($language)) {
			$language = Db::sqlRow(sprintf("
				SELECT id, locale, shortDateFormat, longDateFormat, isDefault
				FROM UniadminLanguages
				%s",
				is_null($id) ? 'ORDER BY isDefault DESC LIMIT 1' : sprintf("WHERE id = '%s'", $id)
			));
			if (!$language) {
				return false;
			}
			if (UniAdmin::app()->cache->isEnabled() 
				&& !UniAdmin::app()->cache->exists('uniAdminLanguageInfo-' . $language['id'])
			) {
				UniAdmin::app()->cache->set('uniAdminLanguageInfo-' . $language['id'], $language, 86400);
			}
		}
		if (is_null($id)) {	// alapértelmezett nyelv
			self::$defaultLanguage = $language['id'];
		}
		self::$locale[$language['id']] = $language['locale'] ?: 'hu_HU.utf8';
		self::$dateFormat['short'][$language['id']] = $language['shortDateFormat'];
		self::$dateFormat['long'][$language['id']] = $language['longDateFormat'];
		return $language;
	}

	public static function detectLanguage() {
		// csak egyszer detektálunk, ha már megtörtént, akkor nem lesz több átirányítás
		if (isset($_SESSION['languageDetected']) || !isset($_SERVER['HTTP_ACCEPT_LANGUAGE'])) {
			return;
		}
		$languages = array();
		$languageRanges = explode(',', trim($_SERVER['HTTP_ACCEPT_LANGUAGE']));
		foreach ($languageRanges as $languageRange) {
			if (preg_match('/^(\*|[a-zA-Z]{2})(?:\s*;\s*q\s*=\s*(0(?:\.\d{0,3})|1(?:\.0{0,3})))?$/', trim($languageRange), $match)) {
				if (!isset($match[2])) {
					$match[2] = '1.0';
				} else {
					$match[2] = (string) floatval($match[2]);
				}
				$languages[$match[2]] = strtolower($match[1]);
			}
		}
		krsort($languages);
		foreach ($languages as $language) {
			$info = self::fetchLanguage($language);
			if ($info) {
				if (!$info['isDefault']) {
					$_SESSION['languageDetected'] = true;
					header('Location: /' . $language . $_SERVER['REQUEST_URI']);
					exit;
				}
			}
		}
	}

	/**
	 * Visszadja a nyelvek közül az alapértelmezettet
	 * @return string
	 */
	public static function getDefaultLanguage() {
		if (self::$defaultLanguage) {
			return self::$defaultLanguage;
		}
		switch (UniAdmin::app()->config['language']) {
			case 'auto':		// alapértelmezett nyelv lekérdezése
			case 'detect':		// ha detektálás történt, és nem találtunk nyelvet
				if (UniAdmin::app()->cache->isEnabled()) {
					$cachedVersion = UniAdmin::app()->cache->get('uniAdminDefaultLanguage');
					if ($cachedVersion) {
						self::$defaultLanguage = $cachedVersion;
						return $cachedVersion;
					}
				}
				self::fetchLanguage();
				if (UniAdmin::app()->cache->isEnabled()) {
					UniAdmin::app()->cache->set('uniAdminDefaultLanguage', self::$defaultLanguage, 86400);
				}
				return self::$defaultLanguage;
			break;
			default:
				self::$defaultLanguage = UniAdmin::app()->config['language'];
				return self::$defaultLanguage;
			break;
		}
	}

	public static function getLanguages() {
		if (self::$languages) {
			return self::$languages;
		}
		if (UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get('uniAdminLanguages');
			if ($cachedVersion) {
				self::$languages = $cachedVersion;
				return $cachedVersion;
			}
		}
		$languages = Db::toArray("
			SELECT `id`, `language` 
			FROM `UniadminLanguages` 
			ORDER BY `isDefault` DESC, `language`", 
			true
		);
		self::$languages = $languages;
		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->set('uniAdminLanguages', $languages, 86400);
		}
		return $languages;
	}

	public static function checkLanguageExists($languageId) {
		$languageId = strtolower($languageId);
		$languages = self::getLanguages();
		return isset($languages[$languageId]);
	}

	public static function getCurrentCountry() {
		if (is_null(self::$currentCountry)) {
			self::$currentCountry = Db::sqlField(sprintf("
				SELECT C.id
				FROM Country C
				JOIN UniadminLanguages UL 
					ON C.id = UL.countryId
				WHERE UL.id = '%s'",
				LANG
			));
		}
		return self::$currentCountry;
	}
	
	public static function getLocale($language = null) {
		if (is_null($language)) {
			$language = LANG;
		}
		if (!isset(self::$locale[$language])) {
			self::fetchLanguage($language);
		}
		return self::$locale[$language];
	}

	public static function getDateFormat($long = false, $language = null) {
		if (is_null($language)) {
			$language = LANG;
		}
		$varName = $long ? 'long' : 'short';
		if (!isset(self::$dateFormat[$varName][$language])) {
			self::fetchLanguage($language);
		}
		return self::$dateFormat[$varName][$language];
	}

	/**
	 * Lekér egy adott beállítást. A személyes beállításról van szó, és az adminisztrációs rendszer fut, az adott felhasználóhoz tartozó értéket adja vissza
	 * @param string $id a beállítás azonosítója
	 * @param string $module a beállítást megadó modul. Ha nincs megadva, a rendszer beállításait veszi alapul
	 * @return string|bool
	 */
	public static function get($id, $module = 'settingmanager') {
		if ($module && $id) {
			if (isset(self::$settings[$module . '--' . $id])) {
				return self::$settings[$module . '--' . $id];
			}
			$value = Db::sqlField(sprintf("
				SELECT `value` 
				FROM `UniadminSettings` 
				WHERE `moduleId` = '%s' 
				AND `id` = '%s'",
				$module,
				$id
			));
			self::$settings[$module . '--' . $id] = $value;
			return $value;
		}
		return false;
	}

	/**
	 * Elment egy beállítást. Ha személyes beállításról van szó, és az adminisztrációs rendszer fut, a bejelentkezett felhasználóhoz rendeli hozzá az értéket
	 * @param string $id a beállítás azonosítója
	 * @param string $value a beállítandó érték
	 * @param string $module a beállítást megadó modul. Ha nincs megadva, a rendszer beállításait veszi alapul
	 * @return bool
	 */
	public static function save($id, $value, $module = 'settingmanager') {
		if ($module && $id) {
			Db::sql(sprintf("
				UPDATE `UniadminSettings` 
				SET `value` = '%s' 
				WHERE `moduleId` = '%s' 
				AND `id` = '%s'",
				$value,
				$module,
				$id
			));
			self::$settings[$module . '--' . $id] = $value;
			return true;
		}
		return false;
	}

	public static function setCurrency($currencyId = null) {
		if (is_null($currencyId)) {
			if (isset($_GET['currency'])) {
				$currencyId = $_GET['currency'];
			} else if (isset($_SESSION['currency'])) {
				$currencyId = $_SESSION['currency'];
			}
		}
		if ($currencyId) {
			$exists = Db::sqlBool(sprintf("
				SELECT COUNT(*) 
				FROM UniadminCurrency 
				WHERE id = '%s'",
				Db::escapeString($currencyId)
			));
			if ($exists) {
				UniAdmin::app()->setVariable('currency', $currencyId);
				$_SESSION['currency'] = $currencyId;
				return true;
			}
		}
		return false;
	}
}
