<?php

class EmailQueue {
	public function add(Email $data) {
		$to = $data->getTo();
		if (empty($to)) {
			return false;
		} else {
			$email = current($to);
		}
		return Db::sql(sprintf("
			INSERT INTO UniadminEmailQueue
			(email, data, insertedAt)
			VALUES
			('%s', '%s', NOW())",
			Db::escapeString($email),
			Db::escapeString(serialize($data))
		));
	}

	public function revokeById($id) {
		return Db::sql(sprintf("
			DELETE FROM UniadminEmailQueue
			WHERE id = %d",
			$id
		));
	}

	public function revokeByEmail($email) {
		return Db::sql(sprintf("
			DELETE FROM UniadminEmailQueue
			WHERE email = '%s'",
			Db::escapeString($email)
		));
	}

	public function processEmails() {
		$limit = isset(UniAdmin::app()->config['emailQueue'])
			&& isset(UniAdmin::app()->config['emailQueue']['sendingLimit'])
			? intval(UniAdmin::app()->config['emailQueue']['sendingLimit']) : 50;

		// letiltjuk az üzenetsort, hogy most azonnal kimenjenek az üzenetek
		$config = Uniadmin::app()->config['emailQueue'];
		Uniadmin::app()->config['emailQueue'] = false;

		$mails = Db::sql(sprintf("
			SELECT id, email, data
			FROM UniadminEmailQueue
			WHERE isFailed = 0
			ORDER BY id
			LIMIT %d",
			$limit
		));
		while ($mail = Db::loop($mails)) {
			$email = unserialize($mail['data']);

			if (is_a($email, 'Email')) {
				if (isset($config['smtp']) && is_array($config['smtp'])) {
					$email->setSmtp(
						isset($config['smtp']['host']) ? $config['smtp']['host'] : null,
						isset($config['smtp']['username']) ? $config['smtp']['username'] : null,
						isset($config['smtp']['password']) ? $config['smtp']['password'] : null,
						isset($config['smtp']['port']) ? $config['smtp']['port'] : null
					);
				}

				$ret = $email->send();

				echo 'email to ' . $mail['email'] . ' ' . ($ret ? 'sent' : 'failed') . "\n";
			} else {
				$ret = false;
			}

			UniAdmin::app()->log->setType('email');
			if ($ret) {
				Db::sql(sprintf("
					DELETE FROM UniadminEmailQueue
					WHERE id = %d",
					$mail['id']
				));
				UniAdmin::app()->log->write('queued email sent successfully to ' . $mail['email']);
			} else {
				Db::sql(sprintf("
					UPDATE UniadminEmailQueue
					SET isFailed = 1
					WHERE id = %d",
					$mail['id']
				));
				UniAdmin::app()->log->write('queued email failed to ' . $mail['email']);
			}
			UniAdmin::app()->log->setType('');
		}

		Uniadmin::app()->config['emailQueue'] = $config;
	}
}
