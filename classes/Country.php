<?php

class Country extends BaseModel {
	protected static $_countries = null;
    static $table_name = 'CountryText';

	public static function getCountries() {
		if (!is_null(self::$_countries)) {
			return self::$_countries;
		}
		$cacheId = 'uniAdminCountries-' . (defined('IS_BACKEND') ? CONTENT_LANG : LANG);

		if (UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get($cacheId);
			if ($cachedVersion) {
				self::$_countries = $cachedVersion;
				return $cachedVersion;
			}
		}

		self::$_countries = self::getCountryList();

		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->set($cacheId, self::$_countries, 86400);
		}

		return self::$_countries;
	}

	public static function getCountryList($conditions = null){
		return Db::toArray(sprintf("
			SELECT
				CT.countryId,
				CT.name
			FROM CountryText CT
			INNER JOIN Country C
				ON CT.countryId = C.id AND CT.languageId = '%s'
			%s
			ORDER BY CT.name
			",
			(defined('IS_BACKEND') ? CONTENT_LANG : LANG),
			$conditions
		), true);
	}

	public static function checkCountryExists($countryId) {
		$countryId = strtolower($countryId);
		$countries = self::getCountries();
		return isset(self::$_countries[$countryId]);
	}

	public static function getDeliveryCountryList() {
		return self::getCountryList("WHERE C.allowDelivery = 1");
	}

	public static function countDeliveryCountryList() {
		return count(self::getDeliveryCountryList());
	}

	public static function getRegisterCountryList() {
		return self::getCountryList("WHERE C.allowRegistration = 1");
	}

	public static function getName($countryId) {
		self::getCountries();
		return isset(self::$_countries[$countryId]) ? self::$_countries[$countryId] : false;
	}

	public static function getId($name) {
		return Db::sqlField(sprintf("
			SELECT CT.countryId
			FROM CountryText CT
			WHERE CT.name = '%s'",
			Db::escapeString($name)
		));
	}
}
