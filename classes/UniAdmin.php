<?php

/**
 * Az adminisztrációs rendszer adatait szolgáltató osztály
 * @final
 */
class UniAdmin {
	private static $_importedFiles = array();
	private static $_debugInfos = array();

	/**
	 * Példányosítás letiltva
	 */
	private function __construct() { } 

	/**
	 * Visszaadja a rendszer verziószámát
	 * @return string
	 */
	public static function getVersion() {
		return '6.0';
	}

	/**
	 * Visszaadja az aktuális AdminApplication singleton objektumot
	 * @return Application
	 */
	public static function app() {
		static $_app_instance;

		if (!isset($_app_instance)) {
			$_app_instance = new AdminApplication();
		}

		return $_app_instance;
	}

    public static function importAdmin($path, $forced = false) {
        return self::import('modules.Admin.modules.' . $path, $forced);
    }

	/**
	 * Betölti a megadott komponenst
	 */
	public static function import() {
		$args = func_get_args();
		if (empty($args)) {
			return false;
		}
		if (is_bool(end($args))) {
			$files = array_slice($args, 0, -1);
			$forced = end($args);
		} else {
			$forced = false;
			$files = $args;
		}
		foreach ($files as $path) {
			$realPath = preg_replace('/\.|\\\/', '/', $path);			
			if (substr($realPath, -1) == '*') {		// egész mappa tartalmának betöltése
				$dir = substr($realPath, 0, -2);
				if (is_dir($dir)) {
					if ($handle = opendir($dir)) {
						while (($file = readdir($handle)) !== false) {
							$parts = explode('.', $file);
							if ($file != '.' && $file != '..' && !is_dir($file) && end($parts) == 'php') {
								if ($forced) {
									if (DEBUG_MODE) {
										UniAdmin::addDebugInfo('forced loading ' . $file, 'includes');
									}
									include_once($dir . '/' . $file);
								} else {
									array_pop($parts);
									$className = implode('.', $parts);
									if (preg_match('/([^\.]*\\\\{1}[^\.]*)$/',$path, $matches)) {
										$className = substr($matches[1],0,-2) . '\\' . $className;
									}
									if (!array_key_exists($className, self::$_importedFiles)) {
										self::$_importedFiles[$className] = $dir . '/' . $file;
									}
								}
							}
						}
						closedir($handle);
					} else {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('cannot open directory ' . $dir, 'includes', 2);
						}
					}
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo($dir . ' is not a directory', 'includes', 2);
					}
				}
			} else {	// egy adott fájl betöltése
				$file = $realPath . '.php';
				if ($forced) {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('forced loading ' . $file, 'includes');
					}
					if (file_exists($file)) {
						include_once($file);
					} else {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('error loading file ' . $file, 'includes', 3);
							UniAdmin::addDebugInfo(self::formatBacktrace(true));
							UniAdmin::flushDebugInfos();
						}
						exit;
					}
				} else {
					$parts = explode('.', $path);
					$className = end($parts);
					if (preg_match('/([^\.]*\\\\{1}[^\.]*)$/',$path, $matches)) {
						$className = substr($matches[1],0,-1) . '\\' . $className;
					}
					if (!array_key_exists($className, self::$_importedFiles)) {
						self::$_importedFiles[$className] = $file;
					}
				}
			}
		}
	}

	public static function widget($path, $parameters = null, $autoRender = true) {
		UniAdmin::import($path);
		$parts = explode('.', $path);
		$className = end($parts);
		if (class_exists($className)) {
			$widget = new $className($parameters, $autoRender);
			return $widget;
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot load widget ' . $className, 'includes', 2);
			}
		}
	}

	public static function autoLoadClass($className) {
		if (array_key_exists($className, self::$_importedFiles)) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading ' . self::$_importedFiles[$className], 'includes');
			}
			if (file_exists(self::$_importedFiles[$className])) {
				include_once(self::$_importedFiles[$className]);
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('error loading class ' . self::$_importedFiles[$className], 'includes', 3);
					UniAdmin::addDebugInfo(self::formatBacktrace(true));
					UniAdmin::flushDebugInfos();
				}
				exit;
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot load class ' . $className, 'includes', 3);
				UniAdmin::addDebugInfo(self::formatBacktrace(true));
				UniAdmin::flushDebugInfos();
			}
			exit;
		}
	}

	public static function registerAutoLoader($autoLoader) {
		$funcs = spl_autoload_functions();
		if (is_array($funcs)) {
			foreach ($funcs as $func) {
				if (is_array($func)) {
					$func = implode('::', $func);
				}
				spl_autoload_unregister($func);
			}
			spl_autoload_register($autoLoader);
			foreach ($funcs as $func) {
				if (is_array($func)) {
					$func = implode('::', $func);
				}
				spl_autoload_register($func);
			}
		} else {
			spl_autoload_register($autoLoader);
		}
	}

	public static function addDebugInfo($infoText, $type = '', $debugLevel = '', $fileName = null, $lineNumber = null) {
		$time = explode(' ', microtime());
		self::$_debugInfos[] = array(
			'text' => $infoText,
			'level' => $debugLevel,
			'type' => $type,
			'file' => $fileName,
			'line' => $lineNumber,
			'time' => date('H:i:s.', $time[1]) . round($time[0] * 1000),
		);
	}

	public static function flushDebugInfos() {
		if (count(self::$_debugInfos)) {
			if (!ob_get_contents()
				|| (!defined('IS_BACKEND') && strpos(ob_get_contents(), 'clientside/javascript') === false)
				|| (defined('IS_BACKEND') && strpos(ob_get_contents(), '/loader/') === false)
			) {
				// ha még nem jött volna olyan html kód, ami behúzza a jQuery-t, megtesszük manuálisan
				echo '<script type="text/javascript" src="/clientside/javascript"></script>';
			}
			$types = array();
			foreach (self::$_debugInfos as $info) {
				if ($info['type'] && !in_array($info['type'], $types)) {
					$types[] = $info['type'];
				}
			}
			if (!empty($types)) {
				$colors = array(
					array('#FFFF54', '#000'),
					array('#C8E64C', '#000'),
					array('#8CD446', '#000'),
					array('#4DC742', '#FFF'),
					array('#45D2B0', '#FFF'),
					array('#46ACD3', '#FFF'),
					array('#438CCB', '#FFF'),
					array('#4262C7', '#FFF'),
					array('#5240C3', '#FFF'),
					array('#8C3FC0', '#FFF'),
					array('#D145C1', '#FFF'),
					array('#E64C8D', '#FFF'),
					array('#FF5454', '#FFF'),
					array('#FF8054', '#FFF'),
					array('#FFA054', '#FFF'),
					array('#FFB554', '#FFF'),
				);
				echo '<script type="text/javascript">'
					. 'function __uniAdminHilite(type, num){'
					. 'if (type=="") { $(".ua-dbg .ua-dbg-hilite").removeClass("ua-dbg-hilite"); return false; }'
					. '$(".ua-dbg-type-" + type).each(function(index, item){ $(item).toggleClass("ua-dbg-hilite"); }); if ($(".ua-dbg-type-" + type).hasClass("ua-dbg-hilite")) { var top = $(".ua-dbg-type-" + type).first().offset().top; $("html,body").animate({scrollTop: top - 40}, "fast"); } return false; }'
					. '</script><style type="text/css">'
					. '.ua-dbg{width:100%;margin:20px 0 0;background:#EEE;font: 12px Tahoma;text-align:left;line-height:20px;}'
					. '.ua-dbg a{color:#0094D8;}'
					. '.ua-dbg pre{color:#333;}'
					. '.ua-dbg-head{background:#555;color:#FFF;}'
					. '.ua-dbg-head a{padding:2px 4px;}'
					. '.ua-dbg-head a{color:#FFF;}'
					. '.ua-dbg-row{color:#333;border-bottom:1px dotted #999;}'
					. '.ua-dbg-level-2{color:#05C;}'
					. '.ua-dbg-level-3{color:#D11;}';
					foreach ($colors as $num => $color) {
						echo '.ua-dbg-head .ua-color-' . $num . ', .ua-dbg-hilite .ua-color-' . $num . '{background:' . $color[0] . ';color:' . $color[1] .';}';
					}
					echo '</style>';
			}
			echo '<table cellpadding="3" cellspacing="0" class="ua-dbg">'
				. '<tr><td class="ua-dbg-head">&raquo; UniAdmin ' . self::getVersion() . ' - Debug info [ip: ' . $_SERVER['REMOTE_ADDR'] . ']</td></tr>';
			if (!empty($types)) {
				echo '<tr><td class="ua-dbg-head"><a href="#" onclick="return __uniAdminHilite(\'\')">remove highlights</a>';
				foreach ($types as $num => $type) {
					echo ' &nbsp; | &nbsp; <a href="#" class="ua-color-' . $num . '" onclick="return __uniAdminHilite(\'' . $type . '\', ' . $num . ')">' . $type . '</a>';
				}
				echo '</td></tr>';
			}
			foreach (self::$_debugInfos as $info) {
				echo '<tr class="ua-dbg-type-' . $info['type'] . '"><td class="ua-dbg-row';
				if ($info['type']) {
					echo ' ua-color-' . array_search($info['type'], $types);
				}
				if ($info['level'] && $info['level'] != '1') {
					echo ' ua-dbg-level-' . $info['level'];
				}
				echo '">' . $info['time'] . ' ' . $info['text'];
				if (!is_null($info['file'])) {
					echo ' (' . $info['file'];
					if (!is_null($info['line'])) {
						echo ', line ' . $info['line'];
					}
					echo ')';
				}
				echo '</td></tr>';
			}
			if (count($_GET)) echo '<tr><td style="border-bottom:1px dotted #999;"><a href="javascript:;" onclick="document.getElementById(\'uniadmin-show-get\').style.display=\'block\';">show GET</a> <pre id="uniadmin-show-get" style="display:none">' . print_r($_GET, true) . '</pre></td></tr>';
			if (count($_POST)) echo '<tr><td style="border-bottom:1px dotted #999;"><a href="javascript:;" onclick="document.getElementById(\'uniadmin-show-post\').style.display=\'block\';">show POST</a> <pre id="uniadmin-show-post" style="display:none">' . print_r($_POST, true) . '</pre></td></tr>';
			if (count($_SESSION)) echo '<tr><td style="border-bottom:1px dotted #999;"><a href="javascript:;" onclick="document.getElementById(\'uniadmin-show-session\').style.display=\'block\';">show SESSION</a> <pre id="uniadmin-show-session" style="display:none">' . print_r($_SESSION, true) . '</pre></td></tr>';
			if (count($_COOKIE)) echo '<tr><td style="border-bottom:1px dotted #999;"><a href="javascript:;" onclick="document.getElementById(\'uniadmin-show-cookie\').style.display=\'block\';">show COOKIE</a> <pre id="uniadmin-show-cookie" style="display:none">' . print_r($_COOKIE, true) . '</pre></td></tr>';
			echo '</table>';
		}
		exit;
	}

	public static function formatBacktrace($returnContent = false) {
		if (DEBUG_MODE) {
			$content = '<table cellpadding="3" cellspacing="0" class="uniadmin_debug" style="width:100%;margin:20px 0 0;background:#EEE;font: 12px Tahoma;text-align:left;">
					<tr><td style="background:#555;color:#FFF;">&raquo; UniAdmin ' . self::getVersion() . ' - Trace log</td></tr>';
		}
		foreach (debug_backtrace(false) as $level => $info) {
			$message = (isset($info['file']) ? $info['file'] . ':' . $info['line'] . ' - ' : '') . (isset($info['class']) ? $info['class'] . '::' : '') . $info['function'] . ' (';
			foreach ($info['args'] as $i => $arg) {
				if ($i) $message .= ', ';
				if (is_object($arg)) $message .= '#' . get_class($arg);
				else if (is_array($arg)) $message .= print_r($arg, true);
				else $message .= $arg;
			}
			$message .= ')';
			if (!$level && (!isset(UniAdmin::app()->config['log']['logErrors']) || UniAdmin::app()->config['log']['logErrors'])) {
				UniAdmin::app()->log->write($message);
			}
			if (DEBUG_MODE) {
				$content .= '<tr><td>' . $message . '</td></tr>';
			} else {
				break;
			}
		}
		if (DEBUG_MODE) {
			$content .= '</table>';
		}
		if ($returnContent) {
			return DEBUG_MODE ? $content : null;
		} else {
			if (DEBUG_MODE) {
				echo $content;
			}
			exit;
		}
	}

	public static function exceptionHandler($e) {
		$e->getMessage();
	}
}

function t($id) {
	global $dictionary;

	if (isset($dictionary)) {
		return isset($dictionary[$id]) ? $dictionary[$id] : $id;
	} else {
		return $id;
	}
}

function uniAdminExceptionHandler($e) {
	if (DEBUG_MODE) {
		$content = '<table cellpadding="3" cellspacing="0" class="uniadmin_debug" style="width:100%;margin:20px 0 0;background:#EEE;font: 12px Tahoma;text-align:left;">
				<tr><td style="background:#555;color:#FFF;">&raquo; UniAdmin ' . UniAdmin::getVersion() . ' - Exception trace log</td></tr>';
		$content .= '<tr><td>In file ' . $e->getFile() . ' on line ' . $e->getLine() . ':</td></tr>';
		$content .= '<tr><td>'. get_class($e) . ': "' . $e->getMessage() . '"</td></tr>';
		$content .= '<tr><td>&nbsp;</td></tr>';
		if (is_array($e->getTrace())) {
			foreach ($e->getTrace() as $level => $info) {
				$message = (isset($info['file']) ? $info['file'] . ':' . $info['line'] . ' - ' : '') . (isset($info['class']) ? $info['class'] . '::' : '') . $info['function'] . ' (';
				foreach ($info['args'] as $i => $arg) {
					if ($i) $message .= ', ';
					if (is_object($arg)) $message .= '#' . get_class($arg);
					else if (is_array($arg)) $message .= print_r($arg, true);
					else $message .= $arg;
				}
				$message .= ')';
				$content .= '<tr><td>' . $message . '</td></tr>';
			}
		}
		echo $content;
	}
}
