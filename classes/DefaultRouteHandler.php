<?php

class DefaultRouteHandler implements IRouteHandler {
	private $_route = array();
	private $_originalRoute = array();
	private $_aliasedRoute = array();
	private $_queryString;
	private $_parameters = array();
	private $_language;

	public $urlSeparator = '/';

	public function __construct() {
		$uri = $this->getCurrentUri();
		if (substr($uri, 0, 1) == '/') {
			$uri = substr($uri, 1);
		}
		if (strpos($uri, '?') !== false) {	// van querystring
			$this->_queryString = substr($uri, strpos($uri, '?') + 1);
			$uri = substr($uri, 0, strpos($uri, '?'));
		}
		$uri = preg_split('/\\' . $this->urlSeparator . '/', $uri, -1, PREG_SPLIT_NO_EMPTY);
		$config = UniAdmin::app()->config;
		$indexModule = isset($config['routehandler']['indexModule']) ? $config['routehandler']['indexModule'] : '';
		$defaultModule = $config['routehandler']['defaultModule'];
		if (!empty($uri)) {
			$this->_originalRoute = $uri;
			if (array_key_exists('aliases', $config) &&
				array_key_exists($uri[0], $config['aliases']) &&
				is_array($config['aliases'][$uri[0]])
			) {	// teljes link alias
					$aliasParts = $config['aliases'][$uri[0]];
					if (isset($aliasParts[1]) && !is_null($aliasParts[1])) {	// action
						$uri = array();
						$uri[0] = isset($aliasParts[0]) && $aliasParts[0] ? $aliasParts[0] : $indexModule;
						$uri = array_merge($uri, (array) $aliasParts[1], array_slice($this->_originalRoute, count($aliasParts[1])));
					} else {
						$uri[0] = isset($aliasParts[0]) && $aliasParts[0] ? $aliasParts[0] : $indexModule;
					}
					if (isset($aliasParts[2]) && $aliasParts[2]) {	// nyelv
						array_unshift($uri, $aliasParts[2]);
					}
					if (isset($aliasParts[3])) {	// querystring
						if (is_array($aliasParts[3])) {
							$this->_queryString = http_build_query($aliasParts[3]);
							$_GET = array_merge($_GET, $aliasParts[3]);
						} else {
							$this->_queryString = $aliasParts[3];
							parse_str($aliasParts[3], $queryString);
							$_GET = array_merge($_GET, $queryString);
						}
					}
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('changing route to alias ' . implode('/', $uri) . ($this->_queryString ? '?' . $this->_queryString : ''), 'framework');
					}
			}

			if (preg_match('/^[a-z]{2}$/i', $uri[0])) {	// lehetséges, hogy nyelv az első elem
				if (UniAdmin::app()->cache->isEnabled() && UniAdmin::app()->cache->exists('uniAdminLanguageInfo-' . $uri[0])) {
					$this->_language = strtolower(array_shift($uri));
				} else {
					$found = Db::sqlBool(sprintf("
						SELECT COUNT(*)
						FROM UniadminLanguages
						WHERE id = '%s'",
						$uri[0]
					));
					if ($found) {	// van ilyen nyelv -> beállítás
						$this->_language = strtolower(array_shift($uri));
					}
				}
			}

			if (!empty($uri)) {
				$this->_aliasedRoute = $uri;
				if (strpos($uri[0], ';') !== false) {
					$uri[0] = substr($uri[0], 0, strpos($uri[0], ';'));
				}

				if (array_key_exists('aliases', $config) &&
					array_key_exists($uri[0], $config['aliases']) &&
					!is_array($config['aliases'][$uri[0]])) {	// csak modul alias
						$uri[0] = $config['aliases'][$uri[0]];
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('changing module to alias ' . $uri[0], 'framework');
						}
				}

				$moduleName = ucfirst(strtolower($uri[0]));
				if (!file_exists('modules/' . $moduleName . '/' . $moduleName . 'Controller.php')) {	// nincs kontroller megadva
					array_unshift($uri, $defaultModule);
				}
			} else if ($indexModule) {
				$uri[] = $indexModule;
			} else if ($defaultModule) {
				$uri[] = $defaultModule;
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('no default route handler defined', 'framework', 2);
				}
			}
		} else if ($indexModule) {
			$uri[] = $indexModule;
		} else if ($defaultModule) {
			$uri[] = $defaultModule;
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('no default route handler defined', 'framework', 2);
			}
		}
		$this->_route = $uri;
	}

	public function getRoute() {
		return $this->_route;
	}

	public function getOriginalRoute() {
		return $this->_originalRoute;
	}

	public function getAliasedRoute() {
		return $this->_aliasedRoute;
	}

	public function getLanguage() {
		return $this->_language;
	}

	public function getCurrentModule($useAliases = false) {
		$module = ucfirst(strtolower($this->_route[0]));
		if (strpos($module, ';') !== false) {
			$module = substr($module, 0, strpos($module, ';'));
		}
		if ($useAliases && 
			isset(UniAdmin::app()->config['aliases']) && 
			is_array(UniAdmin::app()->config['aliases']) && 
			in_array($module, UniAdmin::app()->config['aliases'])) {
				return array_search($module, UniAdmin::app()->config['aliases']);
		} else {
			return $module;
		}
	}

	public function getCurrentAction($stripBySemicolon = true) {
		if (isset($this->_route[1])) {
			if (!$stripBySemicolon || strpos($this->_route[1], ';') === false) {
				return 'action' . ucfirst(strtolower($this->_route[1]));
			} else {
				return 'action' . ucfirst(strtolower(substr($this->_route[1], 0, strpos($this->_route[1], ';'))));
			}
		} else {
			return 'actionDefault';
		}
	}

	public function getCurrentUri($stripQuerystring = false) {
		$uri = urldecode($_SERVER['REQUEST_URI']);
		if (!$stripQuerystring || strpos($uri, '?') === false) {
			return $uri;
		} else {
			return substr($uri, 0, strpos($uri, '?'));
		}
	}

	public function getFullRoute($useAliases = false) {
		$route = '';
		if (isset($this->_baseUrl)) {
			// ha más nyelvű az admin felület, akkor ahhoz alkalmazkodunk
			// nem túl szép megoldás, btw
			$langPrefix = Setting::getDefaultLanguage() == LANG ? '' : LANG . $this->urlSeparator;
			$route .= $this->urlSeparator . $langPrefix . $this->_baseUrl . $this->urlSeparator;
		} else if (LANG != Setting::getDefaultLanguage()) {
			$route .= $this->urlSeparator . LANG;
		}
		if ($useAliases 
			&& isset(UniAdmin::app()->config['aliases'])
			&& is_array(UniAdmin::app()->config['aliases']) 
			&& in_array($this->_route[0], UniAdmin::app()->config['aliases'])
		) {
			$route .= $this->urlSeparator . array_search($this->_route[0], UniAdmin::app()->config['aliases']) . $this->urlSeparator;
		} else {
			$route .= $this->urlSeparator . $this->_route[0];
		}
		if (count($this->_route) > 1) {
			$route .= $this->urlSeparator . implode($this->urlSeparator, array_slice($this->_route, 1));
		}
		return $route;
	}

	public function getQueryString($asArray = false, $arrayKey = null) {
		if (!$asArray) return $this->_queryString;
		parse_str($this->_queryString, $parts);
		return is_null($arrayKey) ? $parts : (isset($parts[$arrayKey]) ? $parts[$arrayKey] : null);
	}

	public function getParameter($id, $offset = 2) {
		$route = array_slice($this->_route, $offset);
		for ($i = 0; $i < count($route); $i += 2) {
			if ($route[$i] == $id) {
				return isset($route[$i + 1]) ? $route[$i + 1] : null;
			}
		}
		return false;
	}

	public function getParameters($offset = 2) {
		$parameters = array();
		$route = array_slice($this->_route, $offset);
		for ($i = 0; $i < count($route); $i += 2) {
			$parameters[$route[$i]] = isset($route[$i + 1]) ? $route[$i + 1] : null;
		}
		return $parameters;
	}

	public function setRoute($route) {
		if (is_array($route)) {
			$this->_route = $route;
		}
		return $this;
	}

	public function setBaseUrl($url) {
		$this->_baseUrl = $url;
		return $this;
	}

	public function createUrl($module, $parameters = array(), $language = null, $queryString = array(), $hash = null, $goFrontend = false) {
		$urlParts = array();
		if (empty($language)) {
			$language = LANG;
		}
		$urlParts[] = $language;
		if (isset($this->_baseUrl) && (!defined('IS_BACKEND') || !$goFrontend)) {
			$urlParts[] = $this->_baseUrl;
		}
		if (!defined('IS_BACKEND') || $goFrontend) {
			$indexModule = strtolower(UniAdmin::app()->config['routehandler']['indexModule']);
			$defaultModule = strtolower(UniAdmin::app()->config['routehandler']['defaultModule']);
			$aliasesIndex = defined('IS_BACKEND') ? 'frontendAliases' : 'aliases';
		} else {
			$indexModule = '';
			$defaultModule = '';
			$aliasesIndex = 'aliases';
		}

		$aliasHasLanguage = false;
		if ($module) {
			$urlParts[] = $module;
			if (is_null($parameters)) {
				$parameters = array();
			} else if (!is_array($parameters)) {
				$parameters = array($parameters);
			}
			foreach ($parameters as $key => $parameter) {
				if (is_string($key)) {	// szöveges kulcs esetén paraméter befűzése
					$urlParts[] = $key;
				}
				$urlParts[] = $parameter;
			}

			// aliasok ellenőrzése
			if (isset(UniAdmin::app()->config[$aliasesIndex]) && 
				is_array(UniAdmin::app()->config[$aliasesIndex])) {
				foreach (UniAdmin::app()->config[$aliasesIndex] as $aliasUrl => $aliasData) {
					if (!is_array($aliasData)) {
						$aliasData = array($aliasData);
					}

					// modul szerint ellenőrzünk
					if (isset($urlParts[1]) && $urlParts[1] == $aliasData[0]) {
						if (isset($aliasData[1]) && !is_array($aliasData[1])) {
							$aliasData[1] = array($aliasData[1]);
						}

						// ha más nyelvre mutat az alias, akkor nem őt kerestük
						if (isset($aliasData[2]) && $aliasData[2] != $language) {
							continue;
						}
						// ha másak a paraméterek, akkor nem őt kerestük (ha több paraméter van, továbbengedi)
						if (isset($aliasData[1]) && $aliasData[1] != array_slice($urlParts, 2, count($aliasData[1]))) {
							continue;
						}
						// ha más a querystring, akkor nem őt kerestük (ha más paraméterek is vannak, továbbengedi)
						if (isset($aliasData[3]) && array_diff_assoc($aliasData[3], $queryString)) {
							continue;
						}

						// egyéb esetben illeszkedő aliast találtunk
						// csak akkor cseréljük, ha az nem a defaultModule vagy az indexModule
						$hasParameters = count($aliasData) > 1 && !is_null($aliasData[1]);
						if ($hasParameters || !in_array(strtolower($aliasData[0]), array($defaultModule, $indexModule))) {
							$urlParts[1] = $aliasUrl;
						}

						// ha nyelvet is cserélni kell
						if (isset($aliasData[2])) {
							$urlParts[0] = $aliasData[2];
							$aliasHasLanguage = true;
						}

						// egyéb paraméterek cseréje
						if (isset($aliasData[1])) {
							$urlParts = array_merge(
								array_slice($urlParts, 0, 2),
								array_slice($urlParts, 2 + count($aliasData[1]))
							);
						}

						// ha querystring is van, töröljük az aliasolt kulcsokat
						if (isset($aliasData[3])) {
							foreach ($aliasData[3] as $key => $value) {
								unset($queryString[$key]);
							}
						}
						break;
					}
				}
			}
		}

		// index modul eltávolítása, ha nincs további paraméter megadva
		if (isset($urlParts[1]) && count($urlParts) == 2 && strtolower($urlParts[1]) == $indexModule) {
			unset($urlParts[1]);
		}

		// default modul eltávolítása, ha nincs további paraméter megadva
		if (isset($urlParts[1]) && count($urlParts) == 3 && strtolower($urlParts[1]) == $defaultModule) {
			unset($urlParts[1]);
		}

		// nyelv eltávolítása, ha az az alapértelmezett
		if ($aliasHasLanguage || $urlParts[0] == Setting::getDefaultLanguage()) {
			array_shift($urlParts);
		}

		$url = '/' . implode($this->urlSeparator, $urlParts);

		if (!empty($queryString)) {
			$url .= '?' . http_build_query($queryString);
		}

		if (!is_null($hash)) {
			$url .= '#' . $hash;
		}
		return $url;
	}
}
