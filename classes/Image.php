<?php
/**
 * uniAdmin Images class
 *
 * @author Kenéz László <info@kenlas.hu>
 * @copyright Copyright (c) 2008-2011, Kenéz László | www.uniadmin.hu
 * @package uniAdmin
 */


/**
 * Képkezelő osztály
 */
class Image {
	private $_filename;
	private $_extension;
	private $_image = false;
	private $_changed = true;
	private $_isTransparent = false;

	/**
	 * Konstruktor - lehetőség a fájlok automatikus megnyitására
	 */
	public function __construct($filename = null) {
		if (!is_null($filename)) {
			$this->open($filename);
		}
	}

	/**
	 * Megnyit egy képfájlt, és betölti azt
	 * @param string $file
	 * @return Image|bool
	 */
	public function open($path) {
		if (!file_exists($path)) return false;
		if ($this->isCodeInjectionAttack($path)) return false;

		$info = getimagesize($path);
		switch ($info[2]) {
			case 1:
					$this->_extension = 'gif';
					$this->_image = imagecreatefromgif($path);
				break;
			case 2:
					$this->_extension = 'jpg';
					$this->_image = imagecreatefromjpeg($path);
				break;
			case 3:
					$this->_extension = 'png';
					if ($this->detectPngTransparency($path)) {	// megjegyezzük, hogy átlátszó volt
						$this->_isTransparent = true;
					}
					$this->_image = imagecreatefrompng($path);
				break;
			default:
					return false;
				break;
		}
		$parts = explode('.', $path);
		if (count($parts) > 1) {
			array_pop($parts);
		}
		$this->_filename = implode('.', $parts);
		return $this;
	}

	/**
	 * Beágyazott rosszindulatú kódok elleni védelem
	 * @param string $path
	 */
	 private function isCodeInjectionAttack($path) {
	 	if (!preg_match('/^.*\.gif/i', $path)) return false;

	 	$file = file_get_contents($path);

	 	if (stristr($file, '<?')) return true;
	 	return false;
	 }

	/**
	 * PNG átlátszóság ellenőrzése
	 * http://www.jonefox.com/blog/2011/04/15/how-to-detect-transparency-in-png-images/
	 */
	private function detectPngTransparency($path) {
		if (strlen($path) == 0 || !file_exists($path)) {
			return false;
		}

		if (ord(file_get_contents($path, false, null, 25, 1)) & 4) {
			return true;
		}

		$contents = file_get_contents($path);
		if (stripos($contents, 'PLTE') !== false && stripos($contents, 'tRNS') !== false) {
			return true;
		}

		return false;
	}

	public function exists() {
		return (bool) $this->_image;
	}

	public function getFileName() {
		return $this->_filename;
	}

	/**
	 * Visszaadja a betöltött képfájl méretét
	 * @return array|bool
	 */
	public function getSize() {
		if (!$this->_image) {
			return false;
		}
		return array(imagesx($this->_image), imagesy($this->_image));
	}

	/**
	 * A betöltött képet átméretezi
	 * @param int $maxx az átméretezett kép maximális szélessége
	 * @param int $maxy az átméretezett kép maximális magassága
	 * @return Image|bool
	 */
	public function resample($maxx,$maxy) {
		if (!$this->_image) {
			return false;
		}

		$size = $this->getSize();
		if ($size[0]<=$maxx && $size[1]<=$maxy) {
			$this->_changed = false;
			return $this;
		}
		$newsize = array();
		if ($size[0]>$size[1]) {	/* fekvő */
			$newsize[1] = number_format($size[1]/($size[0]/$maxx),0,'.','');
			$newsize[0] = $maxx;
			if ($newsize[1]>$maxy) {
				$newsize[0] = number_format($newsize[0]/($newsize[1]/$maxy),0,'.','');
				$newsize[1] = $maxy;
			}
		} else {	/* álló */
			$newsize[0] = number_format($size[0]/($size[1]/$maxy),0,'.','');
			$newsize[1] = $maxy;
			if ($newsize[0]>$maxx) {
				$newsize[1] = number_format($newsize[1]/($newsize[0]/$maxx),0,'.','');
				$newsize[0] = $maxx;
			}
		}
		$newim = imagecreatetruecolor($newsize[0], $newsize[1]) or die(UniAdmin::formatBacktrace('error creating image'));
		if ($this->_isTransparent) {
			imagecolortransparent($newim, imagecolorallocatealpha($newim, 0, 0, 0, 127));
			imagealphablending($newim, false);
			imagesavealpha($newim, true);
		}
		imagecopyresampled($newim, $this->_image, 0, 0, 0, 0, $newsize[0], $newsize[1], $size[0], $size[1]) or die(UniAdmin::formatBacktrace('unable to resize image'));
		$this->_image = $newim;
		return $this;
	}

	/**
	 * Kivág a kép közepéből egy megadott méretűt
	 */
	public function crop($maxX, $maxY, $hPos = 'center', $vPos = 'center', $moveY = 0) {
		$newim = imagecreatetruecolor($maxX, $maxY);
		if ($this->_isTransparent) {
			imagecolortransparent($newim, imagecolorallocatealpha($newim, 0, 0, 0, 127));
			imagealphablending($newim, false);
			imagesavealpha($newim, true);
		}
		if ($hPos == 'left') {
			$moveX = 0;
		} else if ($hPos == 'right') {
			$moveX = imagesx($this->_image) - $maxX;
		} else {
			$moveX = ceil((imagesx($this->_image) - $maxX) / 2);
		}
		if ($vPos == 'top') {
			$moveY = $moveY;
		} else if ($vPos == 'bottom') {
			$moveY = imagesy($this->_image) - $maxY;
		} else {
			$moveY = ceil((imagesy($this->_image) - $maxY) / 2);
		}
		imagecopyresampled($newim, $this->_image, 0, 0, $moveX, $moveY, $maxX, $maxY, $maxX, $maxY) or die(UniAdmin::formatBacktrace('unable to resize image'));
		$this->_image = $newim;
		return $this;
	}

	/**
	 * Kivág egy maximális méretű négyzetet a kép közepéből
	 */
	public function cropMax() {
		$width = imagesx($this->_image);
		$height = imagesy($this->_image);
		if ($width > $height) {
			$this->crop($height, $height);
		} else {
			$this->crop($width, $width);
		}
		return $this;
	}

	/**
	 * Elforgatja a képet. A forgatás az óramutató járásával ellentétes irányban történik
	 * @param int $angle a forgatás szöge (fokban mérve)
	 */
	public function rotate($angle) {
		$newim = imagerotate($this->_image, $angle, 0);
		if ($this->_isTransparent) {
			imagecolortransparent($newim, imagecolorallocatealpha($newim, 0, 0, 0, 127));
			imagealphablending($newim, false);
			imagesavealpha($newim, true);
		}
		$this->_image = $newim;
		return $this;
	}

	/**
	 * Kép tükrözése vízszintesen vagy függőlegesen
	 * @param string $direction a tükrözés iránya 'horizontal' vagy 'vertical'
	 */
	public function flip($direction = 'horizontal') {
		if ($direction != 'horizontal' && $direction != 'vertical') {
			return $this;
		}

		$width = imagesx($this->_image);
		$height = imagesy($this->_image);

		$newim = imagecreatetruecolor($width, $height);

		if ($this->_isTransparent) {
			imagecolortransparent($newim, imagecolorallocate($newim, 0, 0, 0, 127));
			imagealphablending($newim, false);
			imagesavealpha($newim, true);
		}

		if ($direction == 'horizontal') {
			imagecopyresampled($newim, $this->_image, 0, 0, ($width - 1), 0, $width, $height, 0 - $width, $height);
		} else {
			imagecopyresampled($newim, $this->_image, 0, 0, 0, ($height -1), $width, $height, $width, 0 - $height);
		}

		$this->_image = $newim;

		return $this;
	}

	/**
	 * A betöltött képet elküldi a kimenetre vagy egy fájlba
	 * @param string $out {gif|jpg|png => ekkor az stdout-ra írja a képet, egyébként megpróbálja létrehozni a fájlt.
	 * 						*.jpg és path/to/file.* formátumokat is kezel }
	 * @return bool
	 */
	public function save($out = '', $force = true) {
		if (!$this->_image) {
			return false;
		}

		if (empty($out)) {
			$out = $this->_extension;
		}

		$parts = explode('.', $out);
		if (in_array($out, array('gif', 'jpg', 'png'))) {
			$ext = $out;
		} else if (count($parts) == 1) {
			$ext = $this->_extension;
		} else {
			$ext = array_pop($parts);
		}

		if ($this->_isTransparent) {
			if ($ext == 'png') {	// PNG esetén átlátszóság mentése
				imagecolortransparent($this->_image, imagecolorallocatealpha($this->_image, 0, 0, 0, 127));
				imagealphablending($this->_image, false);
				imagesavealpha($this->_image, true);
			} else {	// ha nem PNG-ként mentjük, akkor fehér hátteret teszünk mögé
				$tempImage = $this->_image;
				$tempSize = array(imagesx($tempImage), imagesy($tempImage));
				$this->_image = imagecreatetruecolor($tempSize[0], $tempSize[1]);
				// @todo a jövőben ezt a színt kaphatná paraméterként is
				$bg = imagecolorallocate($this->_image, 255, 255, 255);
				imagefilledrectangle($this->_image, 0, 0, $tempSize[0], $tempSize[1], $bg);
				imagecopyresampled($this->_image, $tempImage, 0, 0, 0, 0, $tempSize[0], $tempSize[1], $tempSize[0], $tempSize[1]);
			}
		}

		if ($out == 'gif') {
			if (ob_get_contents() !== false) {
				ob_end_clean();
			}
			header("Content-type: image/gif");
			imagegif($this->_image);
			return true;
		} else if ($out == 'jpg') {
			if (ob_get_contents() !== false) {
				ob_end_clean();
			}
			header("Content-type: image/jpeg");
			imagejpeg($this->_image, null, 100);
			return true;
		} else if ($out == 'png') {
			if (ob_get_contents() !== false) {
				ob_end_clean();
			}
			header("Content-type: image/png");
			imagepng($this->_image);
			return true;
		} else {
			// Figyelem! Nem átméretezett GIF képek esetén is FALSE a visszatérési érték
			if (!$this->_changed && $this->_extension == $ext && (!$force || $ext == 'gif')) {
				return false;
			}

			$filename = implode('.', $parts);
			if ($filename == '*') {
				$filename = $this->_filename;
			}
			if ($ext == '*' || ($ext != 'gif' && $ext != 'jpg' && $ext != 'jpeg' && $ext != 'png')) {
				$ext = $this->_extension;
			}
			if ($ext == 'gif' || $ext == 'jpg' || $ext == 'jpeg' || $ext == 'png') {
				if (file_exists($filename . '.' . $ext)) {
					@unlink($filename . '.' . $ext);
				}
				switch ($ext) {
					case 'gif':
						return imagegif($this->_image, $filename . '.' . $ext);
					break;
					case 'jpg':
					case 'jpeg':
						return imagejpeg($this->_image, $filename . '.' . $ext, 100);
					break;
					case 'png':
						return imagepng($this->_image, $filename . '.' . $ext);
					break;
				}
			}
		}
		return false;
	}

	/**
	 * Visszaadja a belső képfájlt további műveletek elvégzéséhez
	 */
	public function getImage() {
		return $this->_image;
	}

	/**
	 * Felülírja a belső képfájlt
	 * @param image $image PHP által létrehozott image resource
	 */
	public function setImage($image) {
		$this->_image = $image;
		$this->_changed = true;
		return $this;
	}

	/**
	 * Megállapítja, hogy kép fájlról van-e szó
	 */
	public static function checkFormat($file) {
		$mimeType = File::getMimeType($file);
		return in_array($mimeType, array('image/gif', 'image/jpeg', 'image/png'));
	}
}
