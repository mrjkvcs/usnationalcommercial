<?php
class BreederInfo {

	protected static $menu = array(
		'breed1'         => 'PRIMARY BREED:',
		'breed2'         => 'OTHER BREED 1:',
		'breed3'         => 'OTHER BREED 2:',
		'yrsPrimary'     => 'YRS BREEDING PRIMARY:',
		'yrsTot'         => 'TOT YRS BREEDING:',
		'yrsAvg'         => 'AVG LITTERS A YR:',
		'numDogs'        => 'NUM DOGS OWNED:',
		'numChampions'   => 'NUM CHAMPIONS BRED:',
		'numPerformance' => 'NUM PERFORMANCE:',
		'titledDogs'     => 'TITLED DOGS BRED:',
		'hasApproved'    => 'APPROVED BITCHES:',
		'hasBoarding'    => 'BOARDING SERVICE:',
		'hasGrooming'    => 'GROOMING SERVICE:',
		'hasRescue'      => 'RESCUE SERVICE:',
		'hasAgility'     => 'AGILITY:',
		'hasObedience'   => 'OBEDIENCE:',
		'hasBehavior'    => 'BEHAVIOR:',
		'hasCompanion'   => 'COMPANION:',
		'hasHerding'     => 'HERDING:',
		'hasSecurity'    => 'SECURITY:',
		'hasTracking'    => 'TRACKING:',
		'hasMantrailing' => 'MANTRAILING:',
		'hasDeaf'        => 'HANDICAPPED DEAF:',
		'hasBlind'       => 'HANDICAPPED BLIND:',
		'hasOther'       => 'HANDICAPPED OTHER:',
	);

	public static function getMenu() {
		return self::$menu;
	}

	public static function getTitle($id) {
		return self::$menu[$id];
	}

	public static function getBreedName($id) {
		return self::Breed($id)['name'];
	}

	public static function getBreedText($id) {
		return self::Breed($id)['idText'];
	}

	protected static function Breed($id) {
		return Db::sqlRow(sprintf("
			SELECT name, idText
			FROM Breed
			WHERE id = %d",
			$id
		));
	}
}