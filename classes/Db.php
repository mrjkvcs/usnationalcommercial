<?php

/**
 * Az adatbázis műveletek lekezelésére szolgáló osztály
 */
class Db {
	private static $isConnected = false;
	private static $connectionInfo = array();
    private static $transactionDepth = 0;
    private static $transactionForceFail = false;
	private static $currentConnection = 'default';
	private static $connections = array();
	private static $sqlDebugMode = false;

	/**
	 * Kapcsolódás az adatbázishoz. A kapcsolódás addig nem történik meg ténylegesen, amíg nincs konkrét SQL kérés
	 * @param string $database
	 * @param string $username
	 * @param string $password
	 * @param string $host
	 * @param string $charset
	 * @return bool
	 */
	public static function connect($database, $username = '', $password = '', $host = 'localhost', $charset = 'utf8', $connection = null) {
		self::$connectionInfo = array(
			'database' => $database,
			'username' => $username,
			'password' => $password,
			'host' => $host,
			'charset' => $charset,
			'connection' => $connection,
		);
		return true;
	}

	/**
	 * Ellenőrzi a kapcsolódás állapotát, és szükség esetén kapcsolódik az adatbázishoz
	 */
	protected static function checkConnection() {
		if (self::$isConnected) {
			return true;
		}

		$database = isset(self::$connectionInfo['database']) ? self::$connectionInfo['database'] : '';
		$username = isset(self::$connectionInfo['username']) ? self::$connectionInfo['username'] : '';
		$password = isset(self::$connectionInfo['password']) ? self::$connectionInfo['password'] : '';
		$host = isset(self::$connectionInfo['host']) ? self::$connectionInfo['host'] : 'localhost';
		$charset = isset(self::$connectionInfo['charset']) ? self::$connectionInfo['charset'] : 'utf8';
		$connection = isset(self::$connectionInfo['connection']) ? self::$connectionInfo['connection'] : null;

		if (is_null($connection)) {
			$connection = self::$currentConnection;
		}
		if ($username && $password) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('connecting to mysql server, connection: ' . $connection, 'db');
			}
			self::$connections[$connection] = new mysqli($host, $username, $password, $database);
			if (!self::$connections[$connection]) {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('error connecting to mysql server', 'db', 3);
				}
				return false;
			} else {
				self::$isConnected = true;
			}
		}
		if (!self::error($connection) && $charset) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('setting database charset ' . $charset, 'db');
			}
			self::$connections[$connection]->set_charset($charset);
		}
	}

	/**
	 * Beállítja vagy letiltja a belső debug módot
	 * @param bool $state
	 */
	public static function setDebugMode($state = false) {
		self::$sqlDebugMode = $state;
	}

	/**
	 * Kiválasztja az aktuális kapcsolatot
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return bool
	 */
	public static function setCurrentConnection($connection) {
		if (isset(self::$connections[$connection])) {
			self::$currentConnection = $connection;
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('setting database connection to ' . $connection, 'db');
			}
			return true;
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('unknown database connection ' . $connection, 'db');
		}
		return false;
	}

	/**
	 * Visszaadja az aktuális kapcsolatot
	 * @return resource
	 */
	public static function getCurrentConnection() {
		return self::$currentConnection;
	}

	/**
	 * Felvesz egy új kapcsolatot a belső kapcsolat-listára
	 * @param string $connection
	 */
	public static function addConnection($connection) {
		self::$connections[$connection] = 0;
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('added database connection ' . $connection, 'db');
		}
	}

	/**
	 * Escape-eli a paraméterként kapott szöveget
	 * @param string $connection a kapcsolat belső azonosítója
	 * @param string $string
	 */
	public static function escapeString($string, $connection = null) {
		self::checkConnection();
		return self::$connections[$connection ?: self::$currentConnection]->real_escape_string($string);
	}

	/**
	 * Visszaadja a legutolsó hibát
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return string
	 */
	public static function error($connection = null) {
		self::checkConnection();
		return self::$connections[$connection ?: self::$currentConnection]->error;
	}

	/**
	 * Kiválaszt egy adatbázist
	 * @param string $database
	 * @param string $connection a kapcsolat belső azonosítója
	 */
	public static function selectDb($database, $connection = null) {
		self::checkConnection();
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('selecting database ' . $database, 'db');
		}
		return self::$connections[$connection ?: self::$currentConnection]->select_db($database);
	}

	/**
	 * Betölt egy adott rekordot a paraméterlista alapján
	 */
    public static function sqlLoad($table, $id, $idField = 'id') {
		self::checkConnection();
		$sql = sprintf("
		SELECT
		*
		FROM `".$table."`
		WHERE
		`%s` = %d
	   ",$idField,$id);
		$returnRow = self::sqlRow($sql);
		if (!$returnRow) {
			//die('Missing id `'.$id.'` from table `'.$table.'`');
			throw new Error('Missing id `'.$id.'` from table `'.$table.'`');
		}
		return $returnRow;
	}

	/**
	 * SQL parancs végrehajtása
	 * @param string $query a végrehajtandó utasítás
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return int|resource
	 */
	public static function sql($query, $connection = null) {
		self::checkConnection();
		$query = trim($query);

		if (DEBUG_MODE && self::$sqlDebugMode) {
			UniAdmin::addDebugInfo('executing sql query ' . $query, 'db');
		}
		$result = self::$connections[$connection ?: self::$currentConnection]->query($query);
		if (self::error($connection)) {
			if (DEBUG_MODE) {
				echo UniAdmin::formatBacktrace(self::error($connection));
				UniAdmin::flushDebugInfos();
			}
			exit;
		}

		// visszatérési érték a kérésnek megfelelő
		if (strtoupper(substr($query, 0, 6)) == 'UPDATE'
			|| strtoupper(substr($query, 0, 6)) == 'DELETE'
			|| strtoupper(substr($query, 0, 7)) == 'REPLACE'
			|| strtoupper(substr($query, 0, 13)) == 'INSERT IGNORE'
		) {
			return self::affectedrows();
		} else if (strtoupper(substr($query, 0, 6)) == 'INSERT') {
			return self::insertid();
		} else {
			return $result;
		}
	}

	/**
	 * SQL parancs végrehajtása és az eredmény első rekordjának visszaadása a megadott formátumban
	 * @param string $query a végrehajtandó utasítás
	 * @param string $fetchType az eredmény formátuma: row, array, assoc vagy object
	 */
	public static function sqlRow($query, $fetchType = null) {
		$result = self::sql($query);
		return self::loop($result, $fetchType);
	}

	/**
	 * SQL parancs végrehajtása és az eredmény első rekordjának az első elemének visszaadása
	 * @param string $query az SQL lekérés
	 * @return string|bool
	 */
	public static function sqlField($query) {
		$result = self::sql($query);
		$rs = self::loop($result, 'row');
		return is_array($rs) && isset($rs[0]) ? $rs[0] : false;
	}

	/**
	 * SQL parancs végrehajtása és az eredmény első rekordjának az első elemének logikai értékének visszaadása
	 * @param string $query az SQL lekérés
	 * @return bool
	 */
	public static function sqlBool($query) {
		$result = self::sql($query);
		$rs = self::loop($result, 'row');
		return is_array($rs) && isset($rs[0]) ? (bool) $rs[0] : false;
	}

	/**
	 * @param resource egy SQL parancs eredménye
	 * @param string $fetchType az eredmény formátuma: row, array, assoc vagy object
	 */
	public static function loop($result, $fetchType = 'assoc') {
		self::checkConnection();
		if ($result) {
			switch ($fetchType) {
				case 'row':		return $result->fetch_row();
					break;
				case 'array':	return $result->fetch_array();
					break;
				case 'object':	return $result->fetch_object();
					break;
				case 'assoc':
				default:		return $result->fetch_assoc();
					break;
			}
		} else return false;
	}

	/**
	 * Az eredményhalmaz adott sorához ugrik
	 * @param resource $result
	 * @param int $position
	 */
	public static function dataSeek($result, $position) {
		self::checkConnection();
		return $result->data_seek($position);
	}

	/**
	 * Visszaadja az eredményhalmaz számosságát
	 * @param resource $result
	 * @return int
	 */
	public static function numRows($result) {
		self::checkConnection();
		return $result->num_rows;
	}

	/**
	 * Visszaadja az eredményhalmaz mezőinek számát
	 * @param resource $result
	 * @return int
	 */
	public static function numFields($result) {
		self::checkConnection();
		return $result->field_count;
	}

	/**
	 * Visszaadja a legutolsó lekérdezés által képzett insert_id-t, vagy ha nem volt auto_increment mező, akkor true-t
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return string|bool
	 */
	public static function insertId($connection = null) {
		self::checkConnection();
		$insertId = self::$connections[$connection ?: self::$currentConnection]->insert_id;
		return $insertId === 0 ? true : $insertId;
	}

	/**
	 * Visszaadja a legutolsó lekezelés által érintett sorok számát
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return int
	 */
	public static function affectedRows($connection = null) {
		self::checkConnection();
		return self::$connections[$connection ?: self::$currentConnection]->affected_rows;
	}

	/**
	 * Visszaadja a mező sorszáma alapján a mező nevét
	 * @param resource $result
	 * @param int $column
	 * @return string
	 */
	public static function fieldName($result, $column) {
		self::checkConnection();
		$field = $result->fetch_field_direct($column);
		return $field ? $field->name : false;
	}

	/**
	 * Visszaadja a mező sorszáma alapján a mező flagjeit
	 * @param resource $result
	 * @param int $column
	 * @return string
	 */
	public static function fieldFlags($result, $column) {
		self::checkConnection();
		$field = $result->fetch_field_direct($column);
		return $field ? $field->flags : false;
	}

	/**
	 * Visszaadja a mező sorszáma alapján a táblát, amelybe tartozik
	 * @param resource $result
	 * @param int $column
	 * @return string
	 */
	public static function fieldTable($result, $column) {
		self::checkConnection();
		$field = $result->fetch_field_direct($column);
		return $field ? $field->table : false;
	}

	/**
	 * Tranzakció kezdete
	 */
    public static function trBegin() {
		self::checkConnection();
        if (0 === self::$transactionDepth ) {
            if (DEBUG_MODE) {
                UniAdmin::addDebugInfo('DbTransaction::BEGIN<pre>'
                           .'</pre>', 'db', 3,__FILE__,__LINE__);
            }
            self::sql('BEGIN');
            self::$transactionForceFail = false;
        }
        else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('Nested transactions not supported!!!','db',3);
			}
        }

        ++self::$transactionDepth;
	}

	/**
	 * Tranzakció lezárása
	 * @param bool $commit COMMIT vagy ROLLBACK
	 */
	public static function trEnd($commit = true) {
		self::checkConnection();
        if (DEBUG_MODE) {
            UniAdmin::addDebugInfo('DbClass transaction end<pre>'
                       . var_export(self::$transactionDepth,true)."\n"
                       . var_export($commit,true)."\n"
                       .'</pre>', 'db',3,__FILE__,__LINE__);
        }
        if ( 1 > self::$transactionDepth ) {
            throw new Error('Database transaction end without start!');
        }
        if ( 1 === self::$transactionDepth ) {
            if (!self::$transactionForceFail && $commit) {
                if (DEBUG_MODE) {
                    UniAdmin::addDebugInfo('DbTransaction::COMMIT <pre>'
                               .'</pre>', 'db',3,__FILE__,__LINE__);
                }
                self::sql('COMMIT');
            }
            else {
                if (DEBUG_MODE) {
                    UniAdmin::addDebugInfo('DbTransaction::ROLLBACK <pre>'
                               .'</pre>', 'db',3,__FILE__,__LINE__);
                }
                self::sql('ROLLBACK');
            }
        }
        self::$transactionForceFail = $commit && self::$transactionForceFail;
        --self::$transactionDepth;
	}

	/**
	 * Ellenőrzi, hogy egy tábla létezik-e
	 * @param string $table
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return bool
	 */
	public static function tableExists($table, $connection = null) {
		self::checkConnection();
		$result = self::sql(sprintf("SHOW TABLES LIKE '%s'", $table), $connection);
		return (bool) $result->num_rows;
	}

	/**
	 * Visszaadja a mező sorszáma alapján a táblát, amelybe tartozik
	 * @param string $connection a kapcsolat belső azonosítója
	 * @return string
	 */
	public static function getServerInfo($connection = null) {
		self::checkConnection();
		return self::$connections[$connection ?: self::$currentConnection]->server_info;
	}

	/**
	 * Tömbbé konvertál egy rekordszettet
	 * @param resource|string $result
	 * @param bool $associative
	 * @param string $fetchType
	 * @return array
	 */
	public static function toArray($result, $associative = false, $fetchType = 'assoc') {
		if (is_string($result)) {	// ha szöveg, előbb lefuttatjuk
			$result = self::sql($result);
		}
		$ret = array();
		$num = self::numfields($result);
		if ($associative && $num <= 2) {
			while ($rs = self::loop($result, 'row')) {
				$ret[$rs[0]] = $num == 2 ? $rs[1] : $rs[0];
			}
		}
		else if ($associative) {
			while ($rs = self::loop($result, $fetchType)) {
				$ret[current($rs)] = $rs;
			}
		}
		else {
			while ($rs = self::loop($result, $fetchType)) {
				$ret[] = $num > 1 ? $rs : current($rs);
			}
		}
		return $ret;
	}

	/**
	 * Elkészít egy INSERT típusú lekérdezést
	 * @return string
	 */
    public static function buildInsert(array $values, $tableName = null) {
        $fieldList = array();
        $valueList = array();
        foreach ($values as $fieldName => $value) {
            $fieldList[] = '`'.$fieldName.'`';
            $valueList[] = "'".self::escapeString($value)."'";
        }

        if ( !is_null($tableName) ) {
            return 'INSERT INTO `'.$tableName.'`('
                    .implode(',',$fieldList)
                    .') VALUES ('.
                    implode(',',$valueList)
                    .')';
        }
        return array (
          implode(',',$fieldList)
          ,implode(',',$valueList)
        );
    }

	/**
	 * Elkészít egy UPDATE típusú lekérdezést
	 * @return string
	 */
    public static function buildUpdate(array $values) {
        $valueList = array();
        foreach ($values as $fieldName => $value) {
            $valueList[] = '`'.$fieldName.'`'
              . "= '".self::escapeString($value)."'"
            ;
        }
        return implode(',',$valueList);
    }
}
