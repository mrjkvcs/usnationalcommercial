<?php

class NoResult extends Widget {

	public $viewFile = 'puppy.google_large_leaderboard';

	public function __construct($parameters = array()) {

		if (isset($parameters['viewFile'])) {
			$this->viewFile = $parameters['viewFile'];
			if (!strstr($parameters['viewFile'], '.')) {
				$this->viewFile = 'puppy.' . $this->viewFile;
			}
		}

		$this->renderView();
	}

	public function renderView() {
		parent::renderView($this->viewFile);
	}
}