<?php if ($this->_hasCompose) {
	UniAdmin::app()->page->addScriptTag("
		jQuery(function($) {
			$('textarea.compose').tinymce({
				script_url : '/extensions/tinymce/tiny_mce_gzip.php',
				theme : 'advanced',
				language : '" . LANG . "',
				button_tile_map : true,
				relative_urls : false,
				convert_urls : true,
				accessibility_warnings : false,
				entity_encoding : 'raw',
				plugins : 'advhr,advimage,advlink,advlist,autolink,contextmenu,fullscreen,inlinepopups,lists,media,nonbreaking,paste,print,searchreplace,style,tabfocus,table,visualchars,xhtmlxtras" .
				(count($this->getTinyMceExtraPlugins()) ? ',' . implode(',', $this->getTinyMceExtraPlugins()) : '') . "',
				theme_advanced_buttons1 : 'search,replace,|,undo,redo,|,styleselect,formatselect,fontselect,fontsizeselect',
				theme_advanced_buttons2 : 'cut,copy,paste,pastetext,|,bold,italic,underline,strikethrough,|,sub,sup,|,justifyleft,justifycenter,justifyright,justifyfull,|,bullist,numlist,|,outdent,indent,blockquote',
				theme_advanced_buttons3 : 'tablecontrols,|,cleanup,removeformat,|,charmap,advhr,|,forecolor,backcolor,|,styleprops',
				theme_advanced_buttons4 : 'link,unlink,anchor,|,image,|,media,|,code,|,cite,abbr,acronym,del,ins,attribs,|,print,fullscreen" .
				(count($this->getTinyMceExtraPlugins()) ? ',|,' . implode(',|,', $this->getTinyMceExtraPlugins()) : '') . "',
				theme_advanced_toolbar_location : 'top',
				theme_advanced_toolbar_align : 'left',
				theme_advanced_statusbar_location : 'bottom',
				theme_advanced_font_sizes : '8px=8px,9px=9px,10px=10px,11px=11px,12px=12px,13px=13px,14px=14px,16px=16px,18px=18px,20px=20px,22px=22px,24px=24px,26px=26px,28px=28px,30px=30px,36px=36px,48px=48px,72px=72px',
				extended_valid_elements : 'iframe[src|style|width|height|scrolling|marginwidth|marginheight|frameborder]',
				content_css : '/clientside/css/style/admin'," .
				(isset(AdminConfig::$composerSize) && is_array(AdminConfig::$composerSize) ? sprintf(
					'theme_advanced_resizing : false, width : %d, height : %d',
					AdminConfig::$composerSize[0],
					AdminConfig::$composerSize[1]
				) : 'theme_advanced_resizing : true') .
				(!$this->getTinyMceRemoveHost() ? ", remove_script_host:''" : '') .
				(file_exists('modules/Tinymceexternal/TinymceexternalController.php') ? ", external_image_list_url : '/tinymceexternal/images', external_link_list_url : '/tinymceexternal/files'" : '') . "
			});
		});"
	);
}

$className = $this->getClassName();
if ($this->isSent()) {
	$className[] = 'isSent';
} ?>
<form action="<?php echo $this->getAction(); ?>"<?php if (!empty($className)) echo ' class="' . implode(' ', $className) . '"'; ?> method="<?php echo $this->getMethod(); ?>"<?php if ($this->getId()) echo ' id="'.$this->getId().'"'; if ($this->isMultipart()) echo ' enctype="multipart/form-data"'; ?>>
	<?php if ($this->getTitle()): ?>
	<div class="form-title"><?php echo $this->getTitle(); ?></div>
	<?php endif; ?>
	<input type="hidden" name="sent" id="sent" value="1" />
	<div class="form">
		<?php if (!count($this->getGroups())): ?>
		<?php foreach ($this->getElements() as $field):
		if (get_class($field) == 'FHidden'):
			echo $field->renderContent();
		elseif ($field->isRenderable()):	?>
		<div class="form-field" id="s_<?php echo $field->getId(); ?>"<?php if ($field->isHidden()) echo ' style="display:none"'; ?>>
			<div class="field-label"><?php echo $field->renderLabel(); ?></div>
			<div class="field-content"><?php
				echo $field->renderContent();
				echo $field->renderError();
			?></div>
		</div>
		<?php endif; endforeach;
		else:
		$i = 0;
		foreach ($this->getGroups() as $groupId => $groupName): ?>
		<div class="form-group" id="g_<?php echo $groupId; ?>">
			<div class="form-group-title form-group-<?php echo $i ? 'open' : 'close'; ?>"><span><?php echo t($groupName); ?></span></div>
			<div class="form-group-elements"<?php if ($i) echo ' style="display:none;"'; ?>>
				<?php
				foreach ($this->getElements() as $field):
				if (($field->getGroup() && $field->getGroup() != $groupId) || (!$field->getGroup() && $i)) continue;
				if (get_class($field) == 'FHidden'):
					echo $field->renderContent();
				elseif ($field->isRenderable()):	?>
				<div class="form-field g_<?php echo $field->getGroup(); ?>" id="s_<?php echo $field->getId(); ?>"<?php if ($field->isHidden()) echo ' style="display:none"'; ?>>
					<div class="field-label"><?php echo $field->renderLabel(); ?></div>
					<div class="field-content"><?php
						echo $field->renderContent();
						echo $field->renderError();
					?></div>
				</div>
				<?php endif; endforeach; $i++; ?>
			</div>
		</div>
		<?php endforeach; endif; ?>

		<div class="form-buttons">
			<?php if ($this->hasSubmitButton()): ?>
			<input type="submit" <?php if ($this->getId()) echo ' id="'.$this->getId().'-submit"'; ?> value="<?php echo t($this->getSubmitValue()); ?>" class="btn btn-success mt25" />
			<?php endif; ?>
			<?php foreach ($this->getExtraButtons() as $num => $button): ?>
			<input type="button" value="<?php echo t($button); ?>" id="<?php echo $this->getId() . '-button-' . $num; ?>" class="submit" />
			<?php endforeach; ?>
			<?php if ($this->hasResetButton()): ?>
			<input type="reset" value="<?php echo t('button-reset'); ?>" class="submit" />
			<?php endif; ?>
			<?php if (defined('IS_BACKEND') && $this->hasBackButton() && count($this->getBackAction())):
				$backAction = $this->getBackAction();
			?>
			<input type="button" value="<?php echo t('button-back'); ?>" onclick="location.href = '<?php echo Url::link($backAction[0], $backAction[1]); ?>'" class="submit" />
			<?php endif; ?>
		</div>
	</div>
</form>
<?php if ($this->hasAutoFocus()) {
	UniAdmin::app()->page->addScriptTag("$('div.form :input[type!=hidden]').first().focus();");
} ?>