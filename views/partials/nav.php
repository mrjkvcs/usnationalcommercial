<?php $module = UniAdmin::app()->route->getCurrentModule(); ?>
<nav class="navbar navbar-default navbar-fixed-top" role="navigation">
    <div class="container-fluid">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse"
                        data-target="#bs-example-navbar-collapse-1">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <!--                <a class="navbar-brand" href="#">Brand</a>-->
            </div>
            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <?php echo $this->isMenuActive('', 'Home'); ?>
                    <?php echo $this->isMenuActive('listing', 'Listings'); ?>
                    <?php echo $this->isMenuActive('project', 'Projects'); ?>
                    <?php echo $this->isMenuActive('golf', 'Golf League'); ?>
                    <?php echo $this->isMenuActive('contact', 'Contact'); ?>
                </ul>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </div>
    <!-- /.container-fluid -->
</nav>
<?php $data = Util::setBackgroundClass($module); ?>
<div class="<?php echo $data['class']; ?>
    <div class=" container">
<?php if ($module != 'Golf') : ?>
    <div class="container">
        <div class="col-xs-offset-2 col-xs-4 hidden-sm hidden-md hidden-lg"><img src="gfx/us-logo.png" style="height: 270px" /> </div>
        <div class="hidden-xs col-xs-6 col-sm-3 col-md-2 logo-bg"><img src="gfx/us-logo.png" class="img-responsive"/></div>
        <div class="hidden-xs col-sm-9 col-md-10">
            <div class="jumbotron">
                <h1 class="hidden-xs hidden-sm"><?php echo $data['title']; ?></h1>

                <h3 class="hidden-lg hidden-md"><?php echo $data['title']; ?></h3>
            </div>
        </div>
    </div>
<?php endif; ?>
</div>
</div>