<footer class="navbar navbar-inverse footer" role="navigation">
    <div class="container-fluid">
        <div class="container">
            <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                <ul class="nav navbar-nav">
                    <li><a href="<?php echo Url::link(); ?>">Home</a></li>
                </ul>
            </div>
        </div>
    </div>
</footer>
