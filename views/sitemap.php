<?php

echo '<?xml version="1.0" encoding="UTF-8"?>' . "\n";
echo '<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">' . "\n";

foreach ($this->elements as $url) {
	echo sprintf(
		"<url><loc>%s</loc>%s%s</url>\n",
		$url['loc'],
		(isset($url['changefreq']) ? '<changefreq>' . $url['changefreq'] . '</changefreq>' : ''),
		(isset($url['lastmod']) ? '<lastmod>' . $url['lastmod'] . '</lastmod>' : '')
	);
}

echo '</urlset>';
