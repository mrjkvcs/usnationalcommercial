<div class="container">
	<?php $message = $this->getMessage(); ?>
	<h1><?php echo $message ?: t('Az oldal nem tekinthető meg'); ?></h1>

	<div class="http-error-description">
		<?php echo t('Nincs jogosultsága a lap megjelenítéséhez'); ?>
	</div>
</div>