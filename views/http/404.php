<?php $message = $this->getMessage(); ?>
<div class="container bg-color min-height">
	<h1><?php echo $message ?: t('Something Bad Happened'); ?></h1>
	<div class="alert alert-danger">
		<h1><?php echo t('404 Page Not Found'); ?></h1>
		The page you are looking for has been moved or does not exist on our server. You may have reached this page from an outside link or re-direct which is currrently being updated. Please contact customer support at support@choosepuppy.com if you feel this is in error.
	</div>
</div>