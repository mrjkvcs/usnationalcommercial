<?php $message = $this->getMessage(); ?>
<h1><?php echo $message ?: t('Hiba az alkalmazásban'); ?></h1>

<div class="http-error-description">
	<?php echo t('A program futása megszakadt'); ?>
</div>