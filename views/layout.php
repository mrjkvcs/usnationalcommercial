<?php
$module = UniAdmin::app()->route->getCurrentModule();
$isIndex = $module == 'Index';
$separator = UniAdmin::app()->route->urlSeparator;
?>
<!doctype html>
<html lang="<?php echo LANG; ?>">
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<?php if ($this->description): ?>
<meta name="description" content="<?php echo $this->description; ?>">
<?php endif; ?>
<?php if ($this->keywords): ?>
<meta name="keywords" content="<?php echo $this->keywords; ?>">
<?php endif; ?>
<?php if ($this->author): ?>
<meta name="author" content="<?php echo $this->author; ?>">
<?php endif; ?>
<?php if ($this->robots): ?>
<meta name="robots" content="<?php echo $this->robots; ?>">
<?php endif; ?>
<?php if (!$this->imagetoolbar): ?>
<meta name="imagetoolbar" content="no">
<?php endif; ?>
<?php if ($this->favicon): ?>
<link rel="shortcut icon" type="image/x-icon" href="<?php echo $this->favicon; ?>">
<link rel="icon" type="image/x-icon" href="<?php echo $this->favicon; ?>">
<link rel="apple-touch-icon" type="image/x-icon" href="<?php echo $this->favicon; ?>">
<?php /*<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed" rel="stylesheet">*/ ?>
<link href="http://fonts.googleapis.com/css?family=Roboto+Condensed:300italic,400italic,700italic,400,300,700" rel="stylesheet">
<?php endif; ?>
<title><?php echo ($this->title || $this->titleprefix || $this->titlesuffix) ? $this->getFullTitle() . ' :: ' : ''; echo WebsiteInfo::get('title'); ?></title>
<!--<link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/bootstrap/3.2.0/css/bootstrap.min.css"/>-->
<!--<link rel="stylesheet" href="style/application.css"/>-->
<!--<link rel="stylesheet" href="style/main.css"/>-->
<?php if (count(UniAdmin::app()->config['css']['files'])): ?>
<link rel="stylesheet" href="<?php echo Url::link('clientside', 'css', Setting::getDefaultLanguage()); ?>">
<?php endif; ?>
<?php foreach ($this->cssFiles as $css => $mediaType): ?>
<link rel="stylesheet" href="<?php echo (substr($css, 0, 7) == 'http://') ? $css : Url::link('clientside', 'css' . $separator . str_replace('.', $separator, $css), Setting::getDefaultLanguage()); ?>"<?php if ($mediaType && $mediaType != 'all') echo ' media="' . $mediaType . '"'; ?>>
<?php endforeach; ?>
<?php if ($this->extraheader) echo $this->extraheader; ?>
</head>
<body <?php echo $isIndex ? 'class="main"' : null; ?>itemscope itemtype="http://schema.org/WebPage">
<?php

echo preg_replace(
	array(
		"/[\t\n\r]/",
		'/<!--(.*)-->/Uis',
		'/(\ )+/',
	), array(
		'',
		'',
		' ',
	),
	$this->content
);

if (count(UniAdmin::app()->config['javascript']['files'])): ?>
<script src="<?php echo Url::link('clientside', 'javascript', Setting::getDefaultLanguage()); ?>"></script>
<?php endif;
foreach ($this->scriptFiles as $script): ?>
<script src="<?php echo (substr($script, 0, 7) == 'http://') ? $script : Url::link('clientside', 'javascript' . $separator . str_replace('.', $separator, $script), Setting::getDefaultLanguage()); ?>"></script>
<?php endforeach; ?>
<script>
(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
})(window,document,'script','//www.google-analytics.com/analytics.js','ga');
ga('create', 'UA-39897323-1', 'choosepuppy.com');
ga('send', 'pageview');
</script>
</body>
</html>