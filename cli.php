<?php
require('init-cli.php');

$commandClass = ucfirst(str_replace('-', '', $argv[1]));

UniAdmin::import('shell.CliCommand');
UniAdmin::import('shell.' . $commandClass);

if (!class_exists($commandClass)) {
	die('no such command' . $commandClass . "\n\n");
}

$command = new $commandClass($argv, $argc);
$command->run();
