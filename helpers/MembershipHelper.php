<?php

class MembershipHelper {
	protected static $plans = array(
		0 => array(),
		1 => array(),
	);

	public static function getPlans($type = null, $onlyActive = true) {
		$id = $onlyActive ? 1 : 0;
		if (empty(self::$plans[$id])) {
			self::$plans[$id] = Db::toArray(sprintf("
				SELECT id, name, title, type, fee, days, isRecurring, isDefault, gratisDays
				FROM MembershipPlan
				%s
				ORDER BY fee, name",
				$onlyActive ? 'WHERE isActive = 1' : ''
			), true);
		}
		$ret = array();
		foreach (self::$plans[$id] as $planId => $plan) {
			if (is_null($type) || $plan['type'] == $type) {
				$ret[$planId] = $plan;
			}
		}
		return $ret;
	}

	public static function getPlan($id) {
		if (empty(self::$plans[0])) {
			self::getPlans(null, false);
		}
		if (!isset(self::$plans[0][$id])) {
			throw new Error('no such plan: ' . $id);
		}
		return self::$plans[0][$id];
	}

	public static function actualizeSubscriptions() {
		Db::trBegin();
		$users = Db::sql("
			SELECT id
			FROM User
			WHERE TO_DAYS(expireDate) = TO_DAYS(NOW())
		");
		while ($user = Db::loop($users)) {
			$nextPayment = Db::sqlField(sprintf("
				SELECT UM.id, UM.planId
				FROM UserMembership UM
				JOIN Payment P
					ON UM.paymentId = P.id AND P.status = 'finished'
				WHERE UM.userId = %d
				AND UM.isProcessed = 0
				ORDER BY UM.id
				LIMIT 1",
				$user['id']
			));
			if (empty($nextPayment)) {
				// stop subscription
				Db::sql(sprintf("
					UPDATE User
					SET planId = NULL, expireDate = NULL
					WHERE is = %d",
					$user['id']
				));
				continue;
			}

			// go on with next payment plan
			$plan = self::getPlan($nextPayment['planId']);

			if ($plan['isRecurring']) {
				$expireDate = 'NULL';
			} else {
				$currentExpire = Db::sqlField(sprintf("
					SELECT UNIX_TIMESTAMP(expireDate)
					FROM User
					WHERE id = %d",
					$user['id']
				));
				$expireDate = date("'Y-m-d'", $currentExpire + ($plan['days'] + $plan['gratisDays']) * 86400);
			}

			Db::sql(sprintf("
				UPDATE User
				SET planId = '%s', expireDate = %s
				WHERE id = %d",
				$plan['id'],
				$expireDate,
				$user['id']
			));

			Db::sql(sprintf("
				UPDATE UserMembership
				SET isProcessed = 1
				WHERE id = %d",
				$nextPayment['id']
			));
		}
		Db::trEnd();
	}
}
