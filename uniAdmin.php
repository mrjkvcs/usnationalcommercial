<?php

/**
 * uniAdmin betöltő fájl
 */

defined('DEBUG_MODE') or define('DEBUG_MODE', false);

mb_internal_encoding('UTF-8');

header('Content-Type: text/html; charset=utf-8');

require_once('classes/UniAdmin.php');

if (DEBUG_MODE) {
	UniAdmin::addDebugInfo('starting application ' . date('Y-m-d H:i:s'), 'framework');
}

UniAdmin::import(
	'interfaces.ICache',
	'interfaces.IEmailQueue',
	'interfaces.ILogWriter',
	'interfaces.IRouteHandler',
	'classes.AdminApplication',
	'classes.AjaxResponse',
	'classes.BaseModule',
	'classes.Country',
	'classes.Currency',
	'classes.Date',
	'classes.Db',
	'classes.DefaultRouteHandler',
	'classes.Email',
	'classes.EventParameter',
	'classes.File',
	'classes.Image',
	'classes.LogWriter',
	'classes.Setting',
	'classes.SysMessage',
	'classes.Template',
	'classes.Text',
	'classes.Url',
	'classes.Widget',
	'classes.exceptions.Error',
	'classes.exceptions.HttpException',
	'classes.exceptions.Http403Exception',
	'classes.exceptions.Http404Exception',
	'classes.exceptions.Http500Exception',
	'modules.Websiteinfo.WebsiteInfo'
);

spl_autoload_register(array('UniAdmin', 'autoLoadClass'));

set_error_handler(array('UniAdmin', 'formatBacktrace'), E_USER_ERROR & E_RECOVERABLE_ERROR);
set_exception_handler('uniAdminExceptionHandler');

/**
 *  hiányzó függvények létrehozása
 **/
if (!function_exists('apache_request_headers')) {
	function apache_request_headers() {
		$headers = array();
		foreach ($_SERVER as $key => $value) {
			if (substr($key, 0, 5) == 'HTTP_') {
				$key = str_replace(' ','-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5)))));
				$headers[$key] = $value;
			} else {
				$out[$key] = $value;
			}
		}
		return $headers;
	}
}

if (!function_exists('mb_strrev')) {
	function mb_strrev($str) {
		return join('', array_reverse(preg_split('//u', $str)));
	}
}

if (!function_exists('mb_str_split')) {
	function mb_str_split($str, $l = 0) {
		if ($l > 0) {
			$ret = array();
			$len = mb_strlen($str, 'utf-8');
			for ($i = 0; $i < $len; $i += $l) {
				$ret[] = mb_substr($str, $i, $l, 'utf-8');
			}
			return $ret;
		}
		return preg_split('//u', $str, -1, PREG_SPLIT_NO_EMPTY);
	}
}
