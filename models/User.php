<?php

class User extends BaseModel {

    static $table_name = 'User';

    /**
     * User letezik?
     *
     * @param  integer $userId
     *
     * @return boolean
     */
    public static function exists($userId) {
        return Db::sqlBool(sprintf("
			SELECT
				id
			FROM User
			WHERE id = %d",
            $userId
        ));
    }

    /**
     * [isVerified description]
     *
     * @param  [type]  $adId [description]
     *
     * @return boolean       [description]
     */
    public static function isVerified($adId) {
        return Db::sqlBool(sprintf("
			SELECT 
				User.id, Puppy.userId, User.isVerified
			FROM Puppy
				JOIN User
				ON User.id = Puppy.userId
			WHERE Puppy.id = %d
			AND (User.isVerified = 1 OR User.isCertified = 1)",
            $adId));
    }

    /**
     * Ha a user type null akkor meg nics befejezve a regisztracio.
     * @return
     */
    public static function checkInfo() {
        $currModule = UniAdmin::app()->route->getCurrentModule();
        $module = array('Puppy', 'User', 'Breeds', 'Index');

        if (!UniAdmin::app()->user->type && UniAdmin::app()->user->checkLogin()) {
            if (in_array($currModule, $module)) {
                if (!isset($_GET['type'])) {
                    header('Location: http://' . $_SERVER['HTTP_HOST'] . '/facebook/register');
                }
            }
        }
    }

    /**
     * Listed By
     *
     * @param  integer $id
     *
     * @return integer
     */
    public static function getUserInfo($id) {
        return Db::sqlRow(sprintf("
			SELECT id,
				User.firstName,
				User.lastName,
				User.registeredAt,
				User.city,
				User.address,
				User.email,
				User.phone,
				User.website,
				User.countryId,
				User.stateId
			FROM User
			WHERE id = %d",
            $id
        ));
    }

    /**
     * User type-ot kapjuk vissza. Ha nincs bejeletkezve akkor hasznos.
     *
     * @param  integer $userId
     *
     * @return string
     */
    public static function getUserType($userId) {
        return Db::sqlField(sprintf("SELECT User.type FROM User WHERE id = %d", $userId));
    }

    /**
     * A hirdeto userId-t adja vissza a hirdetesi id alapjan.
     *
     * @param  integer $id
     *
     * @return integer
     */
    public static function getOwnerId($id) {
        return Db::sqlField(sprintf("
			SELECT userId
			FROM Puppy
			WHERE id = %d",
            $id
        ));
    }

    /**
     * Az expire date-t ervenyesseget kerjuk.
     *
     * @param  integer $id User id-ja.
     *
     * @return boolean
     */
    public static function getExpireDate($id) {
        return Db::sqlBool(sprintf("
			SELECT 
				expireDate
			FROM User
			WHERE id = %d 
			AND expireDate >= CURDATE()",
            $id
        ));
    }

    /**
     * Ellenorizzuk, hogy a usernek vagy a hirdetonek van-e ervenyes datuma.
     * Ha a masodik parameter TRUE akkor onmagaban hasznalom a fuggvenyt ellenorzesre.
     *
     * @param  integer $id Hirdetes id-ja.
     *
     * @return boolean
     */
    public static function checkExpireDate($id, $user = false) {
        $ownerId = (!$user ? self::getOwnerId($id) : $id);
        $userId = Uniadmin::app()->user->getId();

         return (self::getExpireDate($ownerId) || (self::getExpireDate($userId) && !self::hasPlanId() ) );
//        return self::getExpireDate($ownerId);
    }

    /**
     * planId -t adja vissza $id alapjan. Ha ures akkor a bejelentkezett user-et kapjuk.
     *
     * @param  boolean $id
     *
     * @return boolean
     */
    public static function hasPlanId($id = false) {
        if (empty($id)) {
            $planId = UniAdmin::app()->user->planId;
        } else {
            $planId = Db::sqlField(sprintf("
				SELECT
					planId
				FROM User
				WHERE id = %d",
                $id
            ));
        }

        return strpos($planId, 'basic') !== false;
    }

    /**
     * Ha nem ingyenes az idoszak, akkor vizsgaljuk a jogokat.
     *
     * @param  integer $id User vagy Hirdeto id-ja.
     *
     * @return boolean
     */
    public static function hasPermission($id) {
        return (date('Y-m-d') > FREE_ACCESS ? self::checkExpireDate($id) : true);
    }

    /**
     * Facebook login utan letrehozzuk a usert az adatbazisban.
     * @return integer User ID
     */
    public static function getFacebookId($randomPassword) {

        $location = explode(',', $_SESSION['facebook_profile']['location']['name']);
        $locale = explode('_', $_SESSION['facebook_profile']['locale']);
        $city = trim($location[0]);
        $state = trim($location[1]);
        $country = trim($locale[1]);
        $address = $city . ' ' . $state . ' ' . $country;
        $geocode = Geocoder::getLocation($address);

        return Db::sql(sprintf("
				INSERT INTO User (
					email,
					firstName,
					lastName,
					facebookId,
					registeredAt,
					isActivated,
					password,
					city,
					stateId,
					countryId,
					verify,
					lat,
					lng
				)
				VALUES (
					'%s',
					'%s',
					'%s',
					%d,
					NOW(),
					1,
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s',
					'%s'
				)",
            Db::escapeString($_SESSION['facebook_profile']['email']),
            Db::escapeString($_SESSION['facebook_profile']['first_name']),
            Db::escapeString($_SESSION['facebook_profile']['last_name']),
            $_SESSION['facebook_profile']['id'],
            Db::escapeString($randomPassword),
            Db::escapeString($city),
            State::getStateId(Db::escapeString($state)),
            Db::escapeString(strtolower($country)),
            Db::escapeString('f'),
            Db::escapeString($geocode['lat']),
            Db::escapeString($geocode['lng'])
        ));
    }

    public static function updateLatitude($userId, $latitude) {
        Db::sql(sprintf("
			UPDATE User
			SET lat = '%s' , lng = '%s'
			WHERE id = %d",
            $latitude['lat'],
            $latitude['lng'],
            $userId
        ));
    }

    /**
     * User Verification
     *
     * @param  integer $id User ID
     *
     * @return array
     */
    private static function getSmsCode($userId) {
        return Db::sqlField(sprintf("
			SELECT 
				verify
			FROM User
			WHERE id = %d",
            $userId
        ));
    }

    /**
     * [isVerification description]
     * @return boolean [description]
     */
    public static function isVerification($userId) {

        $msg = 'To get additional more 15 days FREE ACCESS';
        $msgNew = 'Congratulations! You have successfully verified your ';

        $verificationArr = array(
            't' => array(
                'id'      => 'phone-button',
                'name'    => 'Phone Number',
                'title'   => 'Verify Phone Number',
                'title2'  => 'Phone Number Verified',
                'msg'     => $msg,
                'success' => false,
                'url'     => Url::link('user', 'phoneverified')
            ),
            'p' => array(
                'name'    => 'PayPal Account',
                'title'   => 'PayPal Verification',
                'title2'  => 'PayPal Verified',
                'msg'     => $msg,
                'success' => false,
            ),
            'f' => array(
                'name'    => 'Facebook Account',
                'title'   => 'Facebook Verification',
                'title2'  => 'Facebook Verified',
                'msg'     => $msg,
                'success' => false,
                'url'     => Url::link('facebook')
            ),
        );

        $str = str_split(self::getSmsCode($userId));

        foreach ($str as $value) {
            if ($value == 't') {
                $verificationArr[$value]['msg'] = $msgNew . $verificationArr[$value]['name'];
                $verificationArr[$value]['success'] = true;
            }
            if ($value == 'p') {
                $verificationArr[$value]['msg'] = $msgNew . $verificationArr[$value]['name'];
                $verificationArr[$value]['success'] = true;
            }
            if ($value == 'f') {
                $verificationArr[$value]['msg'] = $msgNew . $verificationArr[$value]['name'];
                $verificationArr[$value]['success'] = true;
            }
        }

        return $verificationArr;
    }

    /**
     * A cli-nel kerem - reminder
     * @return array
     */
    public static function getReminder($days, $reminderNumber) {
        return Db::toArray(sprintf("
				SELECT
					User.id,
					User.firstName,
					User.lastName,
					User.email,
					User.registeredAt,
					User.activationCode
				FROM User
				WHERE User.isActivated <> 1
				AND User.registeredAt = (CURDATE() - INTERVAL %d DAY)
				AND User.reminderActivate = %d",
            $days,
            $reminderNumber - 1
        ));
    }

    public static function updateSmsCodeSent() {
        if (Uniadmin::app()->user->smsCodeSent == SMS_SENT) {
            return false;
        }
        $userId = Uniadmin::app()->user->checkLogin() ? Uniadmin::app()->user->getId() : $_SESSION['userId'];
        Db::sql(sprintf("UPDATE User SET smsCodeSent = smsCodeSent + 1 WHERE id = %d", $userId));

        return true;
    }

    public static function updateSmsCodeTyped() {
        if (Uniadmin::app()->user->smsCodeTyped == SMS_TYPED) {
            return false;
        }
        echo $userId = Uniadmin::app()->user->checkLogin() ? Uniadmin::app()->user->getId() : $_SESSION['userId'];
        Db::sql(sprintf("UPDATE User SET smsCodeTyped = smsCodeTyped + 1 WHERE id = %d", $userId));

        return true;
    }

    public static function getBreeders($stateId) {
        return Db::sqlField(sprintf("
			SELECT
				COUNT(User.id)
			FROM User
			WHERE User.type = '%s'
			AND User.stateId = %d",
            Db::escapeString('breeder'),
            $stateId
        ));
    }
}