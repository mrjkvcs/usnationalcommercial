<?php

class GolfWeek extends BaseModel {

    public static function getData($id) {
        return Db::toArray(sprintf("SELECT seasonId, event FROM GolfWeek where id = %d", $id));
    }
}