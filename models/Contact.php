<?php

class Contact extends BaseModel {

    public static function getContact($type = 'B')
    {
        return Db::toArray(sprintf("
            SELECT
                Contact.id,
                Contact.firstName,
                Contact.lastName,
                Contact.title,
                Contact.email,
                Contact.web,
                Contact.phone,
                Contact.sex
            FROM Contact
            WHERE isActive = 1 AND type = '%s' ",
            Db::escapeString($type)
       ));
    }
}