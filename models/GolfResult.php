<?php

class GolfResult extends BaseModel {

    public static function getResults($seasonId = 1)
    {
        return Db::toArray(sprintf("
            SELECT
                GolfResult.userId,
                avg(GolfResult.score) as average,
                sum(GolfResult.money) as money,
                Member.firstName,
                Member.lastName,
                Member.email,
                Member.occupation
            FROM GolfResult
            JOIN Member
                ON Member.id = GolfResult.userId
            WHERE GolfResult.seasonId = %d
            GROUP BY GolfResult.userId, GolfResult.seasonId
            ORDER BY average ",
            $seasonId
        ));
    }

    public static function getDetails($userId, $seasonId)
    {
        return Db::toArray(sprintf("
           SELECT
                GolfResult.weekId,
                GolfResult.score
            FROM GolfResult
            WHERE GolfResult.userId = %d AND GolfResult.seasonId = %d",
            $userId,
            $seasonId
        ), true);
    }

    public static function getRound($eventId)
    {
        return Db::sqlField(sprintf("SELECT GolfSeason.round  FROM GolfSeason WHERE id = %d", $eventId));
    }

    public static function exists($data)
    {
        return Db::sqlBool(sprintf("SELECT * FROM GolfResult WHERE userId = %d AND seasonId = %d AND weekId = %d", $data['userId'], $data['seasonId'], $data['weekId']));
    }
}
