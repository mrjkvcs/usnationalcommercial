<?php

class Project extends BaseModel {

    static $table_name = 'Project';

    public static function getProject()
    {
        return Db::toArray(sprintf("
            SELECT
                Project.id,
                Project.name,
                Project.title,
                Project.rep
            FROM Project
            WHERE isDeleted = 0
            ORDER BY Project.id DESC"
        ));
    }
}