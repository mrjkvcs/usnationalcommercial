<?php

class Golf extends BaseModel {

    static $table_name = 'League';

    public static function getResults() {
        return Db::toArray(sprintf("
            SELECT
                Member.firstName,
                SUM(GolfResult.score) as score
            FROM Member
            JOIN GolfResult
                ON GolfResult.userId = Member.id
            GROUP BY GolfResult.userId
        "));
    }
}
