<?php

class GolfSeason extends BaseModel {

    public static function getRounds($id) {
        return Db::sqlField(sprintf("SELECT round FROM GolfSeason WHERE id = %d", $id));
    }

    public static function getSeasons() {
        return Db::toArray(sprintf("SELECT id, name, isDefault FROM GolfSeason ORDER BY isDefault DESC"));
    }
}
