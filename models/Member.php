<?php

class Member extends BaseModel {

    public static function getMembers()
    {
        return Db::toArray(sprintf("SELECT * FROM Member WHERE Member.paid IS NOT NULL"));
    }

    public static function exists($email) {
        return Db::sqlBool(sprintf("SELECT email FROM Member WHERE email = '%s'", Db::escapeString($email)));
    }
}