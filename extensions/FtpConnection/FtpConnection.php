<?php
//@TODO: save file from ftp to a local dir.
//@TOTO: delete recursively a directory;
class FtpConnection
{
	var $connection;
	var $host;
	var $user;
	var $pass;
	var $current_dir;
	var $port;
	var $timeout;
	var $temp_dir;
	var $ssl = false;
	var $ftpresult = array();
	
    /**
    * Ftp kapcsolat
    * 
    * @param mixed $host
    * @param mixed $user
    * @param mixed $pass
    * @param mixed $dir
    * @param mixed $port
    * @param mixed $autoconnect
    * @param mixed $timeout
    * @param mixed $ssl
    * @return FtpConnection
    */
	function __construct($host, $user, $pass, $dir = '/', $port = 21, $autoconnect = true, $timeout = 30, $ssl = false)
	{
		$this->host = $host;
		$this->user = $user;
		$this->pass = $pass;
		$this->current_dir = $dir;
		$this->port = $port;
		$this->temp_dir = sys_get_temp_dir();
		$this->timeout = $timeout;
		$this->ssl = $ssl;
		
		if($autoconnect)
			$this->ftpConnect();
	}
	
    /**
    * Csatlakozás a kiszolgálóhoz.
    * 
    */
	public function ftpConnect()
	{
		if (!function_exists('ftp_connect'))
		{
			$this->ftpresult[] = '<div class="error">Hiba: nem támogatott az FTP kapcsolatok létesítése</div>';
			return false;
		}

		$this->ftpresult[] = '<div class="success">Kapcsolódás az FTP szerverhez: '.$this->host.':'.$this->port.'. Aktuális mappa: '.$this->current_dir.'</div>';
		
		if($this->ssl)
			$conn_id = @ftp_ssl_connect($this->host, intval($this->port), intval($this->timeout));
		else
			$conn_id = @ftp_connect($this->host, intval($this->port), intval($this->timeout));
		
		if (!$conn_id)
		{
			$this->ftpresult[] = '<div class="error">Hiba: nem lehet FTP kapcsolatot létrehozni.</div>';
			return false;
		}
		else
		{	
			$login_result = @ftp_login($conn_id, $this->user, $this->pass);
			if (!$login_result)
			{
				$this->ftpresult[] = '<div class="error">Hiba: nem lehet bejelentkezni az FTP szerverre!</div>';
				return false;
			}else
				$this->ftpresult[] = '<div class="success">Sikeresen bejelentkezve az FTP szerverre!</div>';

			$this->connection = $conn_id;
			
			return true;
		}
	}
	
    /**
    * passzív mód ki/be-kapcsolása.
    * 
    * @param mixed $state
    */
	public function setPassiveMode($state = true)
	{
		@ftp_pasv($this->connection, $state ? true : false);
	}
	
	function fileExists($filename)
	{
		$rawlist = ftp_rawlist($this->connection, '.', false);
		$filelist = $this->prettyList($rawlist);
		foreach($filelist as $dir)
		{
			foreach($dir as $file)
			{
				if($file['chmod'][0] == '-' && $file['name'] == $filename)
					return true;
			}
		}
		
		return false;
	}
	
	function getFileList($path = null, $array = false)
	{
		if(!is_null($path))
			$this->current_dir = $path;
		
		if(@ftp_chdir($this->connection, $this->current_dir))
		{
			if($this->connection)
			{
				$this->ftpresult[] = '<div class="success">Fájlok lekérése folyamatban...</div>';
				$rawlist = ftp_rawlist($this->connection, '.', false);
				if($array)
					return $this->prettyList($rawlist);
				else
					return $rawlist;
				
			}else
				$this->ftpresult[] = '<div class="error">Nem sikerült letölteni a fájlok listáját!</div>';
		}else
			$this->ftpresult[] = '<div class="error">Nem sikerült átlépni a '.$this->current_dir.' mappába.</div>';
		
		return false;
	}
	
    /**
    * Log megjelenítése.
    * 
    */
	public function printLog()
	{
		foreach($this->ftpresult as $res)
		{
			echo $res;
		}
	}
	
    /**
    * Fájl feltöltési az aktuális könyvtárba.
    * 
    * @param mixed $file
    * @param mixed $upmode
    * @return bool
    */
	public function put($file, $upmode = 'ascii')
	{
		$this->ftpresult[] = '<div class="success">`'.$file.'` feltöltése '.$upmode.' módban ide: '.$this->current_dir.'.</div>';
		$result = @ftp_put($this->connection, basename($file), $file, strtolower($upmode) == 'ascii' ? FTP_ASCII : FTP_BINARY);
		if($result)
			$this->ftpresult[] = '<div class="success">`'.$file.'` feltöltve '.$upmode.' módban ide: '.$this->current_dir.'.</div>';
		else
			$this->ftpresult[] = '<div class="error">`'.$file.'` feltöltése nem sikerült '.$upmode.' módban ide: '.$this->current_dir.'.</div>';
		
		return $result;
	}
	
    /**
    * Fájl letöltése az aktuális könyvtárból.
    * 
    * @param mixed $file
    * @param mixed $mymode
    * @return string
    */
	public function get($file, $mymode = 'ascii')
	{
		
		
		$temp_file = $this->temp_dir . '/' . $file;
		$result = @ftp_get($this->connection, $temp_file, $file, strtolower($mymode) == 'ascii' ? FTP_ASCII : FTP_BINARY);
		if($result)
		{
			$this->ftpresult[] = '<div class="success">`'.$file.'` letöltése '.$mymode.' módban ide: '.$temp_file.'</div>';
			return file_get_contents($temp_file);
		}else
			$this->ftpresult[] = '<div class="error">Nem sikerült letölteni a(z) `'.$file.'`-t!</div>';
		
		return false;
	}
	
    /***
    * Belépés egy mappába.
    * 
    * @param mixed $name
    */
	public function setCurrentDir($name)
	{
		if(@ftp_chdir($this->connection, $name))
		{
			$this->ftpresult[] = '<div class="success">Átlépés a '.$name.' mappába.</div>';
			$this->current_dir = $name;
		}
		else
			$this->ftpresult[] = '<div class="error">Nem sikerült átlépni a '.$name.' mappába.</div>';
	}
	
    /**
    * Az aktuális mappa nevének lekérése.
    * 
    */
	public function getCurrentDir()
	{
		return @ftp_pwd($this->connection);
	}
	
    /**
    * Kapcsolat bontása.
    * 
    */
	public function close()
	{
		$this->ftpresult[] = '<div class="success">Kapcsolat bezárva.</div>';
		@ftp_close($this->connection);
	}
	
	
    /**
    * Egy fájl törlése a kiszolgálóról.
    * 
    * @param mixed $file
    */
	public function delete($file)
	{
		$result = @ftp_delete($this->connection, $file);
		if($result)
			$this->ftpresult[] = '<div class="success">`'.$file.'` nevű fájl sikeresen törölve.</div>';
		else
			$this->ftpresult[] = '<div class="error">Nem sikerült törölni a fájlt: `'.$file.'`</div>';
	}
	
    /**
    * Mappa létrehozása az aktuális könyvtárban.
    * 
    * @param mixed $name
    */
	public function makeDir($name)
	{
		$result = @ftp_mkdir($this->connection, $name);
		if($result)
			$this->ftpresult[] = '<div class="success">Mappa `'.$name.'` sikeresen létrehozva.</div>';
		else
			$this->ftpresult[] = '<div class="error">A(z) `'.$name.'` nevű mappát nem sikerült létrehozni!</div>';
	}
	
    /**
    * Mappa törlése az aktuális könyvtárban.
    * 
    * @param mixed $name
    */
	public function removeDir($name)
	{
		$result = @ftp_rmdir($this->connection, $name);
		if($result)
			$this->ftpresult[] = '<div class="success">Mappa `'.$name.'` sikeresen törölve.</div>';
		else
			$this->ftpresult[] = '<div class="error">A(z) `'.$name.'` nevű mappát nem sikerült törölni!</div>';
	}
	
	private function prettyList($data)
	{
		
		foreach ($data as $v)
		{
			$info = array();
			$vinfo = preg_split("/[\s]+/", $v, 9);
		
			if ($vinfo[0] !== "total")
			{
				$info['chmod'] = $vinfo[0];
				$info['files'] = $vinfo[1];
				$info['owner'] = $vinfo[2];
				$info['group'] = $vinfo[3];
				$info['size'] = $vinfo[4];
				//$info['month'] = $vinfo[5];
				//$info['day'] = $vinfo[6];
				//$info['time'] = $vinfo[7];
				$info['last_modified'] = $vinfo[5].' '.$vinfo[6].' '.$vinfo[7];
				$info['name'] = $vinfo[8];
				$rawlist[$info['name']] = $info;
			}
		}
		$dir = array();
		$file = array();
		$return = array();
		if(is_array($rawlist))
		{
			foreach ($rawlist as $k => $v)
			{
				if ($v['chmod']{0} == "d")
					$return['dir'][] = $v;
				if ($v['chmod']{0} == "l")
					$return['link'][] = $v;
				elseif ($v['chmod']{0} == "-")
					$return['file'][] = $v;
			}
		}
		return $return;
	}
}
?>