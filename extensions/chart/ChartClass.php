<?php

class ChartClass {
	// Beállítások
	private $config = array(
		'IMAGEWIDTH' => 670, // Kép szélessége
		'IMAGEHEIGHT' => 170, // Kép magassága
		'CHARTWIDTH' => 600, // Grafikon szélessége
		'CHARTHEIGHT' => 140, // Grafikon magassága
		'CHARTOFFSETX' => 50, // Grafikon eltolása X
		'CHARTOFFSETY' => 5, // Grafikon eltolása Y
		'RULERSNUMMAX' => 5 // Vonalzók maximális száma
	);

	private $chartType = 'LINE'; // Grafikon típusa
	private $renderRulers = true; // Vonalzók kirajzolása
	private $renderPeriods = true; // Periódusok kirajzolása
	private $fontSize = 9; // Betűméret
	private $sqlSource = null; // SQL lekérés
	private $arraySource = null; // Tömb lekérés
	private $transparentBackground = false; // Átlátszó háttér
	private $colorSettings = null; // Szín beállítások

	private $chartData; // Adatok a grafikonhoz
	private $colors; // Színek
	private $fontStyle; // Betűtípusok
	private $chart; // Grafikon
	private $rulers; // Vonalzók

	// Konstruktor
	public function __construct($width = null, $height = null) {
		// Grafikon méretének beállítása, ha megvan adva a szélesség és a magasság
		if (!is_null($width) && !is_null($height)) $this->setChartDimension($width, $height);
	}

	// Destruktor
	public function __destruct() { if ($this->chart) imagedestroy($this->chart); }

	// Grafikon típusának beállítása
	public function setChartType($chartType) {
		$this->chartType = strtoupper($chartType);
		return $this;
	}

	// Grafikon méretének beállítása
	public function setChartDimension($width, $height) {
		$this->config['IMAGEWIDTH'] = $width;
		$this->config['IMAGEHEIGHT'] = $height;
		$this->config['CHARTWIDTH'] = $width - 70;
		$this->config['CHARTHEIGHT'] = $height - 30;
		return $this;
	}

	// Átlátszó háttér beállítása
	public function setTransparentBackground($state = true) {
		$this->transparentBackground = (bool) $state;
		return $this;
	}

	// Szín beállítása
	public function setColor($name, $red, $green, $blue) {
		$this->colorSettings[$name] = array($red, $green, $blue);
		return $this;
	}

	// Betűméret beállítása
	public function setFontSize($fontSize) {
		$this->fontSize = $fontSize;
		return $this;
	}

	// SQL lekérés beállítása
	public function setSqlSource($sqlSource) {
		$this->sqlSource = $sqlSource;
		return $this;
	}

	// Tömb lekérés beállítása
	public function setArraySource($arraySource) {
		$this->arraySource = $arraySource;
		return $this;
	}

	// Csak grafikon eltolás nélkül
	public function outputPureChart() {
		// Offset eltüntetése és szélesség beállítása
		$this->config['CHARTWIDTH'] = $this->config['IMAGEWIDTH'];
		$this->config['CHARTHEIGHT'] = $this->config['IMAGEHEIGHT'];
		$this->config['CHARTOFFSETX'] = $this->config['CHARTOFFSETY'] = 0;
		// Vonalzók kirajzolásának kikapcsolása
		$this->renderRulers = false;
		// Periódusok kirajzolásának kikapcsolása
		$this->renderPeriods = false;
		// Grafikon kirajzolása
		$this->outputChart();
	}

	// Grafikon kirajzolása
	public function outputChart() {
		// Adatok lekérése
		$this->getData();
		// Adatok formázása a grafikon rajzolónak
		$this->formatData();
		// Kép létrehozása
		$this->createImage();
		// Színek beállítása
		$this->initColors();
		// Betűtípusok beállítása
		$this->initFonts();
		// Grafikon rajzolása
		switch ($this->chartType) {
			// Line grafikon
			case 'LINE':
				$this->renderLineChart();
			break;
			// Bar grafikon
			case 'BAR':
				$this->renderBarChart();
			break;
		}
		// Periódusok kirajzolása
		if ($this->renderPeriods) $this->renderPeriods();
		// Kép visszaadása
		header('Content-type: image/png');
		imagepng($this->chart);
		exit;
	}

	// TODO
	// Adatok elkérése JSON-ban
	/*public function getJSON() {
		// Adatok lekérése
		$this->getData();
		// Megjelenítendő adatok formázása
		$viewData = array();
		foreach ($this->chartData['data'] as $d) {
			$viewData[$this->chartType == 'BAR' ? $d['x'] + $this->chartData['barwidth'] : $d['x']] = $d;
			unset($viewData[$d['x']]['x']);
		}
		// Adatok formázása a grafikon rajzolónak
		$this->formatData();
		// Pozicíós adatok formázása
		$posData['CHARTWIDTH'] = $this->config['CHARTWIDTH'];
		$posData['CHARTHEIGHT'] = $this->config['CHARTHEIGHT'];
		$posData['CHARTOFFSETX'] = $this->config['CHARTOFFSETX'];
		$posData['CHARTOFFSETY'] = $this->config['CHARTOFFSETY'];
		foreach ($this->chartData['data'] as $d)
			$posData['points'][$this->chartType == 'BAR' ? $d['x'] + $this->chartData['barwidth'] : $d['x']] = $d['value'];
		// JSON visszaadása
		echo json_encode(array('viewData' => $viewData, 'posData' => $posData));
		exit;
	}*/

	// Adatok lekérése a grafikon rajzolónak
	private function getData() {
		// SQL lekérés ha nem null
		if (!is_null($this->sqlSource)) {
			$resource = Db::sql($this->sqlSource);
			// MySQL resource asszociatív tömbbé konvertálása
			$data = Db::toArray($resource, true);
		}
		// Tömb lekérésnél átadás
		elseif (!is_null($this->arraySource)) $data = $this->arraySource;
		// Ha tömb akkor feldolgozás
		if (is_array($data)) {
			// Nyújtás kiszámolása grafikon típusonként
			switch ($this->chartType) {
				// Vonal
				case 'LINE': $xStretch = $this->config['CHARTWIDTH'] / (count($data) - 1); break;
				// Bar
				case 'BAR': $xStretch = $this->config['CHARTWIDTH'] / count($data); break;
			}
			// Bar grafikon esetén bar-ok szélességének kiszámolása
			if ($this->chartType == 'BAR') {
				if ($xStretch < 3) $this->chartData['barwidth'] = 3;
				elseif ($xStretch > 19) $this->chartData['barwidth'] = 19;
				elseif (!$xStretch % 2) $this->chartData['barwidth'] = $xStretch - 1;
			}
			// Adatok feldolgozása, maximális érték meghatározása
			$this->chartData['max'] = false;
			$i = 0;
			foreach ($data as $xAxis => $yAxis) {
				// X meghatározása nyújtással
				$x = round($i * $xStretch);
				// Max számolása
				if (!$this->chartData['max'] || $yAxis > $this->chartData['max']) $this->chartData['max'] = $yAxis;
				$this->chartData['data'][] = array(
					'x' => round($i * $xStretch), // X pozició
					'xReal' => $xAxis, // X valódi érték
					'y' => $yAxis,	// Y érték
					'yReal' => $yAxis // Y valódi érték
				);
				$i++;
			}
		}
	}

	// Adatok formázása a grafikon rajzolónak
	private function formatData() {
		// Hiba, ha nincs adat
		if (!isset($this->chartData['data'])) throw new Exception('Nincs kirajzolandó adat!');
		// Maximum eltolása
		$this->chartData['max'] += 10;
		// Vonalzók számolása
		$rulerdiff = round($this->chartData['max'] / ($this->config['RULERSNUMMAX'] - 1));
		$this->chartData['max'] = $rulerdiff * ($this->config['RULERSNUMMAX'] - 1);
		// Y egység kiszámolása
		$ypx = $this->config['CHARTHEIGHT'] / $this->chartData['max'];
		// Vonalzó adatok formázása a grafikonnak
		for ($i = 0; $i < $this->config['RULERSNUMMAX']; $i++)
			$this->rulers[] = array(
				'y' => $this->config['CHARTHEIGHT'] - round($i * $rulerdiff * $ypx),
				'value' => number_format($i * $rulerdiff)
			);
		// Adatok formázása a grafikon rajzolónak
		foreach ($this->chartData['data'] as &$d)
			$d['y'] = $this->config['CHARTHEIGHT'] - round($d['y'] * $ypx);
		unset($d);
	}

	// Kép létrehozása
	private function createImage() { $this->chart = imagecreatetruecolor($this->config['IMAGEWIDTH'], $this->config['IMAGEHEIGHT']); }

	// Színek beállítása adatforrásonként
	private function initColors() {
		// Háttér
		if (isset($this->colorSettings['background'])) {
			list($red, $green, $blue) = $this->colorSettings['background'];
			$this->colors['background'] = imagecolorallocate($this->chart, $red, $green, $blue);
		} else{
			$this->colors['background'] = imagecolorallocate($this->chart, 247, 247, 247);
		}
		if ($this->transparentBackground) imagecolortransparent($this->chart, $this->colors['background']);
		imagefill($this->chart, 0, 0, $this->colors['background']);
		// Vonal
		if (isset($this->colorSettings['line'])) {
			list($red, $green, $blue) = $this->colorSettings['line'];
			$this->colors['line'] = imagecolorallocate($this->chart, $red, $green, $blue);
		}
		else $this->colors['line'] = imagecolorallocate($this->chart, 20, 96, 159);
		// Kitöltő
		if (isset($this->colorSettings['fill'])) {
			list($red, $green, $blue) = $this->colorSettings['fill'];
			$this->colors['fill'] = imagecolorallocate($this->chart, $red, $green, $blue);
		}
		else $this->colors['fill'] = imagecolorallocate($this->chart, 180, 200, 214);
		// Szaggatott
		if (isset($this->colorSettings['dashedline'])) {
			list($red, $green, $blue) = $this->colorSettings['dashedline'];
			$this->colors['dashedline'] = imagecolorallocate($this->chart, $red, $green, $blue);
		}
		else $this->colors['dashedline'] = imagecolorallocate($this->chart, 167, 156, 147);
	}

	// Betűtípus beállítása
	private function initFonts() {
		// Méret
		$this->fontStyle['size'] = $this->fontSize;
		// Szín
		$this->fontStyle['color'] = imagecolorallocate($this->chart, 102, 102, 102);
		// Family
		$this->fontStyle['font'] = 'gfx/fonts/arial.ttf';
	}

	// Line grafikon rajzolása
	private function renderLineChart() {
		// Keret
		$first = reset($this->chartData['data']);
		imageline($this->chart, $this->config['CHARTOFFSETX'] + $first['x'], $this->config['CHARTOFFSETY'] + $first['y'], $this->config['CHARTOFFSETX'] + $first['x'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['line']);
		$end = end($this->chartData['data']);
		imageline($this->chart, $this->config['CHARTOFFSETX'] + $end['x'], $this->config['CHARTOFFSETY'] + $end['y'], $this->config['CHARTOFFSETX'] + $end['x'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['line']);
		imageline($this->chart, $this->config['CHARTOFFSETX'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->config['CHARTOFFSETX'] + $this->config['CHARTWIDTH'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['line']);
		for ($i = 0; $i < count($this->chartData['data']) - 1; $i++)
			imageline($this->chart, $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i]['y'], $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i + 1]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i + 1]['y'], $this->colors['line']);
		// Kitöltés a grafikonig
		$first = reset($this->chartData['data']);
		imagefilltoborder($this->chart, $this->config['CHARTOFFSETX'] + $first['x'] + 1, $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 1, $this->colors['line'], $this->colors['fill']);
		// Keret eltűntetése
		imageline($this->chart, $this->config['CHARTOFFSETX'] + $first['x'], $this->config['CHARTOFFSETY'] + $first['y'], $this->config['CHARTOFFSETX'] + $first['x'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['fill']);
		$end = end($this->chartData['data']);
		imageline($this->chart, $this->config['CHARTOFFSETX'] + $end['x'], $this->config['CHARTOFFSETY'] + $end['y'], $this->config['CHARTOFFSETX'] + $end['x'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['fill']);
		imageline($this->chart, $this->config['CHARTOFFSETX'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->config['CHARTOFFSETX'] + $this->config['CHARTWIDTH'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 2, $this->colors['fill']);
		// Vonalzók rajzolása
		if ($this->renderRulers) $this->renderRulers();
		// Grafikon átrajzolása
		imageantialias($this->chart, true);
		for ($i = 0; $i < count($this->chartData['data']) - 1; $i++) {
			imageline($this->chart, $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i]['y'], $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i + 1]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i + 1]['y'], $this->colors['line']);
			imageline($this->chart, $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i]['y'] + 1, $this->config['CHARTOFFSETX'] + $this->chartData['data'][$i + 1]['x'], $this->config['CHARTOFFSETY'] + $this->chartData['data'][$i + 1]['y'] + 1, $this->colors['line']);
		}
		imageantialias($this->chart, false);
	}

	// Bar grafikon rajzolása
	private function renderBarChart() {
		// Vonalzók rajzolása
		if ($this->renderRulers) $this->renderRulers();
		// Bar ok rajzolása
		foreach ($this->chartData['data'] as $data) {
			imagefilledrectangle($this->chart, $this->config['CHARTOFFSETX'] + $data['x'], $this->config['CHARTOFFSETY'] + $data['y'], $this->config['CHARTOFFSETX'] + $data['x'] + $this->chartData['barwidth'], $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'], $this->colors['line']);
			if ($data['y'] + 2 < $this->config['CHARTHEIGHT']) {
				if ($this->chartData['barwidth'] > 5)
					imagefilledrectangle($this->chart, $this->config['CHARTOFFSETX'] + $data['x'] + 2, $this->config['CHARTOFFSETY'] + $data['y'] + 2, $this->config['CHARTOFFSETX'] + $data['x'] + $this->chartData['barwidth'] - 2, $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'], $this->colors['fill']);
				else
					imagefilledrectangle($this->chart, $this->config['CHARTOFFSETX'] + $data['x'] + 1, $this->config['CHARTOFFSETY'] + $data['y'] + 1, $this->config['CHARTOFFSETX'] + $data['x'] + $this->chartData['barwidth'] - 1, $this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'], $this->colors['fill']);
			}
		}
	}

	// Vonalzók rajzolása
	private function renderRulers() {
		// Szaggatott vonal beállítása
		imagesetstyle($this->chart, array($this->colors['dashedline'], $this->colors['background'], $this->colors['background']));
		// Vonalzók rajzolása
		foreach ($this->rulers as $ruler) {
			// Vonalzó
			imageline($this->chart, $this->config['CHARTOFFSETX'] + 0, $this->config['CHARTOFFSETY'] + $ruler['y'], $this->config['CHARTOFFSETX'] + $this->config['CHARTWIDTH'], $this->config['CHARTOFFSETY'] + $ruler['y'], IMG_COLOR_STYLED);
			// Vonalzóhoz tartozó érték kiírása
			$textbox = imagettfbbox($this->fontStyle['size'], 0, $this->fontStyle['font'], $ruler['value']);
			imagefttext($this->chart, $this->fontStyle['size'], 0, $this->config['CHARTOFFSETX'] - round(($textbox[2] - $textbox[0]) / 2) - 24, $this->config['CHARTOFFSETY'] + $ruler['y'] + 4, $this->fontStyle['color'], $this->fontStyle['font'], $ruler['value']);
		}
	}

	// Periódusok kirajzolása
	private function renderPeriods() {
		// Periódusok kiírása
		foreach ($this->chartData['data'] as $data) {
			$textbox = imagettfbbox($this->fontStyle['size'], 0, $this->fontStyle['font'], $data['xReal']);
			imagefttext(
				$this->chart,
				$this->fontStyle['size'],
				0,
				$this->config['CHARTOFFSETX'] + round($data['x'] - ($textbox[2] - $textbox[0]) / 2),
				$this->config['CHARTOFFSETY'] + $this->config['CHARTHEIGHT'] + 15,
				$this->fontStyle['color'],
				$this->fontStyle['font'],
				$data['xReal']
			);
		}
	}
}
