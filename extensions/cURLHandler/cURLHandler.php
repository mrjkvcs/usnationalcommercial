<?php

class cURLHandler {
	protected $handler;
	protected $method = 'get';
	protected $data = array();
	protected $url;
	protected $agent;
	protected $maxRedirs = 10;
	protected $timeout = 30;
	protected $lastResponse;
	protected $getResponse = true;
	protected $getHeaders = false;

	public function __construct(array $parameters = array()) {
		if (isset($parameters['method'])) {
			$this->setMethod($parameters['method']);
		}
		if (isset($parameters['data'])) {
			$this->setData($parameters['data']);
		}
		if (isset($parameters['url'])) {
			$this->setUrl($parameters['url']);
		}
		if (isset($parameters['agent'])) {
			$this->setAgent($parameters['agent']);
		}
		if (isset($parameters['maxRedirs'])) {
			$this->setMaxRedirs($parameters['maxRedirs']);
		}
		if (isset($parameters['timeout'])) {
			$this->setTimeout($parameters['timeout']);
		}
		if (isset($parameters['returnTransfer'])) {
			$this->setReturnTransfer($parameters['returnTransfer']);
		}
		if (isset($parameters['returnHeaders'])) {
			$this->setReturnHeaders($parameters['returnHeaders']);
		}
	}

	public function setMethod($method) {
		if (in_array(strtolower($method), array('get', 'post'))) {
			$this->method = strtolower($method);
		} else {
			throw new Error(sprintf('cURL method %s not allowed', $method));
		}
		return $this;
	}

	public function setData($data) {
		$this->data = $data;
		return $this;
	}

	public function setUrl($url) {
		$this->url = $url;
		return $this;
	}

	public function setAgent($agent) {
		$this->agent = $agent;
		return $this;
	}

	public function setMaxRedirs($redirs) {
		$this->maxRedirs = intval($redirs);
		return $this;
	}

	public function setReturnTransfer($state = true) {
		$this->getResponse = (bool) $state;
		return $this;
	}

	public function setReturnHeaders($state = true) {
		$this->getHeaders = (bool) $state;
		return $this;
	}

	public function getMethod() {
		return $this->method;
	}

	public function getData() {
		return $this->data;
	}

	public function getUrl() {
		return $this->url;
	}

	public function getAgent() {
		return $this->agent;
	}

	public function getLastResponse() {
		return $this->lastResponse;
	}

	public function send() {
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('initializing cURL request', 'curl');
		}
		if (!$this->url) {
			throw new Error('cURL error: no url set');
		}
		$this->handler = curl_init();
		if ($this->method == 'post') {
			curl_setopt($this->handler, CURLOPT_POST, 1);
		}
		if (!empty($this->data)) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cURL request data: ' . print_r($this->data, true), 'curl');
			}
			curl_setopt($this->handler, CURLOPT_POSTFIELDS, is_array($this->data) ? http_build_query($this->data) : $this->data);
		}
		if (!empty($this->agent)) {
			curl_setopt($this->handler, CURLOPT_USERAGENT, $this->agent);
		}
		curl_setopt($this->handler, CURLOPT_URL, $this->url);
		curl_setopt($this->handler, CURLOPT_HEADER, $this->getHeaders);
		if ($this->timeout) {
			curl_setopt($this->handler, CURLOPT_TIMEOUT, $this->timeout); 
		}
		curl_setopt($this->handler, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($this->handler, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($this->handler, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($this->handler, CURLOPT_MAXREDIRS, $this->maxRedirs);
		$this->lastResponse = curl_exec($this->handler);
		if (false && DEBUG_MODE) {
			UniAdmin::addDebugInfo(preg_match('/.*\.(jp?g|png|gif|xls?|pdf|doc?|ppt?)/i', $this->url) ? 'cURL remote file requested: ' . htmlspecialchars($this->url) : 'cURL request raw response: ' . htmlspecialchars($this->lastResponse), 'curl');
		}
		return $this->lastResponse;
	}

	public function getHeaders() {
		if (is_null($this->handler)) {
			throw new Error('cURL error: cannot fetch header before initializing connection');
		}
		return curl_getinfo($this->handler);
	}
}
