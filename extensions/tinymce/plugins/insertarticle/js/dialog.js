tinyMCEPopup.requireLangPack();

var InsertArticleDialog = {
	init : function() {
		var f = document.forms[0];

		// Get the selected contents as text and place it in the input
		//f.articlelist.value = tinyMCEPopup.editor.selection.getContent({format : 'text'});
	},

	insert : function() {
		$.post('/admin/news/pastearticle/', { id: $('#article-id-list').val() }, function(data) {
			tinyMCEPopup.editor.execCommand('mceInsertContent', false, data);
			tinyMCEPopup.close();
		});
	}
};

tinyMCEPopup.onInit.add(InsertArticleDialog.init, InsertArticleDialog);

window.onload = function(){
	$('#articlelist').autocomplete(
		'/admin/news/search',
		{
			'multiple': true,
			'matchContains': true,
			'mustMatch': true,
			'multipleSeparator': '; ',
			'extraParams': {
				'type': 'plain'
			}
		}
	);
	$("#articlelist").result(function(event, data, formatted) {
		$('#article-id-list').val(function(index, value) {
			return value + ';' + data[1];
		});
	});
};
