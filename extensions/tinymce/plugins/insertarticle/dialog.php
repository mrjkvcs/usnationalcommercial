<?php require '../../../../config.php'; 
?><!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<title>{#insertarticle_dlg.title}</title>
	<meta name="Content-type" content="text/html; charset=utf-8" />
	<link rel="stylesheet" type="text/css" href="<?php echo $config['server']; ?>/style/5.0/jquery-autocomplete.css" />
	<script type="text/javascript" src="<?php echo $config['server']; ?>/scripts/5.0/jquery.js"> </script>
	<script type="text/javascript" src="<?php echo $config['server']; ?>/scripts/5.0/jquery-autocomplete.js"> </script>
	<script type="text/javascript" src="../../tiny_mce_popup.js"></script>
	<script type="text/javascript" src="js/dialog.js"></script>
</head>
<body>

<form onsubmit="InsertArticleDialog.insert();return false;" action="#">
	<input type="hidden" name="article-id-list" id="article-id-list" />
	<p>{#insertarticle_dlg.desc}</p>
	<p><input id="articlelist" name="articlelist" size="70" type="text" class="text" /></p>

	<div class="mceActionPanel">
		<input type="button" id="insert" name="insert" value="{#insert}" onclick="InsertArticleDialog.insert();" />
		<input type="button" id="cancel" name="cancel" value="{#cancel}" onclick="tinyMCEPopup.close();" />
	</div>
</form>

</body>
</html>
