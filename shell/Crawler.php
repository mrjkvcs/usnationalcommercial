<?php

class Crawler extends CliCommand {

    protected $url;
    protected $parseUrl;
    protected $hostName;
    protected $ganon;

    public function run() {
        if ($this->argc <= 1) {
            echo "Bad parameters";
            echo "\nUsage php cli.php crawler puppyfind breedId \n\n";
            exit;
        }
        require_once 'extensions/Ganon/Ganon.php';
        require_once 'extensions/Ganon/simple_html_dom.php';
        echo str_repeat('=', 90) . "\n";
        echo "Puppies save from \033[33m{$this->url}\033[0m \n";
        echo str_repeat('=', 90) . "\n";

        if ($this->argv[0] == 'puppyfind') {
            $this->runPuppyFind();
        }

        if ($this->argv[0] == 'nextdaypets') {
            $this->runNextDayPets();
        }

        UniAdmin::app()->cache->delete('listingType-0');
        echo "\n\n" . str_repeat('*', 42) . " DONE " . str_repeat('*', 42) . "\n";
    }

    protected function runPuppyFind() {
        $round = isset($this->argv[2]) ? $this->argv[2] : 1;
        for ($i = 0; $i < $round; $i++) {
            $this->url = 'http://www.' . $this->argv[0] . '.com/for_sale/?breed_id=' . $this->argv[1] . "&order_by=upgrade";
            $this->parseUrl = parse_url($this->url);
            $this->hostName = $this->parseUrl['scheme'] . '://' . $this->parseUrl['host'];

            $rs = '';
            $html = file_get_html($this->url);
            if (empty($html)) {
                echo "\n\033[31m This html doesn't exists! \033[0m \n\n";
                exit;
            }
            foreach ($html->find('a') as $element) {
                if (Util::findMe($element->href, '/view_listing/?list_id')) {
                    if ($rs != $element->href) {
                        $this->url = $this->hostName . $element->href;
                        $this->ganon = str_get_dom(file_get_contents($this->url));
                        $ganon = $this->ganon;
                        $userStatus = Util::findMe(strtolower(strip_tags($ganon('table.bluegrad  tr:nth-child(13) td:last-child', 0)->getInnerText())), 'write a review');
                        if ($userStatus) {
                            $this->save();
                        } else {
                            echo "\n\033[31m User upgrade problem \033[0m";
                            echo "\n" . str_repeat('-', 90);
                        }
                    }
                    $rs = $element->href;
                }
                sleep(3);
            }
        }
    }

    protected function runNextDayPets() {
        $append = isset($this->argv[2]) ? '&page=' . $this->argv[2] : '';
        $breedId = '11' . str_pad($this->argv[1], 5, 0, STR_PAD_LEFT);
        $url = 'http://www.nextdaypets.com/for_sale/?breed=' . $breedId . $append;

        $this->parseUrl = parse_url($url);
        $this->hostName = $this->parseUrl['scheme'] . '://' . $this->parseUrl['host'];
        $rs = '';
        //ebben lesznek az url-k
        $urls = str_get_dom(file_get_contents($url));
        for ($i = 0; $i < 50; $i++) {
            $this->url = $urls('.listing > a', $i)->href;
            $this->ganon = str_get_dom(file_get_contents($this->url));
            if ($rs != $this->url) {
                $rs = $this->url;
                $contactGanon = str_get_dom(file_get_contents($this->getContactUrl($this->url)));
                $permission = $contactGanon('.underline', 0);
                if ($permission === null) {
//                    echo $this->url . ' ' .$contactGanon('.center', 0)->getInnerText() . "\n";
                    $this->save();
                } else {
                    echo "\n\033[31m User upgrade problem \033[0m";
                    echo "\n" . str_repeat('-', 90);
//                    $phone = $permission->getInnerText() . 'Drop off this';
                }
//                echo $url . ' ' . $phone . "\n";
            }
        }

    }

    protected function save() {
        $class = ucfirst($this->getDomainName());
        UniAdmin::import('modules.Admin.modules.Crawler.website.' . $class);
        $website = new $class();
        $website->url = $this->url;
        $website->parseUrl = $this->parseUrl;
        $website->hostName = $this->hostName;
        $website->ganon = $this->ganon;
        $website->userData = $website->getUserData();
        $website->puppyData = $website->getPuppyData();
        $website->paymentData = Util::getPaymentData($website->userData);
        $website->processUser()->processPuppy();
    }

    public function getDomainName() {
        $domain = explode('.', $this->parseUrl['host']);
        array_pop($domain);
        return end($domain);
    }

    protected function getContactUrl($url) {
        $parseUrl = parse_url($url);
        $pathInfo = pathinfo($parseUrl['path']);
        return 'http://' . $parseUrl['host'] . $pathInfo['dirname'] . '/contact.aspx?AdId=' . $pathInfo['filename'] . '&phone=1';
    }

}