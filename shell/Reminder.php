<?php

class Reminder extends CliCommand {

	/**
	 * CLI reminder
	 * emlekezteti a usereket az aktivalasra.
	 * @return [type] [description]
	 */
	public function run() {

		$reminderNumber = $this->argv[0];

		if ($reminderNumber == 1) { $days = 1; $msg=1; }
		elseif ($reminderNumber == 2) { $days = 5; $msg=2; }
		elseif ($reminderNumber == 3) { $days = 10; $msg='one more reminder'; }
		elseif ($reminderNumber == 4) { $days = 30; $msg='last reminder'; }
		else { die("\n\e[31m*** only between [1-4] *** \e[0m \n\n"); }

		$link = 'http://cp-uni.jkl-dev.com/user/activation/id/';

		$userData = User::getReminder($days, $reminderNumber);

		foreach($userData as $data) {

			$data['link'] = $link . $data['id'] .'/code/'. $data['activationCode'];
			$name         = $data['firstName'] .' ' . substr($data['lastName'],0,1);

			$template = new Template('registration', 'en');
			$message = $template->setText($template->getText())
				->addParameter($data)
				->evaluate();

            print_r($message);
			$email = new Email();
			$email
				->to($data['email'])
				->setSubject('Reminder('.$msg.'): Please activate your account')
				->setMessage($message)
				->send();

			Db::sql(sprintf("
				UPDATE User
				SET reminderActivate = %d
				WHERE id = %d",
				$reminderNumber,
				$data['id']
			));

			UniAdmin::app()->log->setType('reminder_' . $reminderNumber);
			UniAdmin::app()->log->write("{$data['id']} \t {$data['firstName']} {$data['lastName']} \t {$data['email']} \t $msg reminder for activation.");
			echo "\n\n" . $name . ' ' . $data['email'] . "\n\n";
		}
	}
}