<?php

class Emailspam extends CliCommand {

	public function run() {

		UniAdmin::import('extensions.cURLHandler.cURLHandler');
		$users = Db::sql("
			SELECT id, email
			FROM User
			WHERE isActivated = 0
		");

		while ($user = Db::loop($users)) {
			$handler = new cURLHandler(array(
				'url' => 'http://www.stopforumspam.com/api?email=' . $user['email'] . '&f=json',
			));
			$response = $handler->send();
			$data = json_decode($response, true);
			if ($data && $data['success'] && $data['email']['appears'] && $data['email']['confidence'] > 80) {
				echo sprintf(
					"%s: found (frequency: %s, confidence: %s)\n",
					$user['email'],
					$data['email']['frequency'],
					$data['email']['confidence']
				);
				Db::sql(sprintf("
					DELETE FROM User
					WHERE id = %d",
					$user['id']
				));
			}
		}
	}
}