<?php

class Sms extends CliCommand {

    protected $hostName = 'http://www.choosepuppy.com';
    protected $country = 'us';
    protected $limit = 5;
    protected $sid = 'AC5027f9d6f8ec8b0a26606bd08b94af04';
    protected $token = '355756cf63950ab392b23cc09e81f69e';
    protected $fromNumber = '17027284553';
    protected $body = [
        '1' => "This is a one time message from ChoosePuppy: Pls. view your listing for free on our site at: {link} ",
        '2' => "Hello, we've included your puppy listings for FREE at ChoosePuppy {link}. This a one time message, thank you.",
        '3' => "You might be interested to view your FREE listing at ChoosePuppy {link} and see what we have to offer. Thank You"
    ];
    protected $template = 1;
    protected $code;
    protected $userId;

    public function run() {
        if (empty($this->argv)) {
            $this->usage();
            exit;
        }
        $this->country = $this->argv[0];
        $this->limit = isset($this->argv[1]) ? $this->argv[1] : $this->limit;
        $this->template = isset($this->argv[2]) ? $this->argv[2] : 1;
        echo str_repeat('=', 88);
        echo "\n\033[36m SMS to User \n\033[0m";
        echo str_repeat('=', 88);
        echo "\n";

        $this->country == 'check' ? $this->checkStatus() : $this->send();

        echo "\n\n" . str_repeat('=', 41);
        echo " DONE ";
        echo str_repeat('=', 41) . "\n";
    }

    protected function usage() {
        echo "Sms usage:\n";
        echo "php cli.php sms country limit template \n\n";
    }

    protected function checkStatus() {
        $results = Db::toArray(sprintf("
            SELECT
                UserEnquiries.userId,
                UserEnquiries.uri
            FROM UserEnquiries
            WHERE UserEnquiries.status = '%s'",
            Db::escapeString('queued')
        ));
        $this->setStatus($results);
    }

    protected function setStatus($results) {
        foreach ($results as $result) {
            $json = $this->setCurl('https://api.twilio.com' . $result['uri'] . '.json');
            Db::sql(sprintf("UPDATE UserEnquiries set UserEnquiries.status = '%s' WHERE userId = %d", $json['status'], $result['userId']));
            echo $json['status'] == 'delivered'
                ? "\n\033[32m{$json['to']}: {$json['status']} \033[0m"
                : "\n\033[31m{$json['to']}: {$json['status']} \033[0m";
        }
    }

    protected function send() {
        UniAdmin::import('modules.User.UserConfig');
        $users = self::getUserData();
        UniAdmin::import('extensions.Twilio.Twilio', true);
        if (!empty($users)) {
            foreach ($users as $user) {
                $isExists = UserEnquiries::find_by_userid($user['userId']);
                $length = true;
                if ($this->country == 'us') {
                    $length = strlen(Util::clearText($user['phone'])) <= 11 ? true : false;
                }
                if ($length) {
                    if (!$isExists) {
                        $this->code = UserConfig::randomPassword(6);
                        $this->userId = $user['userId'];
                        $link = $this->hostName . '/a/code/' . $this->code;
                        $body = str_replace('{link}', $link, trim($this->body[$this->template]));
                        $client = new Services_Twilio($this->sid, $this->token);
                        $message = $client->account->messages->create([
                            'From' => $this->fromNumber,
                            'To'   => $user['phone'],
                            'Body' => $body
                        ]);
                        echo "\n\033[33m{$user['phone']} \033[0m";
                        $this->save($message);
                    } else {
                        echo "\nMessage already has been sent \n";
                    }
                } else {
                    echo "\n\033[31mNot Valid number:{$user['phone']} \033[0m";
                }
            }
        } else {
            echo "\n\033[31mThere is no user! \033[0m";
        }
    }

    protected function save($message) {
        UserEnquiries::create([
            'userId'   => $this->userId,
            'code'     => $this->code,
            'sentAt'   => date('Y-m-d H:i:s'),
            'template' => $this->template,
            'uri'      => $message->uri,
            'status'   => $message->status
        ]);
    }

    protected function setCurl($url) {
        $handle = curl_init();
        curl_setopt($handle, CURLOPT_URL, $url);
        curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($handle, CURLOPT_USERPWD, "$this->sid:$this->token");
        $response = curl_exec($handle);
        curl_close($handle);
        return json_decode($response, true);
    }

    protected function getUserData() {
        return Db::toArray(sprintf("
            SELECT
                User.id as userId,
                User.firstName,
                User.phone
            FROM User
            WHERE NOT EXISTS (SELECT UserEnquiries.userId FROM UserEnquiries WHERE User.id = UserEnquiries.userId)
            AND User.isActive = 1
            AND User.email LIKE '%%%s'
            AND User.countryId = '%s'
            AND User.phone IS NOT NULL
            ORDER BY RAND() LIMIT %d",
            Db::escapeString('choosepuppy.com'),
            Db::escapeString($this->country),
            $this->limit
        ));
    }
}