<?php

class Getnotified extends CliCommand {

	public function run() {
		$rs = self::queryUser();
		print_r($rs);
	}

	public function getData($userId) {

	}

	public function queryUser() {
		return Db::toArray(sprintf("
			SELECT
				BreedNotify.userId,
				BreedNotify.breedId,
				BreedNotify.insertedAt,
				BreedNotify.isDeleted,
				BreedNotify.viewedAt,
				Puppy.insertedAt AS puppyInsertedAt
			FROM BreedNotify
			JOIN Puppy
				ON Puppy.insertedAt > BreedNotify.insertedAt
			"
		));
	}
}