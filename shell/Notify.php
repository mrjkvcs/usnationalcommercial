<?php

/**
 * Email a usereknek.
 */

class Notify extends CliCommand {

	public function run() {
		$line  = 1;
		$users = $this->getUsers();
		foreach ($users as $user) {
			echo "\n $line : {$user['firstName']} {$user['lastName']} | {$user['email']} ";

			// $email = new Email();
			// $email
			// 	->to($user['email'])
			// 	->setSubject('Thank You Email')
			// 	->fromTemplate('loyalty-email', $user, 'en')
			// 	->send();
			sleep(3);
			$line++;
		}

		echo "\n\n";
	}

	public function getUsers() {
		return Db::toArray(sprintf("
			SELECT
				User.firstName,
				User.lastName,
				User.email
			FROM User
			WHERE User.isActivated <> 0"
		));
	}
}