<?php

abstract class CliCommand {
	protected $arv     = array();
	protected $argc    = 0;
	protected $minArgs = 0;
	protected $usage;

	public function __construct($argv, $argc) {
		$this->argv = array_slice($argv, 2);
		$this->argc = $argc - 2;

		if ($this->argc < $this->minArgs) {
			echo "no enough arguments \n\n";
			if (!empty($this->usage)) {
				echo str_repeat('-', 60) . "\n";
				echo $this->usage;
				echo str_repeat('-', 60) . "\n";
			}
			exit;
		}
	}

	abstract public function run();
}