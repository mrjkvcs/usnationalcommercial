<?php

class Puppyview extends CliCommand {

	/**
	 * A kuldott datum es -7 kozott megtekintett puppykat reportolja a gazdinak.
	 * @return
	 */
	public function run() {

		echo "\n\n" . str_repeat('*', 30) . ' MEGTEKINTES '. str_repeat('*', 30);
		$users = self::queryUser();

		foreach($users as $user) {
			echo "\n" . str_repeat('-', 72) . "\n";
			echo "user id: {$user['userId']} {$user['firstName']} {$user['lastName']} {$user['email']}  \n";
			echo str_repeat('-', 72) . "\n";

			$puppies = self::getData($user['userId']);
			$allSum = 0;
			$details = '';
			$detailsData = array();
			$templateType = $user['expireDate'] < date('Y-m-d') ? 'weekly-listing-report' : 'weekly-listing-report-paid';

			foreach($puppies as $puppy) {
				//emailnek allitom itt ossze es teszem $details['text']-be
				$details .= '- ' .$puppy['puppyName'] . ' <em>' . $puppy['breedName'] . '</em> <strong>(' .$puppy['sum'] . ')</strong> visitor(s), Expire Date:<em> ' . $puppy['expireDate'] . '</em> <a href="http://www.choosepuppy.com/puppy/view/' . $puppy['puppyId'] . '">View</a><br />';
				//keprnyore irom ki
				echo '- ' .$puppy['puppyName'] . ' ' . $puppy['breedName'] . ' (' .$puppy['sum'] . ') visitor(s), Expire Date: ' . $puppy['expireDate'] . "\n";
				$allSum += $puppy['sum'];
			}

			$detailsData['sum']  = $allSum;
			$detailsData['text'] = $details;
			$template = new Template($templateType, 'en');
			$message = $template->setText($template->getText())
				->addParameter($detailsData)
				->addParameter($user)
				->evaluate();

			$email = new Email();
			$email
				->to('mrjkvcs@gmail.com')
				->setSubject('Your weekly listing status report...')
				->setMessage($message)
				->send();

			echo str_repeat('=', 15);
			echo "\nOsszesen: {$allSum} \n";
		}
		echo "\n\n";
	}

	protected function queryUser() {
		return Db::toArray(sprintf("
			SELECT
				User.id AS userId,
				User.firstName,
				User.lastName,
				User.email,
				User.expireDate
			FROM PuppyView
			JOIN Puppy
				ON Puppy.id = PuppyView.puppyId
			JOIN User
				ON User.id = Puppy.userId
			WHERE PuppyView.visitedAt BETWEEN CURDATE() - INTERVAL 7 DAY  AND CURDATE()
			GROUP BY User.id"
		));
	}

	protected function getData($userId) {
		return Db::toArray(sprintf("
				SELECT 
					count(*) AS sum,
					PuppyView.puppyId,
					Puppy.puppyName,
					User.expireDate,
					Breed.name AS breedName
				FROM PuppyView
				JOIN Puppy
					ON Puppy.id = PuppyView.puppyId
				JOIN User
					ON User.id = Puppy.userId
				JOIN Breed
					ON Breed.id = Puppy.breedId
				WHERE PuppyView.visitedAt BETWEEN CURDATE() - INTERVAL 7 DAY  AND CURDATE()
				AND Puppy.isDeleted <> 1
				AND User.id = %d
				GROUP BY PuppyView.puppyId",
				$userId
		));
	}
}
