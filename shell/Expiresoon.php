<?php

class Expiresoon extends CliCommand {
	
	protected $days;

	public function run() {
		$this->days = !empty($this->argv) ? $this->argv[0] : 1;
		echo "\n\n" . str_repeat('*', 30) . ' MEGTEKINTES '. str_repeat('*', 30);
		$users = self::queryUser();

		foreach($users as $user) {
			echo "\n" . str_repeat('-', 72) . "\n";
			echo "user id: {$user['userId']} {$user['firstName']} {$user['lastName']} {$user['email']}  \n";
			echo str_repeat('-', 72) . "\n";

			$puppies = self::getData($user['userId']); 
			$allSum = 0;
			$details = '';
			$detailsData = array();

			foreach($puppies as $puppy) {

				echo $updateCode = Puppy::getUpdateCode($puppy);
				
				//emailnek allitom itt ossze es teszem $details['text']-be
				echo $link = 'http://mrjkvcs.choosepuppy.org/puppy/update/id/' . $puppy['puppyId'] . '/code/' . $updateCode;
				$details .= '- ' .$puppy['puppyName'] . ' <em>' . $puppy['breedName'] . '</em> Expire Date:<em> ' . $puppy['expireDate'] . '</em> <a href="' . $link . '">Update</a><br />';
				//keprnyore irom ki
				echo '- ' .$puppy['puppyName'] . ' ' . $puppy['breedName'] . ' Expire Date: ' . $puppy['expireDate'] . "\n";
				$allSum++;
			}

			$user['sum']  = $allSum;
			$user['listings'] = $details;
			$user['days'] = $this->days;
            $user['expireDate'] = '200000';

			$template = new Template('expire-soon', 'en');
			$message = $template->setText($template->getText())
				->addParameter($user)
				->evaluate();

			 $email = new Email();
			 $email
			 	->to($user['email'])
			 	->setSubject('Your listing(s) will expire soon...')
			 	->setMessage($message)
			 	->send();

			echo str_repeat('=', 15);
			echo "\nOsszesen: {$allSum} \n";
		}
		echo "\n\n";
	}

	protected function getData($userId) {
		return Db::toArray(sprintf("
			SELECT
				Puppy.id AS puppyId,
				Puppy.puppyName,
				Puppy.expireDate,
				Puppy.sourceId,
				User.firstName,
				User.lastName,
				Breed.name AS breedName
			FROM Puppy
			JOIN User
				ON User.id = Puppy.userId
			JOIN Breed
				ON Breed.id = Puppy.breedId
			WHERE Puppy.expireDate = (CURDATE() + INTERVAL %d DAY)
			AND User.id = %d
			AND Puppy.sourceId = 0",
			$this->days,
			$userId
		));
	}

	protected function queryUser() {
		return Db::toArray(sprintf("
			SELECT
				User.id AS userId,
				User.firstName,
				User.lastName,
				User.email
			FROM Puppy
			JOIN User
				ON User.id = Puppy.userId
			WHERE Puppy.expireDate = (CURDATE() + INTERVAL %d DAY)
			GROUP BY User.id",
			$this->days
		));
	}
}