<?php

$pager = '<ul class="article-paging pagination">';
if ($this->start) {
	$pager .= '<li><a class="prev" href="' . Url::link('news', '', LANG, array('start' => $this->start - $this->limit)); '">&lt;</a></li>';
}
if ($this->start || $this->start + $this->limit < $this->max) {
	for ($i = 0; $i < ceil($this->max / $this->limit); $i++) {
		$pager .= '<li><a href="' . Url::link('news', '', LANG, array('start' => $i * $this->limit)) . '"' . ($this->start == $i * $this->limit ? ' class="active"' : '') . '>' . ($i + 1) . '</a></li>';
	}
}
if ($this->start + $this->limit < $this->max) {
	$pager .= '<li><a class="next" href="' . Url::link('news', '', LANG, array('start' => $this->start + $this->limit)) . '">&gt;</a></li>';
}
$pager .= '</ul>';

//echo $pager;
?>


<div class="container mt25 min-height">
<?php foreach ($this->articleList as $article): ?>
	<?php $beforeDay = strtotime(date('Y-m-d', strtotime("-14 days"))); ?>
	<div class="well">
		<div class="media" style="padding:20px 0">
			<a class="pull-left" href="<?php echo Url::link('news', array('article', $article['idText'])); ?>">
				<?php if (file_exists(substr($article['picture'], 1))) : ?>
					<img class="img-thumbnail img-responsive" src="/thumbnail/<?php echo $article['picture'] ?>?100" alt="<?php echo ' Puppy for Sale' ?>">
				<?php else: ?>
					<img class="img-thumbnail img-responsive" src="gfx/nopix.png" width="100px" alt="<?php echo ' Puppy for Sale' ?>">
				<?php endif; ?>
				<?php echo $beforeDay < $article['date'] ? '<span class="label label-danger pull-right">New</span> ' : ''; ?>
			</a>
			<div class="media-body">
				<h4 class="media-heading"><a href="<?php echo Url::link('news', array('article', $article['idText'])); ?>" style="color:#000"><?php echo strip_tags($article['title']); ?></a><small> | <?php echo date('Y-m-d',$article['date']); ?></small></h4>
				<?php echo Text::truncate(strip_tags($article['content']), 300, true); ?>
			</div>
		</div>
	</div>
	<div class="well padding-0">
		<p>
            <ul class="list-inline social-2 mb0" >
                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>"  class="share-facebook"><i class="fa fa-fw fa-facebook color-facebook"></i></a></li>
                <li><a href="https://www.twitter.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>" class="share-twitter"><i class="fa fa-fw fa-twitter color-twitter"></i></a></li>
                <li><a href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>" class="share-google-plus"><i class="fa fa-fw fa-google-plus color-google-plus"></i></a></li>
                <li><a href="#" class="share-email-button" data-name="<?php echo $list['puppyName']; ?>" data-id="<?php echo $list['puppyId'] ?>"><i class="fa fa-fw fa-envelope color-envelope"></i></a></li>
            	<li><span class="text-muted"> | </span></li>
            </ul>
		</p>
	</div>
<?php endforeach; ?>
</div>
<?php
//echo $pager;
