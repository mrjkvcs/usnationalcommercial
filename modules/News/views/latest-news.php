<?php foreach ($this->articleList as $article): ?>
	<div class="media" style="padding:20px 0">
		<a class="pull-left" href="<?php echo Url::link('news', array('article', $article['idText'])); ?>">
            <?php if (!empty($article['picture'])) : ?>
			<img class="img-thumbnail img-responsive" src="/thumbnail/<?php echo $article['picture'] ?>?100" alt="<?php echo ' Puppy for Sale' ?>">
            <?php endif; ?>
		</a>
		<div class="media-body">
			<h4 class="media-heading"><a href="<?php echo Url::link('news', array('article', $article['idText'])); ?>" ><?php echo Text::truncate(strip_tags($article['title']),50, true); ?></a></h4>
			<?php echo Text::truncate(strip_tags($article['content']), 260, true); ?>
			<p>
	            <ul class="list-inline social-2 mb0" >
	                <li><a href="https://www.facebook.com/sharer/sharer.php?u=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>"  class="share-facebook"><i class="fa fa-fw fa-facebook color-facebook"></i></a></li>
	                <li><a href="https://www.twitter.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>" class="share-twitter"><i class="fa fa-fw fa-twitter color-twitter"></i></a></li>
	                <li><a href="https://plus.google.com/share?url=<?php echo 'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article', $article['idText'])); ?>" class="share-google-plus"><i class="fa fa-fw fa-google-plus color-google-plus"></i></a></li>
	                <li><a href="#" class="share-email-button" data-name="<?php echo $list['puppyName']; ?>" data-id="<?php echo $list['puppyId'] ?>"><i class="fa fa-fw fa-envelope color-envelope"></i></a></li>
	            </ul>
			</p>
		</div>
	</div>
<?php endforeach; ?>
