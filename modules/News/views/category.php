<h1>
	<a href="<?php echo Url::link('news', array('rss', $this->category['idText'])); ?>">
		<img src="/gfx/icon-rss.png" alt="<?php echo t('RSS hírcsatorna'); ?>" class="rss-icon right" />
	</a>
	<?php echo $this->category['name']; ?>
</h1>

<?php if ($this->category['description']): ?>
<div class="news-category-description">
	<?php echo $this->category['description']; ?>
</div>
<?php endif; ?>

<?php UniAdmin::widget('modules.News.ArticleList', array(
	'category' => $this->category['id'],
)); ?>