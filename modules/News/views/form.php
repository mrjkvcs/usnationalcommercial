<div class="container">
	<h3>Posts and Comments</h3>
<?php
	if (!UniAdmin::app()->user->checkLogin()) {
		SysMessage::displayNotice('You need to Register. It is easy and FREE!');
	} else {
		if (isset($this->success)) {
			SysMessage::displaySuccess('The comment has been sent success!');
		} else {
			$this->form->renderView();
		}
	}
?>
</div>