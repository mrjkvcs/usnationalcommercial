<?php 

$this->parameters->article->renderView();

if (Setting::get('allowComments', 'News')) {
	UniAdmin::widget('modules.News.CommentForm', $this->parameters->article->getId());
	UniAdmin::widget('modules.News.CommentList', $this->parameters->article->getId());
}
