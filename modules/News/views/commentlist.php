<?php if (!empty($this->commentList)): ?>
<div class="container mt25">
	<?php foreach ($this->commentList as $comment): ?>
	<div class="comment-list-item" id="c-<?php echo $comment['id']; ?>">
		<div class="comment-list-info">
		<?php $user = User::getUserInfo($comment['userId']); ?>
			<h4>
				<?php echo $user['firstName'] . ' ' . $user['lastName']; ?>
				<small class="pull-right"> <?php echo Date::format($comment['date'], 'dynamic'); ?></small>
			</h4>
		</div>
		<div class="mb25">
			<div class="comment-list-text">
				<?php if (!$comment['isModerated']): ?>
				<?php echo nl2br(Text::convertLinks($comment['text'])); ?>
				<?php else: ?>
					<div class="alert alert-danger"><?php echo t('Moderate comment'); ?></div>
				<?php endif;?>
			</div>
		</div>
		<hr />
	</div>
	<?php endforeach; ?>
</div>
<?php endif; ?>