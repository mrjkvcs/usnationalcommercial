<?php

$articles = '';
foreach ($this->articleList as $article) {
	$articles .= sprintf(
		'<item>
			<title>%s</title>
			<pubdate>%s</pubdate>
			<link>%s</link>
			<description><![CDATA[%s]]></description>
			<link>%s</link>
		</item>', 
		$article->getTitle(),
		$article->getDate('r'),
		'http://' . $_SERVER['HTTP_HOST'] . Url::link('news', array('article' => $article->getIdText())),
		$article->getLead(),
		$article->getPicture()
	);
}

echo sprintf(
	'<?xml version="1.0" encoding="utf-8" ?>
	<rss version="2.0">
		<channel>
			<title>%s</title>
			<link>http://%s</link>
			<description>%s</description>
			<language>%s</language>
			%s
		</channel>
	</rss>',
	t('NEWS'),
	$_SERVER['HTTP_HOST'],
	t('NEWS'),
	LANG,
	$articles
);
