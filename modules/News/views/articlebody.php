<div class="container mb80">

	<div class="page-header mt0">
		<h3 class="mt0"><?php echo $this->getTitle(); ?></h3>
        <em class="text-muted"><?php echo $this->getDate(); ?> </em>
	</div>

	<div class="article-lead">
		<?php echo $this->getLead(); ?>
	</div>

	<?php if ($this->getPicture()): ?>
	<div class="centered mb20 mt20">
		<a href="/<?php echo $this->getPicture(); ?>" class="lightbox" title="<?php echo $this->getTitle(); ?>">
			<img src="<?php echo Url::link('thumbnail', $this->getPicture() . '?500,500'); ?>" alt="" class="img-thumbnail" />
		</a>
	</div>
	<?php endif; ?>

	<div class="article-body">
		<?php echo $this->getContent(); ?>
	</div>

</div>