<div class="latest-news">
	<?php foreach ($this->articleList as $article): ?>
	<div class="latest-article">
		<div class="latest-article-title">
			<a href="<?php echo Url::link('news', array('article', $article['idText'])); ?>"><?php echo $article['title']; ?></a>
		</div>
		<div class="latest-article-lead">
			<?php echo Text::truncate(strip_tags($article['lead']), 200, true); ?>
		</div>
		<div class="latest-article-more">
			<div style="position:absolute;font-size:10px;font-weight:bold"><?php echo Date::format($article['date'], 'shortnumeric'); ?></div>
			<a href="<?php echo Url::link('news', array('article', $article['idText'])); ?>" class="more" style="font-size:11px"><?php echo t('Continue Reading'); ?></a>
		</div>
	</div>
	<?php endforeach; ?>
</div>
