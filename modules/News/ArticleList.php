<?php

class ArticleList extends Widget {
	public $articleList = array();
	public $viewFile = 'news.articlelist';
	public $limit = 10;
	public $start = 0;
	public $important;

	public function __construct($parameters = array(), $autoLoad = true) {
		UniAdmin::app()->page->addCss('modules.News.news');

		if (isset($_GET['start'])) {
			$this->start = intval($_GET['start']);
		} else if (isset($parameters['start'])) {
			$this->start = intval($parameters['start']);
		}

		if (isset($parameters['limit'])) {
			$this->limit = $parameters['limit'];
		}

		if (isset($parameters['viewFile'])) {
			$this->viewFile = $parameters['viewFile'];
			if (!strstr($parameters['viewFile'], '.')) {
				$this->viewFile = 'news.' . $this->viewFile;
			}
		}

		$sql = array();
		if (isset($parameters['category'])) {
			if (is_array($parameters['category'])) {
				$sql[] = "Article_Category.categoryId IN (" . Db::escapeString(implode(', ', $parameters['category'])) . ")";
			} else if (!is_null($parameters['category'])) {
				$sql[] = "Article_Category.categoryId IN (" . Db::escapeString($parameters['category']) . ")";
			}
		}
		if (isset($parameters['id']) && is_array($parameters['id'])) {
			$sql[] = "Article.id IN (" . Db::escapeString(implode(', ', $parameters['id'])) . ")";
		}
		if (isset($parameters['important'])) {
			$sql[] = ($parameters['important'] ? '' : 'NOT ') . 'Article.isImportant';
		}
		if (isset($parameters['from']) && Date::check($parameters['from'])) {
			$sql[] = sprintf("UNIX_TIMESTAMP(Article.date) >= UNIX_TIMESTAMP('%s')", $parameters['from']);
		}
		if (isset($parameters['to']) && Date::check($parameters['to'])) {
			$sql[] = sprintf("UNIX_TIMESTAMP(Article.date) <= UNIX_TIMESTAMP('%s') + 86400", $parameters['to']);
		}

		$query = sprintf("
			SELECT SQL_CALC_FOUND_ROWS DISTINCT
				Article.id,
				Article.idText,
				Article.title,
				Article.lead,
				Article.content,
				UNIX_TIMESTAMP(Article.date) AS date,
				Article.picture,
				UniadminUsers.name AS author
			FROM Article
			LEFT JOIN UniadminUsers
				ON Article.authorId = UniadminUsers.id, Article_Category
			WHERE
				Article.id = Article_Category.articleId
				AND Article.languageId = '%s'
				AND Article.isActive = 1
				AND Article_Category.categoryId
				AND UNIX_TIMESTAMP(date) <= UNIX_TIMESTAMP(NOW())
				%s
			ORDER BY isImportant DESC, date DESC
			LIMIT %d, %d",
			LANG,
			(count($sql) ? ' AND ' . implode(' AND ', $sql) : ''),
			$this->start,
			intval($this->limit)
		);

		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('article list query <pre>' . $query . '</pre>');
		}
		$result = Db::sql($query);

		$this->max = Db::sqlField("SELECT FOUND_ROWS()");
		$this->pages = ceil($this->max / $this->limit);
		$this->currentPage = ceil($this->start / $this->limit) + 1;

		$this->articleList = Db::toArray($result, true);

		if ($autoLoad) {
			$this->renderView();
		}
	}

	public function renderView() {
		parent::renderView($this->viewFile);
	}
}
