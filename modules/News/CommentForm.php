<?php

class CommentForm extends Widget {
	public $viewFile = 'news.form';
	private $articleId;
	private $loginRequired = false;

	public function __construct($parameters = array(), $autoLoad = false) {
		if (is_array($parameters)) {
			if (isset($parameters['articleId'])) {
				$this->articleId = intval($parameters['articleId']);
			}
			if (isset($parameters['loginRequired'])) {
				$this->loginRequired = (bool) $parameters['loginRequired'];
			}
		} else if (intval($parameters)) {	// talán articleId jött?
			$this->articleId = intval($parameters);
		}

		UniAdmin::import('classes.forms.Form');
		$this->form = new Form();
		$this->form->loadFromFile('news.comment');

		$this->loginCheck = false;
		if ($this->loginRequired) {
			$this->form->removeField('name', 'email');
			if (UniAdmin::app()->user->checkLogin()) {
				$this->loginCheck = true;
			}
		} else {
			$this->loginCheck = true;
		}
		
		if(UniAdmin::app()->user->checkLogin()) {
			$userinfo = UniAdmin::app()->user->getInfo();
			$this->form->getField('name')->setValue($userinfo['firstName'].' '.$userinfo['lastName']);
			$this->form->getField('email')->setValue($userinfo['email']);
		}
		
		if ($this->loginCheck) {
			//ha a mező nincs kitöltve nem robot
			if ($this->form->isSent()) {
				$this->form->queryValues();
				if (!Text::check($this->form->getValue('email'), 'email')) {
					$this->form->setError('email', t('A formátum nem megfelelő'));
				}
				if ($this->form->validateFields() && !$this->form->getValue('checkIt')) {
					if (UniAdmin::app()->user->checkLogin()) {
						$this->form->addDbField('userId', UniAdmin::app()->user->getId());
					}
					if (!isset($_SESSION['comment-form-sent'])) {
						$this->form->addDbField('articleId', $this->articleId);
						$this->form->addDbField('date', date('Y-m-d H:i:s'));
						$this->form->addDbField('ip', $_SERVER['REMOTE_ADDR']);
						$this->form->save();

						$_SESSION['comment-form-sent'] = true;
					}

					$this->success = true;
				}
			} else {
				unset($_SESSION['comment-form-sent']);
			}
		}

		if ($autoLoad) {
			$this->renderView();
		}
	}
}
