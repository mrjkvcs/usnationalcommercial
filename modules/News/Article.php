<?php

class Article extends Widget {
	public function __construct($parameters = array(), $autoLoad = false) {
		if (!is_array($parameters)) {
			$parameters = array('id' => $parameters);
		}
        
		if (!isset($parameters['id'])) {
			return false;
		}

		UniAdmin::app()->page->addCss('modules.News.news');

		if (!isset($parameters['language'])) {
			$parameters['language'] = LANG;
		}

		if (!is_numeric($parameters['id'])) {
			$rs = Db::sqlrow(sprintf("SELECT id FROM Article WHERE idText = '%s'", $parameters['id']));
			if (!$rs) return false;
			$parameters['id'] = $rs['id'];
		}

		$rs = Db::sqlrow(sprintf("
			SELECT Article.*, `UniadminUsers`.`name` AS `author`
			FROM `Article` 
			LEFT JOIN `UniadminUsers` ON `Article`.`authorId` = `UniadminUsers`.`id`
			WHERE `Article`.`id` = %d 
			AND `Article`.`languageId` = '%s' 
			AND `Article`.`isActive` = 1",
			$parameters['id'], $parameters['language']));
		if (!$rs) return false;


		foreach ($rs as $key => $value) {
			$this->{$key} = $value;
		}

		if (isset($this->picture) && substr($this->picture, 0, 1) == '/') {
			$this->picture = substr($this->picture, 1);
		}

		if ($autoLoad) {
			$this->renderView();
		}
	}

	public function getId() {
		return isset($this->id) ? $this->id : null;
	}

	public function getIdText() {
		return isset($this->idText) ? $this->idText : null;
	}

	public function getTitle() {
		return isset($this->title) ? $this->title : null;
	}

	public function getLead() {
		return isset($this->lead) ? $this->lead : null;
	}

	public function getContent() {
		return isset($this->content) ? $this->content : null;
	}

	public function getDate($format = 'full') {
		return isset($this->date) ? Date::format(strtotime($this->date), $format) : null;
	}

	public function getPicture() {
		return isset($this->picture) ? $this->picture : null;
	}

	public function getAuthor() {
		return isset($this->author) ? $this->author : null;
	}

	public function getMetaKeywords() {
		return isset($this->metaKeywords) ? $this->metaKeywords : null;
	}

	public function getMetaDescription() {
		return isset($this->metaDescription) ? $this->metaDescription : null;
	}

	public function getCategory() {
		$this->getAllCategories();
		return count($this->categories) ? current($this->categories) : false;
	}

	public function getAllCategories() {
		if (!isset($this->categories)) {
			$this->categories = array();

			if (isset($this->id)) {
				$query = sprintf("SELECT id 
					FROM ArticleCategory, Article_Category
					WHERE ArticleCategory.id = Article_Category.categoryId
					AND Article_Category.articleId = %d
					ORDER BY position",
					$this->id);
				$result = Db::sql($query);
				while ($rs = Db::loop($result)) {
					$this->categories[] = $rs['id'];
				}
			}
		}
		return $this->categories;
	}

	public function renderView() {
		if (isset($this->id)) {
			parent::renderView('news.articlebody');
		} else {
			throw new Error(t('A keresett cikk nem található a rendszerben'));
		}
	}
}
