<?php

/**
 * Cikkek kezelése
 */
class NewsController extends BaseModule {
	public $viewFile = 'mainpage';

	public function __construct() {
		if (UniAdmin::app()->route->getCurrentModule() != 'News') {
			return;
		}

		parent::__construct();
		UniAdmin::import('modules.News.*');

		UniAdmin::app()->page
			->addCss('modules.News.news')
			->setTitle(t('News'))
			->addScript('modules.News.news');

		UniAdmin::app()->path->addElement(t('News'), 'news');

		$route = UniAdmin::app()->route->getRoute();
		if (isset($route[1]) && $route[1] && !in_array($route[1], array('article', 'rss'))) {
			$this->category = Db::sqlRow(sprintf("
				SELECT
					categoryId AS id,
					idText,
					name,
					description,
					metaKeywords,
					metaDescription
				FROM ArticleCategoryText
				WHERE idText = '%s'
				AND languageId = '%s'",
				Db::escapeString($route[1]),
				LANG
			));
			if (!$this->category) {
				throw new Http404Exception(t('A keresett kategória nem található'));
			}
			$this->viewFile = 'category';

			UniAdmin::app()->path->addElement($this->category['name'], 'news', $route[1]);

			if ($this->category['metaDescription']) {
				UniAdmin::app()->page->setDescription($this->category['metaDescription']);
			}
			if ($this->category['metaKeywords']) {
				UniAdmin::app()->page->setKeywords($this->category['metaKeywords']);
			}

			// $this->articleList = $this->listArticles($this->category['id']);
		}
	}

	public function actionDefault() {
		UniAdmin::app()->page->setTitle(t('News'));
		$this->articleList = array();
	}

	/**
	 * Ezt sosem használta senki szerintem, törölhető is lenne
	 */
	public function actionList() {
		$this->viewFile = 'articlelist';
		$this->articleList = $this->listArticles();
	}

	public function actionArticle($idText) {
		// if (!UniAdmin::app()->user->checkLogin()) {
		// 	$this->redirect('user', 'login');
		// }

		$this->viewFile = 'article';
		$this->parameters = new EventParameter();

		$this->parameters->article = new Article(array(
			'id' => $idText,
			'language' => LANG,
		));

		if (!$this->parameters->article->getId()) {// nem található a cikk
			throw new Http404Exception(t('A keresett cikk nem található'));
		}

		UniAdmin::app()->setVariable('articleId', $this->parameters->article->getId());
		UniAdmin::app()->page->setTitlePrefix($this->parameters->article->getTitle());
		UniAdmin::app()->path->addElement($this->parameters->article->getTitle(), 'news', array('article' => $idText));

		//Olvasás mentése
		Db::sql(sprintf("
			INSERT INTO `ArticleRead`
			(`articleId`)
			VALUES
			(%d)",
			$this->parameters->article->getId()
		));

		//meta adatok beállítása
		if ($this->parameters->article->getMetaDescription()) {
			UniAdmin::app()->page->setDescription($this->parameters->article->getMetaDescription());
		}
		if ($this->parameters->article->getMetaKeywords()) {
			UniAdmin::app()->page->setKeywords($this->parameters->article->getMetaKeywords());
		}

		$this->dispatchEvent('beforeDisplayArticle', $this->parameters);
	}

	public function actionRss($category = null) {
		header('Content-Type: text/xml;charset=utf-8');

		$this->articleList = $this->listArticles(array(
			'limit' => 20,
			'category' => $category,
		));
		parent::renderView('news.rss');

		exit;
	}

	public function listArticles($parameters = null) {
		if (!is_array($parameters)) {
			$parameters = array();
		}
		$categoryList = array();
		if (isset($parameters['category'])) {
			if (!is_array($parameters['category'])) {
				$parameters['category'] = array($parameters['category']);
			}
			foreach ($parameters['category'] as $category) {
				if (!is_numeric($category)) {
					$categoryId = Db::sqlField(sprintf("
						SELECT categoryId
						FROM ArticleCategoryText
						WHERE idText = '%s'
						AND languageId = '%s'",
						$category,
						LANG
					));
					if (!$categoryId) {
						continue;
					}
					$categoryList[] = $categoryId;
				} else {
					$categoryList[] = $categoryId;
				}
			}
		}

		$ret = array();
		$result = Db::sql(sprintf("SELECT id
			FROM Article, Article_Category
			WHERE Article.id = Article_Category.articleId
			AND Article.languageId = '%s'
			AND UNIX_TIMESTAMP(date) <= UNIX_TIMESTAMP(NOW())
			%s
			ORDER BY date DESC%s",
			LANG,
			(!empty($categoryList) ? ' AND categoryId IN (' . implode(', ', $categoryList) . ')' : ''),
			(isset($parameters['limit']) ? ' LIMIT ' . intval($parameters['limit']) : '')));
		while ($rs = Db::loop($result)) {
			$ret[] = new Article(array('id' => $rs['id']));
		}
		return $ret;
	}

	public function searchResults($parameters) {
		if ($parameters->search) {
			$search = Db::escapeString($parameters->search);
			$temp = Db::sql(sprintf("
				SELECT id, idText, title, lead
				FROM Article, Article_Category
				WHERE Article.id = Article_Category.articleId
				AND Article.languageId = '%s'
				AND UNIX_TIMESTAMP(date) <= UNIX_TIMESTAMP(NOW())
				AND (Article.title LIKE '%%%s%%' OR Article.lead LIKE '%%%s%%' OR Article.content LIKE '%%%s%%')
				ORDER BY date DESC",
				LANG,
				$search,
				$search,
				$search
			));
			$results = array();
			while ($rs = Db::loop($temp)) {
				$results[] = array(
					'title' => $rs['title'],
					'link' => Url::link('news', array('article' => $rs['idText'])),
					'description' => Text::truncate(strip_tags($rs['lead']), 200),
				);
			}
			$parameters->results['news'] = array(
				'name' => t('Hírek'),
				'results' => $results,
			);
		}
	}

	public function renderView() {
		parent::renderView('news.' . $this->viewFile);
	}
}