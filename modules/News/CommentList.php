<?php

class CommentList extends Widget {
	public $articleId;
	public $viewFile = 'news.commentlist';

	public function __construct($parameters = array(), $autoLoad = true) {
		UniAdmin::app()->page->addCss('modules.News.news');

		if (is_array($parameters)) {
			if (isset($parameters['articleId'])) {
				$this->articleId = intval($parameters['articleId']);
			}
		} else if (intval($parameters)) {	// talán articleId jött?
			$this->articleId = intval($parameters);
		}

		UniAdmin::app()->initModels();

		$this->commentList = Db::toArray(sprintf("
			SELECT
				id,
				date,
				text,
				name,
				userId,
				isModerated
			FROM ArticleComment
			WHERE articleId = %d
			ORDER BY date DESC",
			$this->articleId
		), true);

		if ($autoLoad) {
			$this->renderView();
		}
	}
}
