<?php

class TinyMCEExternalController extends BaseModule {
	public $files = array();
	public $type;

	public function __construct() {
		UniAdmin::import('modules.Tinymceexternal.TinymceexternalConfig');
		$this->config = new TinymceexternalConfig();
	}

	public function actionImages() {
		$this->type = 'images';
		$this->fileList($this->config->imagesDir);
		$this->renderView();
		exit;
	}

	public function actionFiles() {
		$this->type = 'files';
		$this->fileList($this->config->filesDir);
		$this->renderView();
		exit;
	}

	function fileList($dir = '') {
		if ($handle = opendir($dir)) {
			while (($file = readdir($handle)) !== false) {
				$extension = strtolower(end(explode('.', $file)));
				if ($file != '.' && $file != '..' && is_dir($dir . '/' . $file)) {
					$this->fileList($dir . '/' . $file);
				} else {
					if (!is_dir($dir . '/' . $file) && (($this->type == 'images' && in_array($extension, $this->config->allowedImageExtensions)) || ($this->type == 'files' && !in_array($extension, $this->config->forbiddenFileExtensions)))) {
						$this->files['/' . $dir . '/' . $file] = $file;
					}
				}
			}
			closedir($handle);
		}
	}

	public function renderView() {
		if (count($this->files)) {
			natsort($this->files);
			if ($this->type == 'images') {
				echo 'var tinyMCEImageList = new Array(';
			} else {
				echo 'var tinyMCELinkList = new Array(';
			}
			$i = 0;
			foreach ($this->files as $path => $file) {
				if ($i++) {
					echo ', ';
				}
				echo '["' . $file . '", "' . $path . '"]';
			}
			echo ');';
		}
	}
}
