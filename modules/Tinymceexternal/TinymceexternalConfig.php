<?php

class TinyMCEExternalConfig {
	public $imagesDir = 'images';

	public $filesDir = 'documents';

	public $allowedImageExtensions = array('jpg', 'jpeg', 'gif', 'png');

	public $forbiddenFileExtensions = array('php', 'js', 'css');
}
