<?php

class SitemapController extends BaseModule {
	public function __construct() {
		define('SITEMAP_DOMAIN', 'http://choosepuppy.com');

		$this->elements = array();

		// dokumentum
		$documents = array(
			'privacy',
		);
		foreach ($documents as $document) {
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . '/' . $document,
				'changefreq' => 'yearly',
			);
		}

		// Breed List
		$result = Db::sql(sprintf("
			SELECT idText
			FROM Breed"
		));
		while ($row = Db::loop($result)) {
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . Url::link('breed',array('info', $row['idText'])),
				'changefreq' => 'monthly',
			);
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . Url::link('puppy',array('list', $row['idText'])),
				'changefreq' => 'daily',
			);
		}

		$this->elements[] = array(
			'loc' => SITEMAP_DOMAIN . Url::link('breed', 'list'),
			'changefreq' => 'daily',
		);

		//ABC Breed
		foreach (range('A', 'Z') as $letter) {
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . Url::link('breed', array('list', $letter)),
				'changefreq' => 'monthly',
			);
		}

		$result = Db::sql(sprintf("
			SELECT id, puppyIdText
			FROM Puppy
			WHERE Puppy.isDeleted <> 1"
		));
		while ($row = Db::loop($result)) {
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . Url::link('puppy', array('view', $row['id'] .'-'. $row['puppyIdText'] )),
				'changefreq' => 'daily',
			);
		}

		// puppy/seller/***
		$result = Db::sql(sprintf("
			SELECT id
			FROM User"
		));
		while ($row = Db::loop($result)) {
			$this->elements[] = array(
				'loc' => SITEMAP_DOMAIN . Url::link('puppy', array('seller', $row['id'])),
				'changefreq' => 'daily',
			);
		}

		// review/list/39
		$result = Db::sql(sprintf("
			SELECT receiverId
			FROM Review
			WHERE receiverId <> 0
			GROUP BY receiverId"
		));
		while ($row = Db::loop($result)) {
			$this->elements[] = array(
				'loc'        => SITEMAP_DOMAIN . Url::link('review', array('list', $row['receiverId'])),
				'changefreq' => 'weekly',
			);
		}

		header('Content-Type: text/xml; charset=utf-8');
		ob_start('ob_gzhandler');
		echo $this->loadView('views.sitemap');
		ob_end_flush();
		exit;
	}
}
