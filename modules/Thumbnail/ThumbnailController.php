<?php
/**
 * uniAdmin thumbnail module
 *
 * @author Kenéz László <info@kenlas.hu>
 * @copyright Copyright (c) 2007-2011, Kenéz László | www.uniadmin.hu
 * @package uniAdmin
 * @since 4.4
 */

class ThumbnailController extends BaseModule {
	public function __construct() {
		$route = UniAdmin::app()->route->getRoute();

		if (isset($route[1])) {
			// összevonás, ha könyvtár is meg van adva
			$route[1] = implode('/', array_slice($route, 1));
			$fileName = $route[1];
			$size = '';

			$format = $width = $height = null;
			$cropMax = false;

			$queryString = UniAdmin::app()->route->getQueryString();
			$parameters = preg_split('/\,/', $queryString, -1, PREG_SPLIT_NO_EMPTY);
			if (!empty($parameters)) {
				if (isset($parameters[0])) {
					$width = intval($parameters[0]);
				}
				if (isset($parameters[1])) {
					$height = intval($parameters[1]);
				}
				if (in_array('jpg', $parameters)
					|| in_array('png', $parameters)
					|| in_array('gif', $parameters)
				) {
					$format = $parameters[2];
				}
				if (in_array('crop', $parameters)) {
					$cropMax = true;
				}
			}

			if (!$width) {
				$width = 320;
			}
			if (!$height) {
				$height = 240;
			}
			if (!$format) {
				if ($width <= 320 && $height <= 240) {
					$format = 'png';
				} else {
					$format = 'jpg';
				}
			}

			if (function_exists('apache_request_headers')) {
				$headers = apache_request_headers();
			} else {
				$headers = array();
			}

			if (isset($headers['If-Modified-Since']) && (strtotime($headers['If-Modified-Since']) >= filemtime($fileName))) {
				header('HTTP/1.1 304 Not Modified');
				header('Connection: close');
				exit;
			}

			$parameters = new EventParameter();
			$parameters->image = new Image($fileName);
			if (!$parameters->image->getFileName()) {
				header('Content-type: text/html; charset=utf-8');
				echo 'Hiba: A fájl nem nyitható meg';
				exit;
			}
			$parameters->width = $width;
			$parameters->height = $height;
			$parameters->format = $format;

			$this->dispatchEvent('beforeSave', $parameters);

			header('Last-Modified: ' . date('r', filemtime($fileName)), true, 200);
			header('Cache-Control: must-revalidate');
			header('Pragma: cache');

			if ($cropMax) {
				$parameters->image
					->cropMax()
					->resample(max($width, $height), max($width, $height))
					->crop($parameters->width, $parameters->height)
					->save($parameters->format);
			} else {
				$parameters->image
					->resample($parameters->width, $parameters->height)
					->save($parameters->format);
			}
		}
		exit;
	}
}
