<?php

use Mailgun\Mailgun;

class GolfController extends BaseModule {

    protected $email;

    function __construct()
    {
        $this->email = isset($_POST['email']) ? $_POST['email'] : null;
    }

    /**
     * Golf result list
     */
    public function actionDefault()
    {
        $this->viewFile = 'default';

        $this->success = Util::setSession('success');
        $this->unSuccess = Util::setSession('unSuccess');
        $this->memberExists = Util::setSession('memberExists');
        $this->youAllSet = Util::setSession('youAllSet');
        $this->cancel = Util::setSession('cancel');

        $this->seasons = GolfSeason::getSeasons();
        $this->seasonId = isset($_GET['sid']) ? $_GET['sid'] : $this->seasons[0]['id'];
        $this->title = $this->seasons[0]['name'];
        $this->ranks = GolfResult::getResults($this->seasonId);
        $this->round = GolfResult::getRound($this->seasonId);
    }

    /**
     * Register a golf member
     *
     * @throws Error
     */
    public function actionRegister()
    {
        $this->viewFile = 'register';
        $this->registration = Util::setSession('registration');
        UniAdmin::import('classes.forms.Form');
        $this->form = new Form();
        $this->form
            ->loadFromFile('golf.register');

        if ($this->form->isSent())
        {
            $this->form->queryValues();

            if ($this->form->validateFields())
            {
                if ($this->memberExists($this->form->getValue('email')))
                {
                    $_SESSION['memberExists'] = 1;
                    $this->redirect('golf');
                }

                UniAdmin::import('modules.User.UserConfig');
                $code = UserConfig::randomPassword();
                $this->form
                    ->addDbField('insertedAt', date('Y-m-d'))
                    ->addDbField('code', $code)
                    ->addDbField('isActive', 0);
                $_SESSION['memberId'] = $this->form->save();

                $this->sentEmail($this->form->getValue('email'));

                $this->redirect('payment', 'paypal');
            }
        }
    }

    /**
     * Sent email to user after register
     *
     * @param $email
     */
    public function sentEmail($email)
    {
        # Instantiate the client.
        $mgClient = new Mailgun('key-1270d504f43c0cb7f1bd25488eae1ef5');
        $domain = "sandbox14bf97bd964d4665833771da4bff5cdd.mailgun.org";

        $text = <<<EOT
Congratulations you have successfully registered for the B2B Commercial Development Golf League Season Spring 2015!
We look forward to seeing you on March 11th at 4:00pm at Canyon Gate Country Club

Zoltan Hollo
Managing Director

US NATIONAL COMMERCIAL
REAL ESTATE SERVICES
10161 Park Run Dr. #150
Las Vegas, NV 89145
Direct: 702-518-0875
E-Fax: 866-626-2618
E-mail: zoli@usnationalcommercial.com'
EOT;

        # Make the call to the client.
        $mgClient->sendMessage("$domain",
            array('from'    => 'B2B Commercial Development Golf League <zoli@usnationalcommercial.com>',
                  'to'      => $email,
                  'subject' => 'Golf League Registration',
                  'text'    => $text
            )
        );

    }

    /**
     * Set the member for payment
     */
    public function actionMember()
    {
        $this->viewFile = 'exists-member';
        $this->emailValidation = Util::setSession('emailValidation');

        if (isset($this->email))
        {
            $this->emailValidate();

            $this->checkMemberExistsAndPaidStatus();

            $_SESSION['registration'] = 1;

            $this->redirect('golf', 'register');
        }
    }

    /**
     * Member payment status and exists
     */
    protected function checkMemberExistsAndPaidStatus()
    {
        if ($member = $this->memberExists($this->email))
        {
            $_SESSION['memberId'] = $member[0]['id'];

            if ($this->checkPaid($member[0]['paid']))
            {
                $_SESSION['youAllSet'] = 1;
                $this->redirect('golf');
            }

            $this->redirect('payment', 'paypal');
        }
    }

    /**
     * Email validation
     * @return mixed
     * @internal param $email
     */
    protected function emailValidate()
    {
        if ( ! filter_var($this->email, FILTER_VALIDATE_EMAIL) && ! empty($_POST))
        {
            $_SESSION['emailValidation'] = 1;
            $this->redirect('golf', 'member');
        }
    }

    /**
     * Check if user already exists
     *
     * @param $email
     * @return bool
     */
    protected function memberExists($email)
    {
        $rs = Db::toArray(sprintf("SELECT Member.id, Member.email, Member.paid FROM Member WHERE email = '%s'", Db::escapeString($email)));
        return $rs;
    }

    /**
     * Check the pay for member
     *
     * @param $date
     * @return bool
     */
    protected function checkPaid($date)
    {
        return is_null($date) ? false : true;
    }

}
