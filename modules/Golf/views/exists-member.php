<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="page-header">
                <h2>Golf League Registration Form</h2>
            </div>
            <div class="row">
                <div class="col-md-12">
                    <p>The registration fee of <strong>$125</strong> must be collected on or before March 11th to be eligible for the league.
                        <br/>  Limited to first 100 Players/Sign Ups, first come first serve.</p>
                    <p>
                </div>
                <div class="col-md-6 col-md-offset-3">
                    <?php isset($this->emailValidation) ? SysMessage::displayError('Email is invalid') : null; ?>
                    <form id="register-form" action="<?php echo Url::link('golf', 'member'); ?>" method="post">
                        <div class="form-group">
                            <label for="email">Email: </label>
                            <input type="text" name="email" class="form-control"/>
                        </div>
                        <div class="form-group pull-right">
                            <!--<button type="submit" class="btn btn-success btn-lg hidden-xs">Submit</button>-->
                            <!--<button type="submit" class="btn btn-success btn-block hidden-sm hidden-md hidden-lg">Submit </button>-->
                            <a href="#" onclick="document.getElementById('register-form').submit()"><img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="left" style="margin-right:7px;"></a>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
