<div class="container">
    <div class="page-header">
        <h2><span class="thin">Golf League</span></h2>
    </div>
    <h1>B2B Commercial Development Golf League <?= $this->title; ?></h1>

    <p>The registration fee of <strong>$125</strong> must be collected on or before March 11th to be eligible for the
        league.
        <br/> Limited to first 100 Players/Sign Ups, first come first serve.</p>

    <p>
    <p>
        <a href="/documents/league_schedule_fall_2015.pdf" target="_blank">
            use this link for the PDF schedule.
        </a>
    </p>

    <a href="<?php echo Url::link('golf', 'register'); ?>" class="btn btn-success">Registration</a>
    <a href="<?php echo Url::link('golf', 'member'); ?>">I am already Member</a>
    </p>
    <?php echo isset($this->success) ? SysMessage::displaySuccess('Your registration and payment has been successfully. Your registration and payment has been successful, we look forward to seeing you on March 11th at 4:00pm at Canyon Gate Country Club', true) : null; ?>
    <?php echo isset($this->unSuccess) ? SysMessage::displayError('Something went wrong. Your transaction id : ' . $_SESSION['data']['PAYMENTINFO_0_TRANSACTIONID']) : null; ?>
    <?php echo isset($this->memberExists) ? SysMessage::displayNotice(' This email already exists!', true) : null; ?>
    <?php echo isset($this->youAllSet) ? SysMessage::displayNotice(' No need to do anything, You all set!', true) : null; ?>
    <?php echo isset($this->cancel) ? SysMessage::displayError(' You cancelled the order.', true) : null; ?>

    <form class="form-inline" role="form" method="GET" action="<?php echo Url::link('golf'); ?>">
        <div class="form-group"> Golf League Weekly Scores:</div>
        <div class="form-group">
            <select name="sid" id="sid" class="form-control">
                <?php foreach ($this->seasons as $season) : ?>
                    <option
                        value="<?php echo $season['id']; ?>" <?php if ($season['id'] == $this->seasonId) echo 'selected'; ?>>
                        <?php echo $season['name']; ?>
                    </option>
                <?php endforeach; ?>
            </select>
        </div>
        <button type="submit" class="btn btn-primary">Go!</button>
    </form>

    <br/>
    <table class="table table-bordered table-striped">
        <thead>
        <tr>
            <th class="col-md-1 col-sm-1 col-xs-1" rowspan="2">#</th>
            <th class="col-md-3 col-sm-3 col-xs-3" rowspan="2" colspan="2">Name</th>
            <th class="col-md-3 col-sm-3 hidden-xs" rowspan="2">Email & Occupation</th>
            <th colspan="<?php echo $this->round; ?>" class="text-center hidden-xs hidden-sm">Weeks</th>
            <th class="col-md-1 col-sm-1 col-xs-1" rowspan="2"><span class="pull-right"> Amount </span>
            <th class="col-md-1 col-sm-1 col-xs-1" rowspan="2"><span class="pull-right"> Average</span></th>
        </tr>
        <?php for ($i = 1; $i <= $this->round; $i ++) : ?>
            <th class="hidden-xs hidden-sm text-center"><?php echo $i; ?></th>
        <?php endfor; ?>
        <tr>

        </tr>
        </thead>
        <tbody>
        <?php $count = 1; ?>
        <?php foreach ($this->ranks as $rank) : ?>
            <?php $round = 1; ?>
            <tr>
                <td><?php echo $count; ?>.</td>
                <td class="col-md-1 col-sm-1 col-xs-1">
                    <?php if (Util::isImage($rank['userId'], 'member')) : ?>
                    <img src="images/member/<?php echo $rank['userId']; ?>.jpg" alt="" class="img-responsive"
                         width="60"/></td>
                <?php else : ?>
                    <img src="gfx/no-image.png" width="60px"
                         alt="<?php echo $rank['firstName'] . ' ' . $rank['lastName']; ?>"/>
                <?php endif; ?>
                <td class="col-md-3 col-sm-3 col-xs-3"><?php echo $rank['firstName']; ?>
                    <br/><strong><?php echo $rank['lastName']; ?></strong></td>
                <td class="hidden-xs">
                    <small>
                        <a href="mailto:<?php echo $rank['email']; ?>"><?php echo $rank['email']; ?></a><br/>
                        <span class="text-muted"> <em><?php echo $rank['occupation']; ?></em></span>
                    </small>
                </td>
                <?php $detail = GolfResult::getDetails($rank['userId'], isset($_GET['sid']) ? $_GET['sid'] : 2); ?>
                <?php for ($i = 1; $i <= $this->round; $i ++) : ?>
                    <?php if (isset($detail[$i])) : ?>
                        <td class="hidden-xs hidden-sm text-center"><?php echo $detail[$i]; ?></td>
                    <?php else: ?>
                        <td class="hidden-xs hidden-sm text-center"><span class="text-danger">-</span></td>
                    <?php endif; ?>
                <?php endfor; ?>
                <td><span class="text-success pull-right">$<?php echo Text::formatCurrency($rank['money']); ?></span>
                </td>
                <td><strong><span
                            class="text-success pull-right"><?php echo number_format($rank['average'], 1); ?></span></strong>
                </td>
            </tr>
            <?php $count ++; ?>
        <?php endforeach; ?>
        </tbody>
    </table>
</div>
