<div class="container">
    <div class="col-md-12">
        <div class="page-header">
            <h2>Golf League Registration Form</h2>
        </div>
        <p>The registration fee of <strong>$125</strong> must be collected on or before March 11th to be eligible for the league.
            <br/>  Limited to first 100 Players/Sign Ups, first come first serve.</p>
        <p>
        <div class="row">
            <form id="register-form" action="<?php echo Url::link('golf', 'register'); ?>" method="post">
                <div class="col-md-6">
                    <input type="hidden" name="sent" value="1"/>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <?php echo $this->form->getField('email')->renderContent(); ?>
                        <?php echo $this->form->getField('email')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="firstName">First Name</label>
                        <?php echo $this->form->getField('firstName')->renderContent(); ?>
                        <?php echo $this->form->getField('firstName')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="lastName">Last Name</label>
                        <?php echo $this->form->getField('lastName')->renderContent(); ?>
                        <?php echo $this->form->getField('lastName')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="occupation">Occupation/Title</label>
                        <?php echo $this->form->getField('occupation')->renderContent(); ?>
                        <?php echo $this->form->getField('occupation')->renderError(); ?>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="company">Company</label>
                        <?php echo $this->form->getField('company')->renderContent(); ?>
                        <?php echo $this->form->getField('company')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="phone">Phone</label>
                        <?php echo $this->form->getField('phone')->renderContent(); ?>
                        <?php echo $this->form->getField('phone')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="industry">Industry</label>
                        <?php echo $this->form->getField('industry')->renderContent(); ?>
                        <?php echo $this->form->getField('industry')->renderError(); ?>
                    </div>
                    <div class="form-group">
                        <label for="address">Address</label>
                        <?php echo $this->form->getField('address')->renderContent(); ?>
                        <?php echo $this->form->getField('address')->renderError(); ?>
                    </div>
                    <div class="form-group pull-right">
                        <!--<button type="submit" class="btn btn-success btn-lg hidden-xs">Submit</button>-->
                        <!--<button type="submit" class="btn btn-success btn-block hidden-sm hidden-md hidden-lg">Submit </button>-->
                        <a href="#" onclick="document.getElementById('register-form').submit()"><img src="https://www.paypal.com/en_US/i/btn/btn_xpressCheckout.gif" align="left" style="margin-right:7px;"></a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
