<?php

class ContactController extends BaseModule {

    public function actionDefault()
    {
        $this->viewFile = 'default';
        $this->contacts = [
            'BROKERAGE'           => Contact::getContact(),
            'OPERATIONS'          => Contact::getContact('O'),
            'PROPERTY MANAGEMENT' => Contact::getContact('P')
        ];
    }
}