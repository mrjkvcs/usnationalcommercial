<div class="container">
    <div class="page-header">
        <h2><span class="thin"> Contact</span></h2>
    </div>
    <h1><?php echo TITLE; ?></h1>
    <address>
        10161 Park Run Dr. #150 • Las Vegas, NV 89145 <br/>
        Direct: <a
            href="tel:702-518-0875"><?php echo Util::formatPhone('702-518-0875'); ?></a>
        • E-Fax: <?php echo Util::formatPhone('866-626-2618'); ?>
    </address>
    <?php foreach ($this->contacts as $title => $datas) : ?>
        <div class="page-header">
            <h4><?php echo $title; ?>:</h4>
        </div>
        <div class="row">
            <?php foreach ($datas as $data) : ?>
                <div class="col-xs-6 col-sm-4 col-md-3">
                    <div class="thumbnail">
                        <?php if (Util::isImage($data['id'])) : ?>
                            <img src="images/contact/<?php echo $data['id']; ?>.jpg" class="img-responsive"
                                 alt="<?php echo $data['firstName'] . ' ' . $data['lastName']; ?> " />
                        <?php else : ?>
                           <img src="gfx/no-image.png" class="img-responsive" />
                        <?php endif; ?>

                        <div class="caption">
                            <ul class="list-unstyled">
                                <li class="hidden-xs">
                                    <strong><?php echo $data['firstName'] . ' ' . $data['lastName']; ?></strong>
                                </li>
                                <li class="hidden-sm hidden-md hidden-lg">
                                    <strong><?php echo $data['firstName'] . '</strong><br />' . $data['lastName']; ?>
                                </li>
                                <li>
                                    <small><span class="text-muted hidden-md hidden-lg"> <?php echo Text::truncate($data['title'], 20); ?></span> </small>
                                    <small><span class="text-muted hidden-xs hidden-sm"> <?php echo Text::truncate($data['title'], 32); ?></span> </small>
                                </li>
                                <li>
                                    <small><i class="fa fa-phone hidden-xs"></i>
                                        <a href="tel:<?php echo $data['phone']; ?>"><?php echo Util::formatPhone($data['phone']); ?></a>
                                    </small>
                                </li>
                                <li>
                                    <small>
                                        <a href="mailto:<?php echo $data['email']; ?>">
                                            <i class="fa fa-envelope-o hidden-sm"></i>
                                            <span class="hidden-xs "><?php echo Text::truncate($data['email'], 26); ?></span>
                                        </a>
                                    </small>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            <?php endforeach; ?>
        </div>
    <?php endforeach; ?>

</div>
