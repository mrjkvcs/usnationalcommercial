<?php
/**
 * uniAdmin documents frontend module
 *
 * @author Kenéz László <info@kenlas.hu>
 * @copyright Copyright (c) 2007-2009, Kenéz László | www.uniadmin.hu
 * @package uniAdmin
 * @since 4.4
 */

/**
 * Dokumentumok listázása
 */
class DocumentList extends BaseModule {
	private $_category;
	private $_language;
	private $_containerid;
	private $_containerclass = 'documentlist';
	private $_urlscheme = '[id]';

	public function __construct($category_id = null, $language_id = null) {
		parent::__construct();

		if (!is_null($category_id) && !is_numeric($category_id)) {
			$rs = Db::sqlrow("SELECT id FROM DocumentCategory WHERE idText = '" . $category_id . "'");
			if ($rs) {
				$this->_category = $rs['id'];
			} else {
				return false;
			}
		} else if (is_numeric($category_id)) {
			$this->_category = intval($category_id);
		}

		if (!$language_id) {
			if (defined('LANG')) {
				$this->_language = LANG;
			} else {
				$this->_language = Setting::getDefaultLanguage();
			}
		} else {
			$this->_language = $language_id;
		}
	}

	public function setWrapper($id = '', $class = '') {
		$this->_containerid = $id;
		$this->_containerclass = $class;
		return $this;
	}

	public function setURLScheme($urlscheme) {
		$this->_urlscheme = $urlscheme;
		return $this;
	}

	public function getDocuments() {
		$query = sprintf("SELECT id, title FROM Document WHERE categoryId = %d AND languageId = '%s'", 
			$this->_category, $this->_language);
		$result = Db::sql($query);
		return Db::toArray($result, true);
	}

	public function display() {
		$documentList = $this->getDocuments();
		if (count($documentList)) {
			echo '<ul' . ($this->_containerid ? ' id="' . $this->_containerid . '"' : '') . ($this->_containerclass ? ' class="' . $this->_containerclass . '"' : '') . '>';
			foreach ($documentList as $documentId => $documentTitle) {
				$data = array(
					'id' => $documentId,
					'title' => $documentTitle,
				);
				echo '<li><a href="' . URL::link('info', $data['id']) . '">' . $documentTitle . '</a></li>';
			}
			echo '</ul>';
		}
	}

	public function displaySearchResults($searchString, $extra_sql = '') {
		$searchString = Db::escapeString(stripslashes($searchString));

		$query = sprintf("
			SELECT id, title, textcontent, category,
			MATCH (id, title, textcontent) AGAINST ('%s') AS score
			FROM documents
			WHERE MATCH(id, title, textcontent) AGAINST ('%s' IN BOOLEAN MODE)
			%s
			ORDER BY score, title",
			$searchString, $searchString, $extra_sql);

		$result = Db::sql($query);
		if (!Db::numrows($result)) {	// nincs fulltext találat, keresés hagyományos módon
			$query = sprintf("
				SELECT id, title, textcontent, category
				FROM documents
				WHERE (id LIKE '%%%s%%'
				OR title LIKE '%%%s%%'
				OR textcontent LIKE '%%%s%%')
				%s
				ORDER BY title",
				$searchString, $searchString, $searchString, $extra_sql);

			$result = Db::sql($query);
		}
		if (Db::numrows($result)) {
			echo '<div' . ($this->_containerid ? ' id="' . $this->_containerid . '"' : '') . ' class="document_search_box' . ($this->_containerclass ? ' ' . $this->_containerclass : '') . '">';
			while ($rs = Db::loop($result)) {
				echo '<div class="document_title"><a href="' . URL::fromScheme($this->_urlscheme, $rs) . '">' . $rs["title"] . '</a></div>';
				echo '<div class="document_intro">' . Text::highlight($rs['textcontent'], Text::truncate($searchString, 300, true)) . '</div>';
			}
			echo '</div>';
		}
	}
}
