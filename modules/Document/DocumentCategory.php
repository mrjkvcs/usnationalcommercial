<?php

class DocumentCategory extends Widget {
	private $name;
	private $idText;
	private $description;
	private $access;
	
	public function __construct($id = null) {
		if (is_null($id)) {
			return false;
		}
		
		$result = Db::sqlrow(sprintf('SELECT * FROM DocumentCategory WHERE id = %d', intval($id)));
		if ($result) {
			$this->idText = $result['idText'];
			$this->name = $result['name'];
			$this->description = $result['description'];
			$this->access = $result['limitAccess'];
		} else {
			return false;
		}
	}
	
	public function getIdText() {
		return $this->idText;
	}
	
	public function getName() {
		return $this->name;
	}
	
	public function getDescription() {
		return $this->description;
	}
	
	public function getAccess() {
		return $this->access;
	}
}
