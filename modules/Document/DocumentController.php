<?php

class DocumentController extends BaseModule {
	private $document;

	public function __construct() {
		if (UniAdmin::app()->route->getCurrentModule() != 'Document') {
			return;
		}

		parent::__construct();
		UniAdmin::import('modules.Document.*');

		$route = UniAdmin::app()->route->getRoute();
		if (isset($route[1])) {	// megadott dokumentum betöltése
			$this->document = new Document(array(
				'id' => $route[1], 
				'language' => LANG,
			), false);
		} else {	// alapértelmezett dokumentum betöltése
			$defaultDocumentId = Db::sqlField(sprintf("
				SELECT id, categoryId 
				FROM Document 
				WHERE isDefault = 1 
				AND languageId = '%s' 
				LIMIT 1",
				LANG
			));
			if ($defaultDocumentId) {
				$this->document = new Document(array(
					'id' => $defaultDocumentId, 
					'language' => LANG
				));
			} else {
				throw new Http404Exception(t('Error loading default document'));
			}
		}
		if ($this->document->isInitialized()) {
			UniAdmin::app()->path->addElement($this->document->getTitle(), 'document', $this->document->getId());
			if ($this->document->getTitle()) {
				UniAdmin::app()->page->setTitle($this->document->getTitle());
			}
			if ($this->document->getMetaDescription()) {
				UniAdmin::app()->page->setDescription($this->document->getMetaDescription());
			}
			if ($this->document->getMetaKeywords()) {
				UniAdmin::app()->page->setKeywords($this->document->getMetaKeywords());
			}
		} else {
			throw new Http404Exception(t());
		}
	}

	public function searchResults($parameters) {
		$parts = preg_split('/\s/', $parameters->search, -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts)) {
			UniAdmin::import('modules.Document.Document');
			$search = '';
			foreach ($parts as $searchString) {
				$search .= sprintf(
					" 
						AND (content LIKE '%%%s%%' OR title LIKE '%%%s%%')
					", 
					Db::escapeString($searchString),
					Db::escapeString($searchString)
				);
			}
			$temp = Db::sql(sprintf("
				SELECT id, title, content
				FROM Document 
				WHERE Document.languageId = '%s'
				AND categoryId IN (SELECT id FROM DocumentCategory WHERE isPublic = 1)
				%s
				ORDER BY title LIMIT 10",
				LANG, 
				$search
			));
			$results = array();
			while ($rs = Db::loop($temp)) {
				$results[] = array(
					'title' => $rs['title'],
					'description' => Text::truncate(strip_tags($rs['content']), 200),
					'link' => Url::link('document', $rs['id']),
				);
			}
			$parameters->results['document'] = array(
				'name' => t('Dokumentumok'),
				'results' => $results,
			);
		}
	}

	public function renderView() {
		$this->document->renderView();
	}
}
