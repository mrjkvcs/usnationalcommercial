<?php

class Document extends Widget {
	protected $_initialized = false;
	protected $id;
	protected $title;
	protected $content;
	protected $textContent;
	protected $authorId;
	protected $metaDescription;
	protected $metaKeywords;

	public function __construct($parameters = array(), $autoRender = false) {
		if (!is_array($parameters)) {
			$parameters = array('id' => $parameters);
		}

		if (!isset($parameters['id'])) {
			return false;
		}

		if (!isset($parameters['language'])) {
			if (defined('LANG')) {
				$parameters['language'] = LANG;
			} else {
				$parameters['language'] = Setting::getDefaultLanguage();
			}
		}

		$rs = Db::sqlrow(sprintf("
			SELECT
				`title`,
				`content`,
				`textContent`,
				`authorId`,
				`metaDescription`,
				`metaKeywords`
			FROM `Document`
			WHERE `id` = '%s'
			AND `languageId` = '%s'",
			Db::escapeString($parameters['id']),
			$parameters['language']
		));

		$this->id = $parameters['id'];

		if ($rs) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading document ' . $this->id . ', ' . $parameters['language'], 'document');
			}
			$this->_initialized = true;

			$this->title = $rs['title'];

			if(isset($_GET['h'])) {
				$this->content = Text::highlight($rs['content'], $_GET['h']);
			} else {
				if(UniAdmin::app()->getVariable('googleKeyword')) {
					$this->content = Text::highlight($rs['content'], UniAdmin::app()->getVariable('googleKeyword'));
				} else {
					$referer = isset($_SERVER['HTTP_REFERER']) ? strtolower($_SERVER['HTTP_REFERER']) : '';
					if (strpos($referer, 'google') !== false) {
						$urlParts = array();
						parse_str($referer, $urlParts);
						if (isset($urlParts['q'])) {
							$googlekeyword = addcslashes($urlParts['q'], '/');
						}
					}
					if (isset($googlekeyword)) {
						$this->content = Text::highlight($rs['content'], $googlekeyword);
						UniAdmin::app()->setVariable('googleKeyword', $googlekeyword);
					} else {
						$this->content = $rs['content'];
					}
				}
			}

			$this->textContent = $rs['textContent'];
			$this->authorId = $rs['authorId'];
			$this->metaDescription = $rs['metaDescription'];
			$this->metaKeywords = $rs['metaKeywords'];

			if ($autoRender) {
				$this->renderView();
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('error loading document ' . $this->id . ', ' . $parameters['language'], 'document', 2);
			}
		}
	}

	public function isInitialized() {
		return $this->_initialized;
	}

	public function getId() {
		return $this->id;
	}

	public function getAuthor() {
		// @todo név visszaadása
		return $this->authorId;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getContent() {
		return $this->content;
	}

	public function getTextContent() {
		return $this->textContent;
	}

	public function getMetaDescription() {
		return $this->metaDescription;
	}

	public function getMetaKeywords() {
		return $this->metaKeywords;
	}

	public function renderView() {
		if ($this->_initialized) {
			parent::renderView('document.document');
		}
	}
}
