<div id="uniadmin-modulemenu">
<?php 

$module = strtolower(UniAdmin::app()->route->getCurrentModule());

foreach (array('webshop', 'content', 'function', 'system', 'settings') as $menuType) {
	$compiled = array();
	foreach ($menus as $menu) {
		if ($menu['type'] == $menuType) {
			$compiled[$menu['title']] = $menu;
		}
	}
	if (!empty($compiled)) {
		if ($menuType != 'settings') {
			ksort($compiled);
		}
		$open = isset($_COOKIE['ua_menu_' . $menuType]);
		?>
		<div class="menutype menutype-<?php echo $menuType . ' ' . ($open ? 'open' : 'closed'); ?>">
			<span>
				<a href="#" id="uniadmin-menutype-<?php echo $menuType; ?>">
					<?php echo t($menuType); ?>
				</a>
			</span>
		</div>
		<?php
		echo sprintf(
			'<ul class="menucategory" id="ua-modules-%s"%s>',
			$menuType,
			($open ? '' : 'style="display:none"')
		);
		foreach ($compiled as $menuTitle => $menu) {
			$isActiveModule = $module == $menu['module'];
			echo '<li>';
			echo sprintf(
				'<a href="%s"%s%s>%s</a>',
				Url::link($menu['module'], isset($menu['action']) ? $menu['action'] : null),
				(isset($menu['id']) ? ' id="' . $menu['id'] . '"' : ''),
				($isActiveModule ? ' class="active"' : ''),
				$menu['title']
			);
			if ($isActiveModule 
				&& isset($menu['submenus']) 
				&& is_array($menu['submenus']) 
				&& !empty($menu['submenus'])
			) {
				echo '<ul>';
				foreach ($menu['submenus'] as $submenu) {
					if (!count($submenu)) {
						// $isSeparator = true; 
						continue;
					}
					$link = Url::link($submenu['module'], $submenu['action']);
					echo sprintf(
						'<li%s><a href="%s">%s</a></li>',
						(Url::isActive($link, false, true) ? ' class="active"' : ''),
						$link,
						$submenu['title']
					);
				}
				echo '</ul>';
			}
			echo '</li>';
		}
		echo '</ul>';
	}
}
?></div>