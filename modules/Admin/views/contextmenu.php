<ul id="contextMenu-<?php echo $this->parameters->id; ?>" style="display:none;">
	<?php foreach ($this->parameters->elements as $menuItem): ?>
	<li<?php if (isset($menuItem['disabled']) && $menuItem['disabled']) echo ' class="disabled"'; ?>>
		<a href="<?php echo Url::link(isset($menuItem['module']) ? $menuItem['module'] : $this->parameters->baseModule, $menuItem['action'], LANG, isset($menuItem['queryString']) ? $menuItem['queryString'] : null); ?>"<?php if (isset($menuItem['class'])) echo ' class="' . $menuItem['class'] . '"'; ?>>
			<?php echo $menuItem['text']; ?>
		</a>
		<?php if (isset($menuItem['submenus']) && is_array($menuItem['submenus'])): ?>
		<ul>
			<?php foreach ($menuItem['submenus'] as $submenuItem): ?>
			<li<?php if (isset($submenuItem['disabled']) && $submenuItem['disabled']) echo ' class="disabled"'; ?>>
				<a href="<?php echo Url::link(isset($submenuItem['module']) ? $submenuItem['module'] : $this->parameters->baseModule, $submenuItem['action'], LANG, isset($submenuItem['queryString']) ? $submenuItem['queryString'] : null); ?>"<?php if (isset($submenuItem['class'])) echo ' class="' . $submenuItem['class'] . '"'; ?>>
					<?php echo $submenuItem['text']; ?>
				</a>
			</li>
			<?php endforeach; ?>
		</ul>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>

<?php
if ($this->parameters->initScript) {
	UniAdmin::app()->page->addScriptTag("
		jQuery(document).ready(function($){
			$('table#" . $this->parameters->id . ".data tbody tr').contextMenu({
				menu: 'contextMenu-" . $this->parameters->id . "'" .
				($this->parameters->callbackBefore ? ", beforeShow: '" . $this->parameters->callbackBefore . "'" : '') . "
			}" . ($this->parameters->callbackAfter ? ', ' . $this->parameters->callbackAfter  : '') . ");
		});
	");
}
