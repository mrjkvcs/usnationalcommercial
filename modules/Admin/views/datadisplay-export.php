<?php

UniAdmin::import('extensions.PHPExcel.PHPExcel');

$objPHPExcel = new PHPExcel();
$objPHPExcel->getProperties()->setCreator('uniAdmin v'.UniAdmin::getVersion());
$objPHPExcel->getProperties()->setTitle($this->title);
$objPHPExcel->setActiveSheetIndex(0);

$objPHPExcel->getActiveSheet()->setTitle($this->getId());

if ($type != 'csv') {	// CSV formátum esetén nem kell fejléc
	$objPHPExcel->getActiveSheet()->SetCellValue('A1', $this->title);
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->getColor()->setRGB('00B3B6');
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setBold(true)->setName('Georgia');
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getFont()->setSize('12');
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_CENTER);
	$objPHPExcel->getActiveSheet()->getStyle('A1')->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);

	$headerStyle = array(
		'fill' => array(
			'type' => PHPExcel_Style_Fill::FILL_SOLID,
			'startcolor' => array('rgb' => '333333')
		),
		'font' => array(
			'color' => array('rgb' => 'FFFFFF'),
			'bold' => true
		)
	);
	
	$rowIndex = 3;
} else {
	$rowIndex = 1;
}

if (is_array($this->columns)) {
	$cellIndex = 0;
	foreach ($this->columns as $column) {
		if (!in_array($column, $this->forbiddenFields)) {
			$columnTitle = isset($this->columnFormats[$column]) && isset($this->columnFormats[$column]['title']) ? $this->columnFormats[$column]['title'] : $column;
			$cell = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($cellIndex, $rowIndex);
			$cell->setValueExplicit($columnTitle);	// szövegként
			if ($type != 'csv') {
				$objPHPExcel->getActiveSheet()->getStyle($cell->getCoordinate())->applyFromArray($headerStyle);
			}
			$objPHPExcel->getActiveSheet()->getColumnDimension($cell->getColumn())->setWidth(18);
			$cellIndex++;
		}
	}
	if ($type != 'csv') {
		$objPHPExcel->getActiveSheet()->mergeCells('A1:'.$cell->getColumn().'2');
	}
}

$rowIndex++;

while ($rs = Db::loop($this->result)) {
	if (count($this->keys)) {
		$keys = array();
		foreach ($this->keys as $key) {
			$keys[] = $rs[$key];
		}
		$keys = implode(';', $keys);
	} else {
		$keys = '';
	}
	$cellIndex = 0;
	foreach ($this->columns as $num => $column)
		if (!in_array($column, $this->forbiddenFields)) {
			$value = $rs[Db::fieldname($this->result, $num)];
			if (!in_array($column, array_keys($this->dataHandlers))) {
				$value = is_bool($value) || is_null($value) ? '' : $value;
			} else {
				$ret = $this->callDataHandler($column, $value, $keys, $rs);
				$value = is_bool($ret) || is_null($ret) ? '' : $ret;
			}
			$cell = $objPHPExcel->getActiveSheet()->getCellByColumnAndRow($cellIndex, $rowIndex);
			if (isset($this->columnFormats[$column]) && isset($this->columnFormats[$column]['dataType'])) {
				switch ($this->columnFormats[$column]['dataType']) {
					case 'numeric':
						$cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_NUMERIC);
					break;
					case 'formula':
						$cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_FORMULA);
					break;
					case 'boolean':
						$cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_BOOL);
					break;
					default:
						// dátumok átadásához nincs másra szükség, az PHPExcel a szövegformátum alapján fogja megállapítani
						$cell->setValueExplicit($value, PHPExcel_Cell_DataType::TYPE_STRING);
					break;
				}
			} else {
				$cell->setValueExplicit($value);
			}
			$cellIndex++;
		}
	$rowIndex++;
}

switch ($type) {
	case 'xls':
		header('Content-Type: application/vnd.ms-excel');
		header('Content-Disposition: attachment;filename="'.$this->getId().'-'.date('YmdHi').'.xls"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
	break;
	case 'xlsx':
		header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
		header('Content-Disposition: attachment;filename="'.$this->getId().'-'.date('YmdHi').'.xlsx"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel2007');
	break;
	case 'csv':
		header('Content-Type: text/csv');
		header('Content-Disposition: attachment;filename="'.$this->getId().'-'.date('YmdHi').'.csv"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'CSV');
		$objWriter->setUseBom(true)->setEnclosure('')->setDelimiter(';');
	break;
	case 'html':
		header('Content-Type: text/html');
		header('Content-Disposition: attachment;filename="'.$this->getId().'-'.date('YmdHi').'.html"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'HTML');
		// $objWriter->setUseBom(true);
	break;
	case 'pdf':
		header('Content-Type: application/pdf');
		header('Content-Disposition: attachment;filename="'.$this->getId().'-'.date('YmdHi').'.pdf"');
		$objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'PDF');
		$objWriter->setOrientation(PHPExcel_Worksheet_PageSetup::ORIENTATION_LANDSCAPE);
	break;
}

header('Cache-Control: max-age=0');
$objWriter->save('php://output');
