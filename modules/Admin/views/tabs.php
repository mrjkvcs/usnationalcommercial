<?php if (is_array($this->tabs) && count($this->tabs)): ?>
<ul id="uniadmin_tabs">
	<?php foreach ($this->tabs as $tab): ?>
	<li>
		<a href="<?php echo isset($tab['readOnly']) && $tab['readOnly'] ? 'javascript:;' : Url::link($tab['module'], $tab['action']); ?>"<?php if (Url::isActive(Url::link($tab['module'], $tab['action']), false, true)) echo ' class="active"'; ?>>
			<?php echo $tab['title']; ?>
		</a>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif; ?>