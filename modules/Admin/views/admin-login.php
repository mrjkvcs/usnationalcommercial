<?php
$server = AdminConfig::$server;
$separator = UniAdmin::app()->route->urlSeparator;
?><!doctype html>
<html>
<head>
<!--
uniAdmin <?php echo UniAdmin::getVersion(); ?> | copyright László Kenéz | www.kenlas.hu
-->
<meta name="generator" content="uniAdmin v<?php echo UniAdmin::getVersion(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="<?php echo LANG; ?>" />
<title><?php echo $this->title; ?></title>
<link rel="stylesheet" type="text/css" href="<?php echo $server; ?>loader/3.0/style.php?domain=<?php echo WebsiteInfo::get('siteUrl'); ?>&version=<?php echo UniAdmin::getVersion(); ?>" />
<script type="text/javascript">
adminBase = '<?php echo Url::link(''); ?>';
serverUrl = '<?php echo $server; ?>';
isMobileDevice = <?php echo UniAdmin::app()->isMobileDevice() ? 'true' : 'false'; ?>;
</script>
</head>
<body class="uniadmin-login-page">

<div id="uniadmin-login-container">
	<div id="uniadmin-login-box">
		<h1><?php echo t('login-to') . ' ' . WebsiteInfo::get('title'); ?></h1>
		<form action="<?php echo $_SERVER['REQUEST_URI']; ?>" method="post">
			<input type="hidden" name="redirect" value="<?php echo isset($_GET['redirect']) ? urldecode($_GET['redirect']) : UniAdmin::app()->route->getCurrentUri(); ?>" />
			<input type="text" size="20" placeholder="<?php echo t('username'); ?>" name="login_user" id="uniadmin-login-user"<?php if (isset($_COOKIE['uniadmin_username'])) echo ' value="' . $_COOKIE['uniadmin_username'] . '"'; ?> />
			<input type="password" size="20" placeholder="<?php echo t('password'); ?>" name="login_password" id="uniadmin-login-password" />

			<span class="option"><input type="checkbox" name="login_save" id="login-save" value="1"<?php if (isset($_COOKIE['uniadmin_username'])) echo ' checked="checked"'; ?> /> <label for="login-save"><?php echo t('save-data'); ?></label></span>
			<span class="option"><input type="checkbox" name="login_auto" id="login-auto" value="1" /> <label for="login-auto"><?php echo t('log-in-automatically'); ?></label></span>

			<?php if (isset($_GET['error']) && ($_GET['error'] == 'ip' || $_GET['error'] == 'user')): ?>
			<div id="uniadmin-login-error">
				<?php
				switch ($_GET['error']) {
					case 'user':	echo t('wrong-username-or-password');
						break;
					case 'ip':		echo t('ip-address-blocked');
						break;
				} 
				?>
			</div>
			<?php endif; ?>

			<input type="submit" value="<?php echo t('login'); ?>" id="uniadmin-login-submit" />
		</form>
	</div>
</div>
<script type="text/javascript" src="<?php echo $server; ?>loader/3.0/script.php?domain=<?php echo WebsiteInfo::get('siteUrl'); ?>&version=<?php echo UniAdmin::getVersion(); ?>"> </script>
<script type="text/javascript">$('form :input[type!="hidden"]').first().focus();</script>
</body>
</html>