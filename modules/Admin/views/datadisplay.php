<?php

$parameters = (array) $this->downloadParameters;
foreach (array('s', 'o', 'data_filter') as $key) {
	if (isset($parameters[$key])) {
		unset($parameters[$key]);
	}
}
?>
<div class="data-display">
	<?php

	echo '<h1>' . $this->title . ' (' . $this->numRows . ')';

	if ($this->isDownloadable && $this->getId()) {
		$querystring = $parameters;
		echo '<span class="data-download"><ul>';
		$querystring['type'] = 'xls';
		echo '<li><a href="' . Url::link($this->baseModule, array('getlist' => $this->getId()), LANG, $querystring) . '" class="xls">' . t('download-format-excel') . ' (.xls)</a></li>';
		$querystring['type'] = 'xlsx';
		echo '<li><a href="' . Url::link($this->baseModule, array('getlist' => $this->getId()), LANG, $querystring) . '" class="xls">' . t('download-format-excel') . ' (.xlsx)</a></li>';
		$querystring['type'] = 'csv';
		echo '<li><a href="' . Url::link($this->baseModule, array('getlist' => $this->getId()), LANG, $querystring) . '" class="xls">' . t('download-format-csv') . ' (.csv)</a></li>';
		$querystring['type'] = 'pdf';
		echo '<li><a href="' . Url::link($this->baseModule, array('getlist' => $this->getId()), LANG, $querystring) . '" class="pdf">' . t('download-format-pdf') . ' (.pdf)</a></li>';
		echo '</ul></span>';
	}

	if ($this->hasPaging && $this->numRows > $this->limit) {
		echo '<span class="data-paging">';
		$querystring = $parameters;
		if ($this->start > 0) {
			$querystring['s'] = 0;
			echo '<a href="' . Url::link($this->baseModule, $this->baseAction, LANG, $querystring) . '" title="' . t('first-page') . '" data-start="' . $querystring['s'] . '">&laquo;&laquo;</a> &nbsp;';
			$querystring['s'] = $this->start - $this->limit;
			echo '<a href="' . Url::link($this->baseModule, $this->baseAction, LANG, $querystring) . '" title="' . t('previous-page') . '" data-start="' . $querystring['s'] . '">&laquo;</a> &nbsp;';
		}
		if ($this->numRows > $this->limit) {
			echo '<select class="data-pages">';
			for ($i = 0; $i < ceil($this->numRows / $this->limit); $i++) {
				echo '<option value="' . ($i * $this->limit) . '"' . ($i * $this->limit == $this->start ? ' selected="selected"' : '') . '>' . ($i + 1) . '</option>';
			}
			echo '</select>';
		}
		if ($this->start + $this->limit < $this->numRows) {
			$querystring['s'] = $this->start + $this->limit;
			echo '&nbsp; <a href="' . Url::link($this->baseModule, $this->baseAction, LANG, $querystring) . '" title="' . t('next-page') . '" data-start="' . $querystring['s'] . '">&raquo;</a>';
			$querystring['s'] = ceil(($this->numRows - $this->limit) / $this->limit) * $this->limit;
			echo '&nbsp; <a href="' . Url::link($this->baseModule, $this->baseAction, LANG, $querystring) . '" title="' . t('last-page') . '" data-start="' . $querystring['s'] . '">&raquo;&raquo;</a>';
		}
		echo '</span>';
	}

	echo '</h1>';

	if ($this->insertTitle) {
		echo '<a href="' . Url::link($this->baseModule, $this->insertLink) . '" class="data-add-button"><span>' . $this->insertTitle . '</span></a>';
	}
	if ($this->hasFilter): ?>
	<form action="<?php echo Url::link($this->baseModule, $this->baseAction); ?>" method="get" class="data-filter-form">
		<?php foreach ($parameters as $key => $value) {
			if ($key != 'data_filter') {
				echo '<input type="hidden" name="' . $key . '" value="' . $value . '" />';
			}
		}
		?>
		<p><?php echo t('search'); ?>: <input type="text" name="data_filter" class="data-filter" size="30" placeholder="<?php echo t('search'); ?>" value="<?php echo stripslashes($this->filterText); ?>" 
		/><input type="submit" value="<?php echo t('filter'); ?>" class="data-filter-submit" /></p>
	</form>
	<?php endif; ?>

	<table cellpadding="0" cellspacing="0" id="<?php echo $this->id; ?>" class="data"<?php if ($this->hasFullWidth) echo ' style="width:100%"'; ?> data-list="<?php echo $this->id; ?>" data-module="<?php echo $this->baseModule; ?>"<?php if (!empty($parameters)): ?> data-parameters="<?php echo http_build_query($parameters); ?>"<?php endif; if ($this->beforeContextMenu): ?> data-before="<?php echo $this->beforeContextMenu; ?>"<?php endif; if ($this->contextMenuCallback): ?> data-callback="<?php echo $this->contextMenuCallback; ?>"<?php endif; ?>>
		<thead>
			<?php if (Db::numrows($this->result) > 0):
			$columnClasses = array();
			?>
			<tr class="data-fields">
				<?php
				$route = UniAdmin::app()->route->getRoute();
				$currentModule = $route[0];
				$actions = array_slice($route, 1);
				$querystring = $parameters;
				if (is_array($this->columns)) foreach ($this->columns as $column):
				if (!in_array($column, $this->forbiddenFields)):
				$columnTitle = isset($this->columnFormats[$column]) && isset($this->columnFormats[$column]['title']) ? $this->columnFormats[$column]['title'] : $column;
				$columnClasses[$column] = isset($this->columnFormats[$column]) && isset($this->columnFormats[$column]['class']) ? $this->columnFormats[$column]['class'] : '';
				?>
				<td<?php if ($columnClasses[$column]) echo ' class="' . $columnClasses[$column] . '"'; ?>><?php if ($this->hasOrdering) {
						if ($this->order == $column) {
							$querystring['o'] = $column . ' DESC';
						} else {
							$querystring['o'] = $column;
						}
						echo '<a href="' . Url::link($currentModule, $actions, LANG, $querystring) . '" data-field="' . urlencode($querystring['o']) . '">' . $columnTitle . '</a>';
					} else {
						echo $columnTitle;
					}
				?>
				</td>
				<?php endif; endforeach; ?>
			</tr>
			<?php endif; ?>
		</thead>
		<tbody>
		<?php while ($rs = Db::loop($this->result)):
			if (count($this->keys)) {
				$keys = array();
				foreach ($this->keys as $key) {
					$keys[] = $rs[$key];
				}
				$keys = implode(";", $keys);
			} else {
				$keys = '';
			}
			?><tr<?php if ($keys) echo ' id="' . $keys . '"'; ?>>
				<?php foreach ($this->columns as $num => $column):
				if (!in_array($column, $this->forbiddenFields)): ?>
				<td<?php if ($columnClasses[$column]) echo ' class="' . $columnClasses[$column] . '"'; ?>><?php
				$value = $rs[Db::fieldname($this->result, $num)];
				if (!in_array($column, array_keys($this->dataHandlers))) {
					echo (is_bool($value) || is_null($value) ? '&nbsp;' : nl2br(strip_tags($value)));
				} else {
					$ret = $this->callDataHandler($column, $value, $keys, $rs);
					echo (is_bool($ret) || is_null($ret) ? '&nbsp;' : $ret);
				}
				?></td>
				<?php endif; endforeach; ?>
			</tr>
		<?php endwhile; ?>
		</tbody>

	<?php if (!Db::numrows($this->result)): ?>
	<tfoot>
	<tr class="data-fields"><td colspan="<?php echo (count($this->columns) - count($this->forbiddenFields)); ?>">
	<?php echo t('no-matching-elements-found'); ?></td></tr></tfoot>
	<?php endif; ?>
	</table>

	<?php if (is_array($this->contextMenu) && count($this->contextMenu)) {
		$contextMenu = new ContextMenu(array(
			'id' => $this->id,
			'baseModule' => $this->baseModule,
			'elements' => $this->contextMenu,
			'callbackBefore' => $this->beforeContextMenu ?: null,
			'callbackAfter' => $this->contextMenuCallback ?: null,
		), true);
	}
	?>
</div>