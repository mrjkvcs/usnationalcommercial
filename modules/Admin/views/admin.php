<?php
$server = AdminConfig::$server;
$separator = UniAdmin::app()->route->urlSeparator;
$module = strtolower(UniAdmin::app()->route->getCurrentModule());
$isNavigationLocked = UserPreferences::has('ua-navigator-lock') ? UserPreferences::get('ua-navigator-lock') : 1;
?><!doctype html>
<html>
<head>
<!--
uniAdmin <?php echo UniAdmin::getVersion(); ?> | copyright László Kenéz | www.kenlas.hu
-->
<meta name="generator" content="uniAdmin v<?php echo UniAdmin::getVersion(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
<meta http-equiv="Content-Language" content="<?php echo LANG; ?>" />
<?php if ($this->title || $this->titleprefix || $this->titlesuffix): ?>
<title><?php echo $this->titleprefix . $this->title . $this->titlesuffix; ?></title>
<?php endif; ?>
<link rel="stylesheet" type="text/css" href="<?php echo $server; ?>loader/3.0/style.php?domain=<?php echo WebsiteInfo::get('siteUrl'); ?>&version=<?php echo UniAdmin::getVersion(); ?>" />
<!--<link rel="stylesheet" type="text/css" href="modules/Admin/css/customs.css" />-->
<script type="text/javascript">
adminBase = '<?php echo Url::link(''); ?>';
serverUrl = '<?php echo $server; ?>';
isMobileDevice = <?php echo UniAdmin::app()->isMobileDevice() ? 'true' : 'false'; ?>;
</script>
<?php foreach ($this->cssFiles as $css => $mediaType): ?>
<link rel="stylesheet" type="text/css" href="<?php echo (substr($css, 0, 7) == 'http://') ? $css : '/clientside' . $separator . 'css' . $separator . str_replace('.', $separator, $css); ?>"<?php if ($mediaType && $mediaType != 'all') echo ' media="' . $mediaType . '"'; ?> />
<?php endforeach; ?>
<?php if ($this->extraheader) echo $this->extraheader; ?>
</head>
<body>

<div id="uniadmin_overlaybg" style="display:none"></div>
<div id="uniadmin_overlay" style="display:none"></div>
<div id="uniadmin_indicator" style="display:none"></div>
<div id="uniadmin_contextmenu" style="display:none"></div>

<?php /*<div id="uniadmin_header">
	<a href="http://<?php echo WebsiteInfo::get('siteUrl') . Url::link('', null, CONTENT_LANG, null, null, true); ?>" id="uniadmin_site" target="_blank">
		<?php echo t('view-site'); ?>
	</a>
</div>*/?>

<div id="uniadmin-navigator" <?php if ($isNavigationLocked) echo ' class="locked"'; ?>>
	<div id="uniadmin-logo">
		<a href="<?php echo Url::link(''); ?>">
			<img src="<?php echo $server . 'gfx/' . UniAdmin::getVersion() . '/logo.png'; ?>" alt="" />
		</a>
	</div>
	<div id="uniadmin-navbar">
		<div class="scrollbar"><div class="track"><div class="thumb"><div class="end"></div></div></div></div>
		<div class="viewport"><div class="overview">
			<div id="uniadmin-title"><?php echo WebsiteInfo::get('title'); ?></div>
			<a href="#" id="uniadmin-navigator-lock" title="<?php echo t('lock-unlock-navigator'); ?>">x</a>
			<?php 
			$lanuages = Setting::getLanguages();
			if (count($lanuages) > 1): ?>
			<select id="uniadmin-content-language">
				<?php foreach ($lanuages as $key => $value): ?>
				<option value="<?php echo $key; ?>"<?php if (CONTENT_LANG == $key) echo ' selected="selected"'; ?>><?php echo $value; ?></option>
				<?php endforeach; ?>
			</select>
			<?php endif; ?>
			<?php UniAdmin::widget('modules.Admin.ModuleMenu'); ?>
		</div></div>
	</div>
</div>
<?php
$classes = array('module-' . $module); 
if ($isNavigationLocked) {
	$classes[] = 'nav-visible';
}
?>
<div id="uniadmin-main" class="<?php echo implode(' ', $classes); ?>"><?php 
	UniAdmin::app()->path->setSeparator(' / ')->renderView();

	echo str_replace(array("\t", "\n", "\r"), '', $this->content);
	?><div id="uniadmin-user-profile">
		<span class="user-name"><?php echo UniAdmin::app()->user->getUserName(); ?></span>
		<ul>
			<li><a href="<?php echo Url::link('adminprofile'); ?>"><?php echo t('edit-profile'); ?></a></li>
			<li><a href="<?php echo Url::link('adminprofile', 'logout'); ?>"><?php echo t('logout'); ?></a></li>
		</ul>
	</div>
</div>
<script type="text/javascript" src="<?php echo $server; ?>loader/3.0/script.php?domain=<?php echo WebsiteInfo::get('siteUrl'); ?>&version=<?php echo UniAdmin::getVersion(); ?>"> </script>
<?php foreach ($this->scriptFiles as $script): ?>
<script type="text/javascript" src="<?php echo (substr($script, 0, 7) == 'http://') ? $script : '/clientside' . $separator . 'javascript' . $separator . str_replace('.', $separator, $script); ?>"> </script>
<?php endforeach;
if (!empty($this->scriptTags)): ?>
<script type="text/javascript">
<?php foreach ($this->scriptTags as $script) {
	echo $script . "\n";
} ?>
</script>
<?php endif; ?>
<script type="text/javascript">$(document).LoginKeepAlive(<?php echo (ini_get('session.gc_maxlifetime') - 60); ?>);</script>
</body>
</html>