<?php

class AdminConfig {
	/**
	 * Központi uniAdmin szerver
	 */
	public static $server = 'http://server.uniadmin.hu/';

	/**
	 * Admin felület nyelve
	 */
	public static $language = 'en';

	public static $authentication = 'modules.Admin.classes.AdminUserAuthentication';
	
	/**
	 * Modulmenü csoportjai
	 */
	public static $menuTypes = array(
		'webshop', 
		'content', 
		'function', 
		'system', 
		'settings'
	);

	/**
	 * Fájlok importálása csak az adminisztrációs felülethez
	 */
	public static $import = array();

	/**
	 * Fájlok kötelező betöltése csak az adminisztrációs felülethez
	 */
	public static $forcedImport = array();

	/**
	 * Tartalom szerkesztő méretei, ha fixek (egyébként legyen false)
	 */
	public static $composerSize = array(630, 500);
}
