<?php

class ModuleMenu extends BaseAdminModule {
	const name = 'Modulmenü';

	public function __construct() {
		parent::__construct();

		$parameters = new EventParameter();
		$menuId = 'uniadminmenu-' . UniAdmin::app()->user->getId();

		if (UniAdmin::app()->cache->isEnabled()) {
			$cachedVersion = UniAdmin::app()->cache->get($menuId);
			if ($cachedVersion) {
				$this->renderView('admin.modulemenu', array(
					'menus' => $cachedVersion,
				));
				return;
			}
		}

		$parameters->menus = array();

		$this->dispatchEvent('getMenu', $parameters);

		$parameters->menus[] = array('title' => t('edit-profile'), 'module' => 'adminprofile', 'action' => '', 'type' => 'settings');
		$parameters->menus[] = array('title' => t('add-to-favorites'), 'module' => '#', 'action' => '', 'id' => 'add-to-favorites', 'type' => 'settings');
		// $parameters->menus[] = array('title' => t('logout'), 'module' => 'adminprofile', 'action' => 'logout', 'type' => 'settings');

		if (is_array($parameters->menus)) {
			if (UniAdmin::app()->cache->isEnabled() && !$cachedVersion) {
				UniAdmin::app()->cache->set($menuId, $parameters->menus, 1800);
			}

			$this->renderView('admin.modulemenu', array(
				'menus' => $parameters->menus,
			));
		}
	}

	public static function clearCache() {
		if (UniAdmin::app()->cache->isEnabled()) {
			$users = Db::sql("SELECT id FROM UniadminUsers");
			while ($user = Db::loop($users)) {
				UniAdmin::app()->cache->delete('uniadminmenu-' . $user['id']);
			}
		}
	}
}
