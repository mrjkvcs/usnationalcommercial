<?php

class ContextMenu extends Widget {
	protected $parameters;

	public function __construct($parameters = array(), $autoRender = false) {
		parent::__construct();

		$this->parameters = new EventParameter();

		if (is_array($parameters)) {
			if (isset($parameters['id'])) {
				$this->parameters->id = $parameters['id'];
			}
			if (isset($parameters['baseModule'])) {
				$this->parameters->baseModule = $parameters['baseModule'];
			}
			if (isset($parameters['elements']) && is_array($parameters['elements'])) {
				$this->parameters->elements = $parameters['elements'];
			}
			if (isset($parameters['initScript'])) {
				$this->parameters->initScript = (bool) $parameters['initScript'];
			}
			if (isset($parameters['callbackBefore'])) {
				$this->parameters->callbackBefore = $parameters['callbackBefore'];
			}
			if (isset($parameters['callbackAfter'])) {
				$this->parameters->callbackAfter = $parameters['callbackAfter'];
			}
		}
		if (!isset($this->parameters->id)) {
			throw new Error('contextmenu initializing without ID');
		}
		if (!isset($this->parameters->elements)) {
			$this->parameters->elements = array();
		}
		if (!isset($this->parameters->initScript)) {
			$this->parameters->initScript = true;
		}
		if (!isset($this->parameters->callbackBefore)) {
			$this->parameters->callbackBefore = null;
		}
		if (!isset($this->parameters->callbackAfter)) {
			$this->parameters->callbackAfter = null;
		}

		$this->dispatchEvent('menu-' . $this->parameters->id, $this->parameters);

		if ($autoRender) {
			$this->renderView();
		}
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		parent::renderView('modules.Admin.views.contextmenu');
	}
}
