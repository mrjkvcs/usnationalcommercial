<?php

class Tabs extends Widget {
	public $tabs = array();
	public $viewFile = 'admin.tabs';

	public function __construct($tabs = null, $autoLoad = false) {
		if (is_array($tabs)) {
			$this->tabs = $tabs;
		}

		if ($autoLoad) {
			$this->renderView();
		}
	}

	public function addTab($title, $module, $action = null, $readOnly = false) {
		$this->tabs[] = array(
			'title' => $title, 
			'module' => $module, 
			'action' => $action,
			'readOnly' => $readOnly,
		);
		return $this;
	}
}
