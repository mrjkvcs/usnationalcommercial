<?php

class ModulePermission {
	public $id;
	public $name;
	public $checkedByDefault = true;
	public $allowedActions = array();

	public function __construct($id = null, $name = '', $checkedByDefault = true) {
		if (!is_null($id)) {
			$this->id = $id;
			if (!is_null($name)) {
				$this->name = $name;
			}
			$this->setCheckedByDefault($checkedByDefault);
		}
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setName($name) {
		$this->name = $name;
		return $this;
	}

	public function setCheckedByDefault($status = true) {
		$this->checkAction = (bool) $status;
		return $this;
	}

	public function setAllowedActions() {
		$args = func_get_args();
		$this->allowedActions = array_map('strtolower', $args);
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function getName() {
		return $this->name;
	}

	public function getCheckedByDefault() {
		return $this->checkAction;
	}

	public function getAllowedActions() {
		return $this->allowedActions;
	}

	public function checkAction($action) {
		return in_array(strtolower($action), $this->allowedActions);
	}
}
