<?php

class UserPreferences {
	private static $preferences = array();

	private static function readPreferences() {
		if (!empty(self::$preferences)) {
			return self::$preferences;
		}

		$value = Db::sqlField(sprintf("
			SELECT preferences
			FROM UniadminUsers
			WHERE id = %d",
			UniAdmin::app()->user->getId()
		));
		self::$preferences = json_decode($value, true);
		if (!is_array(self::$preferences)) {
			self::$preferences = array();
		}
		return self::$preferences;
	}

	public static function has($key) {
		if (!defined('IS_BACKEND')) {
			return false;
		}

		$userPreferences = self::readPreferences();

		return isset($userPreferences[$key]);
	}

	public static function get($key) {
		if (!defined('IS_BACKEND')) {
			return false;
		}

		$userPreferences = self::readPreferences();

		if (is_null($key)) {	// minden beállítást küldünk
			return $userPreferences;
		}

		return isset($userPreferences[$key]) ? $userPreferences[$key] : null;
	}

	public static function set($key, $value = null) {
		if (!defined('IS_BACKEND')) {
			return false;
		}

		if (!is_array($key)) {
			$values = array($key => $value);
		} else {
			$values = $key;
		}

		self::readPreferences();

		foreach ($values as $key => $value) {
			self::$preferences[$key] = $value;
		}

		Db::sql(sprintf("
			UPDATE UniadminUsers
			SET preferences = '%s'
			WHERE id = %d",
			json_encode(self::$preferences),
			UniAdmin::app()->user->getId()
		));

		return true;
	}

	public static function delete($key) {
		if (!defined('IS_BACKEND')) {
			return false;
		}

		if (self::has($key)) {
			unset(self::$preferences[$key]);
		}

		Db::sql(sprintf("
			UPDATE UniadminUsers
			SET preferences = '%s'
			WHERE id = %d",
			json_encode(self::$preferences),
			UniAdmin::app()->user->getId()
		));

		return true;
	}

	public static function getHomeDirectory() {
		if (!defined('IS_BACKEND')) {
			return false;
		}

		return Db::sqlField(sprintf("
			SELECT homeDirectory
			FROM UniadminUsers
			WHERE id = %d",
			UniAdmin::app()->user->getId()
		));
	}
}
