<?php

class DataDisplay extends Widget {
	protected $id;
	protected $title;
	protected $connection = null;
	protected $hasPaging = true;
	protected $hasOrdering = true;
	protected $hasFilter = true;
	protected $hasFullWidth = false;
	protected $defaultOrder;
	protected $filterText;
	protected $start = 0;
	protected $limit = 25;
	protected $forbiddenFields = array();
	protected $keyFields = array();
	protected $searchFields = array();
	protected $dataHandlers = array();
	protected $columnFormats = array();
	protected $baseModule;
	protected $baseAction;
	protected $insertTitle;
	protected $insertLink = 'insert';
	protected $updateAction = 'update';
	protected $deleteAction = 'delete';
	protected $contextMenu = array();
	protected $beforeContextMenu;
	protected $contextMenuCallback;
	protected $isDownloadable = true;
	protected $downloadParameters;
	protected $sql;

	public function __construct($parameters = array(), $autoLoad = false) {
		if (is_array($parameters)) {
			foreach ($parameters as $key => $value) {
				switch ($key) {
					case 'id':				$this->setId($value);
						break;
					case 'connection':		$this->setConnection($value);
						break;
					case 'defaultOrder':	$this->setDefaultOrder($value);
						break;
					case 'paging':			$this->setPaging($value);
						break;
					case 'ordering':		$this->setOrdering($value);
						break;
					case 'filter':			$this->setFilter($value);
						break;
					case 'fullWidth':		$this->setFullWidth($value);
						break;
					case 'title':			$this->setTitle($value);
						break;
					case 'start':			$this->setStart($value);
						break;
					case 'limit':			$this->setLimit($value);
						break;
					case 'hideField':		if (!is_array($value)) {
												$value = array($value);
											}
											call_user_func_array(array($this, 'hideField'), $value);
						break;
					case 'keyField':		if (!is_array($value)) {
												$value = array($value);
											}
											call_user_func_array(array($this, 'keyField'), $value);
						break;
					case 'searchFields':	$this->setSearchFields($value);
						break;
					case 'columnFormats':	$this->setColumnFormats($value);
						break;
					case 'dataHandlers':	foreach ($value as $key => $item) {
												$this->addDataHandler($item[0], $item[1]);
											}
						break;
					case 'baseModule':		if (is_array($value)) {
												$this->setBaseModule($value[0], $value[1]);
											} else {
												$this->setBaseModule($value);
											}
						break;
					case 'contextMenu':		$this->setContextMenu($value);
						break;
					case 'beforeContextMenu':	$this->setBeforeContextMenu($value);
						break;
					case 'contextMenuCallback':	$this->setContextMenuCallback($value);
						break;
					case 'downloadable':	$this->setDownloadable($value);
						break;
					case 'insertButton':	if (is_array($value)) {
												$this->setInsertButton($value[0], isset($value[1]) ? $value[1] : null);
											} else {
												$this->setInsertButton($value);
											}
						break;
					case 'updateAction':	$this->setUpdateAction($value);
						break;
					case 'deleteAction':	$this->setDeleteAction($value);
						break;
					case 'sql':				$this->setSql($value);
						break;
				}
			}
		} else if (is_object($parameters)) {
			foreach (get_object_vars($parameters) as $name => $value) {
				$this->{$name} = $value;
			}
		}

		if ($autoLoad) {
			$this->renderView();
		}
	}

	public function getSql() {
		$this->initSql();
		return $this->sql;
	}

	public function setId($id) {
		$this->id = $id;
		return $this;
	}

	public function setConnection($connectionId) {
		$this->connection = $connectionId;
		return $this;
	}

	/**
	 * Beállítja az alapértelmezett rendezést
	 * @param string $column
	 */
	public function setDefaultOrder($column) {
		$this->defaultOrder = $column;
		return $this;
	}

	/**
	 * Beálítja, hogy a táblát ne lehessen lapozni
	 * @return DataDisplay
	 */
	public function setPaging($state) {
		$this->hasPaging = $state;
		return $this;
	}

	/**
	 * Beállítja, hogy a táblát ne lehessen más mezők szerint rendezni
	 * @return DataDisplay
	 */
	public function setOrdering($state) {
		$this->hasOrdering = $state;
		return $this;
	}

	/**
	 * Beállítja, hogy a tábla szűrhető legyen-e
	 * @param bool $state
	 * @return DataDisplay
	 */
	public function setFilter($state) {
		$this->hasFilter = $state;
		return $this;
	}

	/**
	 * Beállítja, hogy a tábla teljes szélességű legyen-e
	 * @param bool $state
	 * @return DataDisplay
	 */
	public function setFullWidth($state = true) {
		$this->hasFullWidth = $state;
		return $this;
	}

	/**
	 * Beállítja a tábla címsorának szövegét
	 * @param string $title
	 * @return DataDisplay
	 */
	public function setTitle($title) {
		$this->title = $title;
		return $this;
	}

	public function setStart($start) {
		$this->start = intval($start);
		return $this;
	}

	public function setLimit($limit) {
		$this->limit = intval($limit);
		return $this;
	}

	public function setBaseModule($module, $action = null) {
		$this->baseModule = $module;
		if (!is_null($action)) {
			$this->baseAction = $action;
		}
		return $this;
	}

	public function setInsertButton($title, $link = null) {
		$this->insertTitle = $title;
		if (!is_null($link)) {
			$this->insertLink = $link;
		}
		return $this;
	}

	public function setUpdateAction($action) {
		$this->updateAction = $action;
		return $this;
	}

	public function setDeleteAction($action) {
		$this->deleteAction = $action;
		return $this;
	}

	public function setContextMenu($contextMenu) {
		$this->contextMenu = $contextMenu;
		return $this;
	}

	public function setBeforeContextMenu($callback) {
		$this->beforeContextMenu = $callback;
		return $this;
	}

	public function setContextMenuCallback($callback) {
		$this->contextMenuCallback = $callback;
		return $this;
	}

	/**
	 * Ezt a jövőben csak boolean értékkel szabad használni
	 * Paraméter átadásara a setParameters függvény alkalmas!
	 */
	public function setDownloadable($status = true) {
		if (is_bool($status)) {
			$this->isDownloadable = $status;
		} else if (is_array($status)) {
			$this->isDownloadable = true;
			$this->setParameters($status);
		}
		return $this;
	}

	/**
	 * Paraméterek átadása a lista fájlnak
	 */
	public function setParameters(array $parameters = array()) {
		$this->downloadParameters = $parameters;
		return $this;
	}

	/**
	 * Manuálisan beállítja a keresési mezőket
	 * @param $searchFields array
	 */
	public function setSearchFields(array $searchFields) {
		$this->searchFields = $searchFields;
		return $this;
	}

	/**
	 * Beállítja az oszlop formátumokat
	 * Az UniAdmin 5.2 verziótól kezdve ez az előírt mód az oszlopnevek formázására, dataHandler-ek megadására,
	 * export adattípusok valamint CSS osztályok hozzárendelésére
	 * @param $columnFormats array
	 */
	public function setColumnFormats(array $columnFormats) {
		$this->columnFormats = $columnFormats;
		foreach ($columnFormats as $column => $format) {
			if (isset($format['dataHandler'])) {
				$this->addDataHandler($column, $format['dataHandler']);
			}
		}
		return $this;
	}

	public function setDataHandlers(array $dataHandlers) {
		$this->dataHandlers = $dataHandlers;
		return $this;
	}
	/**
	 * Elrejti a paraméterként megadott mezőket
	 * @param string
	 * @return DataDisplay
	 */
	public function hideField() {
		$fieldList = func_get_args();
		foreach ($fieldList as $field) {
			$this->forbiddenFields[] = $field;
		}
		return $this;
	}

	/**
	 * Megadja a kulcs mezőket
	 * @param string
	 * @return DataDisplay
	 */
	public function keyField() {
		$fieldList = func_get_args();
		foreach ($fieldList as $field) {
			$this->keyFields[] = $field;
		}
		return $this;
	}

	/**
	 * Hozzárendel egy mezőhöz egy adatkezelő függvényt
	 * @param string|array $column a mező neve, amihez a függvényt rendelni kell, vagy tömb a mezők neveivel
	 * @param string $callback a meghívandó függvény neve
	 * @return DataDisplay
	 */
	public function addDataHandler($column, $callback) {
		if (!is_array($column)) {
			$column = array($column);
		}
		foreach ($column as $colId) {
			$this->dataHandlers[$colId] = $callback;
		}
		return $this;
	}

	/**
	 * Beállít egy SQL parancsot, amelyet a show() metódus fog végrehajtani
	 * Hasznos lehet, hogy be szeretnénk állítani egy megjelenítést, majd megadni a lehetőséget, hogy egy beépülő modul ezt felülbírálja
	 * @param string $sql
	 * @return DataDisplay
	 */
	public function setSql($sql) {
		$this->sql = trim($sql);
		return $this;
	}

	public function getId() {
		return $this->id;
	}

	public function getContextMenu() {
		return $this->contextMenu;
	}

	protected function callDataHandler($column, $value, $key, $record = null) {
		$ret = '';
		if (is_callable($this->dataHandlers[$column])) {
			$ret = call_user_func($this->dataHandlers[$column], $value, $key, $record);
		}
		return $ret;
	}

	protected function initSql() {
		if (!isset($this->sql)) return false;

		$route = UniAdmin::app()->route->getRoute();
		if ($this->getId()) $hash = $route[0] . '-' . $this->getId();
		else $hash = $route[0] . '-' . (isset($route[1]) ? $route[1] : 'default');

		$this->filterText = isset($_GET['data_filter']) ? $_GET['data_filter'] : (isset($_SESSION[$hash]['filter']) ? $_SESSION[$hash]['filter'] : '');
		if ($this->hasFilter && $this->filterText) {
			$testResults = Db::sql($this->sql, $this->connection);
			if (!Db::error()) {
				$filterElements = preg_split('/\s/', $this->filterText, -1, PREG_SPLIT_NO_EMPTY);
				$where = array();
				if (empty($this->searchFields)) {
					// nincsenek kézzel megadott keresési mezők, megpróbálunk mezőket keresni
					$tables = array();
					for ($i = 0; $i < Db::numFields($testResults); $i++) {
						$table = Db::fieldTable($testResults, $i);
						if ($table) {
							$tables[$table] = true;
						}
					}
					foreach ($filterElements as $filterKey) {
						$filterKey = trim($filterKey);
						$conditions = array();
						foreach (array_keys($tables) as $table) {
							// ellenőrizzük, hogy létező tábláról van-e szó
							$testResults = Db::sql("SHOW TABLES LIKE '" . $table . "'", $this->connection);
							if (Db::numRows($testResults) == 0)  {
								continue;
							}
							// az összes oszlopban keresünk
							$testResults = Db::sql('SHOW COLUMNS FROM ' . $table, $this->connection);
							while ($rs = Db::loop($testResults)) {
								if (strpos($rs['Type'], 'char') !== false || strpos($rs['Type'], 'text') !== false) {
									$conditions[] = $table . '.' . $rs['Field'] . " LIKE '%" . Db::escapeString($filterKey) . "%'";
								} else if (is_numeric($filterKey) && strpos($rs['Type'], 'int') !== false) {
									$conditions[] = $table . '.' . $rs['Field'] . " = " . $filterKey;
								}
							}
						}
						$where[] = implode(' OR ', $conditions);
					}
				} else {
					// manuálisan beállított keresési mezők, csak ezeken kell végigmenni
					foreach ($filterElements as $filterKey) {
						foreach ($this->searchFields as $field => $type) {
							if (is_numeric($field) || strtolower($type) == 'like') {
								// csak mező van megadva vagy LIKE szerint keresünk
								$conditions[] = (is_numeric($field) ? $type : $field) . " LIKE '%" . Db::escapeString($filterKey) . "%'";
							} else {
								// minden más esetben egyenlőséget keresünk
								$conditions[] = $field . " = " . (is_numeric($filterKey) ? $filterKey : "'" . $filterKey . "'");
							}
						}
						$where[] = implode(' OR ', $conditions);
					}
				}
				if (count($where)) {
					$where = '(' . implode(') AND (', $where) . ')';
					if (strpos($this->sql, 'WHERE') !== false) {
						$this->sql .= ' AND ' . $where;
					} else {
						$this->sql .= ' WHERE ' . $where;
					}
				}
				if ($this->hasPaging) {
					$testResults = Db::sql(substr_replace($this->sql, 'SELECT SQL_CALC_FOUND_ROWS', 0, 6), $this->connection);
					$max = Db::sqlField("SELECT FOUND_ROWS()", $this->connection);
				} else {
					$testResults = Db::sql($this->sql, $this->connection);
					$max = Db::numrows($testResults);
				}
			} else {
				echo Db::error();
				return false;
			}
		} else {
			if ($this->hasPaging) {
				$testResults = Db::sql(substr_replace($this->sql, 'SELECT SQL_CALC_FOUND_ROWS', 0, 6), $this->connection);
				if (!Db::error()) {
					$max = Db::sqlField("SELECT FOUND_ROWS()", $this->connection);
				} else {
					echo Db::error();
					return false;
				}
			} else {
				$testResults = Db::sql($this->sql, $this->connection);
				if (!Db::error()) {
					$max = Db::numrows($testResults);
				} else {
					echo Db::error();
					return false;
				}
			}
		}

		if ($this->keyFields) {
			$this->keys = $this->keyFields;
			$dbKey = false;
		}
		else {
			$this->keys = array();
			$dbKey = true;
		}
		$this->columns = array();
		for ($i = 0; $i < Db::numfields($testResults); $i++) {
			$this->columns[] = Db::fieldname($testResults, $i);
			if ($dbKey && Db::fieldflags($testResults, $i) & 2) {	// primary key
				$this->keys[] = Db::fieldname($testResults, $i);
			}
		}
		$this->start = isset($_GET['s']) ? intval($_GET['s']) : (isset($_SESSION[$hash]['start']) ? $_SESSION[$hash]['start'] : 0);
		if ($this->start > $max) $this->start = 0;
		$this->order = isset($_GET['o']) ? $_GET['o'] : (isset($_SESSION[$hash]['order']) ? $_SESSION[$hash]['order'] : $this->defaultOrder);
		$this->numRows = $max;

		if ($this->order) {
			$this->sql .= ' ORDER BY `' . (strpos($this->order, ' DESC') === false ? $this->order . '`' : str_replace(' DESC', '` DESC', $this->order));
		}
		if ($this->hasPaging && strpos($this->sql, ' LIMIT ') === false) {
			$this->sql .= ' LIMIT ' . $this->start . ', ' . $this->limit;
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('DataDisplay sql query: ' . $this->sql, 'datadisplay');
		}

		$_SESSION[$hash] = array('start' => $this->start, 'order' => $this->order, 'filter' => $this->filterText);
	}

	protected function executeSql() {
		$this->result = Db::sql($this->sql, $this->connection);
	}

	protected function initContextMenu() {
		if (is_array($this->contextMenu) && !count($this->contextMenu)) {
			$this->contextMenu = array();
			if ($this->updateAction) {
				$this->contextMenu[] = array(
						'action' => array($this->updateAction, '[id]'),
						'text' => t('context-menu-edit'),
						'class' => 'edit',
					);
			}
			if ($this->deleteAction) {
				$this->contextMenu[] = array(
					'action' => array($this->deleteAction, '[id]'),
					'text' => t('context-menu-delete'),
					'class' => 'delete confirm',
				);
			}
		}
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		$this->initSql();
		$this->executeSql();
		$this->initContextMenu();
		parent::renderView('admin.datadisplay');
	}

	public function renderXls() { $this->renderExport('xls'); }

	public function renderXlsx() { $this->renderExport('xlsx'); }

	public function renderCsv() { $this->renderExport('csv'); }

	public function renderHtml() { $this->renderExport('html'); }

	public function renderPdf() { $this->renderExport('pdf'); }

	protected function renderExport($type) {
		$this->setPaging(false);
		$this->initSql();
		$this->executeSql();
		parent::renderView('admin.datadisplay-export', array('type' => $type));
	}
}
