<?php

class ModuleAccessLevel {
	public $permissions = array();

	public function addPermission($permission) {
		$this->permissions[$permission->getId()] = $permission;
		return $this;
	}

	public function getPermission($key) {
		return isset($this->permissions[$key]) ? $this->permissions[$key] : false;
	}

	public function getPermissions() {
		return $this->permissions;
	}
}
