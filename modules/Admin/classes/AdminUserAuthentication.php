<?php
/**
 * uniAdmin AdminUserAuthentication module
 *
 */

class AdminUserAuthentication extends BaseAdminModule {
	protected $_id;
	protected $_userName;

	public function __construct() {
		if ($this->checkLogin() || $this->autoLogin()) {
			$this->_id = intval($_SESSION['adminloginid']);
			$this->_userName = $_SESSION['adminlogin'];
		}
		parent::__construct();
	}

	public function checkLogin() {
		return isset($_SESSION['adminloginid']);
	}

	public function getId() {
		return $this->_id;
	}

	public function getUserName() {
		return $this->_userName;
	}

	public function getInheritance() {
		$isInherited = Db::sqlfield(sprintf("
			SELECT isInherited
			FROM UniadminUsers
			WHERE id = %d",
			$this->_id
		));
		return $isInherited;
	}

	public function getGroupId() {
		$groupId = Db::sqlfield(sprintf("
			SELECT groupId
			FROM UniadminUsers
			WHERE id = %d",
			$this->_id
		));
		return $groupId;
	}

	/**
	 * Ellenőrzi egy modul meglétét és a bejelentkezett felhasználó hozzáférési jogosultságát
	 */
	public function checkModuleAccess($moduleId) {
		$inheritance = $this->getInheritance();
		$type = $inheritance ? 'g' : 'u';
		$id = $inheritance ? $this->getGroupId() : $this->getId();
		$num = Db::sqlField(sprintf("
			SELECT COUNT(*)
			FROM UniadminModuleAccess
			WHERE type = '%s'
			AND userId = %d
			AND moduleId = '%s'",
			$type,
			$id,
			$moduleId
		));
		return (bool) $num;
	}

	public function getAccessibleModules() {
		$inheritance = $this->getInheritance();
		$type = $inheritance ? 'g' : 'u';
		$id = $inheritance ? $this->getGroupId() : $this->getId();

		$modules = array();
		$result = Db::sql(sprintf("
			SELECT id, controller
			FROM UniadminModules UM
			JOIN (SELECT moduleId AS controller, LEFT(moduleId, LENGTH(moduleId) - 10) AS moduleId, type, userId FROM UniadminModuleAccess) UMA ON UMA.moduleId = UM.id
			WHERE type = '%s' 
			AND userId = %d",
			$type,
			$id
		));
		while ($rs = Db::loop($result)) {
			$permissions = Db::toArray(sprintf("
				SELECT permission
				FROM UniadminModulePermissions
				WHERE moduleId = '%s'
				AND type = '%s'
				AND userId = %d",
				$rs['controller'],
				$type,
				$id
			));
			$modules[$rs['id']] = $permissions;
		}
		return $modules;
	}

	public function getInfo() {
		if (!$this->_id) {
			return false;
		}
		return Db::sqlrow(sprintf("SELECT * FROM UniadminUsers WHERE id = %d", $this->_id));
	}

	public function isRoot() {
		$isRoot = Db::sqlfield(sprintf("
			SELECT isRoot
			FROM UniadminUsers
			WHERE id = %d",
			$this->_id
		));
		return $isRoot;
	}

	public function __get($name) {
		if (!$this->_id) {
			return false;
		}
		$content = Db::sqlField(sprintf("
			SELECT `%s`
			FROM `UniadminUsers`
			WHERE `id` = %d",
			$name,
			$this->_id
		));
		if ($content) {
			$this->$name = $content;
		}
		return $content;
	}

	public function autoLogin() {
		if (isset($_COOKIE['uniadmin_autologin']) && isset($_COOKIE['uniadmin_username']) && isset($_COOKIE['uniadmin_password']) && !isset($_GET['error'])) {
			$username = Db::escapeString($_COOKIE['uniadmin_username']);
			$password = Db::escapeString($_COOKIE['uniadmin_password']);
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('autologin, user: ' . $username, 'authentication');
			}
			return $this->login($username, $password);
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot find autologin info', 'authentication', 2);
			}
			return false;
		}
	}

	public function login($username = null, $password = null) {
		if (is_null($username)) {
			$username = Db::escapeString($_POST['login_user']);
			$isPost = true;
		} else {
			$isPost = false;
		}
		if (is_null($password)) {
			$password = md5(Db::escapeString($_POST['login_password']));
		}

		if ($username && $password) {
			$result = Db::sql(sprintf("
				SELECT id, username, ipLimit
				FROM UniadminUsers
				WHERE username = '%s'
				AND password = '%s'",
				$username,
				$password
			));
			if ($rs = Db::loop($result)) {
				if (trim($rs['ipLimit']) && trim($rs['ipLimit']) != '*') {
					$pos = strpos($rs['ipLimit'], '*');
					if (($pos === false && $rs['ipLimit'] != $_SERVER["REMOTE_ADDR"])
						|| ($pos !== false
						&& substr($rs['ipLimit'], 0, $pos) != substr($_SERVER['REMOTE_ADDR'], 0, $pos))
					) {
						if (DEBUG_MODE) {
							UniAdmin::addDebugInfo('ip address not allowed, denied user ' . $rs['username'] . ' (ip: ' . $_SERVER['REMOTE_ADDR'] . ')', 'authentication');
						}
						$this->redirect('admin', '', LANG, array('error' => 'ip'));
					}
				}
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('successfull user login: #' . $rs['id'] . ' - ' . $rs['username'], 'authentication');
				}

				$parameters = new EventParameter();
				$parameters->user = $rs;
				$this->dispatchEvent('beforeLogin', $parameters);

				$this->_id = $rs['id'];
				$this->_userName = $rs['username'];
				$_SESSION['adminloginid'] = $rs['id'];
				$_SESSION['adminlogin'] = $rs['username'];

				Db::sql(sprintf("
					UPDATE UniadminUsers
					SET lastLoginAt = NOW()
					WHERE id = %d",
					$rs['id']
				));

				if ($isPost) {
					if (isset($_POST['login_save'])) {
						setcookie('uniadmin_username', $_POST['login_user'], time()+31536000, '/', $_SERVER['HTTP_HOST']);
						if (isset($_POST['login_auto'])) {
							setcookie('uniadmin_password', md5($_POST['login_password']), time()+31536000, '/', $_SERVER['HTTP_HOST']);
							setcookie('uniadmin_autologin', '1', time()+31536000, '/', $_SERVER['HTTP_HOST']);
						} else {
							setcookie('uniadmin_password', $_POST['login_password'], time()+31536000, '/', $_SERVER['HTTP_HOST']);
							setcookie('uniadmin_autologin', false, time()-3600, '/', $_SERVER['HTTP_HOST']);
						}
					} else {
						setcookie('uniadmin_username', false, time()-3600, '/', $_SERVER['HTTP_HOST']);
						setcookie('uniadmin_password', false, time()-3600, '/', $_SERVER['HTTP_HOST']);
					}
				}

				UniAdmin::app()->log->write('admin login #' . $this->_id);

				$this->dispatchEvent('afterLogin', $parameters);

				return true;
			}
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('error login: ' . $username, 'authentication', 2);
		}

		return false;
	}

	public function logout() {
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('logout', 'authentication');
		}

		$this->dispatchEvent('beforeLogout');

		unset($_SESSION['adminlogin'], $_SESSION['adminloginid']);
		setcookie('uniadmin_username', false, time()-3600, '/', $_SERVER['HTTP_HOST']);
		setcookie('uniadmin_password', false, time()-3600, '/', $_SERVER['HTTP_HOST']);

		$this->dispatchEvent('afterLogout');
	}
}
