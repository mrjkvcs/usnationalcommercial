<?php

class BaseAdminModule extends BaseModule {
	public $accessLevel;

	/**
	 * Ellenőrzi, hogy egy adott felhasználónak megvan-e adott jogosultsági köre
	 */
	protected function checkPermission($permission) {
		$permissions = $this->getPermissions();
		return ($permissions === true) || in_array($permission, $permissions);
	}

	/**
	 * Ellenőrzi egy adott action meghívásának jogát a bejelentkezett felhasználó alapján
	 */
	public function checkActionPermission($action) {
		$action = strtolower(substr($action, 6));

		// a listák lekérése nem függ a jogosultságoktól, ezt lista szinten kell ellenőrizni
		if ($action == 'getlist' || $action == 'getdictionary') {
			return true;
		}

		$permissions = $this->getPermissions();

		if ($permissions === true) {
			return true;
		}

		foreach ($permissions as $permission) {	// bejárjuk az összes engedélyt
			$allowedActions = $this->accessLevel->getPermission($permission)->getAllowedActions();
			if (is_array($allowedActions)) {
				foreach ($allowedActions as $allowed) {	// bejárjuk az engedélyezett műveleteket
					if ($action == strtolower($allowed)) {
						return true;
					}
				}
			} else if ($allowedActions == '*') {	// mindenhez van joga
				return true;
			}
		}

		return false;
	}

	/**
	 * Lekéri és visszaadja az aktuális felhasználó jogosultsági köreit
	 * Amennyiben nincsenek jogosultsági szintek, a visszatérési érték true
	 */
	public function getPermissions() {
		if (!($this->accessLevel instanceof ModuleAccessLevel)) {
			return true;	// nincsenek modulon belüli jogosultságok
		}

		if (UniAdmin::app()->user->isRoot()) {
			return array_keys($this->accessLevel->getPermissions());
		}

		if (UniAdmin::app()->user->getInheritance()) {
			$userGroup = UniAdmin::app()->user->getGroupId();
		}

		return Db::toArray(sprintf("SELECT permission
			FROM UniadminModulePermissions
			WHERE type = '%s' AND moduleId = '%s' AND userId = '%s'",
			isset($userGroup) ? 'g' : 'u',
			get_class($this),
			isset($userGroup) ? $userGroup : UniAdmin::app()->user->getId()));
	}

	/**
	 * Visszaadja a modul jogosultsági szintjeit, függetlenül attól, hogy a bejelentkezett felhasználónak van-e engedélye hozzá
	 * Amennyiben nincsenek jogosultsági szintek, a visszatérési érték true
	 */
	public function getAccessLevels() {
		if (!($this->accessLevel instanceof ModuleAccessLevel)) {
			return true;	// nincsenek modulon belüli jogosultságok
		}
		return $this->accessLevel->getPermissions();
	}

	/**
	 * Betölti a modulhoz tartozó CSS fájlt a szerverről
	 */
	public function loadCss() {
		$className = $this->getClassName();
		UniAdmin::app()->page->addCss(AdminConfig::$server . 'modules/' . $className . '/' . constant(get_class($this).'::version') . '/' . $className . '.css');
	}

	/**
	 * Betölti a modulhoz tartozó CSS fájlt a szerverről
	 */
	public function loadScript() {
		$className = $this->getClassName();
		UniAdmin::app()->page->addScript(AdminConfig::$server . 'modules/' . $className . '/' . constant(get_class($this).'::version') . '/' . $className . '.js');
	}

	public function loadDataDisplay($listFile, array $parameters = array()) {
		$parts = preg_split('/\./', $listFile, -1, PREG_SPLIT_NO_EMPTY);
		if (count($parts) == 1) {
			$parts[1] = $parts[0];
			$parts[0] = get_class($this);
			if (substr($parts[0], -10) == 'Controller') {
				$parts[0] = substr($parts[0], 0, -10);
			}
		}
		if (count($parts) == 2) {
			if (strtolower($parts[0]) == 'lists') {
				$viewFile = 'lists.' . $parts[1];
			} else {
				$parts[0] = ucfirst(strtolower($parts[0]));
				if (!defined('IS_BACKEND') || $parts[0] == 'Admin') {
					$viewFile = 'modules.' . $parts[0] . '.lists.' . $parts[1];
				} else {
					$viewFile = 'modules.Admin.modules.' . $parts[0] . '.lists.' . $parts[1];
				}
			}
		}

		$path = preg_replace('/\./', '/', $viewFile);

		if (file_exists($path . '.php')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading datadisplay file ' . $viewFile, 'datadisplay');
			}
			if (!empty($parameters)) {
				foreach ($parameters as $key => $value) {
					$$key = $value;
				}
			}

			$dataDisplay = require($path . '.php');
			if (is_a($dataDisplay, 'DataDisplay')) {
				if (!empty($parameters)) {
					$dataDisplay->setDownloadable($parameters);
				}
				return $dataDisplay;
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('cannot find datadisplay class ' . $path, 'datadisplay', 2, __FILE__, __LINE__);
				}
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('cannot find datadisplay file ' . $viewFile, 'datadisplay', 2, __FILE__, __LINE__);
			}
		}
	}

	/**
	 * Többnyelvű adatok mentése
	 */
	public function saveMultilingualFields($tableName, $keyName, $id, $fields, $form, $languageId = null) {
		if (is_null($languageId)) {
			$languageId = CONTENT_LANG;
		}
		$values = array();
		foreach ($fields as $item) {
			$field = $form->getField($item);
			$values[$field->getId()] = Db::escapeString($field->getValue());
		}

		$num = Db::sqlBool(sprintf("
			SELECT COUNT(*)
			FROM `%s`
			WHERE `%s` = '%s'
			AND `languageId` = '%s'",
			$tableName,
			$keyName,
			$id,
			$languageId
		));
		if (!$num) {
			$values[$keyName] = $id;
			$values['languageId'] = $languageId;
			Db::sql(sprintf("
				INSERT INTO `%s`
				(`%s`)
				VALUES
				('%s')",
				$tableName,
				implode('`, `', array_keys($values)),
				implode("', '", $values)
			));
		} else {
			$query = '';
			foreach ($values as $key => $value) {
				if ($query) $query .= ', ';
				$query .= "`" . $key . "` = '" . $value . "'";
			}
			Db::sql(sprintf("
				UPDATE `%s`
				SET %s
				WHERE %s = '%s'
				AND `languageId` = '%s'",
				$tableName,
				$query,
				$keyName,
				$id,
				$languageId
			));
		}
	}

	/**
	 * Többnyelvű adatok betöltése
	 */
	public function loadMultilingualFields($tableName, $keyName, $id, $fields, $form, $languageId = null) {
		if (is_null($languageId)) {
			$languageId = CONTENT_LANG;
		}
		$rs = Db::sqlrow(sprintf("
			SELECT `%s`
			FROM `%s`
			WHERE `%s` = '%s'
			AND `languageId` = '%s'",
			implode('`, `', $fields),
			$tableName,
			$keyName,
			$id,
			$languageId
		));
		if ($rs) {
			foreach ($rs as $key => $value) {
				$form->setValue($key, $value);
			}
		}
	}

	private function getClassName() {
		$className = strtolower(get_class($this));
		if (substr($className, -10) == 'controller') {
			$className = substr($className, 0, -10);
		}
		return $className;
	}

	public function actionGetList() {
		$listFile = UniAdmin::app()->route->getParameter('getlist', 1);
		$this->type = isset($_GET['type']) ? strtoupper($_GET['type']) : '';
		$dataDisplay = $this->loadDataDisplay($listFile, $_GET);
		switch ($this->type) {
			case 'XLS':
				$dataDisplay->renderXls();
			break;
			case 'XLSX':
				$dataDisplay->renderXlsx();
			break;
			case 'CSV':
				$dataDisplay->renderCsv();
			break;
			case 'HTML':
				$dataDisplay->renderHtml();
			break;
			case 'PDF':
				$dataDisplay->renderPdf();
			break;
			default:
				$dataDisplay->renderView();
		}
		exit;
	}

	public function actionGetDictionary($dictionaryId) {
		$className = ucfirst($this->getClassName());
		$path = 'modules/Admin/modules/' . $className . '/i18n/' . File::sanitizeName($dictionaryId) . '_' . LANG . '.php';
		$dictionary = array();
		if (file_exists($path)) {
			include($path);
		}
		echo json_encode($dictionary);
		exit;
	}
}
