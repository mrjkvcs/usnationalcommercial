<?php

abstract class AdminInstaller extends BaseAdminModule {
	protected $modulePrefix;
	protected $modulePrefixFrontend;
	protected $moduleId;
	protected $moduleController;
	protected $moduleFrontendController;

	public function __construct ($moduleId) {
		$this->moduleId = strtolower($moduleId);
		$this->modulePrefix = 'modules.Admin.modules.'.ucfirst($this->moduleId) . '.';
		$this->modulePrefixFrontend = 'modules.'.ucfirst($this->moduleId) . '.';
		$this->moduleController = $this->modulePrefix.ucfirst($this->moduleId) . 'Controller';
		$this->moduleFrontendController = $this->modulePrefixFrontend.ucfirst($this->moduleId) . 'Controller';

		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('-Fill- <pre>'
			.var_export($this->moduleId, true)."\n"
			.var_export($this->modulePrefix, true)."\n"
			.var_export($this->moduleController, true)."\n"
			.'</pre>', 'installer', 3, __FILE__, __LINE__);
		}
	}

	protected function externalObservers () { }

	public function install () {
		// korábbi eseménykezelők eltávolítása
		$this->unInstall();

		// adatbázis táblák létrehozása
		$this->checkTables();

		// jelenlegi eseménykezelők felvétele
		$this->addEventObserver('Modulemenu', 'getMenu', $this->moduleController, 'menu');
		$this->externalObservers();

		// beállítások betöltése, amennyiben léteznek
		$this->importSettings();

		// dokumentum sablonok betöltése, amennyiben léteznek
		$this->addTemplates($this->modulePrefix . 'install');
	}

	public function checkTables() {
		if (file_exists(str_replace('.', '/', $this->modulePrefix.'install.'.$this->moduleId).'.xml')) {
			$tables = new DatabaseSchema($this->modulePrefix.'install.'.$this->moduleId);
			$tables->check();
		}
	}

	public function unInstall () {
		$this->removeEventObservers($this->moduleController);
		$this->removeEventObservers($this->moduleFrontendController);
	}

	public function importSettings(){
		$filename = str_replace('.', '/', $this->modulePrefix) . 'install/settings.xml';

		if (!file_exists($filename)) {
			return;
		}

		$settings = simplexml_load_file($filename);

		foreach ($settings as $i => $group) {
			$position = 0;
			foreach ($group as $i2 => $setting) {
				if ($setting->setting) {
					foreach ($setting->setting as $s) {
						$this->updateSetting(array(
							'id'	=> $s->id,
							'name'	=> $s->name,
							'group'	=> $group->name,
							'value'	=> $s->value,
							'type'	=> $s->type,
							'possibleValues' => $s->possibleValues
						), $position++);
					}
				}
			}
		}
	}

	protected function updateSetting($setting, $position) {
		$currentSetting = Db::sqlRow(sprintf("
			SELECT * FROM UniadminSettings WHERE id = '%s' AND moduleId = '%s'
		",
			$setting['id'],
			$this->moduleId
		));

		//insert
		if (empty($currentSetting)) {
			Db::sql(sprintf("
				INSERT INTO `UniadminSettings` (
						`id`, `moduleId`, `group`, `name`, `value`, `position`, `type`, `possibleValues`
					)
					VALUES (
						'%s', '%s', '%s', '%s', '%s', %d, '%s', '%s'
					);
			",
				$setting['id'],
				$this->moduleId,
				$setting['group'],
				$setting['name'],
				$setting['value'],
				$position,
				$setting['type'],
				$setting['possibleValues']
			));
		}
		//update
		//jelenlegi value lekérése, ha az nincs benne az új settingsekbe, akkor a value legyen a legelső az új valeuk közül
		else {
			if (strtolower($setting['type']) == 'select' || strtolower($setting['type']) == 'radio') {
				$oldValue = $currentSetting['value'];
				$possibleValues = explode(';', $setting['possibleValues']);
				$possibleValuesArray = array();
				foreach ($possibleValues as $p) {
					$pv = explode('=', $p);
					$possibleValuesArray[] = $pv[0];
				}
				if (in_array($currentSetting['value'], $possibleValuesArray)) {
					$newValue = $currentSetting['value'];
				} else {
					$newValue = $possibleValuesArray[0];
				}
			} else {
				$newValue = $setting['value'];
			}

			Db::sql(sprintf("
				UPDATE `UniadminSettings`
				SET
					`group` = '%s',
					`name` = '%s',
					`value` = '%s',
					`type` = '%s',
					`position` = %d,
					`possibleValues` = '%s'
				WHERE
					`UniadminSettings`.`id` = '%s'
					AND `UniadminSettings`.`moduleId` = '%s';
			",
				$setting['group'],
				$setting['name'],
				$newValue,
				$setting['type'],
				$position,
				$setting['possibleValues'],
				$setting['id'],
				$this->moduleId
			));
		}
	}

	public function addEventObserver ($remoteModule, $event, $path, $method, $priority = 2) {
		if (DEBUG_MODE){
			UniAdmin::addDebugInfo('AdminInstaller::addEventObserver <pre>'
			.var_export($remoteModule, true)."\n"
			.var_export($event, true)."\n"
			.var_export($path, true)."\n"
			.var_export($method, true)."\n"
			.var_export($priority, true)."\n"
			.'</pre>', 'installer', 3, __FILE__, __LINE__);
		}

		if (Db::tableExists('UniadminModuleAddons')) {
			$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM `UniadminModuleAddons` WHERE `moduleId` = '%s' AND `event` = '%s' AND `path` = '%s' AND `method` = '%s'", $remoteModule, $event, $path, $method));
			if (!$num){
				Db::sql(sprintf("INSERT INTO `UniadminModuleAddons` (`moduleId`, `event`, `path`, `method`, `priority`) VALUES ('%s', '%s', '%s', '%s', %d)", $remoteModule, $event, $path, $method, $priority));
			}
			else{
				Db::sql(sprintf("UPDATE `UniadminModuleAddons` SET `priority` = %d WHERE `moduleId` = '%s' AND `event` = '%s' AND `path` = '%s' AND `method` = '%s'", $priority, $remoteModule, $event, $path, $method));
			}
		}
		return $this;
	}

	public function removeEventObservers ($path) {
		if (DEBUG_MODE){
			UniAdmin::addDebugInfo('AdminInstaller::removeEventObservers <pre>'
			.var_export($path, true)."\n"
			.'</pre>', 'installer', 3, __FILE__, __LINE__);
		}
		if (Db::tableExists('UniadminModuleAddons')) {
			Db::sql(sprintf("DELETE FROM `UniadminModuleAddons` WHERE `path` = '%s'", $path));
		}
		return $this;
	}

	public function addTemplates ($path) {
		$path = str_replace('.', '/', $path);
		if (!file_exists($path) || !is_dir($path)) {
			return false;
		}

		$files = new DirectoryIterator($path);
		if (!empty($files)){
			$categoryId = $this->getCategoryId('templates', 'Honlapsablonok');
		}
		if (!$categoryId) {
			return false;
		}
		foreach ($files as $fileInfo){
			$extension = end(explode('.', $fileInfo->getFileName()));
			if ($fileInfo->isDir()&&$fileInfo->getFileName()!='.'&&$fileInfo->getFileName()!='..'){
				$subDirFiles = new DirectoryIterator($path.'/'.$fileInfo->getFileName());
				if (!empty($subDirFiles)){
					$subCategoryId = $this->getCategoryId($fileInfo->getFileName());
				}
				if (!$subCategoryId) {
					break;
				}
				foreach ($subDirFiles as $subDirFileInfo){
					$subDirExtension = end(explode('.', $subDirFileInfo->getFileName()));
					if (!$subDirFileInfo->isDir()&&$subDirExtension=='htm'){
						$this->readTemplate($path.'/'.$fileInfo->getFileName().'/'.$subDirFileInfo->getFileName(), $subCategoryId);
					}
				}
			}
			else if ($extension=='htm'){
				$this->readTemplate($path.'/'.$fileInfo->getFileName(), $categoryId);
			}
		}
	}

	private function getCategoryId ($category, $categoryName = null) {
		if (!isset($this->categoryCache)){
			$this->categoryCache = array();
		}
		if (isset($this->categoryCache[$category])){
			return $this->categoryCache[$category];
		}
		if (!Db::tableExists('DocumentCategory')) {
			return false;
		}
		if (is_null($categoryName)){
			$categoryName = $category;
		}
		$categoryId = Db::sqlField(sprintf("
			SELECT id
			FROM DocumentCategory
			WHERE idText = '%s'", $category
		));
		if (!$categoryId){
			$categoryId = Db::sql(sprintf("
				INSERT INTO DocumentCategory
				(name, idText)
				VALUES
				('%s', '%s')", ucfirst($categoryName), $category
			));
		}
		$this->categoryCache[$category] = $categoryId;
		return $categoryId;
	}

	private function readTemplate ($path, $categoryId) {
		$fileName = basename($path);
		$parts = explode('_', substr($fileName, 0, -4));
		$languageId = end($parts);
		$id = implode('-', array_slice($parts, 0, -1));
		list ($id, $title) = preg_split('/[\{\}]/', $id);
		if (!mb_check_encoding($title, 'utf-8')){
			$title = iconv('iso-8859-2', 'utf-8', $title);
		}
		if ($id&&$languageId){
			$num = Db::sqlField(sprintf("
				SELECT COUNT(*)
				FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d", $id, $languageId, $categoryId));
			if (!$num){
				$content = file_get_contents($path);
				Db::sql(sprintf("
					INSERT INTO Document
					(id, title, languageId, categoryId, authorId, created, content, textContent)
					VALUES
					('%s', '%s', '%s', %d, %d, NOW(), '%s', '%s')", $id, Db::escapeString($title), $languageId, $categoryId, UniAdmin::app()->user->getId(), Db::escapeString($content), Db::escapeString(strip_tags($content))
				));
			}
		}
	}

}
