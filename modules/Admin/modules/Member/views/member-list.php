<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('Member inserted successfully'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('Member saved successfully'));
		break;
    case 'deleted':
        SysMessage::displaySuccess(t('Member deleted successfully'));
        break;
	case 'exists':
			SysMessage::displaySuccess(t('Member already exists'));
		break;
}

$this->loadDataDisplay('member-list')->renderView();
