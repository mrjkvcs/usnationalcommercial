<?php

class MemberController extends BaseAdminModule {

    const name = 'Member';
    const version = '1.0';

    public function __construct()
    {
        parent::__construct();

        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters)
    {
        $parameters->menus[] = array(
                'title'    => 'GOLF MEMBER',
                'module'   => 'member',
                'action'   => '',
                'submenus' => array(
                    array('title' => 'Add new Member', 'module' => 'member', 'action' => 'insert')
                    ),
                'type'     => 'content'
                );
    }

    protected function checkItemId($memberId)
    {
        $this->memberId = intval($memberId);
        if (! $this->memberId)
        {
            return Url::link('member');
        }
    }

    public function beforeAction()
    {
        UniAdmin::app()->path->addElement('member Results', 'member');
    }

    public function actionDefault()
    {
        $this->viewFile = 'member-list';
        UniAdmin::app()->path
            ->addElement('Season', 'member')
            ->addElement('Event (weeks)', 'member');
        UniAdmin::app()->page->addCss('modules.Admin.modules.member.css.member');
    }

    public function actionInsert()
    {
        $this->viewFile = 'member';
        UniAdmin::app()->path->addElement('Add new user', 'user', 'insert');

        $this->memberEdit();
    }

    public function actionUpdate($memberId)
    {
        $this->viewFile = 'member';
        $this->memberEdit($memberId);
    }

    public function actionDelete($memberId)
    {
        Db::sql(sprintf("DELETE FROM Member WHERE id = %d", $this->memberId));

        $this->redirect('member', '', LANG, array('msg' => 'deleted'));
    }

    protected function memberEdit($memberId = null)
    {
        UniAdmin::import('classes.forms.Form');
        $loadXML = $this->action == 'update' ? 'member-edit' : 'member';
        $this->form = new Form();
        $this->form
            ->loadFromFile('member.' . $loadXML)
            ->setAfterSaveCallBack(function($id, $form, $type) {
                    $image = $form->getField('upload')->getImage();
                    if (!empty($image))
                    {
                    $dir = sprintf('images/member/');
                    ! is_dir($dir) ? mkdir($dir, 0777, true) : null;
                    $path = $dir . sprintf('%d.jpg', $id);
                    Util::saveImage($image, $path, 800, 450);
                    }
                    })
        ->setBackAction('member');

        if ($this->form->isSent())
        {
            $this->form->queryValues();
            if ($this->action != 'update')
            {
                $this->form->addDbField('insertedAt', date('Y-m-d'));
            }
            $this->form->save($this->action);
        } else
        {
            if ($this->action == 'update')
            {
                $field = new FHtml();
                $field->setText('<img src="/images/member/' . $memberId . '.jpg" width="150" />')->setLabel('Default Picture');
                $this->form
                    ->loadValues('id = ' . $memberId)
                    ->addFieldBefore('firstName', $field)
                    ->setSubmitValue('Update');
            }
        }
    }
}
