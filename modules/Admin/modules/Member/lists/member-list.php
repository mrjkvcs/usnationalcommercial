<?php

$list = new DataDisplay(array(
	'sql' => "
		SELECT
			id,
			firstName,
			lastName,
			email,
			occupation,
			insertedAt,
			isActive
		FROM Member",
	'fullWidth' => true,
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'firstName' => array(
			'title' => t('first name'),			
		),
		'lastName' => array(
			'title' => t('last name'),
		),
        'email' => array(
            'title' => t('email'),
        ),
        'occupation' => array(
            'title' => t('occupation'),
        ),
		'insertedAt' => array(
			'title' => t('Registration'),
		),
		'isActive' => array(
			'title' => t('active'),
			'dataHandler' => function($value) {
				return $value ? t('yes') : t('no');
			}
		),
	),
	'title' => t('Member list'),
	'baseModule' => 'member',
    'insertButton' => t('Add New Member'),
));

return $list;
