<?php

$dictionary['website-informations'] = 'Weboldal információk';
$dictionary['message-infos-saved'] = 'A weboldal adatainak mentése sikeresen megtörtént';

$dictionary['form-website-details'] = 'Webolal információk szerkesztése';
$dictionary['group-general'] = 'Általános adatok';
$dictionary['group-specific'] = 'Nyelvfüggő szöveges információk';
$dictionary['label-site-url'] = 'Honlap URL:';
$dictionary['label-email-sender'] = 'Kimenő levelek feladója:';
$dictionary['label-email-address'] = 'Bejövő levelek címzettje:';
$dictionary['label-google-analytics'] = 'Google Analytics tracker:';
$dictionary['label-favicon'] = 'Kedvencek ikon:';
$dictionary['label-title'] = 'Oldal címe:';
$dictionary['label-slogan'] = 'Szlogen:';
$dictionary['label-meta-keywords'] = 'META kulcsszavak:';
$dictionary['label-meta-description'] = 'META leírás:';
$dictionary['label-logo'] = 'Logó:';
$dictionary['label-current-logo'] = 'Jelenlegi logó:';
$dictionary['delete-logo'] = 'Jelenlegi logó eltávolítása';
