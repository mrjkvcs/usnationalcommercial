<?php

$dictionary['website-informations'] = 'Website informations';
$dictionary['message-infos-saved'] = 'Website informations saved successfully';

$dictionary['form-website-details'] = 'Edit website details';
$dictionary['group-general'] = 'General informations';
$dictionary['group-specific'] = 'Language specific text informations';
$dictionary['label-site-url'] = 'Site URL:';
$dictionary['label-email-sender'] = 'Outgoing email address:';
$dictionary['label-email-address'] = 'Incoming email address:';
$dictionary['label-google-analytics'] = 'Google Analytics tracker:';
$dictionary['label-favicon'] = 'Favorites icon:';
$dictionary['label-title'] = 'Site title:';
$dictionary['label-slogan'] = 'Slogan:';
$dictionary['label-meta-keywords'] = 'META keywords:';
$dictionary['label-meta-description'] = 'META description:';
$dictionary['label-logo'] = 'Logo:';
$dictionary['label-current-logo'] = 'Current logo:';
$dictionary['delete-logo'] = 'Remove current logo';
