<?php

class WebsiteinfoInstaller extends AdminInstaller {
    public function __construct() {
        parent::__construct('Websiteinfo');
    }

    protected function externalObservers() {
		$this->addEventObserver('ModulemanagerController', 'getCacheHandlers', 'modules.Admin.modules.Websiteinfo.WebsiteinfoController', 'cacheHandler');
    }

	public function install() {
		parent::install();

		$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM WebsiteInfo"));
		if (!$num) {
			Db::sql(sprintf("
				INSERT INTO WebsiteInfo
				(property, languageId, value)
				VALUES
				('siteUrl', '', '%s'),
				('emailSender', '', '%s'),
				('emailAddress', '', '%s'),
				('favicon', '', '/favicon.ico')",
				Db::escapeString($_SERVER['HTTP_HOST']),
				Db::escapeString('info@' . $_SERVER['HTTP_HOST']),
				Db::escapeString('info@' . $_SERVER['HTTP_HOST'])
			));
		}
	}
}
