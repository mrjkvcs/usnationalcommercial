<?php

class WebsiteinfoController extends BaseAdminModule {
	const name = 'Weboldal információk';
	const version = '1.0';

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('base');

		$this->imgPath = 'images/logo/';
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('website-informations'),
			'module' => 'websiteinfo',
			'action' => '',
			'type' => 'settings'
		);
	}

	public function actionDefault() {
		$this->viewFile = 'edit';

		UniAdmin::import('classes.forms.Form');

		$this->form = new Form('websiteinfo');
		$this->form->loadFromFile('websiteinfo.edit');

		$prevImage = Db::sqlField(sprintf("
			SELECT value 
			FROM WebsiteInfo 
			WHERE property = 'logo'
			AND languageId = '%s'",
			CONTENT_LANG
		));

		$logoFileName = $this->imgPath . 'logo-' . CONTENT_LANG . '.png';
		if ($this->form->isSent()) {
			$this->form->queryValues();
			$this->form->getField('logo')->setDestination($logoFileName);

			if ($this->form->getField('logo')->getTempName()) {
				// kép áthelyezése, ha fel van töltve
				$this->form->getField('logo')->evaluate();
				$this->form->setValue('logo', $logoFileName);
				UniAdmin::app()->log->setType('admin')->write('uploaded new website logo');
			} else if ($this->form->getValue('removePicture')) {
				// igény esetén törlés
				$this->form->setValue('logo', '');
				if ($prevImage) {
					UniAdmin::app()->log->setType('admin')->write('removed website logo');
					@unlink($prevImage);
				}
			} else {
				// egyéb esetben marad a régi
				$this->form->setValue('logo', $prevImage);
			}
			$this->form->removeField('removePicture');

			$currentProperties = Db::toArray(sprintf("
				SELECT property, languageId
				FROM WebsiteInfo
				WHERE languageId = '' OR languageId = '%s'",
				CONTENT_LANG
			), true);
			foreach ($this->form->getElements() as $id => $field) {
				$languageId = $field->getGroup() == 'specific' ? CONTENT_LANG : '';
				if (isset($currentProperties[$id]) && $currentProperties[$id] == $languageId) {	// update
					Db::sql(sprintf("
						UPDATE WebsiteInfo
						SET value = '%s'
						WHERE property = '%s' 
						AND languageId = '%s'",
						Db::escapeString($field->getValue()),
						Db::escapeString($id),
						Db::escapeString($languageId)
					));
				} else {
					Db::sql(sprintf("
						INSERT INTO WebsiteInfo
						(property, languageId, value)
						VALUES
						('%s', '%s', '%s')",
						Db::escapeString($id),
						Db::escapeString($languageId),
						Db::escapeString($field->getValue())
					));
				}
			}

			$this->clearCache();

			$this->redirect('websiteinfo', '', LANG, array('msg' => 'saved'));
		} else {
			$result = Db::sql(sprintf("
				SELECT property, languageId, value
				FROM WebsiteInfo
				WHERE languageId = '' OR languageId = '%s'",
				CONTENT_LANG
			));
			while ($rs = Db::loop($result)) {
				if ($this->form->hasField($rs['property'])) {
					$this->form->setValue($rs['property'], $rs['value']);
				}
			}
		}

		if ($prevImage) {
			$imgField = new FHtml();
			$imgField
				->setId('prevImage')
				->setLabel(t('label-current-logo'))
				->setText('<img src="/' . $prevImage . '?rnd=' . time() . '" alt="" />')
				->appendAfter('logo', $this->form);
		} else {
			$this->form->removeField('removePicture');
		}
	}

	public function cacheHandler($parameters) {
		$parameters->elements['websiteinfo'] = array(
			'title' => t('Weboldal információk'),
			'description' => t('A weboldal szöveges és META információi'),
			'callback' => function(){
				return $this->clearCache();
			},
		);
	}

	protected function clearCache() {
		if (UniAdmin::app()->cache->isEnabled()) {
			foreach (Setting::getLanguages() as $id => $language) {
				UniAdmin::app()->cache->delete('WebsiteInfo-' . $id);
			}
			return true;
		}
		return false;
	}
}
