function initFileBrowser() {
	$('#filebrowser-files a').click(function(event) {
		var link = $(this).attr('href').replace('./', '');
		if (link.substring(link.length - 2) == '/.') {	// ha mappa
			link = link.substring(0, link.length -2);
		}
		fileBrowserData['field'].val(link);
		fileBrowserData['field'].change();
		fileBrowserDialog.dialog('close');
		event.preventDefault();
	});
}