jQuery(document).ready(function(){
	UniAdmin.loadDictionary('filebrowser', 'base');
});

function initFileBrowser() {
	$('#filebrowser-files a').click(function(event) {
		var link = $(this).attr('href').replace('./', '');
		if (link.substring(link.length - 2) == '/.') {	// ha mappa
			link = link.substring(0, link.length -2);
		}
		fileBrowserData['field'].val(link);
		fileBrowserData['field'].change();
		fileBrowserDialog.dialog('close');
		$('.contextMenu').hide();
		event.preventDefault();
	});
	$('#filebrowser-files a').contextMenu({
		menu: 'contextMenu-filelist',
		parent: $('.ui-dialog').first(),
	}, function(item, srcElement, event){
		var path = fileBrowserData['path'];
		// visszaállítjuk a korábbi linket, ha már át lett írva
		$(item).attr('href', unescape($(item).data('originalHref'))
			.replace(/\[path\]/gi, path)
			.replace(/\[id\]/gi, $(srcElement).closest('[id]').attr('id'))
		);
		$(item).attr('href', unescape($(item).attr('href')).replace(/\[path\]/gi, path));
		if (item.hasClass('rename')) {
			var newName = prompt(t('new-filename'), $(srcElement).closest('.item').attr('id'));
			if (newName && $(srcElement).closest('.item').attr('id') != newName) {
				var hasExtension = newName.indexOf('.');
				if (hasExtension > -1 || confirm(t('confirm-save-without-extension'))) {
					$(item).attr('href', $(item).attr('href') + '&newname=' + newName);
				} else {
					$('.contextMenu').hide();
					return false;
				}
			} else {
				$('.contextMenu').hide();
				return false;
			}
		}
		if (item.hasClass('download')) {	// tovább engedjük a linkre
			$(item).attr('href', unescape($(item).data('originalHref'))
				.replace(/\[path\]/gi, path)
				.replace(/\[id\]/gi, $(srcElement).closest('[id]').attr('id')));
			return;
		}
		if (item.hasClass('copy') || item.hasClass('cut')) {	// eddig nem lehetett beilleszteni
			$('.contextMenu .disabled').removeClass('disabled');
		}
		$.post($(item).attr('href'), { 'return': 1 }, function(response){
			var msg = response.parameters.msg;
			if (msg == 'filerenamed' || msg == 'filecopied' || msg == 'filepasted' || msg == 'filedeleted') {
				 UniAdmin.loadFileList(adminBase +  '/filebrowser/renderfilelist?path=' + fileBrowserData['path']);
			} else if (msg) {
				alert(msg);
			}
		});
		$('.contextMenu').hide();
		return false;
	});
}