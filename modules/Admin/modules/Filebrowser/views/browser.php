<div>
	<div id="filebrowser-directories" class="treeview filetree"></div>
	<div id="filebrowser-files"></div>
	<div id="filebrowser-upload"></div>
	<a href="#" id="filebrowser-upload-button">
		<img src="<?php echo AdminConfig::$server . 'gfx/' . UniAdmin::getVersion(); ?>/file-upload-button.png" alt="<?php echo t('upload'); ?>" title="<?php echo t('upload'); ?>" />
		<span><?php echo t('upload-files-from-local-machine'); ?></span>
	</a>
	<?php
	$isClipboardUsed = isset($_SESSION['filemanager']['clipboard']);

	UniAdmin::widget('modules.Admin.classes.ContextMenu', array(
		'id' => 'filelist',
		'baseModule' => 'filemanager',
		'initScript' => false,
		'elements' => array(
			array(
				'action' => 'renamefile',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('rename'),
				'class' => 'rename edit',
			),
			array(
				'action' => 'download',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('download'),
				'class' => 'download',
			),
			array(
				'action' => 'copyfile',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('copy'),
				'class' => 'separator copy',
			),
			array(
				'action' => 'cutfile',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('cut'),
				'class' => 'cut',
			),
			array(
				'action' => 'pastefile',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('paste'),
				'class' => 'paste',
				'disabled' => !$isClipboardUsed,
			),
			array(
				'action' => 'deletefile',
				'queryString' => array('path' => '[path]', 'filename' => '[id]'),
				'text' => t('delete'),
				'class' => 'separator delete confirm',
			),
		),
	));
	?>
</div>
