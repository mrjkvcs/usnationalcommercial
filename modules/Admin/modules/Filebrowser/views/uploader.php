<h1><?php echo t('upload-new-file'); ?></h1>

<?php if (!$this->isWritable): ?>
<?php SysMessage::displayError(t('directory-is-write-protected')); ?>
<?php else: ?>
<form method="post" action="<?php echo Url::link('filemanager', 'upload'); ?>" enctype="multipart/form-data">
	<p>
		<?php echo t('file-to-upload'); ?>: 
		<input type="file" size="40" name="file" id="filebrowser-upload-file" />
		<input type="hidden" name="path" value="<?php echo $this->destinationDir; ?>" />
		<input type="hidden" name="extensions" value="<?php echo $this->extensions; ?>" />
	</p>
</form>

<fieldset id="file-upload-information">
	<legend><?php echo t('upload-details'); ?></legend>
	<p><?php echo t('maximum-upload-size'); ?>: <?php echo Text::formatBytes($this->maxSize); ?></p>
	<p><?php echo t('destination-directory'); ?>: /<?php echo $this->destinationDir; ?></p>
	<p><?php echo t('allowed-file-types'); ?>: <?php echo empty($this->allowedFileTypes) ? t('all-types') : implode(', ', $this->allowedFileTypes); ?></p>
</fieldset>
<?php endif; ?>

<a href="#" id="filebrowser-return">&laquo; <?php echo t('back-to-file-list'); ?></a>