<?php

$dictionary['upload'] = 'Upload';
$dictionary['rename'] = 'Rename...';
$dictionary['download'] = 'Download';
$dictionary['copy'] = 'Copy';
$dictionary['cut'] = 'Cut';
$dictionary['paste'] = 'Paste';
$dictionary['delete'] = 'Delete...';

$dictionary['select-folder'] = 'select folder';
$dictionary['upload-new-file'] = 'Upload new file';
$dictionary['directory-is-write-protected'] = 'The selected directory is not writable, you cannot upload to it.<br />Select another folder or check the file system permissions.';
$dictionary['file-to-upload'] = 'File to upload';
$dictionary['upload-details'] = 'File upload information';
$dictionary['maximum-upload-size'] = 'Maximum upload file size';
$dictionary['destination-directory'] = 'Destination directory';
$dictionary['allowed-file-types'] = 'Allowed file types';
$dictionary['all-types'] = 'all types';
$dictionary['back-to-file-list'] = 'Go back to the file list';
$dictionary['upload-files-from-local-machine'] = 'Upload files from my computer';

$dictionary['new-filename'] = 'New filename:';
$dictionary['confirm-save-without-extension'] = 'Are you sure to save the file without extension?';

$dictionary['alias-documents'] = 'Uploaded files';
$dictionary['alias-images'] = 'Images';
$dictionary['alias-logs'] = 'Logs';
$dictionary['alias-banners'] = 'Banners';
$dictionary['alias-catalogue'] = 'Product catalog';
$dictionary['alias-gallery'] = 'Gallery';
$dictionary['alias-highlights'] = 'Highlights';
$dictionary['alias-logo'] = 'Logo';
