<?php

$dictionary['upload'] = 'Feltöltés';
$dictionary['rename'] = 'Átnevezés...';
$dictionary['download'] = 'Letöltés';
$dictionary['copy'] = 'Másolás';
$dictionary['cut'] = 'Kivágás';
$dictionary['paste'] = 'Beillesztés';
$dictionary['delete'] = 'Törlés...';

$dictionary['select-folder'] = 'mappát kiválaszt';
$dictionary['upload-new-file'] = 'Új fájl feltöltése';
$dictionary['directory-is-write-protected'] = 'A kiválasztott könyvtár nem írható, erre a helyre nem tud feltölteni!<br />Válasszon másik mappát, vagy ellenőrizze a fájlrendszer jogosultságait.';
$dictionary['file-to-upload'] = 'Feltöltendő fájl';
$dictionary['upload-details'] = 'Fájlfeltöltés adatai';
$dictionary['maximum-upload-size'] = 'Maximálisan feltölthető fájlméret';
$dictionary['destination-directory'] = 'Cél könyvtár';
$dictionary['allowed-file-types'] = 'Engedélyezett fájltipusok';
$dictionary['all-types'] = 'minden típus';
$dictionary['back-to-file-list'] = 'Visszalépés a fájllistához';
$dictionary['upload-files-from-local-machine'] = 'Fájlok feltöltése';

$dictionary['new-filename'] = 'Új fájlnév:';
$dictionary['confirm-save-without-extension'] = 'Biztosan elmenti a fájlt kiterjesztés nélkül?';

$dictionary['alias-documents'] = 'Feltöltött fájlok';
$dictionary['alias-images'] = 'Képek';
$dictionary['alias-logs'] = 'Naplófájlok';
$dictionary['alias-banners'] = 'Bannerek';
$dictionary['alias-catalogue'] = 'Termékkatalógus';
$dictionary['alias-gallery'] = 'Galéria';
$dictionary['alias-highlights'] = 'Kiemelt tartalmak';
$dictionary['alias-logo'] = 'Logó';
