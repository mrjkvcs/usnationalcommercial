<?php

class FilebrowserController extends BaseAdminModule {
	const name = 'Fájltallózó';
	const version = '1.0';

	public function __construct() {
		UniAdmin::import('modules.Admin.modules.Filemanager.DirectoryList');
		UniAdmin::import('modules.Admin.modules.Filemanager.FileList');
		$this->homeDirectory = ltrim(UserPreferences::getHomeDirectory(), '/');
		$this->loadDictionary('base');
	}

	public function beforeAction() {
		UniAdmin::import('modules.Admin.modules.Filemanager.*');
		$this->config = new FilebrowserConfig();
	}

	public function actionGetBrowser() {
		$this->viewFile = 'browser';
	}

	public function actionRenderDirectoryList() {
		$directoryList = new DirectoryList();
		if (isset($_GET['actual']) && $_GET['actual']) {
			$directoryList->setActualDirectory($_GET['actual']);
		}
		$directoryList
			->setHomeDirectory($this->homeDirectory)
			->setFileBrowserMode()
			->loadDirectories()
			->renderView();
		exit;
	}

	public function actionRenderFileList() {
		$path = isset($_GET['path']) ? $_GET['path'] : '';
		$extensions = isset($_GET['extensions']) ? $_GET['extensions'] : null;
		$onlyDirs = isset($_GET['onlydirs']) ? (bool) $_GET['onlydirs'] : false;
		$fileList = new FileList();
		if ($onlyDirs) {
			$fileList->setOnlyDirs();
		}
		if ($extensions && $extensions != '*') {
			$fileList->setExtensions(explode(',', $extensions));
		}
		$fileList
			->setHomeDirectory($this->homeDirectory)
			->setPath($path)
			->renderView();
		exit;
	}

	public function actionGetUploader() {
		$maxUpload = intval(ini_get('upload_max_filesize'));
		$maxPost = intval(ini_get('post_max_size'));
		$memoryLimit = intval(ini_get('memory_limit'));
		$limits = array();
		if ($maxUpload != -1) {
			$limits[] = $maxUpload;
		}
		if ($maxPost != -1) {
			$limits[] = $maxPost;
		}
		if ($memoryLimit != -1) {
			$limits[] = $memoryLimit;
		}

		$this->maxSize = min($limits) * 1048576;
		$this->extensions = isset($_POST['extensions']) ? $_POST['extensions'] : '*';
		if ($this->extensions == '*') {
			$this->allowedFileTypes = array();
		} else {
			$this->allowedFileTypes = explode(',', $this->extensions);
			$this->allowedFileTypes = array_map('trim', $this->allowedFileTypes);
		}
		$this->destinationDir = isset($_POST['path']) ? urldecode($_POST['path']) : '';

		$this->isWritable = is_writable(($this->homeDirectory ? $this->homeDirectory . '/' : '') . $this->destinationDir);
		if ($this->isWritable 
			&& isset($this->config->readOnlyDirs) 
			&& in_array(($this->homeDirectory ? $this->homeDirectory . '/' : '') . $this->destinationDir, $this->config->readOnlyDirs)
		) {
			$this->isWritable = false;
		}

		echo $this->loadView('filebrowser.uploader');
		exit;
	}
}
