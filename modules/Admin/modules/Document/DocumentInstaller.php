<?php

class DocumentInstaller extends AdminInstaller {
	public function __construct() {
		parent::__construct('Document');
	}

	public function externalObservers() {
		$this->addEventObserver('WelcomeController', 'showList', 'modules.Admin.modules.Document.DocumentController', 'welcomeShowList', 100);
		$this->addEventObserver('MenumanagerController', 'getMenuHandlers', 'modules.Admin.modules.Document.DocumentController', 'menuHandler');
		$this->addEventObserver('SearchController', 'searchResults', 'modules.Document.DocumentController', 'searchResults', 1);
	}
}
