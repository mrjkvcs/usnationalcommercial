jQuery(document).ready(function($){
	UniAdmin.loadDictionary('document', 'base');

	$('#document-button-0').click(function(){
		$('#back').val('1');
		$('#document').submit();
	});

	$('#document-category').change(function(event){
		location.href = location.pathname + '?category=' + $(event.target).val();
	});

	$('#uniadmin-main').on('click', '.contextMenu .rename', function(event){
		var oldId = $(this).closest('ul').data('target').split(';')[0];
		var newId = prompt(t('new-idetifier'), oldId);
		if (newId != null && $.trim(newId) != '') {
			$(event.target).attr('href', $(event.target).attr('href') + '/' + escape(newId));
		} else {
			event.preventDefault();
		}
	});

	$('#document-list').on('mouseenter', 'tbody .preview', function(event){
		var id = $(event.target).closest('[id]').attr('id');
		var div = $('<div id="documentPreview" class="dropshadow rounded"><iframe src="/admin/document/preview/' + id + '"></iframe></div>');
		$(event.target).append(div);
		$('#documentPreview').css({ top: event.pageY - 200, left: event.pageX - 510 });
	});
	$('#document-list').on('mouseleave', 'tbody .preview', function(){
		$('#documentPreview').remove();
	});

	$('#limitAccess').change(function(event){
		if ($(event.target).val() != 'g') {
			$('#s_limitByGroup').hide();
		} else {
			$('#s_limitByGroup').show();
		}
		if ($(event.target).val() != 'u') {
			$('#s_limitByUser').hide();
		} else {
			$('#s_limitByUser').show();
		}
	});
});