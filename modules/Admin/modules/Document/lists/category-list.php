<?php

if (!UniAdmin::app()->user->isRoot()) {
	$sql = sprintf(" WHERE `limitAccess` = '' OR `limitByGroup` = %d OR `limitByUser` = %d",
		UniAdmin::app()->user->getGroupId(), UniAdmin::app()->user->getId());
} else {
	$sql = '';
}

$categoryList = new DataDisplay(array(
	'sql' => sprintf("
		SELECT `id`, `name`, `description`, `isPublic`
		FROM `DocumentCategory`
		%s",
		$sql
	),
	'defaultOrder' => 'name',
	'fullWidth' => false,
	'hideField' => 'id',
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'name' => array(
			'title' => t('column-name'),
		),
		'description' => array(
			'title' => t('column-description'),
		),
		'isPublic' => array(
			'title' => t('column-public'),
			'dataHandler' => function($value){
				return $value ? t('yes') : t('no');
			},
		),
	),
	'updateAction' => 'categoryupdate',
	'deleteAction' => 'categorydelete',
	'title' => t('table-document-categories'),
	'baseModule' => array('document', 'category'),
));

return $categoryList;
