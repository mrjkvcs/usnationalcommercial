<?php

$moveToCategory = $copyToCategory = $copyToLanguage = array();
$result = Db::sql(sprintf("
	SELECT id, name
	FROM DocumentCategory
	WHERE id <> %d
	ORDER BY name",
	$category
));
while ($rs = Db::loop($result)) {
	$moveToCategory[] = array(
		'action' => array('move', '[id]', $rs['id']),
		'text' => $rs['name'],
		'class' => 'folder',
	);
	$copyToCategory[] = array(
		'action' => array('copytocategory', '[id]', $rs['id']),
		'text' => $rs['name'],
		'class' => 'folder',
	);
}

$languages = Setting::getLanguages();
if (count($languages) > 1) {
	foreach ($languages as $key => $value) {
		if ($key == CONTENT_LANG) {
			continue;
		}
		$copyToLanguage[] = array(
			'action' => array('copytolanguage', '[id]', $key),
			'text' => $value,
			'class' => 'font',
		);
	}
}

$contextMenu = array(
	array(
		'action' => array('update', '[id]'),
		'text' => t('edit'),
		'class' => 'edit',
	),
	array(
		'action' => array('rename', '[id]'),
		'text' => t('rename'),
		'class' => 'rename',
	),
	/*array(
		'action' => array('setasdefault', '[id]'),
		'text' => t('set-as-default'),
		'class' => 'home separator',
	),*/
	array(
		'action' => array('#'),
		'text' => t('move-to-category'),
		'class' => 'move separator',
		'submenus' => $moveToCategory,
	),
	array(
		'action' => array('copy', '[id]'),
		'text' => t('make-a-copy'),
		'class' => 'copy separator',
	),
	array(
		'action' => array('#'),
		'text' => t('copy-to-category'),
		'class' => 'copy',
		'submenus' => $copyToCategory,
	),
	array(
		'action' => array('delete', '[id]'),
		'text' => t('delete'),
		'class' => 'delete separator confirm',
	),
);

array_splice($contextMenu, -1, 0, array(array(
	'action' => array('#'),
	'text' => t('copy-to-language'),
	'class' => 'copy',
	'submenus' => $copyToLanguage,
)));


$documentList = new DataDisplay(array(
	'sql' => sprintf("
		SELECT
			`Document`.`id`,
			`title`,
			`languageId`,
			`UniadminUsers`.`username`,
			`categoryId`,
			`created`,
			`modified`
		FROM `Document`
		LEFT JOIN `UniadminUsers` 
			ON `Document`.`authorId` = `UniadminUsers`.`id`
		WHERE `languageId` = '%s'
		AND `categoryId` = %d",
		// (!isset($this->type) || !$this->type ? ", '' AS preview" : ''),
		CONTENT_LANG,
		$category
	),
	'defaultOrder' => 'title',
	'fullWidth' => true,
	'hideField' => 'categoryId',
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'id' => array(
			'title' => t('column-id'),
		),
		'title' => array(
			'title' => t('column-title'),
		),
		'languageId' => array(
			'title' => t('column-language'),
		),
		'username' => array(
			'title' => t('column-author'),
		),
		'created' => array(
			'title' => t('column-created'),
		),
		'modified' => array(
			'title' => t('column-last-modified'),
		),
		// 'preview' => array(
			// 'title' => t('column-preview'),
			// 'class' => 'preview',
			// 'dataHandler' => function(){
				// return t('hover-your-mouse-here');
			// },
		// ),
	),
	'title' => t('table-documents'),
	'baseModule' => 'document',
	'insertButton' => t('insert-document'),
	'contextMenu' => $contextMenu,
));

return $documentList;
