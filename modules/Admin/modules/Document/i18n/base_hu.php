<?php

$dictionary['form-documents'] = 'Dokumentum adatai';
$dictionary['button-save'] = 'Mentés';
$dictionary['button-save-and-return'] = 'Mentés és visszalépés';
$dictionary['label-title'] = 'Cím:';
$dictionary['label-id'] = 'Azonosító:';
$dictionary['label-language'] = 'Nyelv:';
$dictionary['label-category'] = 'Kategória:';
$dictionary['label-content'] = 'Tartalom:';
$dictionary['label-meta-description'] = 'META leírás:';
$dictionary['label-meta-keywords'] = 'META kulcsszavak:';
$dictionary['separate-keywords-with-commas'] = 'A kulcsszavakat vesszővel válassza el egymástól';

$dictionary['form-document-category'] = 'Dokumentum kategória adatai';
$dictionary['label-name'] = 'Megnevezés:';
$dictionary['label-description'] = 'Leírás:';
$dictionary['label-access-level'] = 'Hozzáférési szint:';
$dictionary['label-limit-by-group'] = 'Választott csoport:';
$dictionary['label-limit-by-user'] = 'Választott felhasználó:';
$dictionary['public-category'] = 'Publikus kategória';
$dictionary['tooltip-allow-or-deny-public-access'] = 'Engedélyezi vagy tiltja külső modulok hozzáférését ehhez a kategóriához';
$dictionary['no-restrictions'] = 'Nincs korlátozás';
$dictionary['group-level-access'] = 'Csoport szintű engedély';
$dictionary['user-level-access'] = 'Felhasználó szintű engedély';

$dictionary['documents'] = 'Dokumentumok';
$dictionary['document'] = 'Dokumentum';

$dictionary['document-list'] = 'Dokumentumok listája';
$dictionary['insert-document'] = 'Új dokumentum felvétele';
$dictionary['document-category-list'] = 'Dokumentum kategóriák';
$dictionary['insert-document-category'] = 'Új kategória felvétele';

$dictionary['categories'] = 'Kategóriák';
$dictionary['all-categories'] = 'Minden kategória';
$dictionary['new'] = 'Új';
$dictionary['recent-documents'] = 'Legutóbbi dokumentumok';
$dictionary['all-documents'] = 'Minden dokumentum';
$dictionary['selected-category'] = 'Kiválasztott kategória';

$dictionary['column-name'] = 'megnevezés';
$dictionary['column-description'] = 'leírás';
$dictionary['column-public'] = 'publikus';
$dictionary['yes'] = 'igen';
$dictionary['no'] = 'nem';
$dictionary['table-document-categories'] = 'Dokumentum kategóriák';

$dictionary['edit'] = 'Szerkesztés';
$dictionary['rename'] = 'Átnevezés...';
// $dictionary['set-as-default'] = 'Beállítás alapértelmezettnek';
$dictionary['move-to-category'] = 'Áthelyezés kategóriába';
$dictionary['make-a-copy'] = 'Másolat készítése';
$dictionary['copy-to-category'] = 'Másolás kategóriába';
$dictionary['copy-to-language'] = 'Másolás más nyelvre';
$dictionary['delete'] = 'Törlés...';
$dictionary['hover-your-mouse-here'] = 'húzza ide az egeret';

$dictionary['column-id'] = 'azonosító';
$dictionary['column-title'] = 'cím';
$dictionary['column-language'] = 'nyelv';
$dictionary['column-author'] = 'szerző';
$dictionary['column-created'] = 'létrehozva';
$dictionary['column-last-modified'] = 'utoljára módosítva';
$dictionary['column-preview'] = 'előnézet';
$dictionary['table-documents'] = 'Dokumentumok listája';

$dictionary['new-idetifier'] = 'Új azonosító:';

$dictionary['copy-format'] = "`%s`, ' másolata (%d)'";

$dictionary['message-document-created'] = 'A dokumentum létrehozása sikeresen megtörtént';
$dictionary['message-document-saved'] = 'A mentés sikeresen megtörtént';
$dictionary['message-document-deleted'] = 'A dokumentum törlése sikeresen megtörtént';
$dictionary['message-document-moved'] = 'A dokumentum áthelyezése sikeresen megtörtént';
$dictionary['message-document-copied'] = 'A dokumentum másolása sikeresen megtörtént';
$dictionary['message-document-set-default'] = 'A dokumentum alapértelmezetté tétele megtörtént';
$dictionary['message-document-already-exists'] = 'Ilyen nevű dokumentum már létezik a kategóriában a megadott nyelven';

$dictionary['message-category-created'] = 'A kategória létrehozása sikeresen megtörtént';
$dictionary['message-category-saved'] = 'A kategória mentése sikeresen megtörtént';
$dictionary['message-category-saved'] = 'A kategória törlése sikeresen megtörtént';
