<?php

$dictionary['form-documents'] = 'Document details';
$dictionary['button-save'] = 'Save';
$dictionary['button-save-and-return'] = 'Save and return';
$dictionary['label-title'] = 'Title:';
$dictionary['label-id'] = 'Identifier:';
$dictionary['label-language'] = 'Language:';
$dictionary['label-category'] = 'Category:';
$dictionary['label-content'] = 'Contents:';
$dictionary['label-meta-description'] = 'META description:';
$dictionary['label-meta-keywords'] = 'META keywords:';
$dictionary['separate-keywords-with-commas'] = 'Separate the keywords with commas';

$dictionary['form-document-category'] = 'Document category details';
$dictionary['label-name'] = 'Name:';
$dictionary['label-description'] = 'Description:';
$dictionary['label-access-level'] = 'Access level:';
$dictionary['label-limit-by-group'] = 'Selected group:';
$dictionary['label-limit-by-user'] = 'Selected user:';
$dictionary['public-category'] = 'Public category';
$dictionary['tooltip-allow-or-deny-public-access'] = 'Allow or deny public module access for this category';
$dictionary['no-restrictions'] = 'No restrictions';
$dictionary['group-level-access'] = 'Group level access';
$dictionary['user-level-access'] = 'User level access';

$dictionary['documents'] = 'Documents';
$dictionary['document'] = 'Document';

$dictionary['document-list'] = 'Browse documents';
$dictionary['insert-document'] = 'Create new document';
$dictionary['document-category-list'] = 'Document categories';
$dictionary['insert-document-category'] = 'Create new document category';

$dictionary['categories'] = 'Categories';
$dictionary['all-categories'] = 'All categories';
$dictionary['new'] = 'New';
$dictionary['recent-documents'] = 'Recent documents';
$dictionary['all-documents'] = 'Every document';
$dictionary['selected-category'] = 'Selected category';

$dictionary['column-name'] = 'name';
$dictionary['column-description'] = 'description';
$dictionary['column-public'] = 'public';
$dictionary['yes'] = 'yes';
$dictionary['no'] = 'no';
$dictionary['table-document-categories'] = 'Document categories';

$dictionary['edit'] = 'Edit';
$dictionary['rename'] = 'Rename...';
// $dictionary['set-as-default'] = 'Set as default';
$dictionary['move-to-category'] = 'Move to category';
$dictionary['make-a-copy'] = 'Make a copy';
$dictionary['copy-to-category'] = 'Copy to category';
$dictionary['copy-to-language'] = 'Copy to language';
$dictionary['delete'] = 'Delete...';
$dictionary['hover-your-mouse-here'] = 'hover your mouse here';

$dictionary['column-id'] = 'identifier';
$dictionary['column-title'] = 'title';
$dictionary['column-language'] = 'language';
$dictionary['column-author'] = 'author';
$dictionary['column-created'] = 'created at';
$dictionary['column-last-modified'] = 'last modified at';
$dictionary['column-preview'] = 'preview';
$dictionary['table-documents'] = 'Documents';

$dictionary['new-idetifier'] = 'New identifier:';

$dictionary['copy-format'] = "'Copy of ', `%s`, ' (%d)'";

$dictionary['message-document-created'] = 'Document created successfully';
$dictionary['message-document-saved'] = 'Document saved successfully';
$dictionary['message-document-deleted'] = 'Document deleted successfully';
$dictionary['message-document-moved'] = 'Document moved successfully';
$dictionary['message-document-copied'] = 'Document copied successfully';
$dictionary['message-document-set-default'] = 'Document has been set to default successfully';
$dictionary['message-document-already-exists'] = 'A document with that name already exists in the selected category';

$dictionary['message-category-created'] = 'Category added successfully';
$dictionary['message-category-saved'] = 'Category saved successfully';
$dictionary['message-category-saved'] = 'Category deleted successfully';
