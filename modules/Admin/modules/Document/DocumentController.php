<?php

class DocumentController extends BaseAdminModule {
	const name = 'Dokumentumkezelő';
	const version = '5.0';

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('document.base');
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('documents'),
			'module' => 'document',
			'action' => '',
			'submenus' => array(
				array('title' => t('document-list'), 'module' => 'document', 'action' => ''),
				array('title' => t('insert-document'), 'module' => 'document', 'action' => 'insert'),
				array(),
				array('title' => t('document-category-list'), 'module' => 'document', 'action' => 'category'),
				array('title' => t('insert-document-category'), 'module' => 'document', 'action' => 'categoryinsert'),
			),
			'type' => 'content'
		);
	}

	public function beforeAction() {
		UniAdmin::app()->page->addScript('modules.Admin.modules.Document.scripts.base');
		UniAdmin::app()->path->addElement(t('documents'), 'document');
	}

	public function actionDefault() {
		$this->viewFile = 'documentlist';
		UniAdmin::app()->path->addElement(t('document-list'), 'document');

		UniAdmin::app()->page->addCss('modules.Admin.modules.Document.css.document');

		$this->categoryList = $this->getCategoryList();

		$this->selectedCategory = isset($_GET['category']) ? intval($_GET['category']) : (UserPreferences::has('document-category') ? UserPreferences::get('document-category') : 0);

		if (!array_key_exists($this->selectedCategory, $this->getCategoryList())) {
			$this->selectedCategory = intval(key($this->categoryList));
		}

		UserPreferences::set('document-category', $this->selectedCategory);
	}

	public function actionInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('insert-document'), 'document', 'insert');

		$this->documentEdit();
	}

	public function actionUpdate() {
		$this->viewFile = 'edit';
		$this->documentEdit();
	}

	public function actionDelete() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		if ($id && $languageId && $categoryId) {
			Db::sql(sprintf("
				DELETE FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d",
				$id,
				$languageId,
				$categoryId
			));

			UniAdmin::app()->log->setType('admin')->write('deleted document: ' . $id . '-' . $languageId);

			$this->redirect('document', '', LANG, array('msg' => 'deleted'));
		}

		$this->redirect('document');
	}

	public function actionRename() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		$route = UniAdmin::app()->route->getRoute();
		$newId = Text::plain($route[3]);
		if ($id && $languageId && $categoryId && $newId) {
			Db::sql(sprintf("
				UPDATE `Document`
				SET `id` = '%s'
				WHERE `id` = '%s'
				AND `languageId` = '%s'
				AND `categoryId` = %d ",
				$newId,
				$id,
				$languageId,
				$categoryId
			));

			UniAdmin::app()->log->setType('admin')->write('renamed document ' . $id . '-' . $languageId . ' to ' . $newId);
		}

		$this->redirect('document', '', LANG, array('msg' => 'renamed'));
	}

	public function actionSetAsDefault() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		if ($id && $languageId && $categoryId) {
			Db::sql(sprintf("
				UPDATE Document
				SET isDefault = 1
				WHERE id = '%s'
				AND languageId = '%s'",
				$id,
				$languageId
			));
			Db::sql(sprintf("
				UPDATE Document
				SET isDefault = 0
				WHERE id <> '%s'
				AND languageId = '%s'",
				$id,
				$languageId
			));

			UniAdmin::app()->log->setType('admin')->write('set default document to ' . $id . '-' . $languageId);

			$this->redirect('document', '', LANG, array('msg' => 'setdefault'));
		}

		$this->redirect('document');
	}

	public function actionPreview() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		if ($id && $languageId && $categoryId) {
			$content = Db::sqlField(sprintf("
				SELECT content
				FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d",
				$id,
				$languageId,
				$categoryId
			));
			echo $this->loadView('document.preview', array('content' => $content));
		}
		exit;
	}

	public function actionCopy() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		if ($id && $languageId && $categoryId) {
			$i = 1;
			do {
				$i++;
				$exists = Db::sqlBool(sprintf("
					SELECT COUNT(*)
					FROM Document
					WHERE id = '%s'
					AND languageId = '%s'
					AND categoryId = %d",
					$id . '--' . $i,
					$languageId,
					$categoryId
				));
			} while ($exists);
			Db::sql(sprintf("
				INSERT INTO `Document`
				(`id`, `title`, `languageId`, `categoryId`, `authorId`, `metaDescription`, `metaKeywords`, `created`, `modified`, `content`, `textContent`)
				(SELECT '%s', CONCAT(%s), `languageId`, `categoryId`, %d, `metaDescription`, `metaKeywords`, NOW(), NOW(), `content`, `textContent` FROM `Document` WHERE `id` = '%s' AND `languageId` = '%s' AND `categoryId` = %d)",
				$id . '--' . $i,
				sprintf(t('copy-format'), 'title', ($i-1)),
				UniAdmin::app()->user->getId(),
				$id,
				$languageId,
				$categoryId
			));

			UniAdmin::app()->log->setType('admin')->write('made a new copy of ' . $id . '-' . $languageId . ' (' . $id . '--' . $i . ')');

			$this->redirect('document', '', LANG, array('msg' => 'copied'));
		}

		$this->redirect('document');
	}

	public function actionCopyToCategory() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		$route = UniAdmin::app()->route->getRoute();
		$newCategory = $route[3];
		if ($id && $languageId && $categoryId && $newCategory) {
			$exists = Db::sqlBool(sprintf("
				SELECT COUNT(*)
				FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d",
				$id,
				$languageId,
				$newCategory
			));
			if (!$exists) {
				Db::sql(sprintf("
					INSERT INTO `Document`
					(`id`, `title`, `languageId`, `categoryId`, `authorId`, `metaDescription`, `metaKeywords`, `created`, `modified`, `content`, `textContent`)
					(SELECT `id`, `title`, `languageId`, %d, %d, `metaDescription`, `metaKeywords`, NOW(), NOW(), `content`, `textContent` FROM `Document` WHERE `id` = '%s' AND `languageId` = '%s' AND `categoryId` = %d)",
					$newCategory,
					UniAdmin::app()->user->getId(),
					$id,
					$languageId,
					$categoryId
				));

				UniAdmin::app()->log->setType('admin')->write('copied document ' . $id . '-' . $languageId . ' to category #' . $newCategory);

				$this->redirect('document', '', LANG, array('msg' => 'copied'));
			} else {
				$this->redirect('document', '', LANG, array('msg' => 'alreadyexists'));
			}
		}

		$this->redirect('document');
	}

	public function actionCopyToLanguage() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		$route = UniAdmin::app()->route->getRoute();
		$newLanguage = $route[3];
		if ($id && $languageId && $categoryId && $newLanguage) {
			$exists = Db::sqlBool(sprintf("
				SELECT COUNT(*)
				FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d",
				$id,
				$newLanguage,
				$categoryId
			));
			if (!$exists) {
				Db::sql(sprintf("
					INSERT INTO `Document`
					(`id`, `title`, `languageId`, `categoryId`, `authorId`, `metaDescription`, `metaKeywords`, `created`, `modified`, `content`, `textContent`)
					(SELECT `id`, `title`, '%s', `categoryId`, %d, `metaDescription`, `metaKeywords`, NOW(), NOW(), `content`, `textContent` FROM `Document` WHERE `id` = '%s' AND `languageId` = '%s' AND `categoryId` = %d)",
					$newLanguage,
					UniAdmin::app()->user->getId(),
					$id,
					$languageId,
					$categoryId
				));

				UniAdmin::app()->log->setType('admin')->write('copied document ' . $id . '-' . $languageId . ' to language: ' . $newLanguage);

				$this->redirect('document', '', LANG, array('msg' => 'copied'));
			} else {
				$this->redirect('document', '', LANG, array('msg' => 'alreadyexists'));
			}
		}

		$this->redirect('document');
	}

	public function actionMove() {
		list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();

		$route = UniAdmin::app()->route->getRoute();
		$newCategory = $route[3];
		if ($id && $languageId && $categoryId && $newCategory) {
			$exists = Db::sqlBool(sprintf("
				SELECT COUNT(*)
				FROM Document
				WHERE id = '%s'
				AND languageId = '%s'
				AND categoryId = %d",
				$id,
				$languageId,
				$newCategory
			));
			if (!$exists) {
				Db::sql(sprintf("
					UPDATE Document
					SET categoryId = %d
					WHERE id = '%s'
					AND languageId = '%s'
					AND categoryId = %d",
					$newCategory,
					$id,
					$languageId,
					$categoryId
				));

				UniAdmin::app()->log->setType('admin')->write('moved document ' . $id . '-' . $languageId . ' to category #' . $newCategory);

				$this->redirect('document', '', LANG, array('msg' => 'moved'));
			} else {
				$this->redirect('document', '', LANG, array('msg' => 'alreadyexists'));
			}
		}

		$this->redirect('document');
	}

	public function actionCategory() {
		$this->viewFile = 'categorylist';
		UniAdmin::app()->path->addElement(t('document-category-list'), 'document', 'category');
	}

	public function actionCategoryInsert() {
		$this->categoryEdit();
		UniAdmin::app()->path->addElement(t('insert-document-category'), 'document', 'categoryinsert');

		$this->viewFile = 'categoryedit';
	}

	public function actionCategoryUpdate() {
		$this->categoryEdit();
		$this->viewFile = 'categoryedit';
	}

	public function actionCategoryDelete() {
		$this->checkCategoryId();

		Db::sql(sprintf("DELETE FROM DocumentCategory WHERE id = %d", $this->categoryId));
		Db::sql(sprintf("DELETE FROM Document WHERE categoryId = %d", $this->categoryId));

		UniAdmin::app()->log->setType('admin')->write('deleted document category #' . $this->categoryId);

		$this->redirect('document', 'category', LANG, array('msg' => 'deleted'));
	}

	public function actionDocumentListForMenu() {
		$value = isset($_GET['value']) ? $_GET['value'] : '';
		$languageId = isset($_GET['language']) ? $_GET['language'] : '';

		$options = Db::toArray(sprintf("
			SELECT CONCAT(Document.id, ';', languageId, ';', categoryId), title
			FROM Document
			JOIN DocumentCategory ON Document.categoryId = DocumentCategory.id
			WHERE languageId = '%s'
			AND isPublic = 1
			ORDER BY title",
			$languageId ?: CONTENT_LANG
		), true);

		UniAdmin::import('classes.forms.*');
		$field = new FSelect();
		$field
			->setId('parameters')
			->setOptions($options)
			->setValue($value);
		echo $field;
		exit;
	}

	protected function documentEdit() {
		if ($this->action == 'update') {
			list ($id, $languageId, $categoryId) = $this->getSelectedDocumentDetails();
		}

		UniAdmin::import('classes.forms.Form');

		$this->parameters = new EventParameter();
		$this->parameters->form = new Form();

		$this->parameters->form->loadFromFile('document.edit')
			->setId('document')
			->setBackAction('document')
			->setCallback();

		if (isset($categoryId)) {
			$categoryIdText = Db::sqlField(sprintf("
				SELECT idText
				FROM DocumentCategory
				WHERE id = %d",
				$categoryId
			));
			if ($categoryIdText == 'templates') {
				$this->parameters->form->setTinyMceRemoveHost(false);
			}
		}

		if ($this->action == 'update') {
			$this->parameters->form->removeField('id', 'languageId', 'categoryId');
		}

		$this->dispatchEvent('beforeShowFields', $this->parameters);

		if ($this->parameters->form->isSent()) {
			$this->parameters->form->queryValues();

			if ($this->action == 'insert') {
				$exists = Db::sqlBool(sprintf("
					SELECT COUNT(*)
					FROM Document
					WHERE id = '%s'
					AND languageId = '%s'",
					Db::escapeString($this->parameters->form->getValue('id')),
					Db::escapeString($this->parameters->form->getValue('languageId'))
				));
				if ($exists) {
					$this->parameters->form->setError('id', t('Már létezik ilyen azonosító az adatbázisban'));
				}
			}

			if ($this->parameters->form->validateFields()) {
				$this->parameters->form
					->addDbField('modified', date('Y-m-d H:i:s'))
					->addDbField('textContent', trim(strip_tags($this->parameters->form->getValue('content'))));

				if ($this->action == 'insert') {
					if (!array_key_exists($this->parameters->form->getValue('categoryId'), $this->getCategoryList())) {
						$this->parameters->form->setValue('categoryId', intval(key($this->getCategoryList())));
					}
					$this->parameters->form
						->addDbField('created', date('Y-m-d H:i:s'))
						->addDbField('authorId', UniAdmin::app()->user->getId());
				} else {
					$this->parameters->form
						->addDbField('id', $id, true)
						->addDbField('languageId', $languageId, true)
						->addDbField('categoryId', $categoryId, true);
				}

				if ($this->action == 'update' && $this->parameters->form->getValue('back')) {
					$this->parameters->form->setCallback('back');
				}

				$this->dispatchEvent('beforeSaveDocument', $this->parameters);

				if ($this->parameters->form->save($this->action)) {
					if ($this->action == 'insert') {
						UserPreferences::set('document-category', $this->parameters->form->getValue('categoryId'));

						UniAdmin::app()->log->setType('admin')->write('created new document: ' . $this->parameters->form->getValue('id') . '-' . $this->parameters->form->getValue('languageId'));

						if ($this->parameters->form->getValue('back')) {
							$this->redirect('document', '', LANG, array('msg' => 'ok'));
						} else {
							$this->redirect('document',
								array(
									'update',
									$this->parameters->form->getValue('id') . ';' . $this->parameters->form->getValue('languageId') . ';' . $this->parameters->form->getValue('categoryId')
								)
							);
						}
					} else {
						$this->parameters->documentSaveSuccess = true;
					}

					$this->dispatchEvent('afterSaveDocument', $this->parameters);
				}
			}
		} else {
			if ($this->action == 'update') {
				$this->parameters->form->loadValues(sprintf("id = '%s' AND languageId = '%s' AND categoryId = %d", $id, $languageId, $categoryId));
			} else {
				$this->parameters->form->setValue('languageId', CONTENT_LANG);
				if (UserPreferences::has('document-category')) {
					$this->parameters->form->setValue('categoryId', UserPreferences::get('document-category'));
				}
			}
		}
	}

	protected function categoryEdit() {
		if ($this->action == 'categoryupdate') {
			$this->checkCategoryId();
		}

		UniAdmin::import('classes.forms.Form');

		$this->form = new Form();
		$this->form
			->loadFromFile('document.category-edit')
			->setBackAction('document', 'category')
			->setCallback();

		if (!UniAdmin::app()->user->isRoot()) {
			$this->form->removeField('idText');
		}

		if ($this->form->isSent()) {
			$this->form->queryValues();

			if ($this->action == 'categoryinsert' && !UniAdmin::app()->user->isRoot()) {
				$this->form->addDbField('idText', Text::plain($this->form->getValue('name')));
			}
			if ($this->form->getValue('limitAccess') != 'g') {
				$this->form->getField('limitByGroup')
					->setValue(0)
					->required(false);
			}
			if ($this->form->getValue('limitAccess') != 'u') {
				$this->form->getField('limitByUser')
					->setValue(0)
					->required(false);
			}

			$categoryId = $this->form->save($this->action == 'categoryinsert' ? 'insert' : 'update');
			if ($categoryId) {
				UniAdmin::app()->log->setType('admin')->write('inserted new document category #' . $categoryId);

				$this->redirect('document', 'category', LANG, array(
					'msg' => ($this->action == 'categoryinsert' ? 'ok' : 'saved')
				));
			}
		} else {
			if ($this->action == 'categoryupdate') {
				$this->form->loadValues('id = ' . $this->categoryId);

				foreach (array('g' => 'limitByGroup', 'u' => 'limitByUser') as $fcount => $fid) {
					if ($this->form->getValue('limitAccess') == $fcount) {
						$this->form->getField($fid)->show();
						break;
					}
				}
			}
		}
	}

	protected function checkCategoryId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->categoryId = isset($route[2]) ? intval($route[2]) : 0;
		if (!array_key_exists($this->categoryId, $this->getCategoryList())) {
			$this->redirect('document', 'category');
		}
	}

	protected function getCategoryList() {
		if (!UniAdmin::app()->user->isRoot()) {
			$sql = sprintf(" WHERE `limitAccess` = '' OR `limitByGroup` = %d OR `limitByUser` = %d",
				UniAdmin::app()->user->getGroupId(), UniAdmin::app()->user->getId());
		} else {
			$sql = '';
		}
		return Db::toArray(sprintf("
			SELECT id, name
			FROM DocumentCategory
			%s
			ORDER BY name",
			$sql
		), true);
	}

	protected function getLatestDocuments($limit = 10) {
		$categoryList = array_keys($this->getCategoryList());
		$result = Db::sql(sprintf("
			SELECT id, languageId, categoryId, title
			FROM Document%s
			ORDER BY modified DESC
			LIMIT %d",
			(count($categoryList) ? ' WHERE categoryId IN (' . implode(', ', $categoryList) .')' : ''),
			$limit
		));
		return Db::toArray($result);
	}

	protected function getSelectedDocumentDetails() {
		$route = UniAdmin::app()->route->getRoute();
		if (isset($route[2])) {
			list ($id, $languageId, $categoryId) = explode(';', $route[2]);
			if (array_key_exists($categoryId, $this->getCategoryList())) {
				return array($id, $languageId, $categoryId);
			}
		}
		$this->redirect('document');
	}

	public function welcomeShowList($parameters) {
		$parameters->elements['documents'] = array(
			'title' => t('documents'),
			'link' => Url::link('document'),
			'data' => $this->loadView('document.welcome'),
		);
	}

	public function menuHandler($parameters) {
		$parameters->elements['document'] = array(
			'name' => t('document'),
			'fieldRequest' => Url::link('document', 'documentListForMenu'),
			'linkTransform' => function($parameters){
				list ($id, $languageId, $categoryId) = explode(';', $parameters);
				return Url::link('document', $id, $languageId, array(), null, true);
			},
		);
	}
}
