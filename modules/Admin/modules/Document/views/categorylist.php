<?php 

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('message-category-created'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('message-category-saved'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('message-category-deleted'));
		break;
}

$this->loadDataDisplay('category-list')->renderView();
