<?php

if (is_array($this->categoryList)) {
	echo t('selected-category') . ': <select id="document-category">';
	foreach ($this->categoryList as $id => $name) {
		echo '<option value="' . $id . '"' . ($this->selectedCategory == $id ? ' selected="selected"' : '') . '>' . $name . '</option>';
	}
	echo '</select><br />&nbsp;';
}

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('message-document-created'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('message-document-saved'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('message-document-deleted'));
		break;
	case 'moved':
			SysMessage::displaySuccess(t('message-document-moved'));
		break;
	case 'copied':
			SysMessage::displaySuccess(t('message-document-copied'));
		break;
	case 'setdefault':
			SysMessage::displaySuccess(t('message-document-set-default'));
		break;
	case 'alreadyexists':
			SysMessage::displayError(t('message-document-already-exists'));
		break;
}

$this->loadDataDisplay('document-list', array(
	'category' => $this->selectedCategory,
))->renderView();
