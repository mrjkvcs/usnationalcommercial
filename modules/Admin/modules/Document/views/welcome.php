<div class="width40">
	<b><?php echo t('categories'); ?></b>
	<ul class="">
		<?php 
		$c = 0;
		foreach ($this->getCategoryList() as $categoryId => $categoryName): 
			if ($c++ > 9) break; ?>
		<li title="<?php echo $categoryName; ?>">
			<a href="<?php echo Url::link('document', '', LANG, array('category' => $categoryId)); ?>">
				<?php echo $categoryName; ?>
			</a>
		</li>
		<?php endforeach; ?>
		<li class="summary">
			<a href="<?php echo Url::link('document', 'category'); ?>"><?php echo t('all-categories'); ?></a>
			&nbsp; | &nbsp; <a href="<?php echo Url::link('document', 'categoryinsert'); ?>"><?php echo t('new'); ?> &raquo;</a>
		</li>
	</ul>
</div>
<div class="width60">
	<b><?php echo t('recent-documents'); ?></b>
	<ul class="">
		<?php foreach ($this->getLatestDocuments() as $documentData): ?>
		<li title="<?php echo $documentData['title']; ?>">
			<a href="<?php echo Url::link('document', array('update', $documentData['id'] . ';' . $documentData['languageId'] . ';' . $documentData['categoryId'])); ?>">
				<?php echo $documentData['title']; ?>
			</a>
		</li>
		<?php endforeach; ?>
		<li class="summary">
			<a href="<?php echo Url::link('document'); ?>"><?php echo t('all-documents'); ?></a>
			&nbsp; | &nbsp; <a href="<?php echo Url::link('document', 'insert'); ?>"><?php echo t('new'); ?> &raquo;</a>
		</li>
	</ul>
</div>
<div class="clear"> </div>