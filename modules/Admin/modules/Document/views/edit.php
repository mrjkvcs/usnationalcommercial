<?php

if (isset($this->parameters->documentSaveSuccess) 
	&& $this->parameters->documentSaveSuccess == true
) {
	SysMessage::displaySuccess(t('message-document-saved'));
}

$this->parameters->form->renderView();
