<?php

class SettingmanagerController extends BaseAdminModule {
	const name = 'Honlap beállítás kezelő';
	const version = '2.0';

	public $viewFile = 'base';

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('base');
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('setting-manager'),
			'module' => 'settingmanager',
			'action' => '',
			'submenus' => array(),
			'type' => 'settings'
		);
	}

	public function actionDefault() {
		UniAdmin::app()->page->addScript('modules.Admin.modules.Settingmanager.scripts.settingmanager');
		$isInherited = UniAdmin::app()->user->getInheritance();

		$this->modules = array();
		$result = Db::sql(sprintf("
			SELECT moduleId
			FROM UniadminModuleAccess
			WHERE type = '%s'
			AND userId = %d",
			$isInherited ? 'g' : 'u',
			$isInherited ? UniAdmin::app()->user->getGroupId() : UniAdmin::app()->user->getId()
		));
		while ($rs = Db::loop($result)) {
			$moduleId = substr($rs['moduleId'], 0, -10);
			$name = Db::sqlField(sprintf("
				SELECT name
				FROM UniadminModules
				WHERE id = '%s'
				AND (
					SELECT COUNT(*)
					FROM UniadminSettings
					WHERE moduleId = UniadminModules.id
				) > 0",
				$moduleId
			));
			if ($name) {
				$this->modules[$moduleId] = $name;
			}
		}

		$this->selectedModuleId = isset($_GET['module']) ? $_GET['module'] : (UserPreferences::has('settingmanager-module') ? UserPreferences::get('settingmanager-module') : '');
		if (!isset($this->modules[$this->selectedModuleId])) {
			$this->selectedModuleId = key($this->modules);
		}

		ob_start();
		$this->actionGetSettings($this->selectedModuleId);
		$this->data = ob_get_clean();
	}

	public function actionGetSettings($moduleId, $return = false) {
		UniAdmin::import('classes.forms.Form');
		$this->form = new Form();
		$this->form->setId('settings')->setSubmitValue(t('Mentés'));

		$result = Db::sql(sprintf("SELECT * FROM UniadminSettings WHERE moduleId = '%s' ORDER BY position",
			$moduleId));
		while ($rs = Db::loop($result)) {
			// hibajavítás a régi rendszerekhez, később eltávolítható lesz
			if (strtolower(substr($rs['type'], 0, 4)) == 'text' && strtolower(substr($rs['type'], 0, 8)) != 'textarea') {
				$rs['type'] = substr_replace($rs['type'], 'InputText', 0, 4);
			}
			$rs['type'] = ucfirst($rs['type']);
			Db::sql(sprintf("UPDATE UniadminSettings SET type = '%s' WHERE moduleId = '%s' AND id = '%s'",
				$rs['type'], $moduleId, $rs['id']));
			// hibajavítás vége

			$fieldType = 'F' . ucfirst($rs['type']);
			$attrs = array();
			if (strpos($fieldType, '[') !== false) {	// méret is meg van adva
				$pos = strpos($fieldType, '[');
				$size = substr($fieldType, $pos + 1, -1);
				$attrs['SIZE'] = $size;
				$fieldType = substr($fieldType, 0, $pos);
			}
			if (class_exists($fieldType)) {
				$field = new $fieldType($attrs);
				$field->setId($rs['id']);
				$field->setValue($rs['value']);
				if (in_array($rs['type'], array('Select', 'SelectMultiple', 'Radio'))) {
					$items = preg_split('/\;/', $rs['possibleValues'], -1, PREG_SPLIT_NO_EMPTY);
					$ret = array();
					foreach ($items as $item) {
						$parts = explode('=', $item);
						$ret[$parts[0]] = isset($parts[1]) ? implode('=', array_slice($parts,1)) : $parts[0];
					}
					$field->setOptions($ret);
				}
				if ($rs['type'] == 'Checkbox') {
					$field->setText($rs['name']);
				} else {
					$field->setLabel($rs['name']);
				}
				$field->required(false);
				$field->appendTo($this->form);
			}
		}

		UserPreferences::set('settingmanager-module', $moduleId);

		echo $this->loadView('settings');

		if (UniAdmin::app()->isAjax()) {
			exit;
		}
	}

	public function actionSaveSettings($moduleId) {
		$isInherited = UniAdmin::app()->user->getInheritance();

		$found = Db::sqlField(sprintf("
			SELECT COUNT(*)
			FROM UniadminModuleAccess
			WHERE type = '%s'
			AND userId = %d
			AND moduleId = '%s'",
			$isInherited ? 'g' : 'u',
			$isInherited ? UniAdmin::app()->user->getGroupId() : UniAdmin::app()->user->getId(),
			Db::escapeString($moduleId . 'Controller')
		));
		if ($found) {
			$result = Db::sql(sprintf("SELECT * FROM UniadminSettings WHERE moduleId = '%s'",
				Db::escapeString($moduleId)));
			while ($rs = Db::loop($result)) {
				if ($rs['type'] == 'Checkbox') {
					$value = isset($_POST[$rs['id']]) ? 1 : 0;
				} else {
					if (!isset($_POST[$rs['id']])) {
						continue;
					}
					$value = $_POST[$rs['id']];
				}

				Db::sql(sprintf("
					UPDATE UniadminSettings
					SET value = '%s'
					WHERE moduleId = '%s'
					AND id = '%s'",
					Db::escapeString($value),
					Db::escapeString($moduleId),
					Db::escapeString($rs['id'])
				));
			}
		}

		exit;
	}
}
