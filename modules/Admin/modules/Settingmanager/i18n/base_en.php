<?php

$dictionary['setting-manager'] = 'Setting manager';
$dictionary['selected-module'] = 'Selected module';
$dictionary['module'] = 'module';
$dictionary['message-settings-saved'] = 'Settings saved successfully';
$dictionary['message-saving-failed'] = 'An error occured while saving settings:';
