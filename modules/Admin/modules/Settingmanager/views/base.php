<h1><?php echo t('setting-manager'); ?></h1>

<p><?php echo t('selected-module'); ?>: 
<select id="module">
	<?php foreach ($this->modules as $id => $name): ?>
	<option value="<?php echo $id; ?>"<?php if ($this->selectedModuleId == $id) echo ' selected="selected"'; ?>><?php echo $name; ?> <?php echo t('module'); ?></option>
	<?php endforeach; ?>
</select></p>

<div id="settings-container">
<?php echo $this->data; ?>
</div>