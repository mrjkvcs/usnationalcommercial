jQuery(document).ready(function($){
	UniAdmin.loadDictionary('settingmanager', 'base');

	$('#module').change(function(){
		$('#settings-container').load(adminBase + '/settingmanager/getsettings/' + $(this).val());
	});

	$('#uniadmin-main').on('submit', '#settings', function(event){
		event.preventDefault();
		$.post(adminBase + '/settingmanager/savesettings/' + $('#module').val(), $('#settings').serialize(), function(response){
			$('#settings-container div.success, #settings-container div.error').remove();
			if (response == '') {
				$('<div class="success">' + t('message-settings-saved') + '</div>').insertBefore($('#settings-container form'));
			} else {
				$('<div class="error">' + t('message-saving-failed') + '<br />' + response + '</div>').insertBefore($('#settings-container form'));
			}
		});
	});
});