<?php

class SeasonController extends BaseAdminModule {

    const name = 'Season';
    const version = '1.0';

    public function __construct()
    {
        parent::__construct();

        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters)
    {
        $parameters->menus[] = array(
            'title'    => 'GOLF SEASON',
            'module'   => 'season',
            'action'   => '',
            'submenus' => array(
                array('title' => 'Add new Season', 'module' => 'season', 'action' => 'insert')
            ),
            'type'     => 'content'
        );
    }

    protected function checkItemId($seasonId)
    {
        $this->seasonId = intval($seasonId);
        if (! $this->seasonId)
        {
            return Url::link('season');
        }
    }

    public function beforeAction()
    {
        UniAdmin::app()->path->addElement('Season', 'season');
    }

    public function actionDefault()
    {
        $this->viewFile = 'season-list';
        UniAdmin::app()->path
            ->addElement('Season', 'season')
            ->addElement('Event (weeks)', 'season');
        UniAdmin::app()->page->addCss('modules.Admin.modules.season.css.season');
    }

    public function actionInsert()
    {
        $this->viewFile = 'season';
        UniAdmin::app()->path->addElement('Add new Season', 'season', 'insert');

        $this->seasonEdit();
    }

    public function actionUpdate($seasonId)
    {
        $this->viewFile = 'season';
        $this->seasonEdit($seasonId);
    }

    public function actionDelete($seasonId)
    {
        Db::sql(sprintf("DELETE FROM GolfSeason WHERE id = %d", $this->seasonId));

        $this->redirect('season', '', LANG, array('msg' => 'deleted'));
    }

    protected function seasonEdit($seasonId = null)
    {
        UniAdmin::import('classes.forms.Form');
        $this->form = new Form();
        $loadXML = $this->action == 'update' ? 'season-edit' : 'season';
        $this->form
            ->loadFromFile('season.' . $loadXML)
            ->setBackAction('season')
            ->setCallback();

        if ($this->form->isSent())
        {
            $this->form->queryValues();
            if ($this->form->validateFields())
            {
                $this->form->save($this->action);
                $this->redirect('season', null, LANG, array('msg' => $this->action == 'insert' ? 'ok' : 'saved'));
            }
        } else
        {
            if ($this->action == 'update')
            {
                $this->form->loadValues('id = ' . $seasonId);
            }
        }
    }
}
