<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('Season inserted successfully'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('Season saved successfully'));
		break;
    case 'deleted':
        SysMessage::displaySuccess(t('Season deleted successfully'));
        break;
	case 'exists':
			SysMessage::displaySuccess(t('Season already exists'));
		break;
}

$this->loadDataDisplay('season-list')->renderView();
