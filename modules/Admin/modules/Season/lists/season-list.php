<?php

$list = new DataDisplay(array(
	'sql' => "
		SELECT
			id,
			name,
			round
		FROM GolfSeason",
	'fullWidth' => true,
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'firstName' => array(
			'title' => t('first name'),			
		),
		'name' => array(
			'title' => t('Name (title)'),
		),
        'round' => array(
            'title' => t('Round (week)'),
        ),
	),
	'title' => t('Season list'),
	'baseModule' => 'season',
    'insertButton' => t('Add New Season'),
));

return $list;
