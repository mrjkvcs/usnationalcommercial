<?php

$list = new DataDisplay(array(
	'sql' => "
		SELECT
			id,
			firstName,
			lastName,
			title,
			phone,
			email,
			isActive,
			web
		FROM Contact",
	'fullWidth' => true,
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'firstName' => array(
			'title' => t('first name'),			
		),
		'lastName' => array(
			'title' => t('last name'),
		),
		'email' => array(
			'title' => t('email'),
		),
		'isActive' => array(
			'title' => t('active'),
			'dataHandler' => function($value) {
				return $value ? t('yes') : t('no');
			}
		),
		'web' => array(
			'title' => t('web'),
		),		
	),
	'title' => t('Contact list'),
	'baseModule' => 'contact',
));

return $list;
