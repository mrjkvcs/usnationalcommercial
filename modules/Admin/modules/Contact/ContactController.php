<?php

class ContactController extends BaseAdminModule {

    const name = 'Contact';
    const version = '1.0';

    public function __construct()
    {
        Util::somaE();
        parent::__construct();
        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters)
    {
        $parameters->menus[] = array(
            'title'    => 'CONTACT',
            'module'   => 'contact',
            'action'   => '',
            'submenus' => array(
                array('title' => 'Add new Contact', 'module' => 'contact', 'action' => 'insert'),
            ),
            'type'     => 'content'
        );
    }

    protected function checkItemId($contactId)
    {
        $this->contactId = intval($contactId);
        if (! $this->contactId)
        {
            return Url::link('contact');
        }
    }

    public function beforeAction()
    {
        UniAdmin::app()->path->addElement('Contact', 'contact');
    }

    public function actionDefault()
    {
        $this->viewFile = 'contact-list';
        UniAdmin::app()->path->addElement('Contact list', 'contact');
        UniAdmin::app()->page->addCss('modules.Admin.modules.Contact.css.contact');
    }

    public function actionInsert()
    {
        $this->viewFile = 'edit';
        UniAdmin::app()->path->addElement('Add new Contact', 'contact', 'insert');
        $this->contactEdit();
    }

    public function actionUpdate($contactId)
    {
        $this->viewFile = 'edit';
        $this->contactEdit($contactId);
    }

    public function actionDelete($contactId)
    {
        Db::sql(sprintf("DELETE FROM Contact WHERE id = %d", $this->contactId));

        $this->redirect('contact', '', LANG, array('msg' => 'deleted'));
    }

    protected function contactEdit($contactId = null)
    {
        UniAdmin::import('classes.forms.Form');
        $loadXML = $this->action == 'update' ? 'contact-edit' : 'contact';
        $this->form = new Form();
        $this->form
            ->loadFromFile('contact.' . $loadXML)
            ->setAfterSaveCallBack(function ($id, $form, $type)
            {
                $image = $form->getField('upload')->getImage();
                if (! empty($image))
                {
                    $dir = sprintf('images/contact/');
                    ! is_dir($dir) ? mkdir($dir, 0777, true) : null;
                    $path = $dir . sprintf('%d.jpg', $id);
                    Util::saveImage($image, $path, 800, 450);
                }
            })
            ->setBackAction('contact');

        if ($this->form->isSent())
        {
            $this->form->queryValues();
            $this->form->save($this->action);
        } else
        {
            if ($this->action == 'update')
            {
                $field = new FHtml();
                if (Util::isImage($contactId))
                {
                    $field->setText('<img src="/images/contact/' . $contactId . '.jpg" width="150" />')->setLabel('Default Picture');
                } else
                {
                    $field->setText('<img src="/gfx/no-image.png" width="150px" />');
                }
                $this->form
                    ->loadValues('id = ' . $contactId)
                    ->addFieldBefore('firstName', $field)
                    ->setSubmitValue('Update');
            }
        }
    }
}
