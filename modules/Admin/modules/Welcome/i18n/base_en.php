<?php

$dictionary['system-informations'] = 'System informations:';
$dictionary['proper-php-version'] = 'proper PHP version';
$dictionary['incompatible-php-version'] = 'incompatible PHP version';
$dictionary['proper-mysql-version'] = 'proper MySQL version';
$dictionary['incompatible-mysql-version'] = 'incompatible MySQL version';
$dictionary['required-php-modules'] = 'Required PHP modules:';
$dictionary['available'] = 'is available';
$dictionary['unavailable'] = 'is unavailable';
$dictionary['required-apache-modules'] = 'Required Apache modules:';
$dictionary['couldnt-fetch-module-list'] = 'Counldn\'t fetch module list.';
$dictionary['folder-write-permissions'] = 'Folder write permissions:';
$dictionary['writable'] = 'is writable';
$dictionary['not-writable'] = 'is not writable';
