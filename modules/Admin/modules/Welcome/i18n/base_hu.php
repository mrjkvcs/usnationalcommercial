<?php

$dictionary['system-informations'] = 'Rendszer információk:';
$dictionary['proper-php-version'] = 'megfelelő PHP verzió';
$dictionary['incompatible-php-version'] = 'nem megfelelő PHP verzió';
$dictionary['proper-mysql-version'] = 'megfelelő MySQL verzió';
$dictionary['incompatible-mysql-version'] = 'nem megfelelő MySQL verzió';
$dictionary['required-php-modules'] = 'Szükséges PHP modulok:';
$dictionary['available'] = 'elérhető';
$dictionary['unavailable'] = 'nem elérhető';
$dictionary['required-apache-modules'] = 'Szükséges Apache modulok:';
$dictionary['couldnt-fetch-module-list'] = 'Nem lehet lekérni a modullistát.';
$dictionary['folder-write-permissions'] = 'Mappák írási engedélye:';
$dictionary['writable'] = 'írható';
$dictionary['not-writable'] = 'nem írható';
