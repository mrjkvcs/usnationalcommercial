<?php

class WelcomeController extends BaseAdminModule {
	const name = 'Üdvözlőképernyő';
	const version = '2.2';

	public $viewFile = 'index';

	public function __construct() {
		parent::__construct();
		
		$this->loadDictionary('welcome.base');
	}

	public function actionDefault() {
		$this->loadCss();
		$this->loadScript();

		$this->parameters = new EventParameter();
		$this->parameters->elements = array();
		$this->parameters->requiredPhpExtensions = array(
			'SPL', 'session', 'mysql', 'json', 'gd', 'pcre', 'curl', 
			'SimpleXML', 'xml', 'fileinfo', 'iconv', 'mbstring', 'apc', 'memcache'
		);
		$this->parameters->folderWritePermissions = array(
			'documents',
			'images',
			'logs',
			'temp',
		);

		$this->dispatchEvent('showList', $this->parameters);

		$this->boxes = UserPreferences::has('welcome-boxes') ? UserPreferences::get('welcome-boxes') : null;
		if (!is_array($this->boxes)) {
			$this->boxes = array();
		}

		if (DEBUG_MODE || UniAdmin::app()->user->isRoot()) {
			$this->parameters->elements['welcome-sysinfo'] = array(
				'title' => t('system-information'),
				'data' => $this->loadView('welcome.sysinfo'),
			);
		}
	}

	public function actionSaveState() {
		if (isset($_POST['boxes']) && is_array($_POST['boxes'])) {
			UserPreferences::set('welcome-boxes', $_POST['boxes']);
		} else {
			UserPreferences::delete('welcome-boxes');
		}
		exit;
	}
}
