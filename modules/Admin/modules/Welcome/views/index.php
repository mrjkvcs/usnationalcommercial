<?php return; ?>
<div class="welcome-boxes">
	<?php
		if (!empty($this->boxes)) {
			foreach($this->boxes as $box) {
				if (isset($this->parameters->elements[$box])) { ?>
					<div class="welcome-box" id="<?php echo $box; ?>">
						<div class="welcome-box-inner">
							<div class="welcome-box-title"><?php 
							if (isset($this->parameters->elements[$box]['link'])) {
								echo sprintf(
									'<a href="%s">%s</a>', 
									$this->parameters->elements[$box]['link'],
									$this->parameters->elements[$box]['title']
								);
							} else {
								echo $this->parameters->elements[$box]['title'];
							}
							?></div>
							<?php echo $this->parameters->elements[$box]['data']; ?>
						</div>
					</div>
					<?php
					if (isset($this->parameters->elements[$box])) {
						unset($this->parameters->elements[$box]);
					}
				} 
			}
		}
		if (is_array($this->parameters->elements)) {
			foreach ($this->parameters->elements as $key => $value) { ?>
			<div class="welcome-box" id="<?php echo $key; ?>">
				<div class="welcome-box-inner">
					<div class="welcome-box-title"><?php 
					if (isset($value['link'])) {
						echo sprintf(
							'<a href="%s">%s</a>', 
							$value['link'],
							$value['title']
						);
					} else {
						echo $value['title'];
					}
					?></div>
				<?php echo $value['data']; ?>
				</div>
			</div>
			<?php }
		} ?>
	<div class="clear"> </div>
</div>