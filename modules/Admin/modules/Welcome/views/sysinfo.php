<?php 
$iconOk = AdminConfig::$server . 'modules/welcome/' . constant(get_class($this) . '::version') . '/ok.png';
$iconFailure = AdminConfig::$server . 'modules/welcome/' . constant(get_class($this) . '::version') . '/failure.png';

?>
<p>&nbsp;<br />
<b><?php echo t('system-informations'); ?></b>  <span>PHP <?php 
echo phpversion() . ' ';
if (version_compare(PHP_VERSION, '5.3.0', '>=')) echo '<img src="' . $iconOk . '" alt="" title="' . t('proper-php-version') . '" />';
else '<img src="' . $iconFailure . '" alt="" title="' . t('incompatible-php-version') . '" />';
?></span>, 
<span>MySQL <?php echo Db::getServerInfo() . ' ';
if (version_compare(Db::getServerInfo(), '5.0', '>=')) echo '<img src="' . $iconOk . '" alt="" title="' . t('proper-mysql-version') . '" />';
else '<img src="' . $iconFailure . '" alt="" title="' . t('incompatible-mysql-version') . '" />';
?></span></p>

<p><b><?php echo t('required-php-modules'); ?></b> <?php
$loadedExtensions = get_loaded_extensions();
foreach ($this->parameters->requiredPhpExtensions as $i => $module) {
	if ($i) echo ', ';
	$ok = in_array($module, $loadedExtensions);
	echo '<span title="' . $module . ' ' . ($ok ? t('available') : t('unavailable')) . '">' . $module . ': ';
	if ($ok) echo '<img src="' . $iconOk . '" alt="" />';
	else echo '<img src="' . $iconFailure . '" alt="" />';
	echo '</span>';
}?></p>

<p><b><?php echo t('required-apache-modules'); ?></b> <?php
if (function_exists('apache_get_modules')) {
	$requiredApacheModules = array('mod_rewrite');
	$loadedApacheModules = apache_get_modules();
	foreach ($requiredApacheModules as $i => $module) {
		if ($i) echo ', ';
		$ok = in_array($module, $loadedApacheModules);
		echo '<span title="' . $module . ' ' . ($ok ? t('available') : t('unavailable')) . '">' . $module . ': ';
		if ($ok) echo '<img src="' . $iconOk . '" alt="" />';
		else echo '<img src="' . $iconFailure . '" alt="" />';
		echo '</span>';
	}
} else {
	echo '<img src="' . $iconFailure . '" alt="" /> ' . t('couldnt-fetch-module-list');
}
?></p>

<p><b><?php echo t('folder-write-permissions'); ?></b> <?php
foreach ($this->parameters->folderWritePermissions as $i => $folder) {
	if ($i) echo ', ';
	$ok = is_writeable($folder);
	echo '<span title="' . $folder . ' ' . ($ok ? t('writable') : t('not-writable')) . '">' . $folder . ': ';
	if ($ok) echo '<img src="' . $iconOk . '" alt="" />';
	else echo '<img src="' . $iconFailure . '" alt="" />';
	echo '</span>';
}?></p>
