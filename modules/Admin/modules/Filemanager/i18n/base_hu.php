<?php

$dictionary['file-manager'] = 'Fájlkezelő';
$dictionary['copy'] = 'Másolás';
$dictionary['cut'] = 'Kivágás';
$dictionary['paste'] = 'Beillesztés';
$dictionary['upload'] = 'Feltöltés';
$dictionary['create-new-folder'] = 'Új mappa létrehozása';
$dictionary['details'] = 'Részletek';
$dictionary['icons'] = 'Ikonok';

$dictionary['rename'] = 'Átnevezés...';
$dictionary['download'] = 'Letöltés';
$dictionary['delete'] = 'Törlés...';
$dictionary['rename-folder'] = 'Mappa átnevezése...';
$dictionary['delete-folder'] = 'Mappa törlése...';

$dictionary['message-error-creating-folder'] = 'Hiba a mappa létrehozása közben';
$dictionary['message-error-renaming-file'] = 'Hiba a fájl átnevezése közben';
$dictionary['message-error-deleting-file'] = 'Hiba a fájl törlése közben';
$dictionary['message-error-pasting-file'] = 'Hiba a fájl beillesztése közben';
$dictionary['message-error-copying-file'] = 'Hiba a fájl másolása közben';
$dictionary['message-folder-created'] = 'A mappa létrehozása sikeresen megtörtént';
$dictionary['message-file-renamed'] = 'A fájl átnevezése sikeresen megtörtént';
$dictionary['message-file-deleted'] = 'A fájl törlése sikeresen megtörtént';
$dictionary['message-file-copied'] = 'A fájl váglólapra helyezése sikeresen megtörtént';
$dictionary['message-file-pasted'] = 'A fájl beillesztése sikeresen megtörtént';
$dictionary['message-file-uploaded'] = 'A fájl feltöltése sikeresen megtörtént';

$dictionary['new-folder-name'] = 'Új mappa neve:';
$dictionary['new-filename'] = 'Új fájlnév:';
$dictionary['confirm-save-without-extension'] = 'Biztosan elmenti a fájlt kiterjesztés nélkül?';

$dictionary['filename'] = 'fájlnév';
$dictionary['size'] = 'méret';
$dictionary['type'] = 'típus';
$dictionary['informations'] = 'információk';
$dictionary['select-folder'] = 'mappa kiválasztása';
