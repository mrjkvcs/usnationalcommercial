<?php

$dictionary['file-manager'] = 'File manager';
$dictionary['copy'] = 'Copy';
$dictionary['cut'] = 'Cut';
$dictionary['paste'] = 'Paste';
$dictionary['upload'] = 'Upload';
$dictionary['create-new-folder'] = 'Create new folder';
$dictionary['details'] = 'Details';
$dictionary['icons'] = 'Icons';

$dictionary['rename'] = 'Rename...';
$dictionary['download'] = 'Download';
$dictionary['delete'] = 'Delete...';
$dictionary['rename-folder'] = 'Rename folder...';
$dictionary['delete-folder'] = 'Delete folder...';

$dictionary['message-error-creating-folder'] = 'Error while creating folder';
$dictionary['message-error-renaming-file'] = 'Error while renaming file';
$dictionary['message-error-deleting-file'] = 'Error while deleting file';
$dictionary['message-error-pasting-file'] = 'Error while pasting file';
$dictionary['message-error-copying-file'] = 'Error while copying file';
$dictionary['message-folder-created'] = 'Folder created successfully';
$dictionary['message-file-renamed'] = 'File renamed successfully';
$dictionary['message-file-deleted'] = 'File deleted successfully';
$dictionary['message-file-copied'] = 'File copied successfully';
$dictionary['message-file-pasted'] = 'File pasted successfully';
$dictionary['message-file-uploaded'] = 'File uploaded successfully';

$dictionary['new-folder-name'] = 'New folder name:';
$dictionary['new-filename'] = 'New filename:';
$dictionary['confirm-save-without-extension'] = 'Are you sure to save the file without extension?';

$dictionary['filename'] = 'filename';
$dictionary['size'] = 'size';
$dictionary['type'] = 'type';
$dictionary['informations'] = 'informations';
$dictionary['select-folder'] = 'select folder';
