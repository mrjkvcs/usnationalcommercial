<?php

class DirectoryList extends Widget {
	protected $directoryList = array();
	protected $homeDirectory = '';
	protected $actualDirectory = '';
	protected $fileBrowserMode = false;

	public function __construct($startDir = '.', $autoRender = false) {
		UniAdmin::import('modules.Admin.modules.Filemanager.FilebrowserConfig');
		$this->config = new FilebrowserConfig();

		if ($autoRender) {
			$this->directoryList = $this->getElements($startDir);
			$this->renderView();
		}
	}

	/**
	 * ha a constructor nem autoRender
	 */
	public function loadDirectories($startDir = '.') {
		$this->directoryList = $this->getElements($startDir);
		return $this;
	}

	public function setHomeDirectory($homeDirectory) {
		$this->homeDirectory = $homeDirectory;
		return $this;
	}

	public function setActualDirectory($actualDirectory) {
		$this->actualDirectory = $actualDirectory;
		return $this;
	}

	public function setFileBrowserMode($mode = true) {
		$this->fileBrowserMode = $mode;
		return $this;
	}

	public function getActualDirectory() {
		return $this->actualDirectory;
	}

	public function getElements($actualDir = '.', $recursive = true) {
		$actualDir = ltrim($actualDir,'/.'); 
		$ret = array();
		$homeDir = $this->homeDirectory ? File::sanitizeName($this->homeDirectory . '/') : '';
		$fullDir = $this->config->baseDir . '/' . $homeDir . $actualDir;
		if (is_dir($fullDir) && is_readable($fullDir)) {
			$dir = new DirectoryIterator($fullDir);
			foreach ($dir as $fileInfo) {
				if (!$fileInfo->isDot() && $fileInfo->isDir() && substr($fileInfo->getFileName(), 0, 1) != '.' && substr($fileInfo->getFileName(), 0, 1) != '_') {
					$fullPath = ($actualDir && $actualDir != '.' ? $actualDir . '/' : '') . $fileInfo->getFileName();
					if (!in_array($fullPath, $this->config->forbiddenDirs) && (!count($this->config->allowedDirs) || in_array($fullPath, $this->config->allowedDirs))) {
						$r = $this->getElements($actualDir . '/' . $fileInfo->getFileName());
						if (!$recursive || count($r) == 0) {
							$ret[$fullPath] = $fileInfo->getFileName();
						} else {
							$ret[$fullPath] = $r;
						}
					}
				}
			}
		}
		ksort($ret);
		return $ret;
	}

	public function drawDirectoryList($directoryList, $level = 0) {
		if (is_array($directoryList) && count($directoryList)) {
			echo '<ul>';
			if (!$level) {
				echo '<li><a class="folder' . (!$this->actualDirectory ? ' active' : '') . '" href="' . ($this->fileBrowserMode ? 
						Url::link('filebrowser', 'renderfilelist', LANG, array('path' => '')) :
						Url::link('filemanager', '', LANG, array('path' => ''))
					) . '">/</a></li>';
			}
			foreach ($directoryList as $fullPath => $subFolders) {
				$displayPath = $fullPath;
				if (substr($displayPath, 0, 2) == './') {
					$displayPath = substr($displayPath, 2);
				}
				echo '<li><a class="folder' 
					. ($this->actualDirectory == $displayPath ? ' active' : '') 
					. '" href="' . ($this->fileBrowserMode ? 
						Url::link('filebrowser', 'renderfilelist', LANG, array('path' => $displayPath)) :
						Url::link('filemanager', '', LANG, array('path' => $displayPath))
					) . '">' . (array_key_exists($displayPath, $this->config->dirAliases) ? t($this->config->dirAliases[$displayPath]) : basename($fullPath)) . '</a>';
				if (is_array($subFolders)) {
					$this->drawDirectoryList($subFolders, ++$level);
				}
				echo '</li>';
			}
			echo '</ul>';
		}
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		$this->drawDirectoryList($this->directoryList);
	}
}
