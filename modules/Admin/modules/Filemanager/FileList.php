<?php

class FileList extends Widget {
	public $fileList = array();
	public $path = '';
	public $homeDirectory = '';
	public $extensions = array();
	public $onlyDirs = false;
	public $view = 'icons';

	public function __construct($parameters = array(), $autoRender = false) {
		UniAdmin::import('modules.Admin.modules.Filemanager.FilebrowserConfig');
		$this->config = new FilebrowserConfig();

		if (isset($parameters['homeDirectory'])) {
			$this->setHomeDirectory($parameters['homeDirectory']);
		}

		if (isset($parameters['path'])) {
			$this->setPath($parameters['path']);
		} else {
			$this->path = $this->config->baseDir;
		}

		if (isset($parameters['onlyDirs'])) {
			$this->setOnlyDirs($parameters['onlyDirs']);
		}

		if (isset($parameters['extensions']) && is_array($parameters['extensions'])) {
			$this->setExtensions($parameters['extensions']);
		}

		if (isset($parameters['view'])) {
			$this->setView($parameters['view']);
		} else {
			$this->setView($this->config->defaultView);
		}

		if ($autoRender) {
			$this->renderView();
		}
	}

	public function setHomeDirectory($homeDirectory) {
		$this->homeDirectory = $homeDirectory;
		return $this;
	}

	public function setPath($path) {
		$this->path = $path;
		return $this;
	}

	public function setOnlyDirs($onlyDirs = true) {
		$this->onlyDirs = (bool) $onlyDirs;
		return $this;
	}

	public function setExtensions(array $extensions) {
		$this->extensions = $extensions;
		return $this;
	}

	public function setView($view) {
		$this->view = $view;
		return $this;
	}

	public function getPath() {
		return $this->path;
	}

	public function getView() {
		return $this->view;
	}

	public function getElements() {
		$ret = array();
		$homeDir = $this->homeDirectory ? File::sanitizeName($this->homeDirectory . '/') : '';
		$fullPath = $this->config->baseDir . '/'  . $homeDir . ltrim($this->path,'/.');
		if (is_dir($fullPath) && is_readable($fullPath)) {
			$files = new DirectoryIterator($fullPath);
			$names = $sizes = $icons = $descriptions = $extras = array();
			foreach ($files as $fileInfo) {
				if ($this->onlyDirs 
					&& $fileInfo->isDir() 
					&& $fileInfo->getFileName() != '..' 
					&& substr($fileInfo->getFileName(), 0, 1) != '_'
				) {
					$fullPath = $homeDir . $this->path . $fileInfo->getFileName();
					if (in_array($fullPath, $this->config->forbiddenDirs) || (!empty($this->config->allowedDirs) && !in_array($fullPath, $this->config->allowedDirs))) {
						continue;
					}
					$data = array(
						'name' => $fileInfo->getFileName(),
						'size' => '',
						'icon' => 'folder',
						'description' => '',
						'extra' => '',
					);
				} else if (!$this->onlyDirs 
					&& !$fileInfo->isDot() 
					&& !$fileInfo->isDir()
				) {
					$extension = strtolower(end(explode('.', $fileInfo->getFileName())));
					if (!in_array($extension, $this->config->forbiddenExtensions) 
						&& (empty($this->config->allowedExtensions) || in_array($extension, $this->config->allowedExtensions))
						&& (empty($this->extensions) || in_array($extension, $this->extensions))
					) {
						$data = array(
							'name' => $fileInfo->getFileName(),
							'size' => Text::formatBytes($fileInfo->getSize(), 2),
							'icon' => File::getIcon($extension),
							'description' => File::getDescription($extension),
							'extra' => '',
						);
						if ($extension == 'jpg' || $extension == 'jpeg' || $extension == 'gif' || $extension == 'png') {
							$size = getimagesize($fullPath .'/'. $fileInfo->getFileName());
							$data['extra'] = $size[0] . ' x ' . $size[1];
						}
					} else {
						continue;
					}
				} else {
					continue;
				}
				$names[] = $data['name'];
				$sizes[] = $data['size'];
				$icons[] = $data['icon'];
				$descriptions[] = $data['description'];
				$extras[] = $data['extra'];
			}

			array_multisort($names, SORT_ASC, SORT_STRING, $sizes, SORT_ASC, SORT_NUMERIC, $icons, $descriptions, $extras);

			for ($i = 0; $i < count($names); $i++) {
				$ret[] = array(
					'name' => $names[$i],
					'size' => $sizes[$i],
					'icon' => $icons[$i],
					'description' => $descriptions[$i],
					'extra' => $extras[$i],
				);
			}
		}

		return $ret;
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		$this->fileList = $this->getElements();
		if ($this->view == 'details') {
			echo '<table cellpadding="0" cellspacing="0" class="details-file-list">';
			echo '<thead><tr><td>&nbsp;</td><td>' . t('filename') . '</td><td>' . t('size') . '</td><td>' . t('type') . '</td><td>' . t('informations') . '</td></tr></thead>';
			echo '<tbody>';
			foreach ($this->fileList as $file) {
				echo '<tr class="item" id="' . $file['name'] . '"><td><img src="' . AdminConfig::$server . 'gfx/5.3/file-icons/' . $file['icon'] . '.png" alt="" /></td><td><a href="/' . $this->path . '/' . $file['name'] . '">' . $file['name'] . '</a></td><td>' . $file['size'] . '</td><td>' . $file['description'] . '</td><td>' . $file['extra'] . '</td></tr>';
			}
			echo '</tbody></table>';
		} else {
			$homeDir = $this->homeDirectory ? File::sanitizeName($this->homeDirectory . '/') : '';
			echo '<div class="icons-file-list">';
			foreach ($this->fileList as $file) {
				$extension = end(explode('.', $file['name']));
				echo '<a class="item" href="/' . $homeDir . ($this->path ? $this->path . '/' : '') . $file['name'] . '" id="' . $file['name'] . '" title="' . $file['name'] . '"><span class="file-thumbnail">';
				switch (strtolower($extension)) {
					case 'jpg':
					case 'jpeg':
					case 'gif':
					case 'png':
							echo '<img src="/thumbnail/' . $homeDir . ($this->path ? $this->path . '/' : '') . $file['name'] . '?120,80,png,crop" alt="" />';
						break;
					default:
							echo '<img src="' . AdminConfig::$server . 'gfx/' . UniAdmin::getVersion() . '/file-icons-big/' . $file['icon'] . '.png" alt="" />';
						break;
				}
				echo '</span>' . ($file['name'] == '.' ? t('select-folder') : $file['name']) . '</a>';
			}
			echo '</div>';
		}
	}
}
