<?php
$isPathWritable = is_writable($this->config->baseDir . '/' . ($this->homeDirectory ? $this->homeDirectory . '/' : '') . $this->path);
if (isset($this->config->readOnlyDirs) 
	&& in_array(($this->homeDirectory ? $this->homeDirectory . '/' : '') . $this->path, $this->config->readOnlyDirs)
) {
	$isPathWritable = false;
	$isReadOnly = true;
} else {
	$isReadOnly = false;
}
$isClipboardUsed = isset($_SESSION['filemanager']['clipboard']);
?>
<h1><?php echo t('file-manager'); ?></h1>

<?php
if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'errorcreate':
		SysMessage::displayError(t('message-error-creating-folder'));
	break;
	case 'errorrenamefile':
		SysMessage::displayError(t('message-error-renaming-file'));
	break;
	case 'errordeletefile':
		SysMessage::displayError(t('message-error-deleting-file'));
	break;
	case 'errorpastefile':
		SysMessage::displayError(t('message-error-pasting-file'));
	break;
	case 'errorcopyfile':
		SysMessage::displayError(t('message-error-copying-file'));
	break;
	case 'created':
		SysMessage::displaySuccess(t('message-folder-created'));
	break;
	case 'filerenamed':
		SysMessage::displaySuccess(t('message-file-renamed'));
	break;
	case 'filedeleted':
		SysMessage::displaySuccess(t('message-file-deleted'));
	break;
	case 'filecopied':
		SysMessage::displaySuccess(t('message-file-copied'));
	break;
	case 'filepasted':
		SysMessage::displaySuccess(t('message-file-pasted'));
	break;
	case 'fileuploaded':
		SysMessage::displaySuccess(t('message-file-uploaded'));
	break;
}
?>

<div class="toolbar">
	<!--<a href="#" class="copy disabled"><?php echo t('copy'); ?></a>-->
	<!--<a href="#" class="cut disabled"><?php echo t('cut'); ?></a>-->
	<a href="<?php echo Url::link('filemanager', 'pastefile', LANG, array('path' => $this->path)); ?>" class="paste<?php if (!$isPathWritable || !$isClipboardUsed) echo ' disabled'; ?>"><span><?php echo t('paste'); ?></span></a>
	<a href="#" class="upload<?php if (!$isPathWritable) echo ' disabled'; ?>"><span><?php echo t('upload'); ?></span></a>
	<!--<a href="#" class="folder<?php if (!$isPathWritable) echo ' disabled'; ?>"><?php echo t('create-new-folder'); ?></a>-->
</div>

<div class="path">
	<span class="toolbar-views">
		<a class="view-details<?php if ($this->view == 'details') echo '-on'; ?>" href="<?php echo Url::link('filemanager', '', LANG, array('path' => $this->path, 'view' => 'details')); ?>" title="<?php echo t('details'); ?>">
			<span><?php echo t('details'); ?></span>
		</a>
		<a class="view-icons<?php if ($this->view == 'icons') echo '-on'; ?>" href="<?php echo Url::link('filemanager', '', LANG, array('path' => $this->path, 'view' => 'icons')); ?>" title="<?php echo t('icons'); ?>">
			<span><?php echo t('icons'); ?></span>
		</a>
	</span>
	<div id="path">
		/<?php echo $this->path; ?>
	</div>
</div>

<div class="directory-list treeview filetree">
	<?php $this->directoryList->renderView(); ?>
</div>

<div class="file-list">
	<?php $this->fileList->renderView(); ?>
</div>

<?php
$fileContextMenu = new ContextMenu(array(
	'id' => 'filelist',
	'baseModule' => 'filemanager',
	'initScript' => false,
	'elements' => array(
		array(
			'action' => 'renamefile',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('rename'),
			'class' => 'rename edit',
			'disabled' => $isReadOnly,
		),
		array(
			'action' => 'download',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('download'),
			'class' => 'download',
		),
		array(
			'action' => 'copyfile',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('copy'),
			'class' => 'separator copy',
		),
		array(
			'action' => 'cutfile',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('cut'),
			'class' => 'cut',
			'disabled' => $isReadOnly,
		),
		array(
			'action' => 'pastefile',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('paste'),
			'class' => 'paste',
			'disabled' => !$isPathWritable || !$isClipboardUsed,
		),
		array(
			'action' => 'deletefile',
			'queryString' => array('path' => '[path]', 'filename' => '[id]'),
			'text' => t('delete'),
			'class' => 'separator delete confirm',
			'disabled' => $isReadOnly,
		),
	),
), true);

$folderContextMenu = new ContextMenu(array(
	'id' => 'dirlist',
	'baseModule' => 'filemanager',
	'initScript' => false,
	'elements' => array(
		array(
			'action' => 'renamefolder',
			'text' => t('rename-folder'),
			'class' => 'rename edit',
		),
		array(
			'action' => 'deletefolder',
			'text' => t('delete-folder'),
			'class' => 'separator delete confirm',
		),
	),
), true);
