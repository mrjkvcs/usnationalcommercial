<?php

class FilebrowserConfig {
	/**
	 * Fájlkezelő alapkönyvtára
	 */
	public $baseDir = '.';

	/**
	 * Tiltott mappák
	 */
	public $forbiddenDirs = array(
		'classes',
		'extensions',
		'gfx',
		'i18n',
		'interfaces',
		'models',
		'modules',
		'scripts',
		'style',
		'temp',
		'views',
		'images/ads',
		'images/breeds',
	);

	/**
	 * Csak olvasható mappák
	 */
	public $readOnlyDirs = array(
		'images/photos',
	);

	/**
	 * Csak olvasható mappák
	 */
	public $dirAliases = array(
		'documents' => 'alias-documents',
		'images' => 'alias-images',
		'logs' => 'alias-logs',
		'images/gallery' => 'alias-gallery',
		'images/highlights' => 'alias-highlights',
		'images/banners' => 'alias-banners',
		'images/catalogue' => 'alias-catalogue',
		'images/logo' => 'alias-logo',
	);

	/**
	 * Engedélyezett mappák
	 */
	public $allowedDirs = array();

	/**
	 * Tiltott kiterjesztések
	 */
	public $forbiddenExtensions = array(
		'php',
		'htaccess',
		'gitignore'
	);

	/**
	 * Engedélyezett kiterjesztések
	 */
	public $allowedExtensions = array();

	/**
	 * Tiltott feltöltési kiterjesztések
	 */
	public $forbiddenUploadExtensions = array();

	/**
	 * Engedélyezett feltöltési kiterjesztések
	 */
	public $allowedUploadExtensions = array();

	/**
	 * Alapértelmezett fájlkezelő nézet
	 */
	public $defaultView = 'icons';
}
