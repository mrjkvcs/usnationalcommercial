var dialog = jQuery('<div />');
dialog.append('<form enctype="multipart/form-data" action="/admin/filemanager/upload" method="post"><input type="hidden" name="path" /><input type="file" name="file" /> <input type="submit" value="Feltöltés" /></form>');

jQuery(document).ready(function($){
	var height = $(window).height() - $('.directory-list').offset().top;
	$('.directory-list, .file-list').css({
		'max-height' : height + 'px'
	});
	
	UniAdmin.loadDictionary('filemanager', 'base');

	$('.toolbar a.disabled').css('opacity', .2).click(function(event){
		event.preventDefault();
	});

	$('.toolbar a.folder').click(function(event){
		if ($(event.target).hasClass('disabled')) {
			return;
		}

		var dirName = prompt(t('new-folder-name'));
		if (dirName) {
			UniAdmin.redirect('filemanager/createfolder/dirname/' + dirName + '?path=' + $('#path').html());
		}
	});

	/*
	$('.directory-list a').contextMenu({
		menu: 'contextMenu-dirlist'
	});
	*/

	$('.file-list a, .file-list td').contextMenu({
			menu: 'contextMenu-filelist'
		}, function(item, srcElement, event){
			var path = $.trim($('#path').html().substring(1));
			$(item).attr('href', unescape($(item).attr('href')).replace(/\[path\]/gi, path));
			if (item.hasClass('rename')) {
				var newName = prompt(t('new-filename'), $(srcElement).closest('.item').attr('id'));
				if (newName && $(srcElement).closest('.item').attr('id') != newName) {
					var hasExtension = newName.indexOf('.');
					if (hasExtension > -1 || confirm(t('confirm-save-without-extension'))) {
						$(item).attr('href', $(item).attr('href') + '&newname=' + newName);
					} else {
						$('.contextMenu').hide();
						return false;
					}
				} else {
					$('.contextMenu').hide();
					return false;
				}
			}
			if (item.hasClass('download')) {
				$(item).attr('href', unescape($(item).data('originalHref'))
					.replace(/\[path\]/gi, path)
					.replace(/\[id\]/gi, $(srcElement).closest('[id]').attr('id')));
			}
	});

	$('.file-list a').click(function(event){
		event.preventDefault();
		//$(event.target).closest('a').toggleClass('selected');
	});

	$('.file-list').css({ width: ($(window).width() - 480) + 'px' });
	$(window).resize(function() {
		$('.file-list').css({ width: ($(window).width() - 480) + 'px' });
	});

	$('.toolbar .upload').click(function(event) {
		if ($(event.target).hasClass('disabled')) {
			return;
		}

		dialog.dialog({
			modal: true,
			width: 600,
			height: 500,
			position: ['center', 100],
			title: 'File feltöltés',
			resizable: false
		});
		var form = dialog.find('form');
		dialog.find('input[name=path]').val($.trim($('#path').text()));
		/*form.ajaxForm({
			url: '/admin/filemanager/upload',
			type: 'POST',
			success: function() {
				dialog.dialog('close');
				location.reload();
			},
			iframe: true
		});*/
		event.preventDefault();
	});
});
