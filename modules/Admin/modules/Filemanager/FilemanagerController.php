<?php

class FilemanagerController extends BaseAdminModule {
	const name = 'Fájlkezelő';
	const version = '4.0';

	public $viewFile = 'filemanager';
	public $directoryList;
	public $fileList;
	public $path = '';

	public function __construct() {
		$this->loadDictionary('filemanager.base');
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('file-manager'), 
			'module' => 'filemanager', 
			'type' => 'function'
		);
	}

	public function beforeAction() {
		UniAdmin::import('modules.Admin.modules.Filemanager.*');
		$this->config = new FilebrowserConfig();
		$this->path = UniAdmin::app()->route->getQueryString(true, 'path');
		$this->view = UniAdmin::app()->route->getQueryString(true, 'view');
		if ($this->view != 'details' && $this->view != 'icons') {
			if (UserPreferences::has('filemanager-view')) {
				$this->view = UserPreferences::get('filemanager-view');
			} else {
				$this->view = 'icons';
			}
		}

		UserPreferences::set('filemanager-view', $this->view);

		$this->homeDirectory = ltrim(UserPreferences::getHomeDirectory(), '/');

		$this->loadDictionary('filebrowser.base');
	}

	public function actionDefault() {
		UniAdmin::app()->page
			->addCss('modules.Admin.modules.Filemanager.css.filemanager')
			->addScript('modules.Admin.modules.Filemanager.scripts.filemanager');

		$this->directoryList = new DirectoryList();
		$this->directoryList
			->setHomeDirectory($this->homeDirectory)
			->setActualDirectory($this->path)
			->loadDirectories();

		$this->fileList = new FileList(array(
			'homeDirectory' => $this->homeDirectory,
			'path' => $this->path,
			'view' => $this->view,
		));
	}

	public function actionCreateFolder() {
		list ($path, $originalPath) = $this->getPath();
		$dirName = File::sanitizeName(UniAdmin::app()->route->getParameter('dirname'));
		if (substr($path, 0, 1) == '/') {
			$path = substr($path, 1);
		}
		if ($dirName) {
			if (!is_dir($path . $dirName)) {
				if (is_writable($path)) {
					mkdir($path . '/' . $dirName, 0777);

					UniAdmin::app()->log->setType('admin')->write('created folder: ' . $path . '/' . $dirName);

					$this->redirect('filemanager', array(), LANG, array(
						'path' => $originalPath,
						'msg' => 'created',
					));
				} else {
					$error = 'n';
				}
			} else {
				$error = 'a';
			}
		} else {
			$error = 'm';
		}

		$this->redirect('filemanager', array(), LANG, array(
			'path' => $originalPath,
			'msg' => 'errorcreate',
			'error' => $error,
		));
	}

	public function actionCopyFolder() {

	}

	public function actionDeleteFolder() {

	}

	public function actionRenameFile() {
		list ($path, $originalPath) = $this->getPath();
		$fileName = File::sanitizeName(UniAdmin::app()->route->getQueryString(true, 'filename'));
		$newName = File::sanitizeName(UniAdmin::app()->route->getQueryString(true, 'newname'));
		if ($fileName && $newName) {
			if (file_exists(($path ? $path . '/' : '') . $fileName)) {
				$parts = preg_split('/\./', $newName, -1, PREG_SPLIT_NO_EMPTY);
				if (count($parts) > 1) {
					$newP1 = Text::plain(implode('.', array_slice($parts, 0, -1)));
					$newP2 = Text::plain(end($parts));
					$filePath = ($path ? $path . '/' : '') . $newP1 . '.' . $newP2;
				} else {
					$newP1 = Text::plain(implode('.', $parts));
					$filePath = ($path ? $path . '/' : '') . $newP1;
				}

				if (@rename(($path ? $path . '/' : '') . $fileName, $filePath)) {
					UniAdmin::app()->log->setType('admin')->write('renamed file: ' . $path . '/' . $fileName . ' to ' . $filePath);

					$this->redirect('filemanager', array(), LANG, array(
						'path' => $originalPath,
						'msg' => 'filerenamed',
					));
				} else {
					$error = 'r';
				}
			} else {
				$error = 'n';
			}
		} else {
			$error = 'm';
		}

		$this->redirect('filemanager', array(), LANG, array(
			'path' => $originalPath,
			'msg' => 'errorrenamefile',
			'error' => $error,
		));
	}

	public function actionCopyFile() {
		$this->addToClipboard('copy');
	}

	public function actionCutFile() {
		$this->addToClipboard('cut');
	}

	public function actionPasteFile() {
		list ($path, $originalPath) = $this->getPath();
		if (isset($_SESSION['filemanager']['clipboard'])) {
			if (file_exists($_SESSION['filemanager']['clipboard']['filename'])) {
				$fileName = basename($_SESSION['filemanager']['clipboard']['filename']);
				$fromPath = substr($_SESSION['filemanager']['clipboard']['filename'], 0, (-1) * strlen($fileName) - 1);
				if ($_SESSION['filemanager']['clipboard']['method'] == 'copy' 
					|| (isset($this->config->readOnlyDirs) && in_array($fromPath, $this->config->readOnlyDirs))
				) {
					$ret = @copy($_SESSION['filemanager']['clipboard']['filename'], ($path ? $path . '/' : '') . $fileName);
					if ($ret) {
						UniAdmin::app()->log->setType('admin')->write('pasted file (copy): ' . $_SESSION['filemanager']['clipboard']['filename'] . ' to ' . $path . '/' . $fileName);
					}
					$error = $ret ? 'o' : 'e';
				} else {
					$ret = @rename($_SESSION['filemanager']['clipboard']['filename'], ($path ? $path . '/' : '') . $fileName);
					if ($ret) {
						UniAdmin::app()->log->setType('admin')->write('pasted file (cut): ' . $_SESSION['filemanager']['clipboard']['filename'] . ' to ' . $path . '/' . $fileName);
						unset($_SESSION['filemanager']['clipboard']);
						$error = 'o';
					} else {
						$error = 'e';
					}
				}
			} else {
				$error = 'n';
			}
		} else {
			$error = 'm';
		}

		if ($error == 'o') {
			$this->redirect('filemanager', array(), LANG, array(
				'path' => $originalPath,
				'msg' => 'filepasted',
			));
		} else {
			$this->redirect('filemanager', array(), LANG, array(
				'path' => $originalPath,
				'msg' => 'errorpastefile',
				'error' => $error,
			));
		}
	}

	public function actionDeleteFile() {
		list ($path, $originalPath) = $this->getPath();
		if (!isset($this->config->readOnlyDirs) || !in_array($path, $this->config->readOnlyDirs)) {
			$fileName = File::sanitizeName(UniAdmin::app()->route->getQueryString(true, 'filename'));
			if ($fileName) {
				if (file_exists(($path ? $path . '/' : '') . $fileName)) {
					if (@unlink(($path ? $path . '/' : '') . $fileName)) {
						UniAdmin::app()->log->setType('admin')->write('deleted file: ' . $path . '/' . $fileName);

						$this->redirect('filemanager', array(), LANG, array(
							'path' => $originalPath,
							'msg' => 'filedeleted',
						));
					} else {
						$error = 'e';
					}
				} else {
					$error = 'n';
				}
			} else {
				$error = 'm';
			}
		} else {
			$error = 'r';
		}

		$this->redirect('filemanager', array(), LANG, array(
			'path' => $originalPath,
			'msg' => 'errordeletefile',
			'error' => $error,
		));
	}

	public function actionDownload() {
		list ($path, $originalPath) = $this->getPath();
		$fileName = File::sanitizeName(UniAdmin::app()->route->getQueryString(true, 'filename'));
		if ($fileName && file_exists(($path ? $path . '/' : '') . $fileName)) {
			UniAdmin::app()->log->setType('admin')->write('downloaded file: ' . $path . '/' . $fileName);

			header('Content-Disposition: attachment; filename="' . basename($fileName) . '"');
			header('Content-Type: ' . File::getMimeType($path . '/' . $fileName));
			readfile($path . '/' . $fileName);
		}
		exit;
	}

	public function actionUpload() {
		list ($path, $originalPath) = $this->getPath('post');
		if (substr($path, 0, 1) == '/') {
			$path = substr($path, 1);
		}
		if (is_uploaded_file($_FILES['file']['tmp_name'])) {
			$extensions = isset($_POST['extensions']) ? $_POST['extensions'] : '*';
			if (isset($_POST['ajax']) && $_POST['ajax']) {
				UniAdmin::app()->setAjaxMode(true);
			}
			if ($extensions == '*') {
				$allowedFileTypes = array();
			} else {
				$allowedFileTypes = explode(',', $extensions);
				$allowedFileTypes = array_map('trim', $allowedFileTypes);
			}

			$parts = preg_split('/\./', $_FILES['file']['name'], -1, PREG_SPLIT_NO_EMPTY);

			if (count($parts) > 1) {
				$newP1 = Text::plain(implode('.', array_slice($parts, 0, -1)));
				$newP2 = Text::plain(end($parts));
				if (!empty($allowedFileTypes) && !in_array($newP2, $allowedFileTypes)) {
					$this->redirect('filemanager', array(), LANG, array(
						'path' => $originalPath,
						'msg' => 'typenotallowed',
					));
				}
				$filePath = $path . '/' . $newP1 . '.' . $newP2;
			} else {
				if (!empty($allowedFileTypes)) {
					$this->redirect('filemanager', array(), LANG, array(
						'path' => $path,
						'msg' => 'typenotallowed',
					));
				}
				$filePath = Text::plain(implode('.', $parts));
			}

			move_uploaded_file($_FILES['file']['tmp_name'], $filePath);

			if (Setting::get('autoResizeImages', 'Filemanager')) {
				File::resizeImage($filePath);
			}

			UniAdmin::app()->log->write('uploaded file: ' . $filePath);
		}

		$this->redirect('filemanager', array(), LANG, array(
			'path' => $originalPath,
			'msg' => 'fileuploaded',
		));
	}

	public function getPath($method = 'get') {
		if ($method == 'get') {
			$originalPath = UniAdmin::app()->route->getQueryString(true, 'path');
		} else {
			$originalPath = isset($_POST['path']) ? $_POST['path'] : '';
		}
		$path = $this->config->baseDir .'/'. ltrim(File::sanitizeName(($this->homeDirectory ? $this->homeDirectory . '/' : '') . $originalPath),'./');
		UniAdmin::addDebugInfo($path,'filemanager');
		return array($path, ltrim($originalPath,'/'));
	}

	public function addToClipboard($method) {
		list($path, $originalPath) = $this->getPath();
		$fileName = File::sanitizeName(UniAdmin::app()->route->getQueryString(true, 'filename'));
		if (file_exists(($path ? $path . '/' : '') . $fileName)) {
			$_SESSION['filemanager']['clipboard'] = array(
				'filename' => ($path ? $path . '/' : '') . $fileName,
				'method' => $method
			);
			$this->redirect('filemanager', array(), LANG, array(
				'path' => $originalPath,
				'msg' => 'filecopied',
			));
		} else {
			$error = 'n';
		}

		$this->redirect('filemanager', array(), LANG, array(
			'path' => $originalPath,
			'msg' => 'errorcopyfile',
			'error' => $error,
		));
	}
}
