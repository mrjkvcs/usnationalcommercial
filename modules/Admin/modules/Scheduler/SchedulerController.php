<?php

class SchedulerController extends BaseAdminModule {
	const name = 'CRON időzítő';
	const version = '1.0';
	
	public function registerJob($parameters) {
		if ($parameters['path'] && $parameters['method']) {
			Db::sql(sprintf("
				REPLACE INTO UniadminScheduler
				(path, method, hours, minutes, days, months, weekdays, emailResults)
				VALUES
				('%s', '%s', '%s', '%s', '%s', '%s', '%s', '%s')",
				$parameters['path'],
				$parameters['method'],
				$parameters['hours'] ? $parameters['hours'] : '',
				$parameters['minutes'] ? $parameters['minutes'] : '',
				$parameters['days'] ? $parameters['days'] : '',
				$parameters['months'] ? $parameters['months'] : '',
				$parameters['weekdays'] ? $parameters['weekdays'] : '',
				$parameters['emailResults'] ? $parameters['emailResults'] : ''));
			return true;
		} else {
			return false;
		}
	}

	public function unRegisterJob($parameters) {
		if ($parameters['path'] && $parameters['method']) {
			Db::sql(sprintf("
				DELETE FROM UniadminScheduler
				WHERE path = '%s'
				AND method = '%s'",
				$parameters['path'], 
				$parameters['method']));
			return true;
		} else {
			return false;
		}
	}
}
