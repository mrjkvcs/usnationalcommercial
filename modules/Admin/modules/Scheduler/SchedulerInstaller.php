<?php 

class SchedulerInstaller extends AdminInstaller {
	public function __construct() {
		parent::__construct('Scheduler');
	}

	public function install() {
		$this->unInstall();
		$this->checkTables();
	}
}
