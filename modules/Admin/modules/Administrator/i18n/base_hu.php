<?php

$dictionary['column-name'] = 'Név';
$dictionary['table-administrator-groups'] = 'Adminisztrátor csoportok';

$dictionary['column-username'] = 'felhasználónév';
$dictionary['column-email'] = 'e-mail';
$dictionary['column-group'] = 'csoport';
$dictionary['column-last-login'] = 'utolsó belépés';
$dictionary['table-administrators'] = 'Adminisztrátorok';
$dictionary['not-logged-in-yet'] = 'még nem lépett be';

$dictionary['form-user-details'] = 'Adminisztrátor adatai';
$dictionary['group-default'] = 'Általános adatok';
$dictionary['group-modules'] = 'Modulszintű hozzáférések';
$dictionary['group-restrictions'] = 'Korlátozások';
$dictionary['label-username'] = 'Felhasználónév:';
$dictionary['change-password'] = 'Jelszó megváltoztatása';
$dictionary['label-password'] = 'Jelszó:';
$dictionary['label-password-again'] = 'Jelszó újra:';
$dictionary['label-name'] = 'Név:';
$dictionary['label-namet'] = 'Név:';
$dictionary['label-email'] = 'E-mail cím:';
$dictionary['notify-user-in-email'] = 'Felhasználó értesítése e-mailben';
$dictionary['root-user'] = 'Rendszeradminisztrátor felhasználó';
$dictionary['label-group'] = 'Csoport:';
$dictionary['rights-specification-at-group-level'] = 'Jogok megadása csoport szinten';
$dictionary['rights-specification-at-user-level'] = 'Jogok megadása egyénileg';
$dictionary['label-ip-limit'] = 'IP cím korlátozás:';
$dictionary['label-home-directory'] = 'Saját mappa:';
$dictionary['tooltip-home-directory'] = 'A felhasználó csak ebbe a mappába tölthet fel fájlokat, és egyéb jogosultságok híján csak ennek a tartalmához férhet hozzá a fájlkezelőben';

$dictionary['administrators'] = 'Adminisztrátorok';
$dictionary['administrator-list'] = 'Adminisztrátorok listája';
$dictionary['insert-administrator'] = 'Új adminisztrátor felvétele';
$dictionary['group-list'] = 'Csoportok listája';
$dictionary['insert-group'] = 'Új csoport felvétele';
$dictionary['administrator-access'] = 'Adminisztrátor hozzáférés';

$dictionary['label-module-access'] = 'Modul hozzáférések:';

$dictionary['message-user-added'] = 'A felhasználó felvétele sikeresen megtörtént';
$dictionary['message-user-saved'] = 'A felhasználó mentése sikeresen megtörtént';
$dictionary['message-user-deleted'] = 'A felhasználó törlése sikeresen megtörtént';
$dictionary['message-user-error-editing'] = 'A felhasználó hozzáférése nem szerkeszthető. Ellenőrizze a jogosultságokat!';
$dictionary['message-user-error-deleting'] = 'Hiba a felhasználó törlése során. Ellenőrizze a jogosultságokat!';
$dictionary['message-group-added'] = 'A csoport felvétele sikeresen megtörtént';
$dictionary['message-group-saved'] = 'A csoport mentése sikeresen megtörtént';
$dictionary['message-group-deleted'] = 'A csoport törlése sikeresen megtörtént';

$dictionary['dear-user'] = 'Kedves %s!';
$dictionary['administrator-account-details'] = 'Az Ön UniAdmin hozzáférésének adatai:';
$dictionary['label-url'] = 'Belépési cím:';
