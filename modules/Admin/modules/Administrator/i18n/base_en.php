<?php

$dictionary['column-name'] = 'Name';
$dictionary['table-administrator-groups'] = 'Administrator groups';

$dictionary['column-username'] = 'username';
$dictionary['column-email'] = 'email';
$dictionary['column-group'] = 'group';
$dictionary['column-last-login'] = 'last login at';
$dictionary['table-administrators'] = 'Administrators';
$dictionary['not-logged-in-yet'] = 'not logged in yet';

$dictionary['form-user-details'] = 'User details';
$dictionary['group-default'] = 'Basic information';
$dictionary['group-modules'] = 'Module access';
$dictionary['group-restrictions'] = 'Restrictions';
$dictionary['label-username'] = 'Username:';
$dictionary['change-password'] = 'Change password';
$dictionary['label-password'] = 'Password:';
$dictionary['label-password-again'] = 'Password again:';
$dictionary['label-name'] = 'Name:';
$dictionary['label-email'] = 'Email address:';
$dictionary['notify-user-in-email'] = 'Send email notification';
$dictionary['root-user'] = 'System administrator user';
$dictionary['label-group'] = 'Group:';
$dictionary['rights-specification-at-group-level'] = 'Specify rights at group level';
$dictionary['rights-specification-at-user-level'] = 'Specify rights at user level';
$dictionary['label-ip-limit'] = 'Limit IP address:';
$dictionary['label-home-directory'] = 'Home directory:';
$dictionary['tooltip-home-directory'] = 'The user can upload files into that folder, and with the lack of any other permissions is only able to manage its contents';

$dictionary['administrators'] = 'Administrators';
$dictionary['administrator-list'] = 'User list';
$dictionary['insert-administrator'] = 'Insert administrator';
$dictionary['group-list'] = 'Group list';
$dictionary['insert-group'] = 'Create new group';
$dictionary['administrator-access'] = 'Administrator access';

$dictionary['label-module-access'] = 'Module access:';

$dictionary['message-user-added'] = 'Administrator added successfully';
$dictionary['message-user-saved'] = 'Administrator saved successfully';
$dictionary['message-user-deleted'] = 'Administrator deleted successfully';
$dictionary['message-user-error-editing'] = 'User access is not editable, please review your permissions!';
$dictionary['message-user-error-deleting'] = 'User access is not deletable, please review your permissions!';
$dictionary['message-group-added'] = 'Group added successfully';
$dictionary['message-group-saved'] = 'Group saved successfully';
$dictionary['message-group-deleted'] = 'Group deleted successfully';

$dictionary['dear-user'] = 'Dear %s!';
$dictionary['administrator-account-details'] = 'Your UniAdmin access details are the following:';
$dictionary['label-url'] = 'Login URL:';
