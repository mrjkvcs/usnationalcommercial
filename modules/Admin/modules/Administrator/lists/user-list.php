<?php

$userList = new DataDisplay(array(
	'sql' => "
		SELECT 
			`UniadminUsers`.`id`,
			`username`, 
			`UniadminUsers`.`name`, 
			`email`,
			`UniadminUserGroups`.`name` AS `group`,
			`lastLoginAt`
		FROM `UniadminUsers` 
		LEFT JOIN `UniadminUserGroups` ON `UniadminUsers`.`groupId` = `UniadminUserGroups`.`id`",
	'defaultOrder' => 'username',
	'fullWidth' => true,
	'hideField' => 'id',
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'username' => array(
			'title' => t('column-username'),
		),
		'name' => array(
			'title' => t('column-name'),
		),
		'email' => array(
			'title' => t('column-email'),
		),
		'group' => array(
			'title' => t('column-group'),
		),
		'lastLoginAt' => array(
			'title' => t('column-last-login'),
			'dataHandler' => function($value) {
				return $value == '0000-00-00 00:00:00' ? t('not-logged-in-yet') : Date::format($value, 'dynamic');
			},
		),
	),
	'title' => t('table-administrators'),
	'baseModule' => 'administrator',
	'insertButton' => t('insert-administrator'),
));

return $userList;
