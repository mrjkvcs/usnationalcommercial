<?php

$groupList = new DataDisplay(array(
	'sql' => "
		SELECT 
			`id`,
			`name`
		FROM `UniadminUserGroups`",
	'defaultOrder' => 'name',
	'fullWidth' => true,
	'hideField' => 'id',
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'name' => array(
			'title' => t('column-name'),
		),
	),
	'updateAction' => 'groupupdate',
	'deleteAction' => 'groupdelete',
	'title' => t('table-administrator-groups'),
	'baseModule' => array('administrator', 'group'),
	'insertButton' => array(t('insert-group'), 'groupInsert'),
));

return $groupList;
