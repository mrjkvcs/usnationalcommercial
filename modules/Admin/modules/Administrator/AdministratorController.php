<?php

class AdministratorController extends BaseAdminModule {
	const name = 'Adminisztrátor kezelő';
	const version = '3.0';

	public $viewFile;
	protected $defaultModules = array(
		'Adminprofile', 'Filebrowser', 'Welcome',
	);

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('administrator.base');
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('administrators'),
			'module' => 'administrator',
			'action' => '',
			'submenus' => array(
				array('title' => t('administrator-list'), 'module' => 'administrator', 'action' => null),
				array('title' => t('insert-administrator'), 'module' => 'administrator', 'action' => 'insert'),
				array('title' => t('group-list'), 'module' => 'administrator', 'action' => 'group'),
				array('title' => t('insert-group'), 'module' => 'administrator', 'action' => 'groupinsert'),
			),
			'type' => 'function'
		);
	}

	public function beforeAction() {
		UniAdmin::app()->path->addElement(t('administrators'), 'administrator');
	}

	public function actionDefault() {
		$this->viewFile = 'userlist';
	}

	public function actionInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('insert-administrator'), 'administrator', 'insert');

		$this->userEdit();
	}

	public function actionUpdate() {
		$this->viewFile = 'edit';
		$this->userEdit();
	}

	public function actionDelete() {
		$this->checkUserId();

		if ($this->userId != UniAdmin::app()->user->getId()) {
			$isRoot = Db::sqlField(sprintf("
				SELECT `isRoot`
				FROM `UniadminUsers`
				WHERE `id` = %d",
				$this->userId
			));
			if (UniAdmin::app()->user->isRoot() || !$isRoot) {
				Db::sql(sprintf("DELETE FROM UniadminUsers WHERE id = %d", $this->userId));
				Db::sql(sprintf("DELETE FROM UniadminModuleAccess WHERE type = 'u' AND `userId` = %d", $this->userId));
				Db::sql(sprintf("DELETE FROM UniadminModulePermissions WHERE type = 'u' AND `userId` = %d", $this->userId));

				UniAdmin::app()->log->setType('admin')->write('deleted administrator #' . $this->userId);

				$this->redirect('administrator', '', LANG, array('msg' => 'deleted'));
			}
		}

		UniAdmin::app()->log->setType('admin')->write('failed to delete administrator #' . $this->useId);

		$this->redirect('administrator', '', LANG, array('msg' => 'errordeleting'));
	}

	public function actionGroup() {
		$this->viewFile = 'grouplist';
		UniAdmin::app()->path->addElement(t('group-list'), 'administrator', 'group');
	}

	public function actionGroupInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('insert-group'), 'administrator', 'groupinsert');

		$this->groupEdit();
	}

	public function actionGroupUpdate() {
		$this->viewFile = 'edit';
		$this->groupEdit();
	}

	public function actionGroupDelete() {
		$this->checkGroupId();

		Db::sql(sprintf("DELETE FROM UniadminUserGroups WHERE id = %d", $this->groupId));
		Db::sql(sprintf("DELETE FROM UniadminUsers WHERE groupId = %d", $this->groupId));
		Db::sql(sprintf("DELETE FROM UniadminModuleAccess WHERE type = 'g' AND `userId` = %d", $this->groupId));
		Db::sql(sprintf("DELETE FROM UniadminModulePermissions WHERE type = 'g' AND `userId` = %d", $this->groupId));

		UniAdmin::app()->log->setType('admin')->write('deleted administrator group #' . $this->groupId);

		$this->redirect('administrator', 'group', LANG, array('msg' => 'deleted'));
	}

	protected function checkUserId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->userId = isset($route[2]) ? intval($route[2]) : 0;
		if (!$this->userId) $this->redirect('administrator');
	}

	protected function checkGroupId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->groupId = isset($route[2]) ? intval($route[2]) : 0;
		if (!$this->groupId) $this->redirect('administrator', 'group');
	}

	protected function userEdit() {
		if ($this->action == 'update') {
			$this->checkUserId();
		}

		UniAdmin::app()->page->addCss('modules.Admin.modules.Administrator.css.administrator');
		UniAdmin::app()->page->addScript('modules.Admin.modules.Administrator.scripts.administrator');
		UniAdmin::import('classes.forms.Form');

		$this->form = new Form(false);
		$this->form->loadFromFile('administrator.edit')
			->setCallback()
			->setBackAction('administrator');

		$allowedModules = $this->getAllowedModules();
		$isRoot = UniAdmin::app()->user->isRoot();
		$isInherited = UniAdmin::app()->user->getInheritance();

		if ($this->action == 'update') {
			$editedInherit = Db::sqlField(sprintf("
				SELECT `isInherited`
				FROM `UniadminUsers`
				WHERE `id` = %d",
				$this->userId
			));
			if (!$isInherited && $editedInherit) {
				$this->redirect('administrator', '', LANG, array('msg' => 'errorediting'));
			}
			// $this->form->removeField('notifyUser');
		} else {
			$this->form->removeField('changepassword');
			$this->form->getField('password')->show()->required();
			$this->form->getField('passwordagain')->show()->required();
		}

		if (!$isInherited) {
			$options = $this->form->getField('isInherited')->getOptions();
			unset($options[1]);
			$this->form->getField('isInherited')
				->setOptions($options)
				->setValue(0);
		}

		if (!$isRoot) {
			$this->form->removeField('isRoot');
		}

		$this->form->getField('groupId')->setOptions($this->getAllowedGroups());

		$i = 0;
		$result = Db::sql("SELECT id, name FROM UniadminModules ORDER BY name");
		while ($rs = Db::loop($result)) {
			if (in_array($rs['id'], $this->defaultModules)) {
				continue;	// alapértelmezett modulok ne is jelenjenek meg a listában
			}
			if (!$isRoot && !in_array($rs['id'], $allowedModules)) {
				continue;	// csak az engedélyezett modulokat sorolja fel
			}
			$field = new FCheckbox();
			if (!$i++) {
				$field->setLabel('Modul hozzáférések:');
			}
			$field->setId('module[' . $rs['id'] . ']')
				->setText($rs['name'])
				->setGroup('modules')
				->required(false);
			$this->form->addField($field);

			$moduleToLoad = $rs['id'] . 'Controller';
			UniAdmin::import('modules.Admin.modules.' . $rs['id'] . '.' . $moduleToLoad);
			if (class_exists($moduleToLoad)) {
				$module = new $moduleToLoad;
				if (isset($module->accessLevel) && $module->accessLevel instanceof ModuleAccessLevel) {
					$permissions = $module->accessLevel->getPermissions();
					$allowedPermissions = $this->getAllowedPermissions($rs['id'] . 'Controller');
					foreach ($permissions as $permission) {
						if (!$isRoot && !in_array($permission->getId(), $allowedPermissions)) {
							continue;	// csak az engedélyezett jogköröket sorolja fel
						}
						$field = new FCheckbox();
						$field->setId('permission[' . $rs['id'] . '][' . $permission->getId() . ']')
							->setText($permission->getName())
							->setGroup('modules')
							->setClassName('permission permission-' . $rs['id'])
							->required(false)
							->hide();
						if ($this->action == 'insert' && !$this->form->isSent() && $permission->getCheckedByDefault()) {
							$field->setValue(1);
						}
						if ($this->form->isSent() && isset($_POST['module'][$rs['id']])) {
							$field->show();
						}
						$this->form->addField($field);
					}
				}
			}
		}

		if ($this->form->isSent()) {
			$passwordChanged = false;
			$this->form->queryValues();
			if ($this->action == 'update') {
				if (!$this->form->getField('changepassword')->getValue()) {
					$this->form->getField('password')
						->setField(null)
						->setMatchTo(null);
				} else{
					$this->form->getField('password')->show()->required();
					$this->form->getField('passwordagain')->show()->required();
					$oldPassword = Db::sqlField(sprintf("
						SELECT `password`
						FROM UniadminUsers
						WHERE id = %d",
						$this->form->getValue('id')
					));
					if ($oldPassword != $this->form->getValue('password')) {
						$passwordChanged = true;
					}
				}
			}
			$userId = $this->form->save($this->action);
			if ($userId) {
				if (!$this->form->getValue('isInherited')) {
					if ($this->action == 'update') {
						$controllerList = array();
						foreach ($allowedModules as $moduleId) {
							$controllerList[] = $moduleId . 'Controller';
							$allowedPermissions = $this->getAllowedPermissions($moduleId . 'Controller');
							Db::sql(sprintf("
								DELETE FROM UniadminModulePermissions 
								WHERE type = 'u' 
								AND userId = %d 
								AND moduleId = '%s' 
								AND permission IN ('%s')",
								$userId, 
								$moduleId . 'Controller', implode("','", $allowedPermissions)
							));
						}
						Db::sql(sprintf("
							DELETE FROM UniadminModuleAccess 
							WHERE type = 'u' 
							AND userId = %d 
							AND moduleId IN ('%s')",
							$userId, 
							implode("','", $controllerList)
						));

						UniAdmin::import('modules.Admin.classes.ModuleMenu');
						ModuleMenu::clearCache();
					}
					foreach ($this->defaultModules as $moduleId) {	// alapértelmezett modulok felvétele
						$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModuleAccess WHERE type = 'u' AND userId = %d AND moduleId = '%s'",
							$userId, $moduleId . 'Controller'));
						if (!$num) {
							Db::sql(sprintf("INSERT INTO UniadminModuleAccess (type, userId, moduleId) VALUES ('u', %d, '%s')",
								$userId, $moduleId . 'Controller'));
						}
					}
					if (is_array($_POST['module'])) {
						foreach ($_POST['module'] as $moduleId => $value) {
							$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModuleAccess WHERE type = 'u' AND userId = %d AND moduleId = '%s'",
								$userId, $moduleId . 'Controller'));
							if (!$num) {
								Db::sql(sprintf("INSERT INTO UniadminModuleAccess (type, userId, moduleId) VALUES ('u', %d, '%s')",
									$userId, $moduleId . 'Controller'));
							}
						}
					}
					if (is_array($_POST['permission'])) {
						foreach ($_POST['permission'] as $moduleId => $data) {
							if (is_array($data)) {
								foreach ($data as $permissionId => $value) {
									$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModulePermissions WHERE type = 'u' AND userId = %d AND moduleId = '%s' AND permission = '%s'",
										$userId, $moduleId . 'Controller', $permissionId));
									if (!$num) {
										Db::sql(sprintf("INSERT INTO UniadminModulePermissions (type, userId, moduleId, permission) VALUES ('u', %d, '%s', '%s')",
											$userId, $moduleId . 'Controller', $permissionId));
									}
								}
							}
						}
					}
				}
				if (($this->action == 'insert' || $passwordChanged)
					&& $this->form->getValue('notifyUser')
					&& $this->form->getValue('email')
				) {
					$email = new Email();
					$email->to($this->form->getValue('email'))
						->setSubject(t('administrator-access'))
						->setMessage($this->loadView('administrator.usercreated-email', $this->form->getValues()))
						->send();
				}

				if ($this->action == 'insert') {
					UniAdmin::app()->log->setType('admin')->write('created administrator #' . $userId . ' (' . $this->form->getValue('username') . ')');
				}

				$this->redirect('administrator');
			}
		} else {
			if ($this->action == 'update') {
				$this->form->loadValues('id = ' . $this->userId);

				if (!$this->form->getValue('isInherited')) {
					$result = Db::sql(sprintf("SELECT moduleId FROM UniadminModuleAccess WHERE type = 'u' AND userId = %d",
						$this->userId));
					while ($rs = Db::loop($result)) {
						$moduleId = substr($rs['moduleId'], 0, -10);
						if ($this->form->hasField('module[' . $moduleId . ']')) {
							$this->form->getField('module[' . $moduleId . ']')
								->setValue('module[' . $moduleId . ']', 1);
						}
						$permissions = Db::sql(sprintf("SELECT permission FROM UniadminModulePermissions WHERE type = 'u' AND userId = %d AND moduleId = '%s'",
							$this->userId, $rs['moduleId']));
						while ($prs = Db::loop($permissions)) {
							if ($this->form->hasField('permission[' . $moduleId . '][' . $prs['permission'] . ']')) {
								$this->form->getField('permission[' . $moduleId . '][' . $prs['permission'] . ']')
									->setValue('permission[' . $moduleId . '][' . $prs['permission'] . ']', 1);
							}
						}
					}
				}
				// @todo szerkesztés bekötése
				//array_splice($this->tabs, 2, 0, array(array('title' => 'Szerkesztés', 'module' => 'administrator', 'action' => array('update', $this->articleId))));
			}
		}
	}

	protected function groupEdit() {
		if ($this->action == 'groupupdate') {
			$this->checkGroupId();
		}

		UniAdmin::import('classes.forms.Form');
		UniAdmin::app()->page->addCss('modules.Admin.modules.Administrator.css.administrator');
		UniAdmin::app()->page->addScript('modules.Admin.modules.Administrator.scripts.administrator');

		$this->form = new Form(false);
		$this->form->loadFromFile('administrator.groupEdit')
			->setCallback()
			->setBackAction('administrator', 'group');

		$allowedModules = $this->getAllowedModules();
		$isRoot = UniAdmin::app()->user->isRoot();

		$i = 0;
		$result = Db::sql("SELECT id, name FROM UniadminModules ORDER BY name");
		while ($rs = Db::loop($result)) {
			if (in_array($rs['id'], $this->defaultModules)) {
				continue;	// alapértelmezett modulok ne is jelenjenek meg a listában
			}
			if (!$isRoot && !in_array($rs['id'], $allowedModules)) {
				continue;	// csak az engedélyezett modulokat sorolja fel
			}
			$field = new FCheckbox();
			if (!$i++) {
				$field->setLabel('label-module-access');
			}
			$field->setId('module[' . $rs['id'] . ']')->setText($rs['name'])->required(false);
			$this->form->addField($field);

			$moduleToLoad = $rs['id'] . 'Controller';
			UniAdmin::import('modules.Admin.modules.' . $rs['id'] . '.' . $moduleToLoad);
			if (class_exists($moduleToLoad)) {
				$module = new $moduleToLoad;
				if (isset($module->accessLevel) && $module->accessLevel instanceof ModuleAccessLevel) {
					$permissions = $module->accessLevel->getPermissions();
					$allowedPermissions = $this->getAllowedPermissions($rs['id'] . 'Controller');
					foreach ($permissions as $permission) {
						if (!$isRoot && !in_array($permission->getId(), $allowedPermissions)) {
							continue;	// csak az engedélyezett jogköröket sorolja fel
						}
						$field = new FCheckbox();
						$field->setId('permission[' . $rs['id'] . '][' . $permission->getId() . ']')
							->setText($permission->getName())
							->setClassName('permission permission-' . $rs['id'])
							->required(false)
							->hide();
						if ($this->action == 'groupinsert' && !$this->form->isSent() && $permission->getCheckedByDefault()) {
							$field->setValue(1);
						}
						if ($this->form->isSent() && isset($_POST['module'][$rs['id']])) {
							$field->show();
						}
						$this->form->addField($field);
					}
				}
			}
		}

		if ($this->form->isSent()) {
			$this->form->queryValues();
			$groupId = $this->form->save($this->action == 'groupinsert' ? 'insert' : 'update');
			if ($groupId) {
				if ($this->action == 'groupupdate') {
					$controllerList = $allowedPermissions = array();
					foreach ($allowedModules as $moduleId) {
						$controllerList[] = $moduleId . 'Controller';
						$allowedPermissions = $this->getAllowedPermissions($moduleId . 'Controller');
						Db::sql(sprintf("
							DELETE FROM UniadminModulePermissions 
							WHERE type = 'g' 
							AND userId = %d 
							AND moduleId = '%s' 
							AND permission IN ('%s')",
							$groupId, 
							$moduleId . 'Controller', implode("','", $allowedPermissions)
						));
					}
					Db::sql(sprintf("
						DELETE FROM UniadminModuleAccess 
						WHERE type = 'g' 
						AND userId = %d 
						AND moduleId IN ('%s')",
						$groupId, 
						implode("','", $controllerList)
					));

					UniAdmin::import('modules.Admin.classes.ModuleMenu');
					ModuleMenu::clearCache();
				}
				foreach ($this->defaultModules as $moduleId) {	// alapértelmezett modulok felvétele
					$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModuleAccess WHERE type = 'g' AND userId = %d AND moduleId = '%s'",
						$groupId, $moduleId . 'Controller'));
					if (!$num) {
						Db::sql(sprintf("INSERT INTO UniadminModuleAccess (type, userId, moduleId) VALUES ('g', %d, '%s')",
							$groupId, $moduleId . 'Controller'));
					}
				}
				if (isset($_POST['module']) && is_array($_POST['module'])) {
					foreach ($_POST['module'] as $moduleId => $value) {
						$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModuleAccess WHERE type = 'g' AND userId = %d AND moduleId = '%s'",
							$groupId, $moduleId . 'Controller'));
						if (!$num) {
							Db::sql(sprintf("INSERT INTO UniadminModuleAccess (type, userId, moduleId) VALUES ('g', %d, '%s')",
								$groupId, $moduleId . 'Controller'));
						}
					}
				}
				if (isset($_POST['permission']) && is_array($_POST['permission'])) {
					foreach ($_POST['permission'] as $moduleId => $data) {
						if (is_array($data)) {
							foreach ($data as $permissionId => $value) {
								$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModulePermissions WHERE type = 'g' AND userId = %d AND moduleId = '%s' AND permission = '%s'",
									$groupId, $moduleId . 'Controller', $permissionId));
								if (!$num) {
									Db::sql(sprintf("INSERT INTO UniadminModulePermissions (type, userId, moduleId, permission) VALUES ('g', %d, '%s', '%s')",
										$groupId, $moduleId . 'Controller', $permissionId));
								}
							}
						}
					}
				}

				if ($this->action == 'groupinsert') {
					UniAdmin::app()->log->setType('admin')->write('created administrator group #' . $groupId . ' (' . $this->form->getValue('name') . ')');
				}

				$this->redirect('administrator', 'group');
			}
		} else {
			if ($this->action == 'groupupdate') {
				$this->form->loadValues('id = ' . $this->groupId);

				$result = Db::sql(sprintf("SELECT moduleId FROM UniadminModuleAccess WHERE type = 'g' AND userId = %d",
					$this->groupId));
				while ($rs = Db::loop($result)) {
					$moduleId = substr($rs['moduleId'], 0, -10);
					if ($this->form->hasField('module[' . $moduleId . ']')) {
						$this->form->getField('module[' . $moduleId . ']')->setValue('module[' . $moduleId . ']', 1);
					}
					$permissions = Db::sql(sprintf("SELECT permission FROM UniadminModulePermissions WHERE type = 'g' AND userId = %d AND moduleId = '%s'",
						$this->groupId, $rs['moduleId']));
					while ($prs = Db::loop($permissions)) {
						if ($this->form->hasField('permission[' . $moduleId . '][' . $prs['permission'] . ']')) {
							$this->form->getField('permission[' . $moduleId . '][' . $prs['permission'] . ']')
								->setValue('permission[' . $moduleId . '][' . $prs['permission'] . ']', 1);
						}
					}
				}

				// @todo szerkesztés bekötése
				//array_splice($this->tabs, 4, 0, array(array('title' => Text::truncate($rs['name'], 30), 'module' => 'administrator', 'action' => array('groupupdate', $this->groupId))));
			}
		}
	}

	public function getAllowedModules() {
		$ret = array();
		if (UniAdmin::app()->user->getInheritance()) {
			$inheritance = true;
			$userId = UniAdmin::app()->user->getGroupId();
		} else {
			$inheritance = false;
			$userId = UniAdmin::app()->user->getId();
		}
		$result = Db::sql(sprintf("SELECT moduleId FROM UniadminModuleAccess WHERE type = '%s' AND userId = %d",
			$inheritance ? 'g' : 'u', $userId));
		while ($rs = Db::loop($result)) {
			$ret[] = substr($rs['moduleId'], 0, -10);
		}
		return $ret;
	}

	public function getAllowedGroups() {
		$groupId = UniAdmin::app()->user->getGroupId();
		$isRoot = UniAdmin::app()->user->isRoot();

		$result = Db::sql("SELECT `id`, `name` FROM `UniadminUserGroups` ORDER BY `name`");
		$ret = array();
		while ($rs = Db::loop($result)) {
			if ($isRoot || $rs['id'] == $groupId) {
				$ret[$rs['id']] = $rs['name'];
			} else {
				$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM `UniadminUsers` WHERE `isRoot` = 1 AND `groupId` = %d",
					$rs['id']));
				if (!$num) {
					$ret[$rs['id']] = $rs['name'];
				}
			}
		}
		return $ret;
	}

	public function getAllowedPermissions($moduleId) {
		$ret = array();
		if (UniAdmin::app()->user->getInheritance()) {
			$inheritance = true;
			$userId = UniAdmin::app()->user->getGroupId();
		} else {
			$inheritance = false;
			$userId = UniAdmin::app()->user->getId();
		}
		$ret = Db::toArray(sprintf("SELECT permission FROM UniadminModulePermissions WHERE type = '%s' AND userId = %d AND moduleId = '%s'",
			$inheritance ? 'g' : 'u', $userId, $moduleId));
		return $ret;
	}
}
