<?php $url = 'http://' . $_SERVER['HTTP_HOST'] . Url::link(''); ?>
<p><?php echo sprintf(t('dear-user'), $name ? $name : $username); ?></p>
<p><?php echo t('administrator-account-details'); ?></p>

<table border="0" cellspacing="0" cellpadding="2">
	<tr>
		<td><?php echo t('label-username'); ?></td>
		<td>
			<b><?php echo $username; ?></b>
		</td>
	</tr>
	<tr>
		<td><?php echo t('label-password'); ?></td>
		<td>
			<b><?php echo $_POST['password']; ?></b>
		</td>
	</tr>
	<tr>
		<td><?php echo t('label-url'); ?></td>
		<td>
			<a href="<?php echo $url; ?>"><?php echo $url; ?></a>
		</td>
	</tr>
</table>

<p>&nbsp;</p>
<p><i><?php echo Date::format() . ' ' . $_SERVER['HTTP_HOST']; ?></i></p>
