<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('message-user-added'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('message-user-saved'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('message-user-deleted'));
		break;
	case 'errorediting':
			SysMessage::displayError(t('message-user-error-editing'));
		break;
	case 'errordeleting':
			SysMessage::displayError(t('message-user-error-deleting'));
		break;
}

$this->loadDataDisplay('user-list')->renderView();
