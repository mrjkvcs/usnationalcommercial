<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('message-group-added'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('message-group-saved'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('message-group-deleted'));
		break;
}

$this->loadDataDisplay('group-list')->renderView();
