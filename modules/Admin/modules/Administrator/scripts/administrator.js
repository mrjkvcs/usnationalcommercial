checkNotify = function() {
	if ($('#email').val()) {
		// ellenőrizzük, hogy szerkesztés esetén kért-e jelszóváltást
		if (!$('#changepassword').exists() || $('#changepassword').attr('checked')) {
			$('#notifyUser').attr('disabled', false);
		} else {
			$('#notifyUser').attr('disabled', true);
		}
	} else {
		$('#notifyUser').attr('disabled', true);
	}
};

checkModules = function() {
	if ($('input:radio[name=isInherited]:checked').val() == '1') {	// öröklött
		$('#g_modules').hide();
	} else {
		$('#g_modules').show();
		$('#g_modules .form-group-title')
			.addClass('form-group-close')
			.removeClass('form-group-open');
		$('#g_modules .form-group-elements').show();
	}
};

checkPermissions = function(target) {
	var moduleId = $(target).attr('id').split(/\[|\]/)[1];
	var permissions = $(target).closest('.form-field').nextAll('.form-field[id^=s_permission]').find('.permission-' + moduleId).closest('.form-field');
	if ($(target).attr('checked')) {
		permissions.show();
	} else {
		permissions.hide();
	}
};

jQuery(document).ready(function($){
	if ($('#email').exists()) {		// csak user kezelő
		$('#isInherited_1, #isInherited_0').click(function(){
			checkModules();
		});

		$('#email').keyup(function(){
			checkNotify();
		});

		$('#changepassword').click(function(){
			$('#s_password, #s_passwordagain').toggle();
			if ($('#changepassword').attr('checked')) {
				$('#password').focus();
				if ($('#email').val()) {
					$('#notifyUser').attr('disabled', false);
				}
			} else {
				$('#notifyUser').attr('disabled', true);
			}
		});

		checkNotify();
		checkModules();
	}

	$('input[type=checkbox][id^=module]').click(function(event){
		checkPermissions(event.target);
	});

	$('input[type=checkbox][id^=module]').each(function(index, item){
		checkPermissions(item);
	});
});