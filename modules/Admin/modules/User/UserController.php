<?php

class UserController extends BaseAdminModule {

    const name = 'Users';
    const version = '1.0';

    public function __construct() {
        parent::__construct();

        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters) {
        $parameters->menus[] = array(
            'title'    => 'User',
            'module'   => 'user',
            'action'   => '',
            'submenus' => array(
                array('title' => 'Add new User', 'module' => 'user', 'action' => 'insert'),
            ),
            'type'     => 'content'
        );
    }

    protected function checkItemId($userId) {
        $this->userId = intval($userId);
        if (!$this->userId) {
            return Url::link('user');
        }
    }

    public function beforeAction() {
        UniAdmin::app()->path->addElement('User', 'user');
    }

    public function actionDefault() {
        $this->viewFile = 'user-list';
        UniAdmin::app()->path->addElement('User list', 'user');
        UniAdmin::app()->page->addCss('modules.Admin.modules.User.css.user');
    }

    public function actionInsert() {
        $this->viewFile = 'edit';
        UniAdmin::app()->path->addElement('Add new user', 'user', 'insert');

        $this->userEdit();
    }

    public function actionUpdate($userId) {
        $this->viewFile = 'edit';
        $this->userEdit($userId);
    }

    public function actionDelete($userId) {
        Db::sql(sprintf("DELETE FROM User WHERE id = %d", $this->userId));

        $this->redirect('user', '', LANG, array('msg' => 'deleted'));
    }

    protected function userEdit($userId = null) {
        UniAdmin::import('classes.forms.Form');
        $this->form = new Form();
        $this->form
            ->loadFromFile('user.user-edit')
            ->setBackAction('user')
			->setCallback();

        if ($this->form->isSent()) {
            $this->form->queryValues();
            $phone = Util::formatPhone($this->form->getValue('phone'));
            $this->form->setValue('phone', $phone);
            if (User::find_by_phone($phone)) $this->redirect('user', null, LANG, array('msg' => 'This user already exists'));
            $this->form
                ->addDbField('password', '$2a$12$MfEngiL7VTVfCelPwAfn4OvVWmGracudnyAQZg85ZIAV33jHZrz4y')
                ->addDbField('registeredAt', date('Y-m-d H:i:s'))
                ->addDbField('expireDate', '2014-12-31')
                ->addDbField('isActivated', 1)
                ->addDbField('isVerified', 1);

            if ($userId = $this->form->save($this->action)) {
				$user = User::find($userId);
				$user->email = 'user' . $userId . '@choosepuppy.com';
				$user->save();

				$this->redirect('user', null, LANG, array('msg' => $this->action == 'insert' ? 'ok' : 'saved'));
			}
        } else {
            if ($this->action == 'update') {
                $this->form->loadValues('id = ' . $userId);
            }
        }
    }
}
