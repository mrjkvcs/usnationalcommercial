<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('User inserted successfully'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('User saved successfully'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('User deleted successfully'));
		break;
}

$this->loadDataDisplay('user-list')->renderView();
