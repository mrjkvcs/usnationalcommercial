<?php

$list = new DataDisplay(array(
	'sql' => "
            SELECT
                GolfResult.id,
                Member.firstName,
                Member.lastName,
                GolfResult.weekId,
                GolfSeason.name AS seasonName,
                GolfWeek.name AS weekName,
                GolfResult.score
            FROM GolfResult
            JOIN Member
                ON Member.id = GolfResult.userId
            JOIN GolfSeason
                ON GolfSeason.id = GolfResult.seasonId
            JOIN GolfWeek
                ON GolfWeek.id = GolfResult.weekId",
    'defaultOrder' => 'id DESC',
	'fullWidth' => true,
    'hideField' => ['id', 'weekId'],
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'firstName' => array(
			'title' => t('first name'),
            'class' => 'text-white'
		),
		'lastName' => array(
			'title' => t('last name'),
            'class' => 'text-white'
		),
        'seasonName' => [
            'title' => t('Season Name'),
            'class' => 'text-white'
        ],
        'weekName' => [
            'title' => t('Event (weeks)'),
            'class' => 'text-white'
        ],
        'score' => array(
            'title' => t('score'),
            'class' => 'text-white'
        )
	),
	'title' => t('Collection list'),
	'baseModule' => 'golf',
    'insertButton' => t('Add New Golf Result'),
));

return $list;
