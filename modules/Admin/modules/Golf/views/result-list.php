<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('Golf result inserted successfully'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('Golf result saved successfully'));
		break;
    case 'deleted':
        SysMessage::displaySuccess(t('Golf deleted successfully'));
        break;
	case 'exists':
			SysMessage::displaySuccess(t('Golf result already exists'));
		break;
}

$this->loadDataDisplay('result-list')->renderView();
