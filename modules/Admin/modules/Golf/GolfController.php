<?php

class GolfController extends BaseAdminModule {

    const name = 'Golf';
    const version = '1.0';

    public function __construct()
    {
        parent::__construct();

        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters)
    {
        $parameters->menus[] = array(
            'title'    => 'GOLF LEAGUE RESULT',
            'module'   => 'golf',
            'action'   => '',
            'submenus' => array(
                array('title' => 'Add new Result', 'module' => 'golf', 'action' => 'insert')
            ),
            'type'     => 'content'
        );
    }

    protected function checkItemId($resultId)
    {
        $this->resultId = intval($resultId);
        if (! $this->resultId)
        {
            return Url::link('golf');
        }
    }

    public function beforeAction()
    {
        UniAdmin::app()->path->addElement('Golf Results', 'golf');
    }

    public function actionDefault()
    {
        $this->viewFile = 'result-list';
        UniAdmin::app()->path
            ->addElement('Season', 'golf')
            ->addElement('Event (weeks)', 'golf');
        UniAdmin::app()->page->addCss('modules.Admin.modules.Golf.css.golf');
    }

    public function actionInsert()
    {
        $this->viewFile = 'golf';
        UniAdmin::app()->path->addElement('Add new user', 'user', 'insert');

        $this->golfResultEdit();
    }

    public function actionUpdate($resultId)
    {
        $this->viewFile = 'golf';
        $this->golfResultEdit($resultId);
    }

    public function actionDelete($resultId)
    {
        Db::sql(sprintf("DELETE FROM GolfResult WHERE id = %d", $this->resultId));

        $this->redirect('golf', '', LANG, array('msg' => 'deleted'));
    }

    protected function golfResultEdit($resultId = null)
    {
        UniAdmin::import('classes.forms.Form');
        $this->form = new Form();
        $loadXML = $this->action == 'update' ? 'golf-edit' : 'golf';
        $this->form
            ->loadFromFile('golf.' . $loadXML)
            ->setBackAction('golf')
            ->setCallback();

        if ($this->form->isSent())
        {
            $this->form->queryValues();
            if ($this->form->validateFields())
            {
                $data = $this->form->getValues();

                if ($this->action != 'update')
                {
                    $data = GolfWeek::getData($data['weekId']);
                    //die(print_r($data));
                    if (! GolfResult::exists($data))
                    {
                        $this->form->addDbField('seasonId', $data[0]['seasonId']);
                        $this->form->addDbField('weekId', $data[0]['event']);
                    } else
                    {
                        $this->redirect('golf', null, LANG, array('msg' => $this->action == 'insert' ? 'exists' : 'saved'));
                    }
                }
                $this->form->save($this->action);
                $this->redirect('golf', null, LANG, array('msg' => $this->action == 'insert' ? 'ok' : 'saved'));
            }
        } else
        {
            if ($this->action == 'update')
            {
                $this->form->loadValues('id = ' . $resultId);
            }
        }
    }
}
