<?php

$dictionary['personal-details'] = 'Személyes adatok';
$dictionary['form-personal-details'] = 'Személyes adataim szerkesztése';
$dictionary['label-name'] = 'Név:';
$dictionary['label-email'] = 'E-mail cím:';
$dictionary['change-password'] = 'Meg szeretném változtatni a jelszavam';
$dictionary['label-password'] = 'Jelszó:';
$dictionary['label-password-again'] = 'Jelszó újra:';

$dictionary['message-data-saved'] = 'Az adatok mentése sikeresen megtörtént';
