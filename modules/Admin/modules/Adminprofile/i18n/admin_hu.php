<?php

$dictionary['confirm-requested-action'] = 'Biztosan megerősíti a kívánt műveletet?';
$dictionary['administration'] = 'Adminisztráció';
$dictionary['browse'] = 'Tallózás';
$dictionary['file-type-is-not-allowed'] = 'Ez a fájltípus nem engedélyezett!';
