<?php

$dictionary['confirm-requested-action'] = 'Are you sure to confirm the requested action?';
$dictionary['administration'] = 'Administration';
$dictionary['browse'] = 'Browse';
$dictionary['file-type-is-not-allowed'] = 'This file type is not allowed!';
