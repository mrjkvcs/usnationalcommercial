<?php

$dictionary['personal-details'] = 'Personal details';
$dictionary['form-personal-details'] = 'Edit my personal details';
$dictionary['label-name'] = 'Name:';
$dictionary['label-email'] = 'Email address:';
$dictionary['change-password'] = 'I would like to change my password';
$dictionary['label-password'] = 'Password:';
$dictionary['label-password-again'] = 'Password again:';

$dictionary['message-data-saved'] = 'Data saved successfully';
