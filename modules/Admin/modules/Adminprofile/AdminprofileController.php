<?php

class AdminprofileController extends BaseAdminModule {
	const name = 'Adminisztrátor profilszerkesztő';
	const version = '1.0';

	public function __construct() {
		parent::__construct();
	}

	public function beforeAction() {
		$this->loadDictionary('base');

		UniAdmin::app()->path->addElement(t('personal-details'), 'adminprofile');
	}

	public function actionLogin() {
		if (UniAdmin::app()->user->checkLogin()) {
			header('Location: ' . Url::link(''));
			exit;
		}

		if (isset($_POST['login_user'])) {
			$redirectTo = isset($_POST['redirect']) ? $_POST['redirect'] : '';
			if (UniAdmin::app()->user->login()) {
				if (!$redirectTo) {
					$redirectTo = Url::link('');
				}
				header('Location: ' . $redirectTo);
				exit;
			} else {
				if (!$redirectTo) {
					$redirectTo = Url::link('', '', LANG, array('error' => 'user'));
				}  else {
					$redirectTo = Url::link('', '', LANG, array('error' => 'user', 'redirect' => urlencode($redirectTo)));
				}
				header('Location: ' . $redirectTo);
				exit;
			}
		}
	}

	public function actionLogout() {
		UniAdmin::app()->user->logout();
		header('Location: ' . Url::link(''));
		exit;
	}

	public function actionKeepalive() {
		echo 'hello';
		exit;
	}

	public function actionSavePreference() {
		$key = isset($_POST['key']) ? trim($_POST['key']) : '';
		$value = isset($_POST['value']) ? trim($_POST['value']) : '';
		if ($key) {
			UserPreferences::set($key, $value);
		}
		exit;
	}

	public function actionDefault() {
		$this->viewFile = 'adminprofile';

		UniAdmin::app()->page->addScript('modules.Admin.modules.Adminprofile.scripts.adminprofile');
		UniAdmin::import('classes.forms.*');

		$this->parameters = new EventParameter();
		$this->parameters->form = new Form();
		$this->parameters->form->loadFromFile('adminprofile.profile');

		$this->dispatchEvent('beforeShowFields', $this->parameters);

		if ($this->parameters->form->isSent()) {
			$this->parameters->form->queryValues();
			if (!$this->parameters->form->getField('changepassword')->getValue()) {
				$this->parameters->form->getField('password')
					->setField(null)
					->setMatchTo(null);
			} else{
				$this->parameters->form->getField('password')->show()->required();
				$this->parameters->form->getField('passwordagain')->show()->required();
			}

			if ($this->parameters->form->validateFields()) {
				$this->parameters->form->addDbField('id', UniAdmin::app()->user->getId(), true);
				$this->dispatchEvent('beforeSave', $this->parameters);
				$this->parameters->form->save('update');
			}
		} else {
			$this->parameters->form->loadValues('id = ' . UniAdmin::app()->user->getId());
		}
	}
}
