<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('Contact inserted successfully'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('Contact saved successfully'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('Contact deleted successfully'));
		break;
}

$this->loadDataDisplay('project-list')->renderView();
