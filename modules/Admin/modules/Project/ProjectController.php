<?php

class ProjectController extends BaseAdminModule {

    const name = 'Project';
    const version = '1.0';

    public function __construct()
    {
        parent::__construct();
        $this->validators['update'] = array($this, 'checkItemId');
        $this->validators['delete'] = array($this, 'checkItemId');
    }

    public function menu($parameters)
    {
        $parameters->menus[] = array(
            'title'    => 'PROJECTS',
            'module'   => 'project',
            'action'   => '',
            'submenus' => array(
                array('title' => 'Add new Project', 'module' => 'project', 'action' => 'insert'),
            ),
            'type'     => 'content'
        );
    }

    protected function checkItemId($projectId)
    {
        $this->projectId = intval($projectId);
        if (! $this->projectId)
        {
            return Url::link('project');
        }
    }

    public function beforeAction()
    {
        UniAdmin::app()->path->addElement('Projects', 'project');
    }

    public function actionDefault()
    {
        $this->viewFile = 'project-list';
        UniAdmin::app()->path->addElement('Projects list', 'project');
        UniAdmin::app()->page->addCss('modules.Admin.modules.Project.css.project');
    }

    public function actionInsert()
    {
        $this->viewFile = 'edit';
        UniAdmin::app()->path->addElement('Add new Project', 'project', 'insert');
        $this->projectEdit();
    }

    public function actionUpdate($projectId)
    {
        $this->viewFile = 'edit';
        $this->projectEdit($projectId);
    }

    public function actionDelete($projectId)
    {
        Db::sql(sprintf("UPDATE Project set isDeleted = 1 WHERE id = %d", $this->projectId));

        $this->redirect('project', '', LANG, array('msg' => 'deleted'));
    }

    protected function projectEdit($projectId = null)
    {
        UniAdmin::import('classes.forms.Form');
        $loadXML = $this->action == 'update' ? 'project-edit' : 'project';
        $this->form = new Form();
        $this->form
            ->loadFromFile('project.' . $loadXML)
            ->setAfterSaveCallBack(function($id, $form, $type) {
                $image = $form->getField('upload')->getImage();
                if (!empty($image))
                {
                    $dir = sprintf('images/project/');
                    ! is_dir($dir) ? mkdir($dir, 0777, true) : null;
                    $path = $dir . sprintf('%d.jpg', $id);
                    Util::saveImage($image, $path, 800, 450);
                }
            })
            ->setBackAction('project');

        if ($this->form->isSent())
        {
            $this->form->queryValues();
            $this->form->save($this->action);
        } else
        {
            if ($this->action == 'update')
            {
                $field = new FHtml();
                $field->setText('<img src="/images/project/' . $projectId . '.jpg" width="150" />')->setLabel('Default Picture');
                $this->form
                    ->loadValues('id = ' . $projectId)
                    ->addFieldBefore('name', $field)
                    ->setSubmitValue('Update');
            }
        }
    }
}
