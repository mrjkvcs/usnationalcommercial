<?php

$list = new DataDisplay(array(
    'sql'           => "
		SELECT
			id,
			name,
			title,
			rep,
			isDeleted
		FROM Project
		ORDER BY id DESC",
    'fullWidth'     => true,
    'id'            => basename(__FILE__, '.php'),
    'columnFormats' => array(
        'isDeleted' => [
            'title'       => t('Status'),
            'dataHandler' => function ($value)
            {
                return ! $value ? t('active') : t('sold');
            }
        ]
    ),
    'title'         => t('Project list'),
    'baseModule'    => 'project',
));

return $list;
