<?php

UniAdmin::import('modules.Admin.modules.Modulemanager.DatabaseField');
UniAdmin::import('modules.Admin.modules.Modulemanager.DatabaseForeignKey');
UniAdmin::import('modules.Admin.modules.Modulemanager.DatabaseIndex');
UniAdmin::import('modules.Admin.modules.Modulemanager.DatabaseTrigger');

class DatabaseSchema {


    protected $tables;

	public function __construct($filename) {

		$filename = str_replace('.', '/', $filename).'.xml';
		if (! file_exists($filename) ) {
            throw new Error('Missing schema xml>'.$filename);
		}

        $this->tables = simplexml_load_file($filename,'SimpleXMLElement',LIBXML_NOCDATA);
	}



	public function check() {
		$indexes = array();
		$foreignKeys = array();
		$triggers = array();

        foreach ($this->tables->table as $table) {
            if ($table['name']) {
				$fields = array();
				$primaryKeys = array();
				$implicitIndexes = array();
				$uniqueKeys = array();

                $indexes = array();

                if ($table->fields->field instanceof SimpleXMLElement) {
					foreach ($table->fields->field as $field) {
						$oField = new DatabaseField($field);
                        if ( $oField->isPrimaryKey() ) {
                            $primaryKeys[] = '`'.$oField->getName().'`';
                        }
                        if ( $oField->isIndex() ) {
                            $implicitIndexes[] = '`'.$oField->getName().'`';
                        }
                        if ( $oField->isUnique() ) {
                            $uniqueKeys[] = '`'.$oField->getName().'`';
                        }
						$fields[] = $oField;
					}
                }
                if ($table->indexes->index instanceof SimpleXMLElement) {
                    foreach ($table->indexes->index as $index) {
						$indexes[] = new DatabaseIndex($index, $table['name']);
                    }
                }
                if ($table->foreignkeys->foreignkey instanceof SimpleXMLElement) {
                    foreach ($table->foreignkeys->foreignkey as $foreignkey) {
						$foreignKeys[] = new DatabaseForeignKey($foreignkey, $table['name']);
                    }
                }
                if ($table->triggers->trigger instanceof SimpleXMLElement) {
                    foreach ($table->triggers->trigger as $trigger) {
						$triggers[] = new DatabaseTrigger($trigger, $table['name']);
                    }
                }
                if (count($fields)) {
					$tableExists = Db::sqlField(sprintf("SHOW TABLES LIKE '%s'", $table['name']));
					if ($tableExists) {
						$alterSql = '';
						$result = Db::sql(sprintf("DESCRIBE `%s`", $table['name']));
						$fieldNames = array();
                        while ( $rs = Db::loop($result) ) {
                            $fieldNames[] = $rs['Field'];
                        }
						foreach ($fields as $field) {
							if ($alterSql) $alterSql .= ', ';
							$fieldId = array_search($field->getName(), $fieldNames);
							$alterSql .= $fieldId !== false ? sprintf('CHANGE `%s`', $field->getName()) : 'ADD';
							$alterSql .= ' ' . $field->getSql();
						}
						if ($alterSql) {
							Db::sql(sprintf('ALTER TABLE `%s` %s', $table['name'], $alterSql));
							Db::sql(sprintf('ALTER TABLE `%s` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci, ENGINE = INNODB', $table['name']));
						}
					}
					else {
						$sql = sprintf('CREATE TABLE `%s` (', $table['name']);

                        $tmp = array();
						foreach ($fields as $field) {
                            $tmp[] = $field->getSql();
                        }

                        $sql .= implode(',',$tmp);

                        if ( count($primaryKeys) ) {
                            $sql .= sprintf(", PRIMARY KEY (%s)", implode(', ', $primaryKeys));
                        }
                        if ( count($implicitIndexes) ) {
                            $sql .= sprintf(", INDEX (%s)", implode(', ', $implicitIndexes));
                        }
                        if ( count($uniqueKeys) ) {
                            $sql .= sprintf(", UNIQUE (%s)", implode(', ', $uniqueKeys));
						}
						$sql .= ')  ENGINE = INNODB DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci';
						Db::sql($sql);
					}
				}
			}
        }

		foreach ($indexes as $index) {
			$sql = $index->getSql();
			if (!Db::sqlBool(sprintf("
				SELECT 1
				FROM information_schema.statistics
				WHERE table_name = '%s'
				AND index_name = '%s'",
				$index->getTableName(),
				$index->getName()
			))) {
				Db::sql($sql);
			}
		}

		foreach ($foreignKeys as $foreignkey) {
			// korábbi idegen kulcsok eltávolítása az adott oszlopokhoz
			$result = Db::sql(sprintf("
				SELECT constraint_name
				FROM information_schema.key_column_usage
				WHERE table_schema = '%s'
				AND table_name = '%s'
				AND column_name = '%s'
				AND referenced_table_name = '%s'
				AND referenced_column_name = '%s'",
				UniAdmin::app()->config['db']['database'],
				$foreignkey->getTableName(),
				$foreignkey->getColumns(),
				$foreignkey->getReferenceTable(),
				$foreignkey->getReferenceColumns()
			));
			while ($rs = Db::loop($result)) {
				Db::sql(sprintf("
					ALTER TABLE `%s` 
					DROP FOREIGN KEY `%s`",
					$foreignkey->getTableName(),
					$rs['constraint_name']
				));
			}

			$sql = $foreignkey->getSql();
			Db::sql($sql);
		}

        /*
			if (count($triggers)) {
				foreach ($triggers as $trigger) {
					$sql = $trigger->getSql();
					Db::sql($sql);
				}
		}
        */
	}
}
