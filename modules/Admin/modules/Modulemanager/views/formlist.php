<?php 

if(isset($_POST['generate'])) {
	foreach($this->tables as $table) {
	   if ($table != "generate") {
    		echo '<textarea style="width:640px;height:480px;">'.$table.'</textarea>';
	   }
	}
}


?>
<form method="post" action="<?php echo Url::link('modulemanager', 'form'); ?>">
<table class="data" cellspacing="0" cellpadding="0" width="400">
<thead>
	<tr><td colspan="2" class="data-title">Táblák listája</td></tr>
	<tr class="data-fields odd">
		<td>Tábla</td>
		<td width="90" align="center">Exportálás</td>
	</tr>
</thead>
<tbody>
<?php
$q = Db::toArray('SHOW TABLES');
foreach($q as $table) {
	echo '<tr id="'.$table.'">
		<td class="e_row">'.$table.'</td>
		<td class="e_row" align="center"><input id="c_'.$table.'" type="checkbox" name="'.$table.'" /></td>
	</tr>';
}
?>
<tr><td colspan="2" align="center"><input type="submit" name="generate" value="Generálás" /></td></tr>
</tbody>
</table>
</form>