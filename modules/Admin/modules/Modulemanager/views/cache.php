<?php 

if (isset($this->success)) {
	if ($this->success) {
		SysMessage::displaySuccess(t('A gyorsítótár ürítése megtörtént'));
	} else {
		SysMessage::displayError(t('A gyorsítótár ürítése sikertelen volt'));
	}
}

if (is_array($this->parameters->elements)) {
	foreach ($this->parameters->elements as $handlerId => $handler) { ?>
<form action="<?php echo Url::link('modulemanager', 'cache'); ?>" method="post">
	<fieldset>
		<legend><?php echo $handler['title']; ?></legend>
		<p>
			<?php echo $handler['description']; ?>
			&nbsp; <input type="submit" value="<?php echo t('Ürítés'); ?>" name="<?php echo $handlerId; ?>" />
		</p>
	</fieldset>
</form>
<?php 
	}
}
