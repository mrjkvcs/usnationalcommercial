<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'installed':
			SysMessage::displaySuccess(t('A telepítés sikeresen megtörtént'));
		break;
	case 'uninstalled':
			SysMessage::displaySuccess(t('A modul eltávolítása sikeresen megtörtént'));
		break;
}

if (count($this->modulesToInstall)) {
	?>
	<h1><?php echo t('Telepíthető modulok'); ?></h1>
	<form action="<?php echo Url::link('modulemanager', 'install'); ?>" method="post">
		<?php foreach ($this->modulesToInstall as $moduleId => $moduleName): ?>
		<input type="checkbox" name="module[<?php echo $moduleId; ?>]" id="module-<?php echo $moduleId; ?>" value="1" checked="checked" />
		<label for="module-<?php echo $moduleId; ?>"><?php echo $moduleName; ?></label><br />
		<?php endforeach; ?>
		<input type="submit" value="<?php echo t('Telepítés'); ?>" />
	</form>
	&nbsp;<br />
	<?php
}

$this->loadDataDisplay('module-list')->renderView();
