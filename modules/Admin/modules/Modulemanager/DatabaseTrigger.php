<?php

class DatabaseTrigger {
	public $tableName;
	public $name;
	public $time;
	public $event;
	public $body;

	public function __construct($xmlData, $tableName) {
		$this->tableName = $tableName;
		$this->body = $xmlData->body;
		if ($xmlData instanceof SimpleXMLElement)
			foreach ($xmlData->attributes() as $property => $value) {
				$methodName = 'set' . ucfirst(strtolower($property));
				if ($property && $value && method_exists($this, $methodName))
					call_user_func(array($this, $methodName), $value);
			}
	}

	public function setName($name) {
		$this->name = (string) $name;
		return $this;
	}

	public function setTime($time) {
		$this->time = (string) $time;
		return $this;
	}

	public function setEvent($event) {
		$this->event = (string) $event;
		return $this;
	}

	public function setBody($body) {
		$this->body = (string) $body;
		return $this;
	}

	public function getName() { return $this->name; }

	public function getTime() { return $this->time; }

	public function getEvent() { return $this->event; }

	public function getBody() { return $this->body; }

	public function isValid() { return $this->getName() && $this->getTime() && $this->getEvent() && $this->getBody(); }

	public function getSql() {
		if (!$this->isValid()) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('Wrong database trigger', 'installer', 3);
				UniAdmin::flushDebugInfos();
			}
			exit;
		}
		$sql = 'CREATE TRIGGER `'.$this->getName().'` '.strtoupper($this->getTime()).' '.strtoupper($this->getEvent()).' ON `'.$this->tableName.'` FOR EACH ROW BEGIN ';
		$sql .= $this->getBody().' ';
		$sql .= 'END';
		return $sql;
	}
}
