<?php

class DatabaseForeignKey {
	public $tableName;
	public $name;
	public $columns;
	public $referenceTable;
	public $referenceColumns;
	public $onUpdate = 'CASCADE';
	public $onDelete = 'CASCADE';

	public function __construct($xmlData, $tableName) {
		$this->tableName = $tableName;
		if ($xmlData instanceof SimpleXMLElement)
			foreach ($xmlData->attributes() as $property => $value) {
				$methodName = 'set' . ucfirst(strtolower($property));
				if ($property && $value && method_exists($this, $methodName))
					call_user_func(array($this, $methodName), $value);
			}
	}

	public function setName($name) {
		$this->name = (string) $name;
		return $this;
	}

	public function setColumns($columns) {
		$this->columns = (string) $columns;
		return $this;
	}

	public function setReferenceTable($referenceTable) {
		$this->referenceTable = (string) $referenceTable;
		return $this;
	}

	public function setReferenceColumns($referenceColumns) {
		$this->referenceColumns = (string) $referenceColumns;
		return $this;
	}

	public function setOnUpdate($onUpdate) {
		$this->onUpdate = (string) $onUpdate;
		return $this;
	}

	public function setOnDelete($onDelete) {
		$this->onDelete = (string) $onDelete;
		return $this;
	}

	public function getTableName() {
		return $this->tableName;
	}

	public function getName() {
        return $this->name;
    }

	public function getColumns() {
        return $this->columns;
    }

	public function getReferenceTable() {
        return $this->referenceTable;
    }

	public function getReferenceColumns() {
        return $this->referenceColumns;
    }

	public function getOnUpdate() {
        return $this->onUpdate;
    }

	public function getOnDelete() {
        return $this->onDelete;
    }

	public function isValid() {
        return $this->getColumns() && $this->getReferenceTable() && $this->getReferenceColumns();
    }

	public function getSql() {
		if ( !$this->isValid() ) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('Wrong database foreign key', 'installer', 3);
				UniAdmin::flushDebugInfos();
			}
			exit;
		}

		$sql = 'ALTER TABLE `'.$this->tableName.'` ADD FOREIGN KEY ';
		if ($this->getName()) $sql .= '`'.$this->getName().'` ';
		$sql .= '('.$this->columns.') REFERENCES `'.$this->getReferenceTable().'` ('.$this->getReferenceColumns().')';
		if ($this->getOnUpdate()) {
			$sql .= ' ON UPDATE '.strtoupper($this->getOnUpdate());
		}
		if ($this->getOnDelete()) {
			$sql .= ' ON DELETE '.strtoupper($this->getOnDelete());
		}
		return $sql;
	}
}
