<?php

class ModulemanagerController extends BaseAdminModule {
	const name = 'Modulkezelő';
	const version = '4.0';

	public $viewFile;

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('modulemanager.base');
	}

	public function menu($parameters) {
		$parameters->menus[] = array(
			'title' => t('Modulkezelő'),
			'module' => 'modulemanager',
			'type' => 'function',
			'submenus' => array(
				array('title' => 'Telepített modulok', 'module' => 'modulemanager', 'action' => ''),
				array('title' => 'Táblák exportálása', 'module' => 'modulemanager', 'action' => 'export'),
				array('title' => 'Űrlapok létrehozása', 'module' => 'modulemanager', 'action' => 'form'),
				array('title' => 'Gyorsítótár', 'module' => 'modulemanager', 'action' => 'cache'),
			),
		);
	}

	public function beforeAction() {
		UniAdmin::app()->path->addElement(t('Modulkezelő'), 'modulemanager');
	}

	public function actionDefault() {
		$this->viewFile = 'modulelist';
		UniAdmin::app()->path->addElement(t('Telepített modulok'), 'modulemanager');

		$this->modulesToInstall = array();

		UniAdmin::import('modules.Admin.modules.Filemanager.DirectoryList');
		$directoryList = new DirectoryList();
		$moduleList = $directoryList->getElements('modules/Admin/modules', false);
		foreach ($moduleList as $path => $moduleId) {
			$num = Db::sqlField(sprintf("SELECT COUNT(*) FROM UniadminModules WHERE id = '%s'",
				$moduleId));
			if (!$num) {
				$moduleToLoad = $moduleId . 'Controller';
				UniAdmin::import('modules.Admin.modules.' . $moduleId . '.' . $moduleToLoad);
				if (file_exists('modules/Admin/modules/' . $moduleId . '/' . $moduleToLoad . '.php')) {
					if (class_exists($moduleToLoad)) {
						$module = new $moduleToLoad();
						$this->modulesToInstall[$moduleId] = defined($moduleToLoad . '::name') ? constant($moduleToLoad . '::name') : $moduleId;
					}
				}
			}
		}
	}

    public function installModule($p_moduleId) {
        // az elnevezési és könyvtár struktúrára vonatkozó konvenciók szerint
        $moduleId = ucfirst(strtolower($p_moduleId));
		$moduleToLoad = $moduleId . 'Controller';
        UniAdmin::importAdmin($moduleId . '.' . $moduleToLoad);

        if ( !class_exists($moduleToLoad) ) {
            throw new Error('Source error missing Controller class');
        }

        // ezt a modult telepítették-e korábban?
		$installed = Db::sqlBool(sprintf("SELECT 1 FROM UniadminModules WHERE id = '%s'",$moduleId));

        if ( $installed ) {
            //a modul nevét frissítjük és kész
            Db::sql(sprintf("
                UPDATE UniadminModules
                SET name = '%s'
                WHERE id = '%s'",
                defined($moduleToLoad . '::name') ? constant($moduleToLoad . '::name') : $moduleId,
                $moduleId
            ));
            //nem történt telepítés

            if (DEBUG_MODE) {
                UniAdmin::addDebugInfo('Install module> module already installed <pre>'
                           . var_export($moduleId,true)."\n"
                           .'</pre>', 'installer',3,__FILE__,__LINE__);
            }
            $this->installExecuteInstaller($moduleId,true) ;
            return false;
        }

        if (DEBUG_MODE) {
            UniAdmin::addDebugInfo('Install module in progress <pre>'
                . var_export($moduleId,true)."\n"
                . var_export($moduleToLoad,true)."\n"
                .'</pre>', 'installer',3,__FILE__,__LINE__);
        }

		$module = new $moduleToLoad();

		// beszúrás a modulok közé
		Db::sql(sprintf("INSERT INTO UniadminModules (id, name) VALUES ('%s', '%s')",
			$moduleId, defined($moduleToLoad . '::name') ? constant($moduleToLoad . '::name') : $moduleId));

		// beszúrás a telepítő csoportjába
        if (
            !Db::sqlBool(sprintf("SELECT 1 FROM UniadminModuleAccess
                                    WHERE type = 'g' AND userId = %d AND moduleId = '%s'",
            UniAdmin::app()->user->getGroupId(), $moduleToLoad))
            ) {
				Db::sql(sprintf("INSERT INTO UniadminModuleAccess (type, userId, moduleId) VALUES ('g', %d, '%s')",
					UniAdmin::app()->user->getGroupId(), $moduleToLoad));
		}

		// ha vannak aljogosultságok, azok felvétele a telepítő csoportjához
		$permissions = $module->getAccessLevels();
		if (is_array($permissions)) {
			foreach (array_keys($permissions) as $permission) {
				$num = Db::sqlField(sprintf("
					SELECT COUNT(*)
					FROM UniadminModulePermissions
					WHERE type = 'g'
					AND userId = %d
					AND moduleId = '%s'
					AND permission = '%s'",
					UniAdmin::app()->user->getGroupId(),
					$moduleToLoad,
					$permission
				));
				if (!$num) {
					Db::sql(sprintf("
						INSERT INTO UniadminModulePermissions
						(type, userId, moduleId, permission)
						VALUES
						('g', %d, '%s', '%s')",
						UniAdmin::app()->user->getGroupId(),
						$moduleToLoad,
						$permission));
				}
			}
		}

        $this->installExecuteInstaller($moduleId,false) ;
	}

    public function installExecuteInstaller($moduleId,$reinstall=false) {
        $installerLocation = 'modules/Admin/modules/' . $moduleId . '/' . $moduleId . 'Installer.php';

		// ha van telepítő class, meghívja azt
        if ( file_exists($installerLocation) ) {
			$moduleInstaller = $moduleId . 'Installer';
            UniAdmin::importAdmin('Modulemanager.DatabaseSchema');
            UniAdmin::importAdmin($moduleId . '.' . $moduleInstaller);
            if ( class_exists($moduleInstaller) ) {
				$installer = new $moduleInstaller($moduleId);

                if ( $reinstall && method_exists($installer, 'uninstall') ) {
                    call_user_func(array($installer, 'uninstall'));
                }

                if ( method_exists($installer, 'install') ) {
					call_user_func(array($installer, 'install'));
				}
                else {
                    if (DEBUG_MODE) {
                        UniAdmin::addDebugInfo('Missing module installer method install at <pre>'
                                   . var_export($moduleInstaller,true)."\n"
                                   . var_export($installerLocation,true)."\n"
                                   .'</pre>', 'installer',3,__FILE__,__LINE__);
								}
							}
						}
            else {
                if (DEBUG_MODE) {
                    UniAdmin::addDebugInfo('Missing module installer class at <pre>'
                               . var_export('install',true)."\n"
                               . var_export($moduleInstaller,true)."\n"
                               . var_export($installerLocation,true)."\n"
                               .'</pre>', 'installer',3,__FILE__,__LINE__);
					}
				}
			}
        else {
            if (DEBUG_MODE) {
                UniAdmin::addDebugInfo('Missing module installer at <pre>'
                           . var_export($installerLocation,true)."\n"
                           .'</pre>', 'installer',3,__FILE__,__LINE__);
			}
		}
    }


	public function actionInstall() {
		if (is_array($_POST['module'])) {
			$count = 0;
			foreach ($_POST['module'] as $moduleId => $checked) {
				if ($checked) {
                    Db::trBegin();
                    Db::sql('SET FOREIGN_KEY_CHECKS = 0');
                    if (DEBUG_MODE) {
                        UniAdmin::addDebugInfo('Start installing ' . $moduleId, 'installer', 1, __FILE__, __LINE__);
                    }
					$success = $this->installModule($moduleId);
                    $count += $success ? 1 : 0;
                    Db::sql('SET FOREIGN_KEY_CHECKS = 1');
                    Db::trEnd(true);
				}
			}

			UniAdmin::app()->log->setType('admin')->write('installed module: ' . $moduleId);

			UniAdmin::import('modules.Admin.classes.ModuleMenu');
			ModuleMenu::clearCache();
			$this->clearEventHandlerCache();

			if ($count) {
				$this->redirect('modulemanager', '', LANG, array('msg' => 'installed'));
			}
		}

		$this->redirect('modulemanager');
	}

	public function actionReinstall($moduleId, $goBack = true) {
		$this->installModule($moduleId);

		UniAdmin::app()->log->setType('admin')->write('reinstalled module: ' . $moduleId);

		if ($goBack) {
			UniAdmin::import('modules.Admin.classes.ModuleMenu');
			ModuleMenu::clearCache();
			$this->clearEventHandlerCache();

			$this->redirect('modulemanager', '', LANG, array('msg' => 'installed'));
		}
	}

	public function actionReinstallAll() {
		$result = Db::sql("SELECT id FROM UniadminModules");
		while ($rs = Db::loop($result)) {
			$this->actionReinstall($rs['id'], false);
		}

		UniAdmin::import('modules.Admin.classes.ModuleMenu');
		ModuleMenu::clearCache();
		$this->clearEventHandlerCache();

		$this->redirect('modulemanager', '', LANG, array('msg' => 'installed'));
	}

	public function actionUninstall() {
		$moduleId = UniAdmin::app()->route->getParameter('uninstall', 1);
		if ($moduleId) {
			Db::sql(sprintf("DELETE FROM UniadminModules WHERE id = '%s'", $moduleId));
			Db::sql(sprintf("DELETE FROM UniadminModuleAccess WHERE moduleId = '%s'", $moduleId . 'Controller'));
			Db::sql(sprintf("DELETE FROM UniadminSettings WHERE moduleId = '%d'", $moduleId));

			// ha van telepítő class, meghívja azt
			if (file_exists('modules/Admin/modules/' . $moduleId . '/' . $moduleId . 'Installer.php')) {
				$moduleInstaller = $moduleId . 'Installer';
				UniAdmin::import('modules.Admin.modules.' . $moduleId . '.' . $moduleInstaller);
				UniAdmin::import('modules.Admin.modules.Modulemanager.DatabaseSchema');
				if (class_exists($moduleInstaller)) {
					$installer = new $moduleInstaller();
					if (method_exists($installer, 'unInstall')) {
						call_user_func(array($installer, 'unInstall'));
					}
				}
			}

			UniAdmin::app()->log->setType('admin')->write('uninstalled module: ' . $moduleId);

			UniAdmin::import('modules.Admin.classes.ModuleMenu');
			ModuleMenu::clearCache();
			$this->clearEventHandlerCache();

			$this->redirect('modulemanager', '', LANG, array('msg' => 'uninstalled'));
		} else {
			$this->redirect('modulemanager');
		}
	}

	public function actionExport() {
		$this->viewFile = 'exportlist';
		UniAdmin::app()->path->addElement(t('Táblák exportálása'), 'modulemanager', 'export');
		UniAdmin::app()->page->addScript('modules.Admin.modules.Modulemanager.scripts.modulemanager');

		if (isset($_POST['tExportme'])) {
			$this->tables = array();
			$output = '<?xml version="1.0" encoding="utf-8"?>'."\n";
			$output .= '<tables>'."\n";
			foreach($_POST as $table => $id) {
				if($table != 'tExportme') {
					$output .= "\t".'<table name="'.$table.'">'."\n";

					/*Fields*/
					$output .= "\t\t".'<fields>'."\n";
					$q = Db::sql('DESCRIBE '.$table);
					while($r = Db::loop($q)) {
						$typeuns = explode(' ', $r['Type']);
						$output .= "\t\t\t".'<field name="'.$r['Field'].'" type="'.($typeuns[0]).'"'.(isset($typeuns[1]) ? ' unsigned="true"' : '').($r['Default'] ? ' default="'.$r['Default'].'"' : '').($r['Null'] == 'YES' ? ' null="true"' : '').($r['Key'] == 'PRI' ? ' primarykey="true"' : '').($r['Extra'] == 'auto_increment' ? ' autoincrement="true"' : '').' />'."\n";
					}
					$output .= "\t\t".'</fields>'."\n";

					/* Indexes */
					$indexes = array();
					$uniques = Db::sql('SELECT *
					FROM information_schema.STATISTICS
					WHERE TABLE_NAME = "'.$table.'"
					AND INDEX_NAME != "PRIMARY"');
					while($rr = Db::loop($uniques)) {
						if ($rr['NON_UNIQUE']) {
							$indexes[$rr['INDEX_NAME']]['non_unique'][] = $rr['COLUMN_NAME'];
						} else {
							$indexes[$rr['INDEX_NAME']]['unique'][] = $rr['COLUMN_NAME'];
						}
					}

					if(count($indexes)) {
						$output .= "\t\t".'<indexes>'."\n";
						foreach($indexes as $index => $ival) {
							if (isset($ival['unique'])) {
								$output .= "\t\t\t".'<index name="'.$index.'" type="unique" columns="'.implode(', ', $ival['unique']).'" />'."\n";
							} else {
								$output .= "\t\t\t".'<index name="'.$index.'" columns="'.implode(', ', $ival['non_unique']).'" />'."\n";
							}
						}
						$output .= "\t\t".'</indexes>'."\n";
					}

					/* Foreign keys */
					$foreigns = array();
					$v = Db::sql('SELECT * FROM information_schema.KEY_COLUMN_USAGE WHERE TABLE_NAME = "'.$table.'"');
					while($r = Db::loop($v)) {
						if($r['CONSTRAINT_NAME'] != 'PRIMARY' && !is_null($r['REFERENCED_TABLE_NAME'])) {
							$foreigns[] = "\t\t\t".'<foreignkey name="'.$r['CONSTRAINT_NAME'].'" columns="'.$r['COLUMN_NAME'].'" referencetable="'.$r['REFERENCED_TABLE_NAME'].'" referencecolumns="'.$r['REFERENCED_COLUMN_NAME'].'" />'."\n";
						}
					}

					if(count($foreigns)) {
						$output .= "\t\t".'<foreignkeys>'."\n";
						foreach ($foreigns as $foreign) {
							$output .= $foreign;
						}
						$output .= "\t\t".'</foreignkeys>'."\n";
					}

					/* Triggers */
					$triggers = Db::sql('SELECT * FROM information_schema.TRIGGERS');
					while($tr = Db::loop($triggers)) {
						print_r($tr);
					}

					$output .= "\t".'</table>'."\n";
				}
			}
			$output .= '</tables>';
			$this->tables[] = $output;
		}
	}

	public function actionForm() {
		$this->viewFile = 'formlist';
		UniAdmin::app()->path->addElement(t('Űrlapok létrehozása'), 'modulemanager', 'form');
		UniAdmin::app()->page->addScript('modules.Admin.modules.Modulemanager.scripts.modulemanager');

		$numArray = array('smallint', 'int', 'tinyint', 'bigint');

		if (isset($_POST['generate'])) {
			$this->tables = array();
			foreach ($_POST as $table => $id) {
				if ($table != 'generate') {
					$output = '<?xml version="1.0" encoding="utf-8"?>'."\n";
					$output .= '<form table="' . $table . '" title="' . $table . '">'."\n";

					$q = Db::sql('DESCRIBE ' . $table);
					while ($r = Db::loop($q)) {
						$typeuns = explode(' ', $r['Type']);
						$pattern = "/[\s]*[(][\s]*[)]*/";
						$matches = preg_split($pattern, $typeuns[0]);

						$output .= "\t" . '<field ';

						if ($r['Key'] == 'PRI') {
							if (in_array($matches[0], $numArray)){
								$output .= 'type="Hidden" field="' . $r['Field'] . '" key="key" ';
							} else {
								$output .= 'type="InputText[20]" field="' . $r['Field'] . '" key="key" ';
							}
						} else {
							switch ($matches[0]){
								case 'smallint':
									$output .= 'type="InputText[10]" ';
								break;

								case 'int':
									$output .= 'type="InputText[30]" ';
								break;

								case 'varchar':
									$output .= 'type="InputText[40]" ';
								break;

								case 'tinyint':
									$output .= 'type="Checkbox" ';
								break;

								case 'text':
									$output .= 'type="Textarea[50,4]" ';
								break;

								case 'datetime':
									$output .= 'type="DateTime" ';
								break;

								case 'date':
									$output .= 'type="DatePicker" ';
								break;

								default:
									$output .= 'type="InputText[30]" ';
							}
							$output .= 'field="' . $r['Field'] . '" ';
							if ($matches[0] == 'tinyint') {
								$output .= 'text="' . $r['Field'] . '" ';
							} else {
								$output .= 'label="' . $r['Field'] . '" ';
							}
						}

						if ($r['Key'] == 'UNI') {
							$output .= 'unique="unique" ';
						}

						if ($r['Null'] != 'NO' || $r['Key'] == 'PRI' || $matches[0] == 'tinyint') {
							$output .= 'required="false" ';
						}

						$output.= "/>\n";
					}
					$output .= '</form>';
					$this->tables[] = $output;
				}
			}
		}
	}

	public function actionCache() {
		$this->viewFile = 'cache';
		UniAdmin::app()->path->addElement(t('Gyorsítótár'), 'modulemanager', 'cache');

		$this->parameters = new EventParameter();
		$this->parameters->elements = array();

		$this->parameters->elements['uniadminevents'] = array(
			'title' => t('Keretrendszer eseménykezelője'),
			'description' => t('Az adminisztrációs rendszer személyre szabott menüje'),
			'callback' => function($key){
				if (UniAdmin::app()->cache->isEnabled()) {
					UniAdmin::app()->cache->delete('uniAdminEventHandlers');
					return true;
				}
				return false;
			},
		);

		$this->parameters->elements['adminmenu'] = array(
			'title' => t('Admin modulmenü'),
			'description' => t('Az adminisztrációs rendszer személyre szabott menüje'),
			'callback' => function($key){
				if (UniAdmin::app()->cache->isEnabled()) {
					UniAdmin::app()->cache->delete('uniadminmenu-' . UniAdmin::app()->user->getId());
					return true;
				}
				return false;
			},
		);

		$this->dispatchEvent('getCacheHandlers', $this->parameters);

		if (!empty($_POST)) {
			foreach ($_POST as $key => $value) {
				if (isset($this->parameters->elements[$key])
					&& is_callable($this->parameters->elements[$key]['callback'])
				) {
					$this->success = call_user_func(
						$this->parameters->elements[$key]['callback'],
						$key
					);
					break;
				}
			}
		}
	}

	public function clearEventHandlerCache() {
		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->delete('uniAdminEventHandlers');
		}
	}
}
