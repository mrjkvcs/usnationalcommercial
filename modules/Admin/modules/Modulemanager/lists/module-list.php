<?php

$moduleList = new DataDisplay(array(
	'sql' => "
		SELECT `id`, `name`
		FROM UniadminModules",
	'defaultOrder' => 'name',
	'paging' => false,
	'fullWidth' => false,
	'filter' => false,
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'id' => array(
			'title' => t('column-id'),
		),
		'name' => array(
			'title' => t('column-name'),
		),
	),
	'contextMenu' => array(
		array(
			'action' => array('reinstall', '[id]'),
			'text' => t('Újratelepítés'),
			'class' => 'setup',
		),
		array(
			'action' => array('reinstallall', '[id]'),
			'text' => t('Összes modul újratelepítése'),
			'class' => 'setup',
		),
		array(
			'action' => array('uninstall', '[id]'),
			'text' => t('Eltávolítás...'),
			'class' => 'delete separator confirm',
		),
	),
	'title' => t('table-modules'),
	'baseModule' => 'modulemanager',
));

return $moduleList;
