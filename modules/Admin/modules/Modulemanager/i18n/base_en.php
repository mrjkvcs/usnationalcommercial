<?php

$dictionary['column-id'] = 'identifiery';
$dictionary['column-name'] = 'name';
$dictionary['table-modules'] = 'Installed modules';

$dictionary['A telepítés sikeresen megtörtént.'] = 'The module has been installed successfully.';
$dictionary['A modul eltávolítása sikeresen megtörtént.'] = 'The module has been uninstalled successfully.';

$dictionary['Modulkezelő'] = 'Module manager';
$dictionary['Modulok listája'] = 'Modules list';
$dictionary['Táblák exportálása'] = 'Export tables';
$dictionary['Telepített modulok'] = 'Installed modules';
$dictionary['Telepíthető modulok'] = 'Installable modules';
$dictionary['Telepítés'] = 'Install';
