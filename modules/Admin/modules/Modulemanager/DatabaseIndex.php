<?php

class DatabaseIndex {
	public $tableName;
	public $name;
	public $type;
	public $columns;

	public function __construct($xmlData, $tableName) {
		$this->tableName = $tableName;
		if ($xmlData instanceof SimpleXMLElement)
			foreach ($xmlData->attributes() as $property => $value) {
				$methodName = 'set' . ucfirst(strtolower($property));
				if ($property && $value && method_exists($this, $methodName))
					call_user_func(array($this, $methodName), $value);
			}
	}

	public function setName($name) {
		$this->name = (string) $name;
		return $this;
	}

	public function setType($type) {
		$this->type = (string) $type;
		return $this;
	}

	public function setColumns($columns) {
		$this->columns = (string) $columns;
		return $this;
	}

	public function getTableName() {
		return $this->tableName;
	}

	public function getName() {
		return $this->name;
	}

	public function getType() {
		return $this->type;
	}

	public function getColumns() {
		return $this->columns;
	}

	public function isValid() {
		return $this->getName() && $this->getColumns();
	}

	public function getSql() {
		if (!$this->isValid()) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('Wrong database index', 'installer', 3);
				UniAdmin::flushDebugInfos();
			}
			exit;
		}
		$sql = 'CREATE ';
		if ($this->getType()) {
			$sql .= strtoupper($this->getType()).' ';
		}
		$sql .= 'INDEX `'.$this->getName().'` ON `'.$this->tableName.'` (`'.$this->getColumns().'`)';
		return $sql;
	}
}
