<?php

class DatabaseField {
	public $name;
	public $type;
	public $defaultValue;
	public $isUnsigned = false;
	public $isAutoIncrement = false;
	public $isNull = false;
	public $isPrimaryKey = false;
	public $isUnique = false;
	public $isIndex = false;

	public function __construct($xmlData) {
			foreach ($xmlData->attributes() as $property => $value) {
				$methodName = 'set' . ucfirst(strtolower($property));
				if ($property && $value && method_exists($this, $methodName))
					call_user_func(array($this, $methodName), $value);
			}
	}

	public function setName($name) {
		$this->name = (string) $name;
		return $this;
	}

	public function setType($type) {
		$this->type = (string) $type;
		return $this;
	}

	public function setDefault($value) { $this->setDefaultValue($value); }

	public function setDefaultValue($value) {
		$this->defaultValue = (string) $value;
		return $this;
	}

	public function setUnsigned($state = true) {
		$this->isUnsigned = (bool) $state;
		return $this;
	}

	public function setAutoIncrement($state = true) {
		$this->isAutoIncrement = (bool) $state;
		return $this;
	}

	public function setNull($state = true) {
		$this->isNull = (bool) $state;
		return $this;
	}

	public function setPrimaryKey($state = true) {
		$this->isPrimaryKey = (bool) $state;
		return $this;
	}

	public function setUnique($state = true) {
		$this->isUnique = (bool) $state;
		return $this;
	}

	public function setIndex($state = true) {
		$this->isIndex = (bool) $state;
		return $this;
	}

	public function getName() { return $this->name; }

	public function getType() { return $this->type; }

	public function getDefaultValue() { return $this->defaultValue; }

	public function isUnsigned() { return $this->isUnsigned; }

	public function isAutoIncrement() { return $this->isAutoIncrement; }

	public function isPrimaryKey() { return $this->isPrimaryKey; }

	public function isUnique() { return $this->isUnique; }

	public function isIndex() { return $this->isIndex; }

	public function isNull() { return $this->isNull; }

	public function isValid() { return $this->getName() && $this->getType(); }

	public function getSql() {
		if (!$this->isValid()) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('Wrong database field', 'installer', 3);
				UniAdmin::flushDebugInfos();
			}
			exit;
		}
		$sql = '`'.$this->getName().'`';
		$sql .= ' '.$this->getType();
		if ($this->isUnsigned()) $sql .= ' UNSIGNED';
		if ($this->isAutoIncrement()) $sql .= ' AUTO_INCREMENT';
		$sql .= ($this->isNull() ? '' : ' NOT' ).' NULL';
		if ($this->getDefaultValue()) {
			if (in_array($this->getDefaultValue(), array('CURRENT_TIMESTAMP', 'NULL'))) {
				// parancsokat nem kell idézőjelbe tenni
				$sql .= sprintf(" DEFAULT %s", $this->getDefaultValue());
			} else {
				$sql .= sprintf(" DEFAULT '%s'", $this->getDefaultValue());
			}
		}
		return $sql;
	}
}
