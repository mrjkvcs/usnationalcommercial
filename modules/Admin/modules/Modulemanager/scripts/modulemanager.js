jQuery(document).ready(function($){
	$('.e_row').click(function(event){
		if ($(event.target).attr('type') == 'checkbox') {
			return true;
		}
		var field = $(this).closest('tr').attr('id');
		if ($('#c_'+field).attr('checked')) {
			$('#c_'+field).attr('checked', false);
		} else {
			$('#c_'+field).attr('checked', true);
		}
	});
});