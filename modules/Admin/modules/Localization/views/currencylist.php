<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			echo SysMessage::displaySuccess(t('message-currency-added'));
		break;
	case 'saved':
			echo SysMessage::displaySuccess(t('message-currency-saved'));
		break;
	case 'deleted':
			echo SysMessage::displaySuccess(t('message-currency-deleted'));
		break;
}

$this->loadDataDisplay('currency-list')->renderView();
