<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			echo SysMessage::displaySuccess(t('message-country-added'));
		break;
	case 'saved':
			echo SysMessage::displaySuccess(t('message-country-saved'));
		break;
	case 'deleted':
			echo SysMessage::displaySuccess(t('message-country-deleted'));
		break;
}

$this->loadDataDisplay('country-list')->renderView();
