<div id="date-format-examples">
	<p class="info"><?php echo t('format-hints'); ?></p>
	<table class="data">
		<thead>
			<tr class="data-fields">
				<td><?php echo t('value'); ?></td>
				<td><?php echo t('meaning'); ?></td>
				<td><?php echo t('sample'); ?></td>
			</tr>
		</thead>
		<tbody>
			<?php 
			$formats = array('a', 'A', 'd', 'e', 'b', 'B', 'm', 'y', 'Y');
			foreach ($formats as $format): ?>
			<tr>
				<td>%<?php echo $format; ?></td>
				<td><?php echo t('info-' . $format); ?></td>
				<td><?php echo strftime('%' . $format); ?></td>
			</tr>
			<?php endforeach; ?>
		</tbody>
	</table>
</div>