<?php

if (isset($_GET['msg'])) switch ($_GET['msg']) {
	case 'ok':
			SysMessage::displaySuccess(t('message-language-added'));
		break;
	case 'saved':
			SysMessage::displaySuccess(t('message-language-saved'));
		break;
	case 'deleted':
			SysMessage::displaySuccess(t('message-language-deleted'));
		break;
}

$this->loadDataDisplay('language-list')->renderView();
