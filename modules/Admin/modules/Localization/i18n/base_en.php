<?php

$dictionary['localization'] = 'Localization';

$dictionary['edit-countries'] = 'Manage countries';
$dictionary['edit-currencies'] = 'Manage currencies';
$dictionary['edit-languages'] = 'Manage languages';
$dictionary['allowed-countries'] = 'Allowed countries';
$dictionary['add-new-country'] = 'Add new country';
$dictionary['currencies'] = 'Accepted currencies';
$dictionary['add-new-currency'] = 'Add new currency';
$dictionary['languages'] = 'Foreign languages';
$dictionary['add-new-language'] = 'Add new language';

$dictionary['form-country'] = 'Country details';
$dictionary['label-name'] = 'Name:';
$dictionary['label-id'] = 'Identifier:';
$dictionary['label-currency'] = 'Sign:';
$dictionary['tooltip-currency'] = 'Users from this country will see the prices in this currency';
$dictionary['allow-registration'] = 'Allow registration from here';
$dictionary['allow-delivery'] = 'Allow delivery to here';

$dictionary['form-currency'] = 'Currency details';
$dictionary['label-decimals'] = 'Number of decimals:';
$dictionary['label-decimal-point'] = 'Decimal point:';
$dictionary['label-thousands-separator'] = 'Thousands separator:';
$dictionary['label-rate'] = 'Rate:';
$dictionary['tooltip-rate'] = 'Relative to the default currency';
$dictionary['default'] = 'Default';

$dictionary['form-language'] = 'Language details';
$dictionary['label-default-currency'] = 'Default currency:';
$dictionary['label-default-country'] = 'Default country:';
$dictionary['label-locale'] = 'Locale code:';
$dictionary['label-short-date-format'] = 'Numeric date format:';
$dictionary['label-short-date-preview'] = 'Preview:';
$dictionary['label-long-date-format'] = 'Textual date format:';
$dictionary['label-long-date-preview'] = 'Preview:';

$dictionary['column-id'] = 'identifier';
$dictionary['column-name'] = 'name';
$dictionary['column-allow-registration'] = 'allow registration';
$dictionary['column-allow-delivery'] = 'allow delivery';
$dictionary['table-country'] = 'Allowed countries';

$dictionary['column-language'] = 'name';
$dictionary['column-default-currency'] = 'default currency';
$dictionary['column-default-country'] = 'default country';
$dictionary['column-date-format'] = 'date format';
$dictionary['column-is-default'] = 'is default';
$dictionary['table-languages'] = 'Language list';

$dictionary['column-currency'] = 'currency';
$dictionary['column-format'] = 'format';
$dictionary['column-rate'] = 'rate';
$dictionary['table-currencies'] = 'Currency list';

$dictionary['yes'] = 'yes';
$dictionary['no'] = 'no';

$dictionary['message-country-added'] = 'Country added successfully';
$dictionary['message-country-saved'] = 'Country saved successfully';
$dictionary['message-country-deleted'] = 'Country deleted successfully';
$dictionary['message-currency-added'] = 'Currency added successfully';
$dictionary['message-currency-saved'] = 'Currency saved successfully';
$dictionary['message-currency-deleted'] = 'Currency deleted successfully';
$dictionary['message-language-added'] = 'Language added successfully';
$dictionary['message-language-saved'] = 'Language saved successfully';
$dictionary['message-language-deleted'] = 'Language deleted successfully';

$dictionary['format-hints'] = 'The following values are available for date formatting:';
$dictionary['value'] = 'value';
$dictionary['meaning'] = 'meaning';
$dictionary['sample'] = 'sample';
$dictionary['info-a'] = 'Abbreviated textual representation of the day';
$dictionary['info-A'] = 'Full textual representation of the day';
$dictionary['info-d'] = 'Two-digit day of the month with leading zeros';
$dictionary['info-e'] = 'Day of the month without leading zeros';
$dictionary['info-b'] = 'Abbreviated month name';
$dictionary['info-B'] = 'Full month name';
$dictionary['info-m'] = 'Two digit number of the month with leading zeros';
$dictionary['info-y'] = 'Two digit representation of the year';
$dictionary['info-Y'] = 'Four digit representation for the year';
