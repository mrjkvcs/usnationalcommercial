<?php

$dictionary['localization'] = 'Területi beállítások';

$dictionary['edit-countries'] = 'Országok kezelése';
$dictionary['edit-currencies'] = 'Pénznemek kezelése';
$dictionary['edit-languages'] = 'Nyelvek kezelése';
$dictionary['allowed-countries'] = 'Felvett országok';
$dictionary['add-new-country'] = 'Új ország felvétele';
$dictionary['currencies'] = 'Felvett pénznemek';
$dictionary['add-new-currency'] = 'Új pénznem felvétele';
$dictionary['languages'] = 'Felvett nyelvek';
$dictionary['add-new-language'] = 'Új nyelv felvétele';

$dictionary['form-country'] = 'Ország adatai';
$dictionary['label-name'] = 'Megnevezés:';
$dictionary['label-id'] = 'Azonosító:';
$dictionary['label-currency'] = 'Pénznem:';
$dictionary['tooltip-currency'] = 'Az ország felhasználói ebben a valutában látják az árakat';
$dictionary['allow-registration'] = 'Regisztráció engedélyezett innen';
$dictionary['allow-delivery'] = 'Kiszállítás engedélyezett ide';

$dictionary['form-currency'] = 'Pénznem adatai';
$dictionary['label-decimals'] = 'Tizedesjegyek száma:';
$dictionary['label-decimal-point'] = 'Tizedes elválasztó:';
$dictionary['label-thousands-separator'] = 'Ezres elválasztó:';
$dictionary['label-rate'] = 'Árfolyam:';
$dictionary['tooltip-rate'] = 'Az alapértelmezett valutához viszonyítva';
$dictionary['default'] = 'Alapértelmezett';

$dictionary['form-language'] = 'Nyelv adatai';
$dictionary['label-default-currency'] = 'Alapértelmezett pénznem:';
$dictionary['label-default-country'] = 'Alapértelmezett ország:';
$dictionary['label-locale'] = 'Nyelvkód:';
$dictionary['label-short-date-format'] = 'Numerikus dátum formátum:';
$dictionary['label-short-date-preview'] = 'Előnézet:';
$dictionary['label-long-date-format'] = 'Szöveges dátum formátum:';
$dictionary['label-long-date-preview'] = 'Előnézet:';

$dictionary['column-id'] = 'azonosító';
$dictionary['column-name'] = 'megnevezés';
$dictionary['column-allow-registration'] = 'regisztráció engedélyezve';
$dictionary['column-allow-delivery'] = 'kiszállítás engedélyezve';
$dictionary['table-country'] = 'Engedélyezett országok';

$dictionary['column-language'] = 'megnevezés';
$dictionary['column-default-currency'] = 'alapértelmezett pénznem';
$dictionary['column-default-country'] = 'alapértelmezett ország';
$dictionary['column-date-format'] = 'dátum formátum';
$dictionary['column-is-default'] = 'alapértelmezett';
$dictionary['table-languages'] = 'Nyelvek listája';

$dictionary['column-currency'] = 'pénznem';
$dictionary['column-format'] = 'formátum';
$dictionary['column-rate'] = 'árfolyam';
$dictionary['table-currencies'] = 'Pénznemek';

$dictionary['yes'] = 'igen';
$dictionary['no'] = 'nem';

$dictionary['message-country-added'] = 'Az ország felvétele sikeresen megtörtént';
$dictionary['message-country-saved'] = 'Az ország mentése sikeresen megtörtént';
$dictionary['message-country-deleted'] = 'Az ország törlése sikeresen megtörtént';
$dictionary['message-currency-added'] = 'A pénznem felvétele sikeresen megtörtént';
$dictionary['message-currency-saved'] = 'A pénznem mentése sikeresen megtörtént';
$dictionary['message-currency-deleted'] = 'A pénznem törlése sikeresen megtörtént';
$dictionary['message-language-added'] = 'A nyelv felvétele sikeresen megtörtént';
$dictionary['message-language-saved'] = 'A nyelv mentése sikeresen megtörtént';
$dictionary['message-language-deleted'] = 'A nyelv törlése sikeresen megtörtént';

$dictionary['format-hints'] = 'A dátumok formátumának megadásakor az alábbi értékek közül lehet választani:';
$dictionary['value'] = 'érték';
$dictionary['meaning'] = 'jelentés';
$dictionary['sample'] = 'minta';
$dictionary['info-a'] = 'A hét napja szöveggel, rövidítve';
$dictionary['info-A'] = 'A hét napja szöveggel';
$dictionary['info-d'] = 'A hónap napja, számmal és vezető nullával';
$dictionary['info-e'] = 'A hónap napja, számmal';
$dictionary['info-b'] = 'Hónap szöveggel, rövidítve';
$dictionary['info-B'] = 'Hónap szöveggel';
$dictionary['info-m'] = 'Hónap számmal és vezető nullával';
$dictionary['info-y'] = 'Évszám két karakteren';
$dictionary['info-Y'] = 'Évszám négy karakteren';
