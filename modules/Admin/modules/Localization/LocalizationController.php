<?php

class LocalizationController extends BaseAdminModule {
	const name = 'Területi beállítások';
	const version = '1.0';

	public $viewFile;

	public function __construct() {
		parent::__construct();

		$this->loadDictionary('localization.base');

		$this->accessLevel = new ModuleAccessLevel();
		$permission = new ModulePermission('editCountries', t('edit-countries'));
		$permission->setAllowedActions('default', 'insert', 'update', 'delete');
		$this->accessLevel->addPermission($permission);
		$permission = new ModulePermission('editCurrencies', t('edit-currencies'));
		$permission->setAllowedActions('currency', 'currencyinsert', 'currencyupdate', 'currencydelete');
		$this->accessLevel->addPermission($permission);
		$permission = new ModulePermission('editLanguages', t('edit-languages'));
		$permission->setAllowedActions('language', 'languageinsert', 'languageupdate', 'languagedelete', 'loaddateformats', 'datepreview');
		$this->accessLevel->addPermission($permission);

		$this->permissions = $this->getPermissions();
	}

	public function menu($parameters) {
		$submenus = array();
		if (in_array('editCountries', $this->permissions)) {
			$submenus[] = array('title' => t('allowed-countries'), 'module' => 'localization', 'action' => '');
			$submenus[] = array('title' => t('add-new-country'), 'module' => 'localization', 'action' => 'insert');
		}
		if (in_array('editCountries', $this->permissions) && in_array('editCurrencies', $this->permissions)) {
			$submenus[] = array();
		}
		if (in_array('editCurrencies', $this->permissions)) {
			$submenus[] = array('title' => t('currencies'), 'module' => 'localization', 'action' => 'currency');
			$submenus[] = array('title' => t('add-new-currency'), 'module' => 'localization', 'action' => 'currencyinsert');
		}
		if (
			(in_array('editCurrencies', $this->permissions)
				&& in_array('editLanguages', $this->permissions)
			)
			||
			(in_array('editCountries', $this->permissions)
				&& !in_array('editCurrencies', $this->permissions)
				&& in_array('editLanguages', $this->permissions)
			)
		) {
			$submenus[] = array();
		}
		if (in_array('editLanguages', $this->permissions)) {
			$submenus[] = array('title' => t('languages'), 'module' => 'localization', 'action' => 'language');
			$submenus[] = array('title' => t('add-new-language'), 'module' => 'localization', 'action' => 'languageinsert');
		}
		$parameters->menus[] = array(
			'title' => t('localization'),
			'module' => 'localization',
			'action' => '',
			'submenus' => $submenus,
			'type' => 'settings'
		);
	}

	public function beforeAction() {
		UniAdmin::app()->path->addElement(t('localization'), 'localization');
	}

	public function actionDefault() {
		$this->viewFile = 'countrylist';
		UniAdmin::app()->path->addElement(t('allowed-countries'), 'localization');
	}

	public function actionInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('add-new-country'), 'localization', 'insert');

		$this->countryEdit();
	}

	public function actionUpdate() {
		$this->viewFile = 'edit';
		$this->countryEdit();
	}

	public function actionDelete() {
		$this->checkCountryId();

		Db::sql(sprintf("DELETE FROM Country WHERE id = '%s'", $this->countryId));

		UniAdmin::app()->log->setType('admin')->write('deleted country #' . $this->countryId);

		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->delete('uniAdminCountries');
		}

		$this->redirect('localization', '', LANG, array('msg' => 'deleted'));
	}

	public function actionCurrency() {
		$this->viewFile = 'currencylist';
		UniAdmin::app()->path->addElement(t('currencies'), 'localization', 'currency');
	}

	public function actionCurrencyInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('add-new-currency'), 'localization', 'currencyinsert');

		$this->currencyEdit();
	}

	public function actionCurrencyUpdate() {
		$this->viewFile = 'edit';
		$this->currencyEdit();
	}

	public function actionCurrencyDelete() {
		$this->checkCurrencyId();

		Db::sql(sprintf("DELETE FROM UniadminCurrency WHERE id = '%s'", $this->currencyId));

		UniAdmin::app()->log->setType('admin')->write('deleted currency: ' . $this->currencyId);

		$this->redirect('localization', 'currency', LANG, array('msg' => 'deleted'));
	}

	public function actionLanguage() {
		$this->viewFile = 'languagelist';
		UniAdmin::app()->path->addElement(t('languages'), 'localization', 'language');
	}

	public function actionLanguageInsert() {
		$this->viewFile = 'edit';
		UniAdmin::app()->path->addElement(t('add-new-language'), 'localization', 'languageinsert');

		$this->languageEdit();
	}

	public function actionLanguageUpdate() {
		$this->viewFile = 'edit';
		$this->languageEdit();
	}

	public function actionLanguageDelete() {
		$this->checkLanguageId();

		Db::sql(sprintf("DELETE FROM UniadminLanguages WHERE id = '%s'", $this->languageId));

		if (UniAdmin::app()->cache->isEnabled()) {
			UniAdmin::app()->cache->delete('uniAdminLanguages');
		}

		UniAdmin::app()->log->setType('admin')->write('deleted language: ' . $this->languageId);

		$this->redirect('localization', 'language', LANG, array('msg' => 'deleted'));
	}

	public function actionLoadDateFormats() {
		$locale = isset($_POST['locale']) ? $_POST['locale'] : '';
		if ($locale) {
			setlocale(LC_ALL, $locale);
			echo $this->loadView('localization.date-formats');
		}
		exit;
	}

	public function actionDatePreview() {
		$locale = isset($_POST['locale']) ? $_POST['locale'] : '';
		$format = isset($_POST['format']) ? $_POST['format'] : '';
		if ($locale && $format) {
			setlocale(LC_ALL, $locale);
			echo '<p>' . strftime($format) . '</p>';
		}
		exit;
	}

	protected function checkCountryId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->countryId = isset($route[2]) ? $route[2] : 0;
		if (!$this->countryId) $this->redirect('localization');
	}

	protected function checkCurrencyId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->currencyId = isset($route[2]) ? $route[2] : 0;
		if (!$this->currencyId) $this->redirect('localization', 'currency');
	}

	protected function checkLanguageId() {
		$route = UniAdmin::app()->route->getRoute();
		$this->languageId = isset($route[2]) ? $route[2] : 0;
		if (!$this->languageId) $this->redirect('localization', 'language');
	}

	protected function countryEdit() {
		if ($this->action == 'update') {
			$this->checkCountryId();
		}

		$multilingualFields = array(
			'name',
		);

		UniAdmin::import('classes.forms.Form');

		$this->form = new Form();
		$this->form
			->loadFromFile('localization.country-edit')
			->setBackAction('localization')
			->setCallback();

		if ($this->form->isSent()) {
			$this->form->queryValues();
			if ($this->form->save($this->action)) {
				$countryId = $this->form->getValue('id');
				// többnyelvű adatok mentése
				$this->saveMultilingualFields(
					'CountryText',
					'countryId',
					$countryId,
					$multilingualFields,
					$this->form
				);

				if (UniAdmin::app()->cache->isEnabled()) {
					UniAdmin::app()->cache->delete('uniAdminCountries');
				}

				UniAdmin::app()->log->setType('admin')->write('inserted new country: ' . $countryId);

				$this->redirect('localization', '', LANG, array(
					'msg' => ($this->action == 'insert' ? 'ok' : 'saved')
				));
			}
		} else {
			if ($this->action == 'update') {
				$this->form->loadValues(sprintf("id = '%s'", Db::escapeString($this->countryId)));

				$this->loadMultilingualFields(
					'CountryText',
					'countryId',
					$this->countryId,
					$multilingualFields,
					$this->form
				);
			}
		}
	}

	protected function currencyEdit() {
		if ($this->action == 'currencyupdate') {
			$this->checkCurrencyId();
		}

		UniAdmin::import('classes.forms.Form');

		$this->form = new Form();
		$this->form
			->loadFromFile('localization.currency-edit')
			->setBackAction('localization', 'currency')
			->setCallback();

		if ($this->form->isSent()) {
			$this->form->queryValues();
			if ($this->form->save($this->action == 'currencyinsert' ? 'insert' : 'update')) {
				UniAdmin::app()->log->setType('admin')->write('inserted new currency: ' . $this->form->getValue('id'));

				$this->redirect('localization', 'currency', LANG, array(
					'msg' => ($this->action == 'currencyinsert' ? 'ok' : 'saved')
				));
			}
		} else {
			if ($this->action == 'currencyupdate') {
				$this->form->loadValues(sprintf("id = '%s'", Db::escapeString($this->currencyId)));
			}
		}
	}

	protected function languageEdit() {
		if ($this->action == 'languageupdate') {
			$this->checkLanguageId();
		}

		UniAdmin::app()->page->addCss('modules.Admin.modules.Localization.css.localization');
		UniAdmin::app()->page->addScript('modules.Admin.modules.Localization.scripts.localization');
		UniAdmin::import('classes.forms.Form');

		$this->form = new Form();
		$this->form
			->loadFromFile('localization.language-edit')
			->setBackAction('localization', 'language')
			->setCallback();

		if ($this->form->isSent()) {
			$this->form->queryValues();
			if ($this->form->save($this->action == 'languageinsert' ? 'insert' : 'update')) {
				if ($this->form->getValue('isDefault')) {
					Db::sql(sprintf("
						UPDATE UniadminLanguages
						SET isDefault = 0
						WHERE id <> '%s'",
						$this->form->getValue('id')
					));
				}

				if (UniAdmin::app()->cache->isEnabled()) {
					UniAdmin::app()->cache->delete('uniAdminLanguages');
					UniAdmin::app()->cache->delete('uniAdminLanguageInfo-' . $this->form->getValue('id'));
				}

				UniAdmin::app()->log->setType('admin')->write('inserted new language: #' . $this->form->getValue('id'));

				$this->redirect('localization', 'language', LANG, array(
					'msg' => ($this->action == 'languageinsert' ? 'ok' : 'saved')
				));
			}
		} else {
			if ($this->action == 'languageupdate') {
				$this->form->loadValues(sprintf("id = '%s'", Db::escapeString($this->languageId)));
			}
		}
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		if ($this->action == 'languageupdate') {
			setlocale(LC_ALL, $this->form->getValue('locale'));
			parent::renderView();
			setlocale(LC_ALL, Setting::getLocale());
		} else {
			parent::renderView();
		}
	}
}
