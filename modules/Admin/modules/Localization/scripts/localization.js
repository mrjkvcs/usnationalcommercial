jQuery(document).ready(function($){
	loadPreview = function(field){
		var previewField = $('#' + field).attr('id').replace('Format', 'Preview');
		$('#' + previewField).load(adminBase + '/localization/datepreview', { locale: $('#locale').val(), format: $('#' + field).val() });
	};
	loadPreview('shortDateFormat');
	loadPreview('longDateFormat');
	$('#shortDateFormat, #longDateFormat').keyup(function(){
		loadPreview($(this).attr('id'));
	});
	$('#locale').change(function(){
		return $.post(adminBase + '/localization/loaddateformats', { locale: $('#locale').val() }, function(response){
			$('#date-format-examples').replaceWith(response);
			$('table.data tr:odd').addClass('odd');
		});
	});
});
