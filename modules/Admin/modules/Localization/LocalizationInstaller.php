<?php

class LocalizationInstaller extends AdminInstaller {
    public function __construct() {
        parent::__construct('Localization');
    }

	public function install() {
		parent::install();

		$defaultCurrency = Db::sqlField(sprintf("
			SELECT id
			FROM UniadminCurrency
			ORDER BY isDefault DESC
			LIMIT 1
		"));

		if (!$defaultCurrency) {	// valuta beszúrása, ha még nincs felvéve
			$defaultCurrency = 'huf';
			Db::sql(sprintf("
				INSERT INTO UniadminCurrency
				(id, currency, decimals, decimalPoint, thousandsSeparator, isDefault)
				VALUES
				('%s', '%s', %d, '%s', '%s', 1)",
				$defaultCurrency,
				'Ft',
				0,
				',',
				''
			));
		}

		$countries = Db::sqlBool("SELECT COUNT(*) FROM Country");
		if (!$countries) {
			Db::sql(sprintf("
				INSERT INTO Country
				(id, currencyId, allowRegistration, allowDelivery)
				VALUES
				('hu', '%s', 1, 1)",
				$defaultCurrency
			));
		}

		$languages = Db::sqlBool("SELECT COUNT(*) FROM UniadminLanguages");
		if (!$languages) {	// nyelv beszúrása, ha még nincs felvéve
			Db::sql(sprintf("
				INSERT INTO UniadminLanguages
				(`id`, `language`, `currencyId`, `countryId`, `locale`, `shortDateFormat`, `longDateFormat`, `isDefault`)
				VALUES
				('%s', '%s', '%s', '%s', '%s', '%s', '%s', 1)",
				'hu',
				'magyar',
				$defaultCurrency,
				'hu',
				'hu_HU.utf8',
				'%Y. %m. %d.',
				'%Y. %B %d.'
			));
		}

		if (!$countries) {
			Db::sql(sprintf("
				INSERT INTO CountryText
				(countryId, languageId, name)
				VALUES
				('%s', '%s', '%s')",
				'hu',
				'hu',
				'Magyarország'
			));

			$countryList = array(
				array('at', 'Ausztria'),
				array('be', 'Belgium'),
				array('bg', 'Bulgária'),
				array('cy', 'Ciprus'),
				array('cz', 'Csehország'),
				array('de', 'Németország'),
				array('dk', 'Dánia'),
				array('ee', 'Észtország'),
				array('es', 'Spanyolország'),
				array('fi', 'Finnország'),
				array('fr', 'Franciaország'),
				array('gr', 'Görögország'),
				// array('hu', 'Magyarország'),
				array('ie', 'Írország'),
				array('it', 'Olaszország'),
				array('lt', 'Litvánia'),
				array('lu', 'Luxemburg'),
				array('lv', 'Lettország'),
				array('mt', 'Málta'),
				array('nl', 'Hollandia'),
				array('pl', 'Lengyelország'),
				array('pt', 'Portugália'),
				array('ro', 'Románia'),
				array('se', 'Svédország'),
				array('si', 'Szlovénia'),
				array('sk', 'Szlovákia'),
				array('uk', 'Egyesült Királyság'),
			);
			foreach ($countryList as $country) {
				Db::sql(sprintf("
					INSERT INTO Country
					(id, currencyId, allowRegistration, allowDelivery)
					VALUES
					('%s', '%s', 1, 1)",
					$country[0],
					$defaultCurrency
				));
				Db::sql(sprintf("
					INSERT INTO CountryText
					(countryId, languageId, name)
					VALUES
					('%s', '%s', '%s')",
					$country[0],
					LANG,
					$country[1]
				));
			}
		}
	}
}
