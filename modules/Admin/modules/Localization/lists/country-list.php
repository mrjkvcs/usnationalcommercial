<?php

$countryList = new DataDisplay(array(
	'sql' => sprintf("
		SELECT
			`Country`.`id`,
			`CountryText`.`name`,
			`UniadminCurrency`.`currency`,
			`allowRegistration`,
			`allowDelivery`
		FROM `Country`
		LEFT JOIN `CountryText` 
			ON `CountryText`.`countryId` = `Country`.`id` AND CountryText.languageId = '%s'
		LEFT JOIN `UniadminCurrency` 
			ON `UniadminCurrency`.`id` = `Country`.`currencyId`",
		CONTENT_LANG
	),
	'defaultOrder' => 'name',
	'fullWidth' => true,
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'id' => array(
			'title' => t('column-id'),
		),
		'name' => array(
			'title' => t('column-name'),
		),
		'currency' => array(
			'title' => t('column-currency'),
		),
		'allowRegistration' => array(
			'title' => t('column-allow-registration'),
			'dataHandler' => function($value){
				return $value ? t('yes') : t('no');
			},
		),
		'allowDelivery' => array(
			'title' => t('column-allow-delivery'),
			'dataHandler' => function($value){
				return $value ? t('yes') : t('no');
			},
		),
	),
	'title' => t('table-country'),
	'baseModule' => 'localization',
));

return $countryList;
