<?php

$currencyList = new DataDisplay(array(
	'sql' => "
		SELECT 
			`id`,
			`currency`,
			'' AS `format`,
			`rate`,
			`isDefault`
		FROM `UniadminCurrency`",
	'defaultOrder' => 'currency',
	'fullWidth' => true,
	'filter' => false,
	'hideField' => 'id',
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'id' => array(
			'title' => t('column-id'),
		),
		'currency' => array(
			'title' => t('column-currency'),
		),
		'format' => array(
			'title' => t('column-format'),
			'dataHandler' => function($value, $currencyId) {
				return Text::formatCurrency(999999, true, $currencyId);
			},
		),
		'rate' => array(
			'title' => t('column-rate'),
		),
		'isDefault' => array(
			'title' => t('column-is-default'),
			'dataHandler' => function($value){
				return $value ? t('yes') : t('no');
			},
		),
	),
	'title' => t('table-currencies'),
	'updateAction' => 'currencyupdate',
	'deleteAction' => 'currencydelete',
	'baseModule' => array('localization', 'currency'),
));

return $currencyList;
