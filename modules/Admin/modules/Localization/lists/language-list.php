<?php

$languageList = new DataDisplay(array(
	'sql' => "
		SELECT 
			`UniadminLanguages`.`id`,
			`language`,
			`locale`,
			`UniadminCurrency`.`currency` AS `defaultCurrency`,
			`CountryText`.`name` AS `defaultCountry`,
			'' AS `dateFormat`,
			`shortDateFormat`,
			`longDateFormat`,
			`UniadminLanguages`.`isDefault`
		FROM `UniadminLanguages`
		LEFT JOIN UniadminCurrency ON UniadminLanguages.currencyId = UniadminCurrency.id
		LEFT JOIN CountryText ON UniadminLanguages.countryId = CountryText.countryId",
	'defaultOrder' => 'name',
	'fullWidth' => true,
	'hideField' => array('id', 'shortDateFormat', 'longDateFormat', 'locale'),
	'id' => basename(__FILE__, '.php'),
	'columnFormats' => array(
		'id' => array(
			'title' => t('column-id'),
		),
		'language' => array(
			'title' => t('column-language'),
		),
		'defaultCurrency' => array(
			'title' => t('column-default-currency'),
		),
		'defaultCountry' => array(
			'title' => t('column-default-country'),
		),
		'dateFormat' => array(
			'title' => t('column-date-format'),
			'dataHandler' => function($value, $languageId, $row) {
				setlocale(LC_ALL, $row['locale']);
				$short = strftime($row['shortDateFormat']);
				$long = strftime($row['longDateFormat']);
				setlocale(LC_ALL, Setting::getLocale());
				return $short . '<br />' . $long;
			},
		),
		'isDefault' => array(
			'title' => t('column-is-default'),
			'dataHandler' => function($value){
				return $value ? t('yes') : t('no');
			},
		),
	),
	'title' => t('table-languages'),
	'updateAction' => 'languageupdate',
	'deleteAction' => 'languagedelete',
	'baseModule' => array('localization', 'language'),
));

return $languageList;
