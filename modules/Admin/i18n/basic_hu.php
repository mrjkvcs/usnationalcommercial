<?php

$dictionary['login-to'] = '';
$dictionary['username'] = 'felhasználónév';
$dictionary['password'] = 'jelszó';
$dictionary['save-data'] = 'Beírt adatok megjegyzése';
$dictionary['log-in-automatically'] = 'Maradjon bejelentkezve';
$dictionary['login'] = 'Bejelentkezés';

$dictionary['wrong-username-or-password'] = 'Hibás felhasználónév vagy jelszó!';
$dictionary['ip-address-blocked'] = 'Erről az IP címről nem engedélyezett a belépés!';

$dictionary['content'] = 'Tartalom';
$dictionary['function'] = 'Működés';
$dictionary['system'] = 'Rendszer';
$dictionary['settings'] = 'Beállítások';

$dictionary['lock-unlock-navigator'] = 'Menü rögzítése/elrejtése';
$dictionary['view-site'] = 'Az oldal megtekintése';
$dictionary['edit-profile'] = 'Adataim módosítása';
$dictionary['logout'] = 'Kijelentkezés';
$dictionary['add-to-favorites'] = 'Kedvencekhez ad';

$dictionary['context-menu-edit'] = 'Szerkesztés';
$dictionary['context-menu-delete'] = 'Törlés...';
$dictionary['search'] = 'Keresés';
$dictionary['filter'] = 'Filter';
$dictionary['first-page'] = 'legelső oldal';
$dictionary['previous-page'] = 'előző oldal';
$dictionary['next-page'] = 'következő oldal';
$dictionary['last-page'] = 'legutolsó oldal';
$dictionary['download-format-excel'] = 'Excel formátumban';
$dictionary['download-format-excel2007'] = 'Excel 2007 formátumban';
$dictionary['download-format-csv'] = 'CSV formátumban';
$dictionary['download-format-pdf'] = 'Adobe PDF formátumban';
$dictionary['no-matching-elements-found'] = 'Nem található megfelelő elem';
