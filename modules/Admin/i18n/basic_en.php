<?php

$dictionary['login-to'] = 'Login to';
$dictionary['username'] = 'username:';
$dictionary['password'] = 'password:';
$dictionary['save-data'] = 'Save entered data';
$dictionary['log-in-automatically'] = 'Stay signed in';
$dictionary['login'] = 'Log in';

$dictionary['wrong-username-or-password'] = 'Wrong username or password!';
$dictionary['ip-address-blocked'] = 'This IP address is blocked!';

$dictionary['content'] = 'Content';
$dictionary['function'] = 'Function';
$dictionary['system'] = 'System';
$dictionary['settings'] = 'Settings';

$dictionary['lock-unlock-navigator'] = 'Lock/unlock menu';
$dictionary['view-site'] = 'Jump to the site';
$dictionary['edit-profile'] = 'Edit profile';
$dictionary['logout'] = 'Sign out';
$dictionary['add-to-favorites'] = 'Add to favorites';

$dictionary['context-menu-edit'] = 'Edit';
$dictionary['context-menu-delete'] = 'Delete...';
$dictionary['search'] = 'Search';
$dictionary['filter'] = 'Filter';
$dictionary['first-page'] = 'legelső oldal';
$dictionary['previous-page'] = 'előző oldal';
$dictionary['next-page'] = 'következő oldal';
$dictionary['last-page'] = 'legutolsó oldal';
$dictionary['download-format-excel'] = 'Excel format';
$dictionary['download-format-excel2007'] = 'Excel 2007 format';
$dictionary['download-format-csv'] = 'CSV format';
$dictionary['download-format-pdf'] = 'Adobe PDF format';
$dictionary['no-matching-elements-found'] = 'No matching elements found';
