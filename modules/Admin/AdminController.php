<?php
/**
 * uniAdmin Admin module
 *
 * @author Kenéz László <info@kenlas.hu>
 * @copyright Copyright (c) 2007-2009, Kenéz László | www.uniadmin.hu
 * @package uniAdmin
 * @since 5.0
 */

/**
 * Adminisztrációs rendszer
 */
class AdminController extends BaseModule {
	public $module;

	public function __construct() {
		parent::__construct();
		define('IS_BACKEND', true);

		UniAdmin::import(
			'modules.Admin.AdminConfig',
			'modules.Admin.classes.AdminInstaller',
			'modules.Admin.classes.BaseAdminModule',
			'modules.Admin.classes.ContextMenu',
			'modules.Admin.classes.DataDisplay',
			'modules.Admin.classes.ModuleAccessLevel',
			'modules.Admin.classes.ModuleMenu',
			'modules.Admin.classes.ModulePermission',
			'modules.Admin.classes.UserPreferences',
			'modules.Admin.classes.Tabs'
		);

		if (isset(AdminConfig::$import) && !empty(AdminConfig::$import)) {
			foreach (AdminConfig::import as $module) {
				UniAdmin::import($module);
			}
		}
		
		if (isset(AdminConfig::$forcedImport) && !empty(AdminConfig::$forcedImport)) {
			foreach (AdminConfig::$forcedImport as $module) {
				UniAdmin::import($module, true);
			}
		}

		UniAdmin::app()->language = AdminConfig::$language;
		UniAdmin::app()->setVariable('lang', AdminConfig::$language);
		if (!defined('LANG')) {
			define('LANG', AdminConfig::$language);
		}
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('setting admin language ' . AdminConfig::$language, 'framework');
		}

		setlocale(LC_ALL, Setting::getLocale());

		global $dictionary;
		if (file_exists('i18n/base_' . LANG . '.php')) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('loading basic dictionary i18n.base_' . LANG, 'dictionary');
			}
			include('i18n/base_' . LANG . '.php');
		} else {
			throw new Error('dictionary not found for admin language ' . LANG);
		}

		$this->loadDictionary('admin.basic');

		$parts = explode('.', AdminConfig::$authentication);
		$authenticationClassName = end($parts);
		UniAdmin::import(AdminConfig::$authentication);

		UniAdmin::app()->config['page'] = '';
		UniAdmin::app()->config['frontendAliases'] = UniAdmin::app()->config['aliases'];
		UniAdmin::app()->config['aliases'] = array();
		UniAdmin::app()->user = new $authenticationClassName;

		if (isset($_GET['content-language']) && $_GET['content-language']) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('setting content language ' . strtolower($_GET['content-language']), 'framework');
			}
			UserPreferences::set('contentLanguage', strtolower($_GET['content-language']));
		}
		define('CONTENT_LANG', UserPreferences::has('contentLanguage') ? UserPreferences::get('contentLanguage') : LANG);

		$route = UniAdmin::app()->route->setBaseUrl('admin')->getRoute();
		if (UniAdmin::app()->user->checkLogin()) {	// be van jelentkezve
			UniAdmin::app()->page->setTitle('Adminisztráció');
			UniAdmin::app()->config['layout'] = 'modules.Admin.views.admin';

			$route = array_slice($route, 1);

			if (!isset($route[0])) {
				$route[0] = 'welcome';
			}
		} else {	// nincs bejelentkezve
			UniAdmin::app()->page->setTitle(t('login'));
			UniAdmin::app()->config['layout'] = 'modules.Admin.views.admin-login';

			$route = array('adminprofile', 'login');
		}
		UniAdmin::app()->route->setRoute($route);

		$moduleToLoad = ucfirst(strtolower($route[0]));
		$moduleController = $moduleToLoad . 'Controller';

		if (UniAdmin::app()->user->isRoot()) {
			$accessGranted = true;	// root usernek alapból engedélyezett a hozzáférés
		} else if ($moduleController == 'AdminprofileController') {
			$accessGranted = true;	// saját profil vagy bejelentkezés
		} else {
			if (UniAdmin::app()->user->getInheritance()) {
				$userGroup = UniAdmin::app()->user->getGroupId();
			}
			$query = sprintf("
				SELECT COUNT(*)
				FROM UniadminModuleAccess
				WHERE type = '%s' AND moduleId = '%s' AND userId = '%s'",
				isset($userGroup) ? 'g' : 'u',
				$moduleController,
				isset($userGroup) ? $userGroup : UniAdmin::app()->user->getId());
			$accessGranted = Db::sqlfield($query);	// csoport vagy felhasználói szintű ellenőrzés
		}

		if ($accessGranted) {
			UniAdmin::import('modules.Admin.modules.' . $moduleToLoad . '.' . $moduleController);

			$action = UniAdmin::app()->route->getCurrentAction();
			$actionFiltered = strtolower(substr($action, 6));

			$this->module = new $moduleController($actionFiltered);

			if ($action && method_exists($this->module, $action)) {
				$this->module->action = $actionFiltered;
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('loading action ' . get_class($this->module) . '->' . $action, 'framework');
				}
				if ($this->module->checkActionPermission($action)) {
					$this->dispatchEvent('beforeModule');

					$route = array_slice($route, 1);

					// paraméter validálás
					$ret = call_user_func_array(array($this->module, 'callValidator'), $route);
					if (empty($ret) || $ret === true) {
						// beforeAction metódus hívása
						call_user_func_array(array($this->module, 'beforeAction'), $route);

						// kiválasztott action hívása
						call_user_func_array(array($this->module, $action), array_slice($route, 1));

						// afterAction metódus hívása
						call_user_func_array(array($this->module, 'afterAction'), $route);
					} else {
						$ret = str_replace(array('[module]', '[action]'), array(strtolower($moduleToLoad), isset($route[0]) ? $route[0] : null), (string) $ret);
						header('Location: ' . $ret);
						exit;
					}
				} else {
					if (DEBUG_MODE) {
						UniAdmin::addDebugInfo('user does not have permission to access this action: ' . get_class($this->module) . '->' . $action, 'framework', 2);
					}
				}
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('error loading action ' . get_class($this->module) . '->' . $action, 'framework', 2);
				}
			}
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('user does not have permission to access this module: ' . $moduleController, 'framework', 2);
			}
		}
	}

	public function renderView($viewFile = null, $parameters = array(), $cacheTtl = null) {
		if (is_object($this->module) && method_exists($this->module, 'renderView')) {
			$this->dispatchEvent('beforeRender');
			try {
				$this->module->renderView();
			} catch (Error $e) {
				ob_end_clean();
				$e->renderView();
			}
		}
	}
}
