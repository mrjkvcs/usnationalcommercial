<?php
$baseUrl = 'http://www.propertyline.com/listing/emarket_report/';
$listings = [
    ['id' => '671736', 'title' => 'Craig/Revere Commercial Pad -BANK OWNED!'],
    //['id' => '814240', 'title' => 'Fort Apache Point'],
    ['id' => '813844', 'title' => 'El Camino & Levi'],
    ['id' => '9080', 'title' =>  'w Cheyenne' ],
];
?>
<div class="container">
    <div class="row">
        <div class="col-xs-12 hidden-sm hidden-md hidden-lg text-center">
            <a href="tel:7025180875" class="btn btn-success btn-lg"><i class="fa fa-phone"></i> (702) 518-0875</a>
        </div>
    </div>
    <div class="row">
        <div class="col-md-9">
            <div class="page-header">
                <h2><span class="thin">Welcome to</span></h2>
            </div>
            <h1><?php echo TITLE; ?></h1>

            <p> US National Commercial Real Estate Services is a full service real estate and financial company specializing in brokerage, property management, consulting & investments. </p>
            <blockquote class="hidden-xs">We are Task Oriented and Goals Driven to achive maximum results to our
                clients
            </blockquote>
            <blockquote class="hidden-sm hidden-md hidden-lg blockquote-xs">We are Task Oriented and Goals Driven to
                achive maximum results to our clients
            </blockquote>
            <p> We are ready to share our market knowledge and expertise with you in order to satisfy your real estate
                objective. </p>

            <div class="page-header">
                <h2><span class="thin"> Featured Properties</span></h2>
            </div>

            <div class="row">
                <?php foreach ($listings as $listing) : ?>
                    <div class="col-xs-6 col-sm-4 col-md-3 text-center">
                        <a href="<?php echo $baseUrl . $listing['id']; ?>" target="_blank" class="thumbnail">
                            <img src="<?php echo 'images/listings/' . $listing['id']; ?>.jpg" alt="<?php echo $listing['title']; ?>" class="img-responsive">
                        </a>
                        <small class="hidden-xs"><a href="<?php echo $baseUrl . $listing['id']; ?>" target="_blank">Listing #<?php echo $listing['id']; ?> :</a> <br/><?php echo $listing['title']; ?> </small>
                    </div>
                <?php endforeach; ?>
            </div>

            <!-- <div class="page&#45;header"> -->
            <!--     <h2><span class="thin"> <?php echo date('Y'); ?> Golf League</span></h2> -->
            <!-- </div> -->
            <!-- <img src="/gfx/corpconepts&#45;bnr.png" alt="" class="img&#45;responsive"/> -->
            <!--<p>-->
            <!--    --><?php //if (date('Y-m-d') < '2015-04-07') : ?>
            <!--        <a href="--><?php //echo Url::link('golf'); ?><!--">CLICK HERE</a> to register for 2015 spring golf league. Limited to 100 sign ups.-->
            <!--    --><?php //else: ?>
            <!--        <a href="--><?php //echo Url::link('golf'); ?><!--">CLICK HERE</a> for current leaderboard.-->
            <!--    --><?php //endif; ?>
            <!--</p>-->
        </div>
        <div class="col-md-3 col-sm-12">
            <div class="page-header"><h2><span class="thin"> Latest News</span></h2></div>
            <div class="right_box_twitter corners">
                <div>
                    <a class="twitter-timeline" href="https://twitter.com/USNCCRE" data-widget-id="293478589175840768">Tweets
                        by @USNCCRE</a>
                    <script>!function (d, s, id) {
                            var js, fjs = d.getElementsByTagName(s)[0];
                            if (!d.getElementById(id)) {
                                js = d.createElement(s);
                                js.id = id;
                                js.src = "//platform.twitter.com/widgets.js";
                                fjs.parentNode.insertBefore(js, fjs);
                            }
                        }(document, "script", "twitter-wjs");</script>
                </div>
            </div>
            <div class="page-header">
                <h2><span class="thin"> What is your <br/> real estate need?</span></h2>
            </div>
            <a href="Hollo_Zoltan.vcf"><img src="/gfx/contacticon.png" alt="Zoltan Hollo"/></a> <small> ← Download my VCard!</small>
            <br/><br/>
            <a href="tel:702-518-0875" class="btn btn-lg btn-success"><i class="fa fa-phone"></i> (702) 518-0875</a>
        </div>
    </div>
</div>
