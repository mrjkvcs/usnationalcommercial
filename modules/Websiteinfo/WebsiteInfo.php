<?php

final class WebsiteInfo {
	private static $info;

	private static function loadInfo($lang) {
		if (is_null(self::$info)) {
			if (UniAdmin::app()->cache->isEnabled()) {
				$cachedVersion = UniAdmin::app()->cache->get('WebsiteInfo-' . $lang);
				if ($cachedVersion) {
					self::$info = $cachedVersion;
					return;
				}
			}
			self::$info = Db::toArray(sprintf("
				SELECT property, value
				FROM WebsiteInfo
				WHERE languageId = '' OR languageId = '%s'",
				$lang
			), true);
			if (UniAdmin::app()->cache->isEnabled()) {
				UniAdmin::app()->cache->set('WebsiteInfo-' . $lang, self::$info, 3600);
			}
		}
	}

	public static function get($property, $lang = null) {
		$lang = $lang ?: LANG;
		self::loadInfo($lang);
		return isset(self::$info[$property]) ? self::$info[$property] : null;
	}
}
