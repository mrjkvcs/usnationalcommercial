<ul id="choose-language">
	<?php foreach ($this->languages as $id => $language): ?>
	<li><a href="<?php echo $link = Url::link('', '', $id); ?>" <?php echo LANG == $id ? 'class="active"' : ''; ?>><?php echo $language; ?></a></li>
	<?php endforeach; ?>
</ul>