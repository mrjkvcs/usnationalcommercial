<?php

class ChooseLanguage extends Widget {
	protected $viewFile = 'websiteinfo.choose-language';
	protected $languages = array();

	public function __construct($parameters = array(), $autoRender = false) {
		$this->languages = Setting::getLanguages();

		if ($autoRender) {
			$this->renderView();
		}
	}

	public function renderView() {
		if (count($this->languages) > 1) {
			parent::renderView($this->viewFile);
		}
	}
}
