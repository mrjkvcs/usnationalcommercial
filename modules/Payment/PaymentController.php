<?php

class PaymentController extends BaseModule {

    protected $data;
    protected $product;
    protected $price;
    protected $gateway;

    public function __construct()
    {
        UniAdmin::import( 'modules.Payment.PaypalConfig' );

        $this->gateway = \Omnipay\Omnipay::create('PayPal_Express');

        $this->gateway->setUsername(PaypalConfig::API_USERNAME);
        $this->gateway->setPassword(PaypalConfig::API_PASSWORD);
        $this->gateway->setSignature(PaypalConfig::API_SIGNATURE);
        $this->gateway->setTestMode(PaypalConfig::SANDBOX_MODE);

        $this->product = 'Golf Membership ' . date('Y');
        $this->price = (float) 125;
        $this->data = [
            'cancelUrl'   => 'http://' . $_SERVER['SERVER_NAME'] . '/payment/cancel',
            'returnUrl'   => 'http://' . $_SERVER['SERVER_NAME'] . '/payment/success',
            'description' => $this->product,
            'amount'      => $this->price,
            'currency'    => 'USD'
        ];
    }

    /**
     * Settings payPal
     *
     * @return mixed
     */
    protected function setPayPal()
    {
        $response = $this->gateway->purchase($this->data)->send();
        $response->redirect();
    }

    /**
     * Redirect to payPal
     */
    public function actionPayPal()
    {
        $this->setPayPal();
    }

    /**
     * Cancel payment
     */
    public function actionCancel()
    {
        $_SESSION['cancel'] = 1;
        $this->redirect('golf');
    }

    /**
     * Success payment
     */
    public function actionSuccess()
    {
        $response = $this->gateway->completePurchase($this->data)->send();

        $data = $response->getData();

        $this->update();

        $this->check($data);

        unset($_SESSION['memberId']);

        $this->redirect('golf');
    }

    /**
     * Update member table
     */
    protected function update()
    {
        Db::sql(sprintf("
          UPDATE Member
          SET
            paid = NOW(),
            payment = '%s'
          WHERE id = %d ",
            Db::escapeString('paypal'),
            $_SESSION['memberId']
        ));
    }

    /**
     * @param $data
     */
    protected function insert($data)
    {
        Db::sql(sprintf("
            INSERT INTO MemberFee
            (userId, paidDate, transactionId, transactionType, orderTime, amount, fee, paymentStatus, pendingReason, ReasonCode, merchantId, errorCode, ACK)
            VALUES (%d, NOW(), '%s', '%s', '%s', %d, %d, '%s', '%s', '%s', '%s', %d, '%s') ",
            $_SESSION['memberId'],
            Db::escapeString($data['PAYMENTINFO_0_TRANSACTIONID']),
            Db::escapeString($data['PAYMENTINFO_0_TRANSACTIONTYPE']),
            Db::escapeString($data['PAYMENTINFO_0_ORDERTIME']),
            $data['PAYMENTINFO_0_AMT'],
            $data['PAYMENTINFO_0_FEEAMT'],
            Db::escapeString($data['PAYMENTINFO_0_PAYMENTSTATUS']),
            Db::escapeString($data['PAYMENTINFO_0_PENDINGREASON']),
            Db::escapeString($data['PAYMENTINFO_0_REASONCODE']),
            Db::escapeString($data['PAYMENTINFO_0_SECUREMERCHANTACCOUNTID']),
            $data['PAYMENTINFO_0_ERRORCODE'],
            Db::escapeString($data['PAYMENTINFO_0_ACK'])
        ));
    }

    /**
     * @param $data
     */
    protected function check($data)
    {
        if ($data['PAYMENTINFO_0_PAYMENTSTATUS'] != 'Completed')
        {
            $_SESSION['unSuccess'] = 1;
            $_SESSION['data'] = $data;
        } else
        {
            $_SESSION['data'] = $data;
            $_SESSION['success'] = 1;
            $this->insert($data);
        }
    }

}