<?php

class Pagesetup extends BaseModule {
	private $cssFiles = array();
	private $scriptFiles = array();
	private $scriptTags = array();
	private $rss = array();
	private $title = '';
	private $titleprefix = '';
	private $titlesuffix = '';
	private $titleSeparator = ' - ';
	private $description = '';
	private $keywords = '';
	private $author = '';
	private $robots = '';
	private $ga = '';
	private $imagetoolbar = true;
	private $favicon = '';
	private $extraheader = '';

	/**
	 * Felvesz egy CSS fájlt a betöltendő stíluslapok közé
	 * @param string $file a fájl neve
	 * @param string $mediaType a kiválasztott média típus
	 * @return pagesetup
	 */
	public function addCss($file, $mediaType = 'all') {
		if ($file && !array_key_exists($file, $this->cssFiles)) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('adding css ' . $file, 'page');
			}
			$this->cssFiles[$file] = $mediaType;
		}
		return $this;
	}

	/**
	 * Felvesz egy JavaScript fájlt a betöltendő script fájlok közé
	 * @param string $file a fájl neve
	 * @return pagesetup
	 */
	public function addScript($file) {
		if ($file && !in_array($file, $this->scriptFiles)) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('adding javascript ' . $file, 'page');
			}
			$this->scriptFiles[] = $file;
		}
		return $this;
	}

	/**
	 * Felvesz egy JavaScript kódrészletet a meghívandó scriptek közé
	 * @param string $script a kódrészlet
	 * @return pagesetup
	 */
	public function addScriptTag($script) {
		if ($script) {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('adding javascript tag', 'page');
			}
			$this->scriptTags[] = $script;
		}
		return $this;
	}

	/**
	 * Felvesz egy RSS fájlt az oldal feed-jei közé
	 * @param string $file a fájl neve
	 * @param string $title a feed címe
	 * @return pagesetup|bool
	 */
	public function addRSS($file, $title = '') {
		if ($file) {
			$this->rss[] = array('file' => $file, 'title' => $title);
			return $this;
		} else {
		    return false;
		}
	}

	public function removeCss($file) {
		if (isset($this->cssFiles[$file])) {
			unset($this->cssFiles[$file]);
		}
		return $this;
	}

	public function removeAllCss() {
		$this->cssFiles = array();
		return $this;
	}

	public function removeScript($file) {
		$key = array_search($file, $this->scriptFiles);
		if ($key !== false) {
			unset($this->scriptFiles[$key]);
		}
		return $this;
	}

	public function removeAllScripts() {
		$this->scriptFiles = array();
		return $this;
	}

	public function removeRss($file) {
		$key = array_search($file, $this->rss);
		if ($key !== false) {
			unset($this->rss[$key]);
		}
		return $this;
	}

	/**
	 * Beállítja a holnap címének előtagját
	 * A rendszer a címeket három részre bontva kezeli: PREFIX | TITLE | SUFFIX
	 * @param string $value
	 * @return pagesetup
	 */
	public function setTitlePrefix($value) {
		$this->titleprefix = $value;
		return $this;
	}

	/**
	 * Beállítja a holnap címét
	 * A rendszer a címeket három részre bontva kezeli: PREFIX | TITLE | SUFFIX
	 * @param string $value
	 * @return pagesetup
	 */
	public function setTitle($value) {
		$this->title = $value;
		return $this;
	}

	/**
	 * Beállítja a holnap címének utótagját
	 * A rendszer a címeket három részre bontva kezeli: PREFIX | TITLE | SUFFIX
	 * @param string $value
	 * @param pagesetup
	 */
	public function setTitleSuffix($value) {
		$this->titlesuffix = $value;
		return $this;
	}

	/**
	 * Beállítja az oldal META DESCRIPTION fejlécét
	 * @param string $value
	 * @return pagesetup
	 */
	public function setDescription($value) {
		$this->description = $value;
		return $this;
	}

	/**
	 * Beállítja az oldal META KEYWORDS fejlécét
	 * @param string $value
	 * @return pagesetup
	 */
	public function setKeywords($value) {
		$this->keywords = $value;
		return $this;
	}

	/**
	 * Beállítja az oldal META AUTHOR fejlécét
	 * @param string $value
	 * @return pagesetup
	 */
	public function setAuthor($value) {
		$this->author= $value;
		return $this;
	}

	/**
	 * Beállítja az oldal META ROBOTS fejlécét
	 * A fejléc az alábbi kifejezéseket tartalmazhatja:
	 * index, follow, noindex, nofollow,all
	 * @param string $value
	 * @return pagesetup
	 */
	public function setRobots($value) {
		$this->robots = $value;
		return $this;
	}

	/**
	 * Beállítja a Google Analytics tracker azonosítóját, hogy ez alapján a
	 * rendszer el tudja készíteni a követőkódot
	 * @param string $value
	 * @return pagesetup
	 */
	public function setGATracker($value) {
		$this->ga = $value;
		return $this;
	}

	/**
	 * Kikapcsolja az Internet Explorer 'Image Toolbar'-ját, ami az oldal képein jelenik meg
	 * @param bool $state
	 * @return pagesetup
	 */
	public function setImageToolbar($state) {
		$this->imagetoolbar = $state;
		return $this;
	}

	/**
	 * Beállítja a honlap FAVICON-ját, a böngészősávon megjelenő kis ikont
	 * @param string $value alapértelmezetten a böngészők által keresett /favicon.ico
	 * @@return pagesetup
	 */
	public function setFavIcon($value = '/favicon.ico') {
		$this->favicon = $value;
		return $this;
	}

	/**
	 * Csatol egy HTML kódot, ami a honlap fejlécébe a <HEAD> </HEAD> tagek közé fog kerülni
	 * @param string $html
	 * @return pagesetup
	 */
	public function addExtraHeader($html) {
		$this->extraheader .= $html;
		return $this;
	}

	public function getCssFiles() {
		return $this->cssFiles;
	}

	public function getScriptFiles() {
		return $this->scriptFiles;
	}

	public function getTitlePrefix() {
		return $this->titleprefix;
	}

	public function getTitle() {
		return $this->title;
	}

	public function getTitleSuffix() {
		return $this->titlesuffix;
	}

	public function getFullTitle() {
		$elements = array();
		if ($this->titleprefix) {
			$elements[] = $this->titleprefix;
		}
		if ($this->title) {
			$elements[] = $this->title;
		}
		if ($this->titlesuffix) {
			$elements[] = $this->titlesuffix;
		}
		return implode($this->titleSeparator, $elements);
	}

	public function getContent() {
		return $this->content;
	}

	/**
	 * Megjeleníti az oldal fejlécét a megadott beállításokkal
	 */
	public function display() {
		if (UniAdmin::app()->isAjax()) {
			$page = false;
			UniAdmin::app()->config['layout'] = 'ajax';
		} else if (isset(UniAdmin::app()->config['page'])) {
			$page = UniAdmin::app()->config['page'];
		} else {
			$page = 'page';
		}

		if (!$this->description) {
			$this->description = WebsiteInfo::get('metaDescription');
		}
		if (!$this->keywords) {
			$this->keywords = WebsiteInfo::get('metaKeywords');
		}
		if (!$this->ga) {
			$this->ga = WebsiteInfo::get('gaTracker');
		}
		if (!$this->favicon) {
			$this->favicon = WebsiteInfo::get('favicon');
		}
		
		if ($page) {
			if (file_exists('views/' . $page . '.php')) {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('loading page file: ' . $page, 'page');
				}
				ob_start();
				include('views/' . $page . '.php');
				$this->content = ob_get_clean();
			} else {
				if (DEBUG_MODE) {
					UniAdmin::addDebugInfo('error loading page file: ' . $page, 'page', 3, __FILE__, __LINE__);
				}
			}
		} else {
			ob_start();
			UniAdmin::app()->render();
			$this->content = ob_get_clean();
		}

		UniAdmin::app()->dispatchEvent('application', 'afterRender');

		if (strpos(UniAdmin::app()->config['layout'], '.') !== false) {
			$path = str_replace('.', '/', UniAdmin::app()->config['layout']) . '.php';
		} else {
			$path = 'views/' . UniAdmin::app()->config['layout'] . '.php';
		}
		if (file_exists($path)) {
			$bufferContents = ob_get_clean();
			ob_start('ob_gzhandler');
			echo $bufferContents;
			include($path);
		} else {
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('error loading layout ' . $path, 'page', 3, __FILE__, __LINE__);
			}
		}
	}
}
