<?php
/**
 * uniAdmin UserAuthentication module
 *
 */

class UserAuthentication extends BaseModule {
	protected $_id;

	public function __construct() {
		parent::__construct();
		if ($this->checkLogin() || $this->autoLogin()) {
			$this->_id = $_SESSION['loginid'];
		}
	}

	public function checkLogin() {
		return isset($_SESSION['loginid']);
	}

	public function getId() {
		return $this->_id;
	}

	public function getInfo() {
		if (!$this->_id) {
			return false;
		}
		return Db::sqlrow('SELECT * FROM User WHERE id = ' . $this->_id);
	}

	public function getGroup() {
		return $this->groupId;
	}

	public function __get($name) {
		if (!$this->_id) {
			return false;
		}
		$content = Db::sqlField(sprintf('SELECT `%s` FROM `User` WHERE `id` = %d', $name, $this->_id));
		if ($content) {
			$this->$name = $content;
		}
		return $content;
	}

	public function autoLogin() {
		if (isset($_COOKIE['authToken'])) {
			$token = $_COOKIE['authToken'];
			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('autologin, token: ' . $token, 'authentication');
			}
			return $this->login($token);
		}

		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('cannot find autologin info', 'authentication', 2);
		}
		return false;
	}

	public function login($token = null) {
		if (is_null($token)) {
			UniAdmin::import('modules.User.UserConfig');
			$email = Db::escapeString($_POST['user']);
			$isPost = true;
			if (!$email) {
				return false;
			}
			$rs = Db::sqlRow(sprintf("
				SELECT *
				FROM User
				WHERE email = '%s'
				AND isActivated = 1
				AND isActive = 1",
				$email
			));
			if ($rs && !UserConfig::verifyPassword($_POST['password'], $rs['password'])) {
				$rs = false;
			}
		} else {
			$userId = substr($token, 0, -32);
			$userToken = substr($token, -32);
			$isPost = false;
			if (!$userId || !$userToken) {
				return false;
			}
			$rs = Db::sqlRow(sprintf("
				SELECT *
				FROM User
				WHERE id = %d
				AND MD5(CONCAT(email, password, '%s')) = '%s'
				AND isActivated = 1
				AND isActive = 1",
				$userId,
				Db::escapeString($_SERVER['HTTP_USER_AGENT']),
				Db::escapeString($userToken)
			));
		}

		if ($rs) {

			$parameters = new EventParameter();
			$parameters->user = $rs;

			$this->dispatchEvent('beforeLogin', $parameters);

			if (DEBUG_MODE) {
				UniAdmin::addDebugInfo('successfull user login: #' . $rs['id'] . ' - ' . $rs['email'], 'authentication');
			}

			$_SESSION['loginid'] = $rs['id'];

			Db::sql(sprintf("
				UPDATE User
				SET lastLoginAt = NOW()
				WHERE id = %d",
				$rs['id']
			));

			if ($isPost) {
				$authToken = $rs['id'] . md5($rs['email'] . $rs['password'] . $_SERVER['HTTP_USER_AGENT']);
				if (isset($_POST['remember'])) {
					setcookie('authToken', $authToken, time() + 31536000, '/', $_SERVER['HTTP_HOST']);
				} else {
					setcookie('authToken', false, time()-3600, '/', $_SERVER['HTTP_HOST']);
				}
			}

			UniAdmin::app()->log->write('login');

			$this->dispatchEvent('afterLogin', $parameters);

			return true;
		}

		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('error login: ' . ($isPost ? $email : $token), 'authentication', 2);
		}

		return false;
	}

	public function logout() {
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('logout', 'authentication');
		}

		$this->dispatchEvent('beforeLogout');

		UniAdmin::app()->log->write('logout');

		unset($_SESSION['loginid']);
		unset($_SESSION['facebook_profile']);
		setcookie('authToken', false, time()-3600, '/', $_SERVER['HTTP_HOST']);

		$this->dispatchEvent('afterLogout');
	}
}
