<?php

class EmailModal extends Widget {
    public function __construct($parameters = array()) {
        $this->puppyName = isset($parameters['puppyName']) ? $parameters['puppyName'] : null; 
        $this->renderView();
    }

    public function renderView() {
        parent::renderView('user.email-modal');
    }
}
