<?php

class SubscriptionExpired extends Widget {

	public function __construct($parameters = array()) {

		$this->renderView();
	}

	public function renderView() {
		parent::renderView('user.subscription_expired');
	}
}