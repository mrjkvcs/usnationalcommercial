<?php

class LoginForm extends Widget {
	public function __construct() {
		$this->renderView();
	}

	public function renderView() {
		parent::renderView('user.modal-login');
	}
}
