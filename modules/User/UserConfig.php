<?php

class UserConfig {
	protected static $passwordEncryptionRounds = 12;

	public static function encodePassword($password) {
		UniAdmin::import('extensions.bcrypt.Bcrypt');
		$bcrypt = new Bcrypt(self::$passwordEncryptionRounds);
		$hash = $bcrypt->hash($password);
		return $hash;
	}

	public static function verifyPassword($password, $existingHash) {
		$password = trim($password);	// szóköz és új sor levágása, ha lenne benne véletlenül
		$hash = crypt($password, $existingHash);
		return $hash === $existingHash;
	}

	public static function randomPassword($len = 8) {
	    $alphabet = "abcdefghijklmnopqrstuwxyzABCDEFGHIJKLMNOPQRSTUWXYZ0123456789";
	    $pass = array(); //remember to declare $pass as an array
	    $alphaLength = strlen($alphabet) - 1; //put the length -1 in cache
	    for ($i = 0; $i < $len; $i++) {
	        $n = rand(0, $alphaLength);
	        $pass[] = $alphabet[$n];
	    }
	    return implode($pass); //turn the array into a string
	}
}
