<?php

class UserController extends BaseModule {

	public function actionDefault() {
		$this->viewFile = 'login';
	}

	public function beforeAction() {
		UniAdmin::import('modules.User.UserConfig');
	}

	public function actionSignup() {
		UniAdmin::app()->page->setTitle('Sign Up');
		if (isset($_SESSION['hasNoFacebook'])) { $this->hasNoFacebook = 1; unset($_SESSION['hasNoFacebook']); }
		$this->viewFile = 'signup';
		UniAdmin::app()->path
			->addElement('Don\'t have an Account? Create on here', 'user', 'signup')
			->addElement('Forgot Password?', 'user', 'resetpassword');
	}

	public function actionLogin() {
		if (isset($_SESSION['incorrectLogin'])) { $this->incorrectLogin = 1; unset($_SESSION['incorrectLogin']); }
		if (isset($_SESSION['notLogin'])) { $this->notLogin = 1; unset($_SESSION['notLogin']); }
		if (isset($_SESSION['isActivated'])) { $this->isActivated = 1; unset($_SESSION['isActivated']); }
		if (isset($_SESSION['hasActivated'])) { $this->hasActivated = 1; unset($_SESSION['hasActivated']); }
		if (isset($_SESSION['supportGuest'])) { $this->supportGuest = 1; unset($_SESSION['supportGuest']); }
		if (isset($_SESSION['activationCode'])) { unset($_SESSION['activationCode']); }
		if (isset($_SESSION['userId'])) { unset($_SESSION['userId']); }
		if (isset($_SESSION['smsCode'])) {  unset($_SESSION['smsCode']); }

		if (UniAdmin::app()->user->checkLogin()) { // ha már be van jelentkezve, küldés a főoldalra
			if (UniAdmin::app()->isAjax()) {
				return new AjaxResponse(array('success' => 1 ));
			} else {
				$this->redirect('puppy', 'list');
			}
		}

		if (isset($_POST['user'])) {
			if (UniAdmin::app()->user->login()) { // sikeres bejelentkezés esetén újratöltés
				if (UniAdmin::app()->isAjax()) {
					return new AjaxResponse(array('success' => 1 ));
				} else {
					header('Location: ' . (isset($_SESSION['HTTP_REFERER']) ? $_SERVER['HTTP_REFERER'] : '/puppy/list'));
					unset($_SESSION['HTTP_REFERER']);
					exit;
				}
			} else {
				if (UniAdmin::app()->isAjax()) {
					return new AjaxResponse(array('success' => 0 ));
				} else {
					$_SESSION['incorrectLogin'] = 1;
					$this->redirect('user','login');
				}
			}
		}
		$this->viewFile = 'login';
		UniAdmin::app()->path->addElement('Don\'t have an Account? Create on here', 'user', 'signup');
		UniAdmin::app()->page->setTitle(t('Sign In'));
	}

	public function actionLogout() {
		UniAdmin::app()->user->logout();
		$this->redirect('');
	}

	public function actionAccount() {
		$this->edited            = Util::setSession('edited');
		$this->expireDate        = Util::setSession('expireDate');
		$this->changepassword    = Util::setSession('changepassword');
		$this->notExistsPhone    = Util::setSession('notExistsPhone');
		$this->isActivated       = Util::setSession('isActivated');
		$this->sendPhoneVerified = Util::setSession('sendPhoneVerified');
		$this->noMoreSms         = Util::setSession('noMoreSms');
		$this->wrongSmsCode      = Util::setSession('wrongSmsCode');
		$this->noMoreTry         = Util::setSession('noMoreTry');
		$this->buyerType         = Util::setSession('buyerType');

		$this->type = UniAdmin::app()->user->type;
		!UniAdmin::app()->user->checkLogin() ? $this->redirect('user', 'login') : null;

		UniAdmin::app()->path
			->addElement('My Message', 'message')
			->addElement('My Favorite', 'puppy', 'favorite')
			->addElement('Notified List', 'notify');

		UniAdmin::app()->page->setTitle('Membership Status');
		$this->viewFile       = $this->type;
		$this->name           = UniAdmin::app()->user->firstName .' '. UniAdmin::app()->user->lastName;
		$this->userId         = UniAdmin::app()->user->getId();
		$this->smsCode        = UniAdmin::app()->user->smsCode;
		$this->disabledButton = UniAdmin::app()->user->smsCodeTyped < SMS_SENT;

		if ($this->type != 'buyer') {
			UniAdmin::app()->path->addElement('My Listing', 'puppy', 'mylisting');
			$this->plans          = MembershipHelper::getPlans($this->type);
			$this->currentPlan    = MembershipHelper::getPlan(UniAdmin::app()->user->planId);
			$this->basicPlan      = strpos(UniAdmin::app()->user->planId, '-basic') !== false;
			$this->paymentHistory = Payment::getPaymentHistory(UniAdmin::app()->user->getId());
			// echo '<pre>'; print_r($this->paymentHistory); die();
			$expireDate           = UniAdmin::app()->user->expireDate;
			$this->expireDateMsg  = $expireDate >= date('Y-m-d') ? '<span class="text-warning">Your Access Until: ' . $expireDate . '</span>' : '<span class="text-danger">Your Subscription Expired: ' . $expireDate . '</span>';

			$this->hasRecurring = Db::sqlBool(sprintf("
				SELECT COUNT(*)
				FROM UserMembership UM
				JOIN MembershipPlan MP
					ON UM.planId = MP.id
					AND MP.isRecurring = 1
					AND UM.userId = %d
					AND UM.isRecurringActive = 1",
					UniAdmin::app()->user->getId()
				));
			if (!$this->hasRecurring) {
				if ($_SERVER['REQUEST_METHOD'] == 'POST') {
					$planId = isset($_POST['plan']) ? $_POST['plan'] : '';
					$plan = MembershipHelper::getPlan($planId);
					$paymethodId = isset($_POST['method']) ? $_POST['method'] : '';
					if (!in_array($paymethodId, array('paypal'))) {
						$paymethodId = false;
					}

					if ($plan && $paymethodId) {
						$this->redirect('payments', array($paymethodId, $planId));
					}
				}
			}
		}
	}

	public function actionRegister() {
		$userType = isset($_POST['type']) ? $_POST['type'] : null;
		if (method_exists(__CLASS__, $userType)) {
			throw new Http404Exception();
		}
		return call_user_func_array(array(__CLASS__, 'userRegister'), array($userType));
	}

	public function userRegister($type) {
		UniAdmin::app()->page->setTitle('Sign Up as ' . ucfirst($type));
		UniAdmin::app()->path->addElement('Back to the Sign Up Choose', 'user', 'signup');
		UniAdmin::import('classes.forms.Form');

		$this->viewFile = $type . '_register';
		$this->type     = $type;
		$days 			= MembershipHelper::getPlan($type .'-basic');
		$this->form     = new Form();
		$this->form->loadFromFile('user.' . $type);

		if ($this->form->isSent()) {

			$this->form->queryValues();

			$this->form
				->addDbField('registeredAt', date('Y-m-d H:i:s'))
				->addDbField('activationCode', substr(md5(time()), 0, 16))
				->addDbField('expireDate', date('Y-m-d',strtotime($days['days'] . ' day')))
				->addDbField('type', $this->type)
				->addDbField('planId', $type . '-basic');

			if ($this->form->validateFields()) {

				$this->form->getField('password')
					->setMatchTo('')
					->setValue(UserConfig::encodePassword($this->form->getValue('password')));

				$phone  = $this->form->getValue('phone');
				$code   = $this->form->getValue('activationCode');
				$userId = $this->form->save();

				if ($userId) {

					if ($type == 'breeder') {
						Db::sql(sprintf("INSERT INTO Breeder (id) VALUES(%d)", $userId));
					}

					$_SESSION['userDone'] = 1;
					$_SESSION['userId']   = $userId;

//					$checkPayment = Payment::hasPayment($userId);
//					empty($checkPayment) ? Payment::insertPayment($userId) : null;

					if (!empty($phone) && strlen($phone) >= 10) {
						$_SESSION['activationCode'] = $code;
						$_SESSION['hasPhone'] = 1;
						Twilio::sendMessage($phone, $userId);
					}

					$data['link']      = 'http://' . $_SERVER['HTTP_HOST'] . Url::link('user', array('activation', 'id' => $userId, 'code' => $code));
					$data['firstName'] = $this->form->getValue('firstName');

					$template = new Template('registration', LANG);
					$message = $template->setText($template->getText())
						->addParameter($data)
						->evaluate();

					$email = new Email();
					$email
						->to($this->form->getValue('email'))
						->setSubject('Welcome')
						->setMessage($message)
						->send();

					if (UniAdmin::app()->isAjax()) {
						return new AjaxResponse(array(
							'success' => 1,
							'message' => $this->loadView('user.done'),
						));
					} else {
						$this->redirect('user', 'done');
					}
				}
			}

			if (UniAdmin::app()->isAjax()) {
				return new AjaxResponse(array(
					'success' => 0,
					'form' => $this->loadView($this->viewFile),
				));
			}
		}
	}


	/**
	 * Emailben kapott linkre jut ide.
	 * @return
	 */
	public function actionActivation() {
        $params = UniAdmin::app()->route->getParameters();
		if ($params['id'] && $params['code']) {
			$result = Db::sqlRow(sprintf("
				SELECT id, firstName, lastName, email, password, isActive, isActivated
				FROM User
				WHERE id = %d
				AND activationCode = '%s'",
				intval($params['id']), Db::escapeString($params['code'])
			));
			if ($result) {
				if ($result['isActivated']) {
					$_SESSION['hasActivated'] = 1;
				}
				else {
					Db::sql(sprintf("
						UPDATE User
						SET isActivated = 1
						WHERE id = %d",
						$params['id']
					));

                    $checkPayment = Payment::hasPayment($userId);
					empty($checkPayment) ? Payment::insertPayment($params['id']) : null;

					$result['subscriptionDate'] = SUBSCRIPTION_DATE;
					$email = new Email();
					$email
						->to(Db::sqlField(sprintf("SELECT email FROM User WHERE id = %d", intval($params['id']))))
						->setSubject('Your ChoosePuppy account has been activated!')
						->fromTemplate('user-activation', $result)
						->send();
					$_SESSION['isActivated'] = 1;
				}
			}
		$this->redirect('user', 'login');
		}
		else {
			throw new Http404Exception();
		}
	}

    /**
     * SMS Activation
     * @param string $redirect
     */
    public function actionSmsActivation($redirect = 'done') {
		if (isset($_POST['smsCode'])) { $_SESSION['smsCode'] = $_POST['smsCode']; }

		if (Twilio::verifyCode()) {
			$_SESSION['isActivated'] = 1;
			UniAdmin::app()->user->checkLogin() ? $this->redirect('user', 'account') : $this->redirect('user', 'login');
		} else {
			!User::updateSmsCodeTyped() ? $_SESSION['noMoreTry'] = 1 : $_SESSION['wrongSmsCode'] = 1;
			$this->redirect('user', $redirect);
		}
	}

    /**
     * My Account oldalon tudja a user verify-olni a telefon szamat.
     */
    public function actionPhoneVerified() {
		$phone = UniAdmin::app()->user->phone;
		if (!UniAdmin::app()->user->checkLogin()) { $this->redirect('user', 'login'); }

		if (empty($phone) || strlen($phone) < 10) {
			$_SESSION['notExistsPhone'] = 1; $this->redirect('user', 'account');
		}

		if (UniAdmin::app()->user->smsCodeTyped == SMS_TYPED) {
			$_SESSION['noMoreTry'] = 1; $this->redirect('user', 'account');
		}

		Twilio::sendMessage($phone) ? $_SESSION['sendPhoneVerified'] = 1 : $_SESSION['noMoreSms'] = 1;
		$this->redirect('user', 'account');
	}

	/**
	 * User adatainak szerkesztese.
	 * @return array
	 */
	public function actionEdit($load = false) {

		if (!UniAdmin::app()->user->checkLogin()) { $this->redirect('user', 'login'); }

		UniAdmin::import('classes.forms.Form');
		UniAdmin::app()->path->addElement('My Account', 'user', 'account');

		$this->form     = new Form();
		$userId         = UniAdmin::app()->user->getId();
		$this->type     = UniAdmin::app()->user->type;
		$this->loadFile = !$load ? 'user.' . $this->type . '-edit' : 'user.' . $this->type . '-details';
		$this->viewFile = !$load ? 'edit' : 'edit-details';
		$suffix         = $load ? 'Details' : null;
		$this->title    = 'Edit My <span class="bg-danger">'. ucfirst($this->type) . ' ' . $suffix . '</span> Profile';

		$this->form->loadFromfile($this->loadFile);

		if ($this->form->isSent()) {
			$this->form->queryValues();

			if ($this->type == 'breeder') {
				// $address = $this->form->getValue('address') .' '. $this->form->getValue('city') .' '. $this->form->getValue('stateId') .' '. $this->form->getValue('postCode') .' '. $this->form->getValue('countryId');
				// $loc = Geocoder::getLocation($address);
				// $this->form
				// 	->addDbField('lat', $loc['lat'])
				// 	->addDbField('lng', $loc['lng']);

				if ($this->loadFile = 'breeder-details') {
					Db::sql(sprintf("UPDATE User SET isBreederForm = %d WHERE id = %d ", 1, $userId ));
				}

			}

			$this->form->addDbField('id', $userId, true);

			// Ha a facebook login utan jon, akkor van erre szukseg.
			if (isset($_GET['type'])) {
				$this->form
					->addDbField('type', $this->type)
					->addDbField('planId', $this->type . '-basic');
			}

			if ($this->form->validateFields()) {
				$this->form->save('update');
				$_SESSION['edited'] = 1;
				$this->redirect('user', 'account');
			}
		} else {
			$this->form->loadValues('id = ' . $userId);
		}
	}

    /**
     * Change password.
     * @throws Error
     */
    public function actionChangePassword() {
		$this->title    = 'Change Password';
		$this->viewFile = 'changepassword';

		UniAdmin::app()->path->addElement('My Account', 'user', 'account');
		UniAdmin::import('classes.forms.Form');
		$this->form = new Form();
		$this->form
			->loadFromfile('user.changepassword')
			->setSubmitValue('Save Changes');

		if ($this->form->isSent()) {
			$currPassword = Db::sqlField(sprintf("
				SELECT password
				FROM User
				WHERE id = %d",
				UniAdmin::app()->user->getId()
			));

			$this->form
				->queryValues()
				->addDbField('id', UniAdmin::app()->user->getId(), true);

			if ($this->form->validateFields()) {
				$this->form->getField('password')
					->setMatchTo('')
					->setValue(UserConfig::encodePassword($this->form->getValue('password')));

				if (UserConfig::verifyPassword($this->form->getValue('currPassword'), $currPassword)) {
					if ($this->form->save('update')) {
						$_SESSION['changepassword'] = 1;
						$this->redirect('user', 'account');
					}
				} else {
					$this->form->setError('currPassword', 'Current password is incorrect.');
				}
			}
		}
	}

	public function actionBreederProfile() {
		$this->title = 'Edit Breeder Information';
		UniAdmin::app()->page->setTitle($this->title);
		UniAdmin::app()->path
				->addElement('My Account','user','account');
		$this->viewFile = 'my-profile';
		UniAdmin::import('classes.forms.Form');
		$this->form = new Form();
		$this->form
			->loadFromfile('user.breeder')
			->addDbField('user_id', UniAdmin::app()->user->getId(), true);

		if ($this->form->isSent()) {
			$this->form
				->queryValues()
				->save('update');
			$_SESSION['breederProfile'] = 1;
			$this->redirect('user', 'account');
		} else {
			$this->form->loadValues('user_id= ' . UniAdmin::app()->user->getId());
		}
	}

	public function actionResetPassword() {
		UniAdmin::app()->page->setTitle('New password request');
		UniAdmin::import('classes.forms.Form');

		UniAdmin::app()->path
			->addElement('Sign In', 'user', 'login');

		$this->viewFile = 'reset-password';

		if (!isset($_POST['user']) || !$_POST['user']) {
			$this->success = false;
			return;
		}

		$this->success = true;
		$this->message = 'We have successfully sent the confirmation email to your inbox, please follow the provided instructions.';

		$send = true;
		$userData = Db::sqlrow(sprintf("
			SELECT *
			FROM User
			WHERE email = '%s'",
			Db::escapeString($_POST['user'])
		));
		if (empty($userData)) {
			$send = false;
		}
		if (!empty($userData) && !$userData['isActive']) {
			$this->success = false;
			$this->message = 'User account is forbidden';
			$send = false;
		}

		if ($send) {
			$code = md5($userData['id'] . $userData['email'] . $userData['registeredAt']);
			$userData['link'] = 'http://' . $_SERVER['HTTP_HOST'] . Url::link('user', array('newPassword', 'id' => $userData['id'], 'code' => $code));

			$template = new Template('user-password-request-email', LANG);
			$message = $template->addParameter($userData)->evaluate();

			$email = new Email();
			$email->to($userData['email'])
				->setSubject('New password request')
				->setMessage($message)
				->send();

			Db::sql(sprintf("
				INSERT INTO UserPasswordRequest
				(userId, requestedAt)
				VALUES
				(%d, NOW())
				ON DUPLICATE KEY UPDATE requestedAt = NOW()",
				$userData['id']
			));
		}

		if (UniAdmin::app()->isAjax()) {
			return new AjaxResponse(array(
				'success' => $this->success ? 1 : 0,
				'message' => $this->message,
			));
		}
	}

	public function actionNewPassword() {
		$this->viewFile = 'new-password';

		$params = UniAdmin::app()->route->getParameters();
		if ($params['id'] && $params['code']) {
			$userData = Db::sqlrow(sprintf("
				SELECT *
				FROM User
				WHERE id = %d
				AND MD5(CONCAT(id, email, registeredAt)) = '%s'",
				$params['id'],
				Db::escapeString($params['code'])
			));
			if ($userData) {
				$isPasswordRequested = Db::sqlBool(sprintf("
					SELECT COUNT(*)
					FROM UserPasswordRequest
					WHERE userId = %d",
					$params['id']
				));
				if ($isPasswordRequested) {
					$this->password = '';
					$allowed = 'abcdefghijkmnopqrstuvwxyz123456789ABCDEFGHJKLMNPQRSTUVWXYZ';
					for($i = 0; $i < 8; $i++) {
						$this->password .= substr($allowed, mt_rand(0, strlen($allowed) -1), 1);
					}

					Db::sql(sprintf("
						UPDATE User
						SET password = '%s'
						WHERE id = %d",
						UserConfig::encodePassword($this->password),
						$params['id']
					));

					Db::sql(sprintf("
						DELETE FROM UserPasswordRequest
						WHERE userId = %d",
						$params['id']
					));

					$this->success = true;
					$this->message = 'Your new password has been created';
				} else {
					$this->success = false;
					$this->message = 'No password requested for this user';
				}
			} else {
				$this->success = false;
				$this->message = 'No such user in the database';
			}
		} else {
			$this->success = false;
			$this->message = 'Missing parameters';
		}
	}

	public function actionDone() {

		$this->noMoreTry      = Util::setSession('noMoreTry');
		$this->smsCodeTyped   = Twilio::getSmsCodeTyped();
		$this->disabledButton = (!UniAdmin::app()->user->checkLogin() ? $this->smsCodeTyped : UniAdmin::app()->user->smsCodeTyped) < SMS_SENT ? true : false;

		if (isset($_SESSION['userDone'])) { $this->userAdd = 1; unset($_SESSION['userDone']); }
		if (isset($_SESSION['userEdit'])) { $this->userEdit = 1; unset($_SESSION['userEdit']); }
		if (isset($_SESSION['smsActivate'])) { $this->smsActivate = 1; unset($_SESSION['smsActivate']); }
		if (isset($_SESSION['wrongSmsCode'])) { $this->hasPhone = 1; $this->wrongSmsCode = 1; unset($_SESSION['wrongSmsCode']); }
		if (isset($_SESSION['hasPhone'])) { $this->hasPhone = 1; unset($_SESSION['hasPhone']); }

		$this->viewFile = 'done';
	}

	public function actionUpgrade() {
		UniAdmin::app()->path
			->addElement('My Account', 'user', 'account')
			->addElement('Upgrade', 'user', 'upgrade');
		UniAdmin::app()->page->setTitle('Membership Status');
		$this->viewFile = 'upgrade';
		$this->plans = MembershipHelper::getPlans(UniAdmin::app()->user->type);
		$this->hasRecurring = Db::sqlBool(sprintf("
			SELECT COUNT(*)
			FROM UserMembership UM
			JOIN MembershipPlan MP
				ON UM.planId = MP.id
				AND MP.isRecurring = 1
				AND UM.userId = %d
				AND UM.isRecurringActive = 1",
				UniAdmin::app()->user->getId()
			));

		if (!$this->hasRecurring) {
			if ($_SERVER['REQUEST_METHOD'] == 'POST') {
				$planId = isset($_POST['plan']) ? $_POST['plan'] : '';
				$plan = MembershipHelper::getPlan($planId);
				$paymethodId = isset($_POST['method']) ? $_POST['method'] : '';
				if (!in_array($paymethodId, array('paypal'))) {
					$paymethodId = false;
				}

				if ($plan && $paymethodId) {
					$this->redirect('payments', array($paymethodId, $planId));
				}
			}
		}
	}

	// public function isPost($field) {
	// 	return isset($_POST[$field]) ? true : false;
	// }

	public function actionEmailShare() {
		if (!UniAdmin::app()->user->checkLogin()) {
			$this->redirect('user', 'login');
		}

		if (isset($_POST)) {

			$data = $_POST;
			$data['link'] = 'http://' . $_SERVER['HTTP_HOST'] . '/puppy/view/' . $_POST['puppyId'];
			$data['sender'] = UniAdmin::app()->user->firstName . ' ' . UniAdmin::app()->user->lastName;

			$template = new Template('email-share', 'en');
			$message = $template->setText($template->getText())
				->addParameter($data)
				->evaluate();

			$email = new Email();
			$email
				->to($data['email'])
				->setSubject('Share email')
				->setMessage($message)
				->send();

			$_SESSION['emailShareSent'] = 1;
			$this->redirect('');
		}
	}
}


