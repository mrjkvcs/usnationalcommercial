<div class="container min-height">
	<div class="row">
		<div class="col-md-6 col-md-offset-3">
			<div class="page-header mt0">
				<h3 class="mt0">Forgot Login Information?</h3>
			</div>
			<div class="well">
				<?php if(!isset($this->message)) : ?>
					<p>If you have misplaced your login details, please enter your email address below that is linked to your account. <br />An email will be sent immediately which will allow you to reset your password.</p>
					<form role="form" action="<?php echo Url::link('user', 'resetpassword'); ?>" method="post" >
						<div class="form-group">
							<label for="exampleInputEmail1">Email address</label>
							<input type="email" class="form-control" id="exampleInputEmail1" placeholder="Enter email" name="user">
						</div>
						<button type="submit" class="btn btn-success">Send</button>&nbsp;
						<small class="pull-right"><a href="<?php echo Url::link('user', 'login'); ?>">Back to Sign In</a></small>
					</form>
				<?php else: ?>
					<?php SysMessage::displaySuccess(' An email has been sent with instructions on how to reset your password.');  ?>
					<a href="<?php echo Url::link('user', 'login'); ?>" class="btn btn-primary">Go to Login</a>
				<?php endif; ?>
			</div>
		</div>
	</div>
</div>