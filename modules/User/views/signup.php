<div class="container min-height bg-color">
	<div class="row padding-top-30">
		<?php isset($this->hasNoFacebook) ? SysMessage::displayError( 'You have no Facebook account. Please Sign Up!', true) : null; ?>
		<div class="col-md-8 col-md-offset-2">
			<div class="page-header mt0">
				<h3>Sign Up with Facebook</h3>
			</div>
			<div class="well">
					<form action="<?php echo Url::link('facebook') ?>" method="post">
						<p>
							<div class="form-group">
								<label class="radio">
									<input type="radio" id="type" name="type" value="breeder" checked> <span class="bg-danger condensed">I AM A TRUSTED BREEDER</span>
								</label>
							</div>
							<div class="form-group">
								<label class="radio">
									<input type="radio" id="type" name="type" value="seller" checked> I want to find homes for my dogs (SELLER)
								</label>
							</div>
							<div class="form-group">
								<label class="radio">
									<input type="radio" id="type" name="type" value="buyer"> I am looking for a dog (BUYER)
								</label>
							</div>
						</p>
						<ul class="list-unstyled">
							<li>
								<p class="mb25">You can register using Facebook (it's fast and easy)!</p>
								<button class="btn btn-facebook btn-loading" data-msg="Account Creating. Please wait..." role="button"><i class="fa fa-facebook"></i> | Sign Up with Facebook</button>
								<!-- <a href="<?php echo Url::link('facebook'); ?>" class="btn btn-facebook"><i class="fa fa-facebook"></i> | Sign Up with Facebook</a> -->
							</li>
						</ul>
					</form>
			</div>
			<div class="page-header mt0">
				<h3>Or, create a new account using a standard User Name and Password</h3>
			</div>
			<div class="well">
				<p>
					<form action="<?php echo Url::link('user', 'register') ?>" method="post">
						<div class="form-group">
							<label class="radio">
								<input type="radio" id="type" name="type" value="breeder" checked> <span class="bg-danger condensed">I AM A TRUSTED BREEDER </span>
							</label>
						</div>
						<div class="form-group">
							<label class="radio">
								<input type="radio" id="type" name="type" value="seller" checked> I want to find homes for my dogs (SELLER)
							</label>
						</div>
						<div class="form-group">
							<label class="radio">
								<input type="radio" id="type" name="type" value="buyer"> I am looking for a dog (BUYER)
							</label>
						</div>
						<div class="form-group">
							<button class="btn btn-success mt10 btn-loading"  role="button"><i class="fa fa-envelope-o"></i> | Sign Up with Email</button>
						</div>
					</form>
				</p>
			</div>
		</div>
	</div>
</div>
