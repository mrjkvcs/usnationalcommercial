<div class="container min-height bg-color" >
	<?php isset($this->incorrectLogin) ? SysMessage::displayError(' Login information was incorrect.') : null; ?>
	<?php isset($this->notLogin) ? SysMessage::displayError(' You should login to do this.', true) : null; ?>
	<?php isset($this->isActivated) ? SysMessage::displaySuccess(' Your <strong>' . SUBSCRIPTION_DATE . ' days FREE </strong>account with ChoosePuppy has been activated. You now have 24/7 access to your account online, anytime you need it.') : null; ?>
	<?php isset($this->hasActivated) ? SysMessage::displayNotice('You have already activated Your account. <a href="' . Url::link('user', 'resetpassword'). '"> Forgot Login Information?</a>') : null; ?>
	<div class="row padding-top-30">
		<div class="col-md-8 col-md-offset-2">
			<div class="page-header mt0">
				<h3>Sign In with Facebook</h3>
			</div>
			<div class="well">
				<ul class="list-unstyled">
					<li>
						<p class="mb25">Please login to access your account via Facebook:</p>
						<a href="<?php echo Url::link('facebook'); ?>" class="btn btn-facebook btn-loading" data-msg="Login..." style="color:#fff"><i class="fa fa-facebook"></i> | Sign In with Facebook</a>
					</li>
				</ul>
			</div>
			<h3>Or, Sign In using a standard User Name and Password</h3>
			<div class="well">
				<?php if (isset($this->supportGuest)) :?>
					<div class="alert alert-success">
						<p><i class="fa fa-fw fa-check"></i> Not logged in. Please login to continue.</p>
						<p class="text-danger">
							To submit email as a guest <a href="<?php echo Url::link('support', 'guest'); ?>">click here</a>.
						</p>
					</div>
				<?php endif; ?>
				<form role="form" action="<?php echo Url::link('user', 'login') ?>" method="post">
					<input type="hidden" name="sent" value="1">
					<div class="form-group">
						<label for="">ENTER YOUR EMAIL</label>
						<input type="email" class="form-control" id="exampleInputEmail1" name="user">
					</div>
					<div class="form-group">
						<label for="">ENTER YOUR PASSWORD</label>
						<input type="password" class="form-control" id="exampleInputPassword1" name="password">
					</div>
					<button class="btn btn-success mt10 btn-loading" data-msg="Login..." role="button" type="submit"><i class="fa fa-envelope-o"></i> | Sign In with Email</button>
					<div class="checkbox mt25">
						<label>
							<small>
								<input type="checkbox"> <span class="text-muted">Remember me</span> |
								<span><a href="<?php echo Url::link('user', 'resetpassword'); ?>"> Forgot Password?</a></span>
							</small>
						</label>
					</div>
				</form>	
			</div>
			<p>DON’T HAVE AN ACCOUNT? <a href="<?php echo Url::link('user', 'signup'); ?> ">CREATE ONE HERE</a></p>
		</div>
	</div>
</div>