<?php
    $fields = array(
        'HISTORY' => array(
                array('label' => 'Primary Breed', 'field' => 'breed1', 'col' => 4),
                array('label' => 'Other Breed 1', 'field' => 'breed2', 'col' => 4),
                array('label' => 'Other Breed 2', 'field' => 'breed3', 'col' => 4),
                array('label' => 'Yrs Breeding Primary', 'field' => 'yrsPrimary', 'col' => 4),
                array('label' => 'Tot Yrs Breeding', 'field' => 'yrsTot', 'col' => 4),
                array('label' => 'Avg Litters a YR', 'field' => 'yrsAvg', 'col' => 4),
                array('label' => 'Titled Dogs Bred', 'field' => 'titledDogs', 'col' => 4),
                array('label' => 'Num Dogs Owned', 'field' => 'numDogs', 'col' => 4),
                array('label' => 'Num Champions Bred', 'field' => 'numChampions', 'col' => 4),
                array('label' => 'Num Performance', 'field' => 'numPerformance', 'col' => 6),
                array('label' => 'Kennel Name', 'field' => 'kennelName', 'col' => 6)
        ),
        'SERVICES (STUD SERVICE TO)' => array(
                array('label' => 'Approved Bitches', 'field' => 'hasApproved', 'col' => 3),
                array('label' => 'Boarding Service', 'field' => 'hasBoarding', 'col' => 3),
                array('label' => 'Grooming Service', 'field' => 'hasGrooming', 'col' => 3),
                array('label' => 'Rescue Service', 'field' => 'hasRescue', 'col' => 3),
        ),
        'TRAINING' => array(
                array('label' => 'Agility', 'field' => 'hasAgility', 'col' => 3),
                array('label' => 'Obedience', 'field' => 'hasObedience', 'col' => 3),
                array('label' => 'Behavior', 'field' => 'hasBehavior', 'col' => 3),
                array('label' => 'Companion', 'field' => 'hasCompanion', 'col' => 3),
                array('label' => 'Herding', 'field' => 'hasHerding', 'col' => 3),
                array('label' => 'Security', 'field' => 'hasSecurity', 'col' => 3),
                array('label' => 'Tracking', 'field' => 'hasTracking', 'col' => 3),
                array('label' => 'Mantrailing', 'field' => 'hasMantrailing', 'col' => 3),
                array('label' => 'Handicapped Deaf', 'field' => 'hasDeaf', 'col' => 3),
                array('label' => 'Handicapped Blind', 'field' => 'hasBlind', 'col' => 3),
                array('label' => 'Handicapped Other', 'field' => 'hasOther', 'col' => 3),
        ),
        'ABOUT YOU' => array(
                array('label' => 'Short Description (max:500)', 'field' => 'hasProve', 'col' => 12),
        ),
    );
?>

<div class="container mb80 bg-white">
	<div class="page-header mt0">
		<h3><?php echo $this->title; ?></h3>
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<i class="fa fa-fw fa-gear"></i><a href="<?php echo Url::link('user', 'account'); ?>"> back to my account</a>
			<form action="" method="post">
				<input type="hidden" name="sent" value="1">
                <?php foreach($fields as $title => $field) : ?>
                <div class="col-md-12">
                    <h4><?php echo $title; ?></h4>
                    <div class="well">
                        <div class="row">
                            <?php foreach ($field as $subField) : ?>
                                <?php $error = $this->form->getField($subField['field'])->renderError(); ?>
                                <div class="col-md-<?php echo $subField['col']; ?>">
                                    <div class="form-group <?php echo $error ? 'has-error has-feedback' : null; ?> ">
                                        <label for="firstName"><?php echo $subField['label']; ?> </label>
                                        <?php if ($error) : ?><i class="fa fa-fw fa-times form-control-feedback"></i><?php endif; ?>
                                        <?php echo $this->form->getField($subField['field'])->renderContent(); echo $error; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>
                <button type="submit" class="btn btn-success pull-right">Submit</button>
            </form>
		</div>
	</div>
</div>