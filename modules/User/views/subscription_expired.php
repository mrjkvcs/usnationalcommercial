<div class="alert alert-danger mt25 expired">
	The seller's subscription expired. You can send direct message on click the button.
</div>
<?php if (UniAdmin::app()->user->checkLogin()) : ?>
<div class="sent-seller-form">
	<div class="page-header">
		<h3>Direct Message to the Seller.</h3>
	</div>
<?php //$this->formMessage->renderView(); ?>
<!-- 	<form role="form" action="<?php echo Url::link('message') ?>" method="post">
		<div class="form-group">
			<label for="message">Messsage</label>
			<textarea type="password" name="message" class="form-control" id="message" rows="5"></textarea>
		</div>
		<button type="submit" class="btn btn-success">Send</button>
	</form>
 --></div>
<?php else : ?>
<div class="mt25 sent-seller-form">
	<?php echo SysMessage::displayNotice('You must be sign in to send direct message.'); ?>
</div>
<?php endif; ?>
<a href="#" class="btn btn-primary btn-lg direct-button" >Direct message to Seller</a>
