<div class="container">
	<div class="page-header mt0">
		<h3>Password Retrieval</h3>
	</div>
<?php

if (!$this->success) {
	SysMessage::displayError($this->message);
} else {
	SysMessage::displaySuccess($this->message);

	echo '<p>Your new password is: <strong>' . $this->password . '</strong></p>';
	echo '<p>From now on, you can only use this password to sign in.</p>';
}
?>
</div>
