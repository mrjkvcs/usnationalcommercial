<div class="container min-height bg-white">
	<div class="page-header mt0">
		<h3>Change Password</h3>
	</div>
	<div class="col-md-8 col-md-offset-2">
		<?php isset($this->updated) ? SysMessage::displaySuccess('Your password has been updated.', true) : null; ?>
		<i class="fa fa-fw fa-gear"></i><a href="<?php echo Url::link('user', 'account'); ?>"> back to my account</a>
		<div class="well">
			<?php $this->form->renderView(); ?>
		</div>
	</div>
</div>