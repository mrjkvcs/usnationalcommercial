<div class="container mb80 bg-white">
	<div class="page-header mt0">
		<h3><?php echo $this->title; ?></h3>
	</div>
	<div class="row">
		<div class="col-md-8 col-md-offset-2">
			<i class="fa fa-fw fa-gear"></i><a href="<?php echo Url::link('user', 'account'); ?>"> back to my account</a>
			<div class="well">
				<?php echo $this->form->renderView(); ?>
			</div>
		</div>
	</div>
</div>