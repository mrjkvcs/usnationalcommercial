<div class="container bg-color min-height">
	<div class="page-header mt0">
	<?php if (!UniAdmin::app()->isAjax()) : ?>
		<h2 class="mt0">Account Created</h2>
	<?php endif; ?>
</div>
	<p>Thank you for registering at ChoosePuppy.com!</p>
		<?php if(!isset($this->smsActivate)) : ?>
			<div class="alert alert-warning">
				<?php if (!isset($this->hasPhone)) : ?>
					Please check your email to activate your new account. <br />
				<?php else: ?>
					Please enter Your SMS verification code here.
					<div style="padding:30px 0">
						<form action="<?php echo Url::link('user', 'smsactivation'); ?>" method="post" class="form-inline" role="form">
							<input type="hidden" name="sent" value="1">
							<div class="form-group">
								<label class="sr-only" for="smsCode">Email address</label>
								<input type="text" class="form-control" id="smsCode" name="smsCode" placeholder="Enter Your SMS code" maxlength="4" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>
								<p><small>(If you did not receive your code via SMS, please check your email. Thanks!)</small></p>
							</div>
							<button type="submit" class="btn btn-success" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>Submit</button>
						</form>
						<div class="mt25">
							<?php isset($this->wrongSmsCode) ? SysMessage::displayError(' You have entered the incorrect verification code. Please re-try (you have '. (3 - $this->smsCodeTyped) .') tries available)', true) : null; ?>
							<?php !$this->disabledButton ? SysMessage::displayError('<strong>Error!</strong><br /> Please do not hesitate to contact <a href="'. Url::link('support', 'email') .'">us </a> if you have any further questions. Otherwise, I hope that we can work together successfully again another time.') : null; ?>
						</div>
					</div>
				<?php endif; ?>
				<h4>You will not be able to login until your account has been activated.</h4>
				If an email from ChoosePuppy.com does not appear in your inbox, please be sure to check the spam folder on your email account.
			</div>
		<?php else: ?>
			<?php SysMessage::displaySuccess('Your ChoosePuppy account has been activated successfully and <em>You</em> are now a verified user!'); ?>
		<?php endif; ?>
	<a href="<?php echo Url::link('puppy', 'list'); ?>" class="btn btn-lg btn-primary mb25">Browse All Puppies</a>
</div>
