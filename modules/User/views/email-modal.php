    <div class="modal fade email">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h2 class="modal-title">Share <strong><span id="puppyName" class="bg-primary" style="padding:0 10px;"></span></strong> via email</h2>
                </div>
                <div class="modal-body">
                    <p>
                        <div class="message-box" style="display:none">
                            <?php SysMessage::displaySuccess(' The Message has been sent successfully.'); ?>
                        </div>
                        <div class="share-email-form-box">
                            <form role="form" class="share-email-form" action="<?php echo Url::link('user', 'emailshare'); ?>" method="post">
                                <input type="hidden" name="puppyId" value="" class="puppy-id">   
                                <div class="form-group">
                                    <label for="name">Recipient name</label>
                                    <input type="text" class="form-control" id="name" name="name">
                                </div>
                                <div class="form-group">
                                    <label for="email">Recipient email</label>
                                    <input type="email" class="form-control" id="email" name="email">
                                </div>
                                <div class="form-group">
                                    <label for="message">Your message (optional)</label>
                                    <textarea type="email" class="form-control" id="message" name="message" ></textarea>
                                </div>
                                <button type="submit" class="btn btn-success">Send</button>
                            </form>         
                        </div>       
                    </p>
                </div>
            </div>
        </div>
    </div>