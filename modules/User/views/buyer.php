<div class="container min-height bg-color">
	<div class="page-header mt0 padding-top-30">
		<h2 class="mt0">My <span class="bg-danger condensed"><?php echo ucfirst($this->type);  ?></span> Account</h2>
	</div>
	<?php isset($this->buyerType) ? SysMessage::displayError('Sorry, You have a buyer status account. You are not allowed to list a puppy at this time.') : null; ?>
	<?php isset($this->edited) ? SysMessage::displaySuccess('Your account information has been updated.', true) : null; ?>
	<?php isset($this->changepassword) ? SysMessage::displaySuccess('Your password has been updated.', true) : null; ?>
	<div class="row">
		<div class="col-sm-8 col-md-8 col-md-offset-2 col-sm-offset-2">
			<div class="well">
				<div class="row">
					<div class="col-md-10">
						<h3><?php echo $this->name; ?></h3>
					</div>
				</div>
				<div class="row mt25">
					<div class="col-md-12">
						Member since: <strong><?php echo Date::format(UniAdmin::app()->user->registeredAt, 'short'); ?></strong>
					</div>
					<div class="col-md-12">
						<div class="row">
							<div class="col-xs-4 col-sm-3 col-md-3 col-md-offset-6">
								<small><a href="<?php echo Url::link('user', 'changepassword'); ?>"><span class="fa fa-fw fa-unlock-alt"></span> change password</a></small>
							</div>
							<div class="col-xs-4 col-sm-3 col-md-3">
								<small><a href="<?php echo Url::link('user', 'edit'); ?>"><span class="fa fa-fw fa-edit"></span> edit profile</a></small>
							</div>
						</div>
					</div>
				</div>
			</div>
			<h4 class="mt25">My Verification</h4>
			<div class="well">
				<?php foreach(User::isVerification( $this->userId ) as $verification) : ?>
						<div class="row mt10">
							<div class="col-md-4">
								<i class="fa fa-fw fa-<?php echo $verification['success'] ? 'check' : 'times'; ?> text-<?php echo $verification['success'] ? 'success' : 'danger'; ?>"></i>
								<?php echo $verification['title2']; ?>
							</div>
							<div class="col-md-8">

		                    <?php if (!$verification['success'] && isset($verification['url'])) : ?>
		                    	<?php if (isset($verification['id']) && $verification['id'] == 'phone-button') : ?>

		                    		<?php if (empty($this->smsCode)):?>
			                    		<a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="btn btn-primary btn-xs confirm">Get Verified</a>	
			                    	<?php else: ?>
				                    	<form action="<?php echo Url::link('user', array('smsactivation', 'account')); ?>" method="post" class="form-inline" role="form">
				                    		<div class="form-group">
				                    			<label class="sr-only" for="smsCode">Verify Code</label>
				                    			<input type="text" class="form-control input-sm" id="smsCode" name="smsCode" placeholder="Your SMS code" maxlength="4" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>
				                    		</div>
				                    		<button type="submit" class="btn btn-success btn-sm" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>Submit</button>
				                    		<?php if ($this->disabledButton) : ?>
				                    			<a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="confirm">Send it again</a>
				                    		<?php endif; ?>
				                    	</form>
			                    	<?php endif; ?>

			                    <?php else: ?>
				                    <a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="btn btn-primary btn-xs confirm">Get Verified</a>
				                <?php endif; ?>
			                <?php endif; ?>
							</div>
						</div>
				<?php endforeach; ?>
			</div>
			<?php isset($this->notExistsPhone) ? SysMessage::displayError(' You have no phone number or an invalid number.', true) : null; ?>
			<?php isset($this->isActivated) ? SysMessage::displaySuccess(' Your phone has been verified successfully. Thank You.', true) : null; ?>
			<?php isset($this->sendPhoeVerified) ? SysMessage::displaySuccess(' Verification code has been sent successfully. Thank You.', true) : null; ?>
			<?php isset($this->wrongFormatNumber) ? SysMessage::displayError('Wrong Phone Number Format', true) : null; ?>
			<?php isset($this->wrongSmsCode) ? SysMessage::displayError(' Code information was incorrect. You have ('. (3 - UniAdmin::app()->user->smsCodeTyped) .') more chance(s).', true) : null; ?>
			<?php isset($this->noMoreTry) ? SysMessage::displayError(' You have been reached 3 times.', true) : null; ?>
			<?php isset($this->noMoreSms) || !$this->disabledButton ? SysMessage::displayError(' 
				<strong>Error!</strong><br /> Please do not hesitate to contact <a href="'. Url::link('support', 'email') .'">us </a> if you have any further questions. Otherwise, I hope that we can work together successfully again another time.') : null; 
			?>
		</div>
	</div>
</div>