<?php
    $fields = array(
        'ENTER CONTACT INFORMATION' => array(
                array('label' => 'First Name', 'field' => 'firstName', 'col' => 6, 'require' => true),
                array('label' => 'Last Name', 'field' => 'lastName', 'col' => 6, 'require' => true),
                array('label' => 'Phone', 'field' => 'phone', 'col' => 6, 'require' => false),
                array('label' => 'Website', 'field' => 'website', 'col' => 6, 'require' => false)
        ),
        'ENTER ADDRESS INFORMATION' => array(
                array('label' => 'Address', 'field' => 'address', 'col' => 6, 'require' => false),
                array('label' => 'City', 'field' => 'city', 'col' => 6, 'require' => true),
                array('label' => 'State', 'field' => 'stateId', 'col' => 4, 'require' => true),
                array('label' => 'Zip Code', 'field' => 'postcode', 'col' => 4, 'require' => true),
                array('label' => 'Country', 'field' => 'countryId', 'col' => 4, 'require' => true)
        ),
        'ABOUT YOU' => array(
                array('label' => 'Short Description (max:500)', 'field' => 'description', 'col' => 12, 'require' => false),
        ),
        'ENTER ACCESS INFORMATION' => array(
                array('label' => 'Email', 'field' => 'email', 'col' => 6, 'require' => true),
                array('label' => 'Confirm Email', 'field' => 'emailconfirm', 'col' => 6, 'require' => true),
                array('label' => 'Password', 'field' => 'password', 'col' => 6, 'require' => true),
                array('label' => 'Confirm Password', 'field' => 'passwordagain', 'col' => 6, 'require' => true),
                array('label' => '', 'field' => 'acceptTerms', 'col' => 12, 'require' => false),
        )
    );
?>
<div class="container bg-color">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <form action="<?php echo Url::link('user', 'register'); ?>" method="post" >
                <input type="hidden" name="sent" value="1">
                <input type="hidden" name="type" value="seller">
                <div class="col-md-12">
                    <div class="page-header mt0">
                        <h3 class="mt0"><span class="glyphicon glyphicon-edit"></span> Become a Registered <span class="bg-danger"><?php echo ucfirst($this->type); ?></span> </h3>
                    </div>
                </div>
                <?php foreach($fields as $title => $field) : ?>
                <div class="col-md-12">
                    <h4><?php echo $title; ?></h4>
                    <div class="well">
                        <div class="row">
                            <?php foreach ($field as $subField) : ?>
                                <?php $error = $this->form->getField($subField['field'])->renderError(); ?>
                                <div class="col-md-<?php echo $subField['col']; ?>">
                                    <div class="form-group <?php echo $error ? 'has-error has-feedback' : null; ?> ">
                                        <label for="firstName"><?php echo $subField['label']; ?> <?php if ($subField['require']) : ?><span class="text-danger">*</span><?php endif; ?></label>
                                        <?php if ($error) : ?><i class="fa fa-fw fa-times form-control-feedback"></i><?php endif; ?>
                                        <?php echo $this->form->getField($subField['field'])->renderContent(); echo $error; ?>
                                    </div>
                                </div>
                            <?php endforeach; ?>
                        </div>
                    </div>
                </div>
                <?php endforeach; ?>

                <div class="col-md-12 mb25"><button class="btn btn-success btn-lg">Sign Up</button></div>
            </form>
        </div>
    </div>
</div>
