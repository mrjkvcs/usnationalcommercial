<div class="container bg-color">
	<?php isset($this->edited) ? SysMessage::displaySuccess('Your account information has been updated.', true) : null; ?>
	<?php isset($this->changepassword) ? SysMessage::displaySuccess('Your password has been updated.', true) : null; ?>
	<div class="page-header mt0 padding-top-30">
		<h3 class="mt0">My <span class="bg-danger"><?php echo ucfirst($this->type);  ?></span> Account</h3>
	</div>
	<div class="row">
		<div class="col-sm-8 col-md-8">
			<div class="well">
				<div class="row">
                    <div class="col-md-1 mt15">
                        <i class="fa fa-3x fa-user"></i>
                    </div>
					<div class="col-md-11">
						<h3><?php echo 'Hi again, ' . $this->name . '!'; ?> <?php if (UniAdmin::app()->user->isCertified) :?><small class="bg-danger">Certified Breeder</small><?php endif; ?></h3>
					</div>
				</div>
				<div class="row mt25">
					<div class="col-md-12">
						<small>Member since: <strong><?php echo Date::format(UniAdmin::app()->user->registeredAt, 'short'); ?></strong></small>
					</div>
				</div>
			</div>
			<div class="well">
				<div class="row">
					<div class="col-xs-4 col-sm-3 col-md-4">
						<small><a href="<?php echo Url::link('user', 'changepassword');  ?>"><i class="fa fa-fw fa-unlock-alt"></i> change password</a></small>
					</div>
					<div class="col-xs-4 col-sm-3 col-md-4">
						<small><a href="<?php echo Url::link('user', 'edit');  ?>"><i class="fa fa-fw fa-edit"></i> edit profile</a></small><br />
					</div>
				</div>
			</div>

			<h4 class="mt25">My Verification</h4>
			<div class="well">
				<?php foreach(User::isVerification( $this->userId ) as $verification) : ?>
						<div class="row mt10">
							<div class="col-md-4">
								<i class="fa fa-fw fa-<?php echo $verification['success'] ? 'check' : 'times'; ?> text-<?php echo $verification['success'] ? 'success' : 'danger'; ?>"></i>
								<?php echo $verification['title2']; ?>
							</div>
							<div class="col-md-8">

		                    <?php if (!$verification['success'] && isset($verification['url'])) : ?>
		                    	<?php if (isset($verification['id']) && $verification['id'] == 'phone-button') : ?>

		                    		<?php if (empty($this->smsCode)):?>
			                    		<a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="btn btn-primary btn-xs confirm">Get Verified</a>	
			                    	<?php else: ?>
				                    	<form action="<?php echo Url::link('user', array('smsactivation', 'account')); ?>" method="post" class="form-inline" role="form">
				                    		<div class="form-group">
				                    			<label class="sr-only" for="smsCode">Verify Code</label>
				                    			<input type="text" class="form-control input-sm" id="smsCode" name="smsCode" placeholder="Your SMS code" maxlength="4" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>
				                    		</div>
				                    		<button type="submit" class="btn btn-success btn-sm" <?php echo !$this->disabledButton ? 'disabled="disabled"' : null; ?>>Submit</button>
				                    		<?php if ($this->disabledButton) : ?>
				                    			<a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="confirm">Send it again</a>
				                    		<?php endif; ?>
				                    	</form>
			                    	<?php endif; ?>

			                    <?php else: ?>
				                    <a href="<?php echo $verification['url']; ?>" <?php echo isset($verification['id']) ? 'id="' . $verification['id'] .'"' : null;  ?> class="btn btn-primary btn-xs confirm">Get Verified</a>
				                <?php endif; ?>
			                <?php endif; ?>
							</div>
						</div>
				<?php endforeach; ?>
			</div>
			<?php isset($this->wrongFormatNumber) ? SysMessage::displayError('Wrong Phone Number Format', true) : null; ?>
			<?php isset($this->wrongSmsCode) ? SysMessage::displayError(' Code information was incorrect. You have ('. (3 - UniAdmin::app()->user->smsCodeTyped) .') more chance.', true) : null; ?>
			<?php isset($this->noMoreTry) ? SysMessage::displayError(' You have been reached 3 times.', true) : null; ?>
			<?php isset($this->notExistsPhone) ? SysMessage::displayError(' You have no phone number or invalid number.', true) : null; ?>
			<?php isset($this->isActivated) ? SysMessage::displaySuccess(' Your phone has been verified successfully. Thank You.', true) : null; ?>
			<?php isset($this->sendPhoneVerified) ? SysMessage::displaySuccess(' Verification code has been sent successfully ('.UniAdmin::app()->user->smsCodeSent.'). Thank You.', true) : null; ?>
			<?php isset($this->noMoreSms) || !$this->disabledButton ? SysMessage::displayError(' 
				<strong>Error!</strong><br /> Please do not hesitate to contact <a href="'. Url::link('support', 'email') .'">us </a> if you have any further questions. Otherwise, I hope that we can work together successfully again another time.') : null; 
			?>

			<?php if ($this->basicPlan) : ?>
				<div class="well">
					<h5>If you decide to upgrade to a paid membership account now , your paid billing period will start after your FREE trial period has expired.</h5>
					<h5><u>Example:</u><br />
					Original registration date: <strong>Jan-01-2014</strong>, <br />
					Current expire date will be (90 days): <strong>Apr-01-2014</strong> <br />
					You have decided to upgrade today, <strong>Mar-01-2014</strong>, the new expiration date will be <strong>May-01-2014</strong></em></p>
					</h5>
				</div>
			<?php endif; ?>
			<?php if ($this->hasRecurring): ?>
			<span class="rednote">Note:</span>
			<div class="panel panel-default panel-default">
				<div class="panel-body">
					<p class="text-danger">You cannot upgrade as you have a recurring payment already enabled.</p>
					<a href="#">Cancel</a>
				</div>
			</div>
			<?php else: ?>
			<?php isset($this->expireDate) ? SysMessage::displayError($this->expireDateMsg .' Please Upgrade Your account to get full access.') : null; ?>
			<?php endif; ?>
			<?php if (!empty($this->paymentHistory)) : ?>
			<h4 class="mt50">Payment History</h4>
			<div class="panel panel-default">
				<!-- Default panel contents -->
				<!-- Table -->
				<table class="table table-striped">
					<tr><th>Product</th> <th>Period</th> <th>Payment System</th> <th>Status</th> <th>Amount</th></tr>
					<?php foreach($this->paymentHistory as $history) : ?>
					<tr>
						<td><strong><?php echo $history['title']; ?></strong><br /> <small><em>(<?php echo $history['name']; ?>)</em></small></td>
						<td><small><?php echo $history['status'] != 'cancelled' ? $history['startDate'] . '   -   ' . $history['endDate'] : null; ?></small></td>
						<td><small><?php echo strtoupper($history['method']); ?></small></td>
						<td><small><?php echo $history['status'] != 'cancelled' ? '<span class="text-success">'. $history['status'] .'</span>' : '<del class="text-danger">'. $history['status'] .'</del>' ; ?></small></td>
						<td><strong><?php echo $history['price'] != 0 ? ($history['status'] != 'cancelled' ? '<span class="text-success">$' . $history['price'] . '</span>' : '<small><del class="text-danger">'. $history['price'] .'</del><small>') : '<span class="text-success">FREE</span>'; ?></strong></td>
					</tr>
					<?php endforeach; ?>
				</table>
			</div>
			<?php endif; ?>
		</div>
		<div class="col-sm-4 col-md-4">
			<div class="alert alert-warning text-center">
				<h4>Your membership is:</h4>
				<hr />
				<h3><?php echo $this->currentPlan['title']; ?></h3>
				<small><?php echo $this->currentPlan['name']; ?></small>
				<h2><?php echo $this->currentPlan['fee'] ? 'FREE' : '$'.$this->currentPlan['fee']; ?></h2>
				<hr />
				<strong><?php echo $this->currentPlan['title'] != 'Monthly Plan' ? $this->expireDateMsg : 'Full Access'; ?></strong><br />
				<a href="<?php echo Url::link('plans'); ?>" class="btn btn-success btn-lg mt25">Upgrade Now</a>
			</div>
			<div class="panel panel-default">
				<div class="panel-heading">
					<h4 class="panel-title">Embed My Puppies Listings on My own Website</h4>
				</div>
				<div class="panel-body">
					<div class="row">
						<div class="alert alert-info">
							You can integrate your puppy listings on your site if you have your own website.
						</div>
						<p>Add this javascript to your website:</p>
						<textarea class="code form-control" rows="3">
							<script src="http://<?php echo $_SERVER['HTTP_HOST']; ?>/embed.js"></script>
						</textarea>
						<p class="mt15">After that, insert this HTML code to your page:</p>
						<textarea class="code form-control" rows="8">
							<a href="http://<?php echo $_SERVER['HTTP_HOST']; ?>/puppies/listedby/<?php echo $this->userId; ?>" class="choosepuppy-ads" data-user="<?php echo $this->userId; ?>" data-width="500" data-height="400" data-count="12" data-order="rand">View puppies listed by <?php echo $this->name; ?></a>
						</textarea>
						<p class="mt15">You are free to adjust any of the data parameters and insert as many different boxes to a single page.</p>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>