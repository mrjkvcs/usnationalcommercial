<div class="modal fade" id="byEmail" role="dialog">
    <div class="modal-dialog">
        <div class="modal-content">
            <form class="form-horizontal" role="form" action="<?php echo Url::link('user', 'login'); ?>" method="post" >
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4>Sign In</h4>
                </div>
                <div class="modal-body">
                    <div class="centered">
                        <a href="<?php echo Url::link('facebook'); ?>" class="btn btn-facebook"><i class="fa fa-facebook"></i> | Sign In with Facebook</a>
                        <h3>OR</h3>
                    </div>
                    <div class="form-group">
                        <label for="user" class="col-sm-2 control-label">Email</label>
                        <div class="col-sm-10">
                            <input type="email" class="form-control" id="user" name="user" placeholder="Email">
                        </div>
                    </div>

                    <div class="form-group">
                        <label for="password" class="col-sm-2 control-label">Password</label>
                        <div class="col-sm-10">
                            <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-2 col-sm-10">
                            <div class="checkbox">
                                <label>
                                    <input type="checkbox" > <span class="text-muted">Remember me</span>
                                </label>
                                <a href="<?php echo Url::link('user', 'resetpassword'); ?>"> Forgot Password</a>
                            </div>
                        </div>
                    </div>

                </div>

                <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary">Sign In</button>
                </div>
            </form>
        </div>            
    </div>
</div>