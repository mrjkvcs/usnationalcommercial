<?php

class SchedulerController extends BaseModule {
	public function actionRun() {
		UniAdmin::import('modules.Scheduler.SchedulerConfig');
		// titkos kulcs ellenőrzése
		if (isset(SchedulerConfig::$secretKey)
			&& !empty(SchedulerConfig::$secretKey)
			&& (!isset($_GET['secret']) || $_GET['secret'] != SchedulerConfig::$secretKey)
		) {
			exit;
		}

		// hívó szerver ip címének ellenőrzése
		if (isset(SchedulerConfig::$remoteIpAddress)
			&& !empty(SchedulerConfig::$remoteIpAddress)
			&& ((
				is_array(SchedulerConfig::$remoteIpAddress)
				&& !in_array($_SERVER['REMOTE_ADDR'], SchedulerConfig::$remoteIpAddress)
			) || (
				!is_array(SchedulerConfig::$remoteIpAddress)
				&& $_SERVER['REMOTE_ADDR'] != SchedulerConfig::$remoteIpAddress
			))
		) {
			exit;
		}

		$partition = isset($_GET['partition']) ? intval($_GET['partition']) : 0;

		$jobs = Db::sql(sprintf("
			SELECT path, method, emailResults
			FROM UniadminScheduler
			WHERE isActive = 1
			AND partition = %d
			AND (isRunning = 0 OR DATE_SUB(NOW(), INTERVAL 5 MINUTE) > lastRunAt)
			AND (hours = '*' OR hours = %d OR CONCAT(',', hours, ',') LIKE '%%,%d,%%')
			AND (minutes = '*' OR minutes = %d OR CONCAT(',', minutes, ',') LIKE '%%,%d,%%')
			AND (days = '*' OR days = %d OR CONCAT(',', days, ',') LIKE '%%,%d,%%')
			AND (months = '*' OR months = %d OR CONCAT(',', months, ',') LIKE '%%,%d,%%')
			AND (weekdays = '*' OR weekdays = %d OR CONCAT(',', weekdays, ',') LIKE '%%,%d,%%')",
			$partition,
			date('G'), date('G'),
			intval(date('i')), intval(date('i')),
			date('j'), date('j'),
			date('n'), date('n'),
			date('w'), date('w')
		));
		while ($rs = Db::loop($jobs)) {
			set_time_limit(600);
			try {
				$className = end(explode('.', $rs['path']));
				UniAdmin::import($rs['path']);
				if (class_exists($className)) {
					$_module = new $className;
					if (method_exists($_module, $rs['method'])) {
						UniAdmin::app()->log->write('scheduler ' . $rs['path'] . '::' . $rs['method'] . ($partition ? ' (partition ' . $partition . ')' : ''));
						Db::sql(sprintf("
							UPDATE UniadminScheduler
							SET isRunning = 1, lastRunAt = NOW()
							WHERE path = '%s'
							AND method = '%s'",
							$rs['path'],
							$rs['method']));
						ob_start();
						call_user_func(array($_module, $rs['method']));
						$results = ob_get_clean();
						Db::sql(sprintf("
							UPDATE UniadminScheduler
							SET isRunning = 0
							WHERE path = '%s'
							AND method = '%s'",
							$rs['path'],
							$rs['method']));
						if ($results && $rs['emailResults']) {
							$results .= '<p>&nbsp;</p><p>' . date('Y-m-d H:i:s') . ', ' . $_SERVER['HTTP_HOST'] . '</p>';
							$email = new Email();
							$email->to($rs['emailResults'])
								->setSubject($className . ' scheduler')
								->setMessage($results)
								->send();
						}
					}
				}
			} catch (Exception $e) {
				echo $e->getMessage();
				if ($rs['emailResults']) {
					$results = $e->getMessage() . '<p>&nbsp;</p><p>' . date('Y-m-d H:i:s') . ', ' . $_SERVER['HTTP_HOST'] . '</p>';
					$email = new Email();
					$email->to($rs['emailResults'])
						->setSubject($className . ' scheduler')
						->setMessage($results)
						->send();
				}
			}
		}
		exit;
	}
}
