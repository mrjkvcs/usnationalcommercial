<?php

class ProjectController extends BaseModule {

    public function actionDefault()
    {
        $this->viewFile = 'default';
        $this->projects = Project::getProject();
    }
}