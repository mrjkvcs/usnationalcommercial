<div class="container">
    <div class="page-header">
        <h2><span class="thin"> Projects History:</span></h2>
    </div>
    <h1><?php echo TITLE; ?></h1>

    <div class="row">
        <?php UniAdmin::widget('modules.Document.Document', array(
            'id'       => 'project-page',
            'language' => LANG,
            'category' => 'default'));
        ?>
    </div>
    <div class="row">
        <?php foreach ($this->projects as $project) : ?>
            <div class="col-xs-6 col-md-3">
                <div class="thumbnail">
                    <img src="images/project/<?php echo $project['id']; ?>.jpg"
                         alt="<?php echo $project['name']; ?>"
                         class="img-responsive"/>
                </div>
                <div class="caption caption-contact">
                    <ul class="list-unstyled">
                        <li><strong><?php echo $project['name']; ?></strong></li>
                        <li><?php echo $project['title']; ?></li>
                        <li><?php echo $project['rep']; ?></li>
                    </ul>
                </div>
            </div>
        <?php endforeach; ?>
    </div>
</div>