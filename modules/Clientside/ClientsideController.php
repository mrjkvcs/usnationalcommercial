<?php
/**
 * uniAdmin ClientSide module
 *
 * @author Kenéz László <info@kenlas.hu>
 * @copyright Copyright (c) 2010, Kenéz László | www.uniadmin.hu
 * @package uniAdmin
 */
class ClientSideController extends BaseModule {
	const tempDir = 'temp';

	public function __construct() {
		$route = UniAdmin::app()->route->getRoute();
		$config = UniAdmin::app()->config;

		switch ($route[1]) {
			case 'javascript':
					$lastModificationTime = 0;
					$fileList = array();
					if (isset($route[2])) {
						if ($route[2] == 'modules' && $route[3] && count($route) <= 5) {	// adott modul fájljai
							$moduleName = ucfirst(strtolower($route[3]));
							$fileList[] = 'modules/' . $moduleName . '/scripts/' . ($route[4] ? $route[4] : strtolower($moduleName)) . '.js';
						} else {	// adott fájl
							$fileList[] = implode('/', array_slice($route, 2)) . '.js';
						}
					} else {	// konfiguráció fájljai
						if (!empty($config['javascript']['autoload'])) {
							$dir = $config['javascript']['autoload'];
							if ($handle = opendir($dir)) {
								while (($file = readdir($handle)) !== false) {
									if (strtolower(substr($file, -3)) == '.js') {
										$rawList[] = $file;
									}
								}
								closedir($handle);
							}

							sort($rawList);

							$fileList = array();
							foreach ($rawList as $num => $file) {
								if ($file == 'jquery.js') {	// jquery előre
									$fileList[0] = $dir . '/' . $file;
								} else {
									$fileList[$num + 1] = $dir . '/' . $file;
								}
							}
							ksort($fileList);
						}
						if (count($config['javascript']['files'])) {
							foreach ($config['javascript']['files'] as $file) {
								if (!in_array($file, $fileList)) {
									$fileList[] = $file;
								}
							}
						}
					}
					foreach ($fileList as $file) {
						if (filemtime($file) > $lastModificationTime) {
							$lastModificationTime = filemtime($file);
						}
					}
					$lastModificationTime = max($lastModificationTime, filemtime('config.php'));

					$this->checkLastModification($lastModificationTime);

					$cachedFileName = self::tempDir . '/' . implode('-', $route) . '.js';
					if (file_exists($cachedFileName) 
						&& filemtime($cachedFileName) > $lastModificationTime
					) {
						// ha már létezik tárolt változat belőle, azt küldjük
						$scriptContents = file_get_contents($cachedFileName);
					} else {
						$scriptContents = '';
						foreach ($fileList as $file) {
							if (file_exists($file)) {
								$scriptContents .= file_get_contents($file);
							}
						}
						if (!$scriptContents) {
							exit;
						}

						if (isset($config['javascript']['compress']) && $config['javascript']['compress']) {
							UniAdmin::import('extensions.jsmin.JSMin');
							$scriptContents = JSMin::minify($scriptContents);
							$this->writeCache($cachedFileName, $scriptContents);
						}

					}

					$this->writeCache($cachedFileName, $scriptContents);

					@ob_end_clean();
					ob_start('ob_gzhandler');

					$cacheTime = isset($config['javascript']['ttl']) ? intval($config['javascript']['ttl']) : 0;
					if (!$cacheTime) {
						$cacheTime = 7;
					}
					$cacheTime = time() + $cacheTime * 86400;
					header('Content-Type: text/javascript; charset=utf-8');
					header('Last-Modified: ' . date('r', $lastModificationTime));
					header('Expires: ' . date('r', $cacheTime));
					header('Cache-Control: public, must-revalidate');

					echo $scriptContents;

					ob_end_flush();
				break;

			case 'css':
					$lastModificationTime = 0;
					$fileList = array();

					if (isset($config['css']['less']) && $config['css']['less']
						&& isset($config['css']['lessBase']) && file_exists($config['css']['lessBase'])
					) {
						$fileList[] = $config['css']['lessBase'];
					}

					if (isset($route[2])) {
						if ($route[2] == 'modules' && $route[3] && count($route) <= 5) {	// adott modul fájljai
							$moduleName = ucfirst(strtolower($route[3]));
							$fileList[] = 'modules/' . $moduleName . '/css/' . (isset($route[4]) && $route[4] ? $route[4] : strtolower($moduleName)) . '.css';
						} else {	// adott fájl
							$fileList[] = implode('/', array_slice($route, 2)) . '.css';
						}
					} else {	// konfiguráció fájljai
						if (count($config['css']['files'])) {
							foreach ($config['css']['files'] as $file) {
								if (!in_array($file, $fileList)) {
									$fileList[] = $file;
								}
							}
						}
					}
					foreach ($fileList as $file) {
						if (file_exists($file)) {
							if (filemtime($file) > $lastModificationTime) {
								$lastModificationTime = filemtime($file);
							}
						}
					}
					$lastModificationTime = max($lastModificationTime, filemtime('config.php'));

					$this->checkLastModification($lastModificationTime);

					$cachedFileName = self::tempDir . '/' . implode('-', $route) . '.css';
					if (file_exists($cachedFileName)
						&& filemtime($cachedFileName) > $lastModificationTime
					) {
						// ha már létezik tárolt változat belőle, akkor azt küldjük, ha nem régebbi,
						// mint az összetevők, vagy a config.php fájl
						$styleContents = file_get_contents($cachedFileName);
					} else {
						$styleContents = '';
						foreach ($fileList as $file) {
							if (file_exists($file)) {
								$styleContents .= file_get_contents($file);
							}
						}

						if (!$styleContents) {
							exit;
						}

						if (isset($config['css']['less']) && $config['css']['less']) {
                            die('lessnel');
							UniAdmin::import('extensions.lessphp.lessc');
							$less = new lessc();
							$styleContents = $less->parse($styleContents);
						}

						if (isset($config['css']['compress']) && $config['css']['compress']) {
							UniAdmin::import('extensions.cssmin.CssMin');
							UniAdmin::import('extensions.jsmin.JSMin');	// expression-ökhöz kellhet
							$filters = array(
								'ImportImports' => false,
								'RemoveComments' => true,
								'RemoveEmptyRulesets' => true,
								'RemoveEmptyAtBlocks' => true,
								'ConvertLevel3AtKeyframes' => false,
								'ConvertLevel3Properties' => false,
								'Variables' => true,
								'RemoveLastDelarationSemiColon' => true
							);
							$plugins = array(
								'Variables' => true,
								'ConvertFontWeight' => true,
								'ConvertHslColors' => true,
								'ConvertRgbColors' => true,
								'ConvertNamedColors' => true,
								'CompressColorValues' => true,
								'CompressUnitValues' => true,
								'CompressExpressionValues' => true,
							);
							$styleContents = CssMin::minify($styleContents, $filters, $plugins);
						}
					}

					$this->writeCache($cachedFileName, $styleContents);

					@ob_end_clean();
					ob_start('ob_gzhandler');

					$cacheTime = isset($config['css']['ttl']) ? intval($config['css']['ttl']) : 0;
					if (!$cacheTime) {
						$cacheTime = 7;
					}
					$cacheTime = time() + $cacheTime * 86400;
					header('Content-Type: text/css; charset=utf-8');
					header('Last-Modified: ' . date('r', $lastModificationTime));
					header('Expires: ' . date('r', $cacheTime));
					header('Cache-Control: public, must-revalidate');

					echo $styleContents;

					ob_end_flush();
				break;
		}
		exit;
	}

	protected function checkLastModification($lastModificationTime) {
		if (function_exists('apache_request_headers')) {
			$headers = apache_request_headers();
		} else {
			$headers = array();
			foreach ($_SERVER as $key => $value) {
                if (substr($key, 0, 5) == 'HTTP_') {
                    $key = str_replace(' ','-', ucwords(strtolower(str_replace('_', ' ', substr($key, 5))))); 
                    $headers[$key] = $value; 
                } else { 
                    $out[$key] = $value; 
				} 
            } 
		}

		if (isset($headers['If-Modified-Since']) 
			&& (strtotime($headers['If-Modified-Since']) >= $lastModificationTime)
		) {
			header('HTTP/1.1 304 Not Modified');
			header('Cache-Control: must-revalidate, public');
			header('Connection: close');
			exit;
		}
	}

	protected function writeCache($file, $content) {
		if (!is_dir(self::tempDir)) {
			@mkdir(self::tempDir, 0777, true);
		}
		if (!is_writable(self::tempDir)) {
			@chmod(self::tempDir, 0777);
		}
		if (is_writable(self::tempDir)) {
			@file_put_contents($file, $content);
		}
	}
}
