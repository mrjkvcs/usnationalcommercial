<?php if (count($this->elements)): ?>
<ul id="page-path" itemprop="breadcrumb">
	<?php if ($this->displayHome): ?>
	<li>
		<a href="<?php echo Url::link(''); ?>" title="<?php echo t('Home'); ?>"></a>
	</li>
	<?php endif;
	foreach ($this->elements as $num => $data): ?>
	<li>
		<?php echo $this->separator . ' '; ?>
		<?php if ($this->linkLastElement || $num + 1 < count($this->elements)): ?>
		<a href="<?php echo $data['link']; ?>" title="<?php echo $data['title']; ?>"><?php echo $data['title']; ?></a>
		<?php else: ?>
		<?php echo $data['title']; ?>
		<?php endif; ?>
	</li>
	<?php endforeach; ?>
</ul>
<?php endif;