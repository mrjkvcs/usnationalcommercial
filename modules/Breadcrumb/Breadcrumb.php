<?php

class Breadcrumb extends BaseModule {
	public $elements = array();
	public $displayHome = true;
	public $linkLastElement = true;
	// public $separator = ' &raquo; ';
	public $separator = '';

	public function setDisplayHome($state) {
		$this->displayHome = $state;
		return $this;
	}

	public function setLinkLastElement($state) {
		$this->linkLastElement = $state;
		return $this;
	}

	public function setSeparator($separator) {
		$this->separator = $separator;
		return $this;
	}

	public function setElements($elements) {
		$this->elements = $elements;
		return $this;
	}

	public function addElement($title, $moduleId, $action = null, $languageId = null, $parameters = null) {
		if (DEBUG_MODE) {
			UniAdmin::addDebugInfo('adding breadcrumb element ' . $title, 'breadcrumb');
		}
		$this->elements[] = array(
			'title' => $title,
			'link' => Url::link($moduleId, $action, $languageId, $parameters),
		);
		return $this;
	}

	public function getSeparator() {
		return $this->separator;
	}

	public function getDisplayHome() {
		return $this->displayHome;
	}

	public function getLinkLastElement() {
		return $this->linkLastElement;
	}

	public function getElements() {
		return $this->elements;
	}

	public function renderView() {
         UniAdmin::app()->page->addCss('modules.breadcrumb.path');
		parent::renderView('modules.Breadcrumb.views.path');
	}
}
